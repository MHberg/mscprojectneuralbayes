Author: Mikael Henneberg, Aarhus University 2020
Repository containing source code for Master's project in physics about open quantum systems, neural networks and parameter estimation.
