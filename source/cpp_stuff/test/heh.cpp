#include <cstdint>
#include <string>
#include <iostream>
#include <armadillo>
#include <random>
#include <cstdlib>
#include <ctime>
#include <chrono>
#include <experimental/filesystem>

namespace fs = std::experimental::filesystem;
using namespace std;
using namespace arma;

#if defined(__linux__) || defined(linux) || defined(__linux)
#   // Check the kernel version. `getrandom` is only Linux 3.17 and above.
#   include <linux/version.h>
#   if LINUX_VERSION_CODE >= KERNEL_VERSION(3,17,0)
#       define HAVE_GETRANDOM
#   endif
#endif

// also requires glibc 2.25 for the libc wrapper
#if defined(HAVE_GETRANDOM)
#   include <sys/syscall.h>
#   include <linux/random.h>

size_t sysrandom(void* dst, size_t dstlen)
{
    int bytes = syscall(SYS_getrandom, dst, dstlen, 0);
    if (bytes != dstlen) {
    	throw std::runtime_error("Unable to read N bytes from CSPRNG.");
    }
    return dstlen;
}

#elif defined(_WIN32)

// Windows sysrandom here.

#else

// POSIX sysrandom here.

#endif



class Vehicle {
	public:
		virtual string getBrand() = 0;
		virtual string getModel() = 0;
		virtual string getClass() {
			return "Vehicle";
		}

};

class Car1: public Vehicle {
	string brand = "Ford";
	string model = "Mustang";

	public:
		string getBrand() override {
			return brand;
		}
		string getModel() override {
			return model;
		}
		string getClass() override {
			return "Car1";
		}


};

class Car2: public Vehicle {
	string brand = "Audi";
	string model = "R8";

	public:
		string getBrand() override {
			return brand;
		}
		string getModel() override {
			return model;
		}
		string getClass() override {
			cout << Vehicle::getClass() << endl;
			return "Car2";
		}
};


Vehicle* createVehicle(bool use_audi) {
	if (use_audi) return new Car2();
   	else return new Car1();
}

// Function to get total pseudo-cycles since processor was powered on
// Used as source of random seed for armadillo
// Tip by Mysticial as answer on question on stackoverflow:
// Mysticial answered Oct 1 '11 at 1:44
// https://stackoverflow.com/questions/7617587/is-there-an-alternative-to-using-time-to-seed-a-random-number-generation
unsigned long long rdtsc(){
	unsigned int lo,hi;
	__asm__ __volatile__ ("rdtsc" : "=a" (lo), "=d" (hi));
	return ((unsigned long long)hi << 32) | lo;
}


int main() {
	bool use_audi = true;
	Car1 ford;
	Vehicle* vehicle = createVehicle(use_audi);


	std::random_device rd;
	std::mt19937 rng_rdseed(rd());
	std::mt19937 rng_chronoSeed(chrono::steady_clock::now().time_since_epoch().count());
	std::mt19937 rng_noseed;
	int N = 10;

	//std::uint_least32_t seed;
	//sysrandom(&seed, sizeof(seed));
	//std::mt19937 gen(seed);

	std::mt19937_64 gen64;
	cout << "state size: " << gen64.state_size << endl;
	std::uint_least64_t seeds[gen64.state_size];
	for (int i=0; i<gen64.state_size; i++) {
		sysrandom(&seeds[i], sizeof(seeds[i]));
	}
	std::seed_seq state(std::begin(seeds),std::end(seeds));
	cout << "seeds size: " << sizeof(seeds) << endl;
	cout << "double size: " << sizeof(double) << endl;
	gen64.seed(state);

	std::normal_distribution<double> rnorm(0,1);

	arma::vec v_mt64_sysrandom(N);
	for (int i=0; i<N; i++) {
		v_mt64_sysrandom(i) = rnorm(gen64);
	}

	cout << "v_mt64_sysrandom:\n" << v_mt64_sysrandom << endl;
	cout << "arma rng method: " << arma_rng::rng_method << endl;
	cout << "c++11 default rng method: " << std::default_random_engine() << endl;

	//arma::vec v_mt(N);
	//for (int i=0; i<N; i++) {
	//	v_mt(i) = rnorm(rng_rdseed);
	//}

    //arma::vec v_chronoSeed(N);
	//for (int i=0; i<N; i++) {
	//	v_chronoSeed(i) = rnorm(rng_chronoSeed);
	//}

	//arma::vec v_rd(N);
	//for (int i=0; i<N; i++) {
	//	v_rd(i) = rnorm(rd);
	//}

	//arma::vec v_no(N);
	//for (int i=0; i<N; i++) {
	//	v_no(i) = rnorm(rng_noseed);
	//}

	//arma::vec v_arma = arma::randn(N);

	//cout << "MT:\n" << v_mt << endl;
	//cout << "random device:\n" << v_rd << endl;
	//cout << "MT no seed:\n" << v_no << endl;
	//cout << "arma randn:\n" << v_arma << endl;





	//#pragma omp parallel for // do CPU multithreading
	//for (int i=0; i<10; i++) {
	//	int heh = time(NULL);
	//	long long seed = rdtsc();
	//	long long seed2 = chrono::steady_clock::now().time_since_epoch().count();
	//	arma_rng::set_seed(seed2);
	//	vec dummy = randn<vec>(1); // throw away first random value
	//	vec dW = randn<vec>(7);
	//	cout << dW << endl;

	//	cout << "processor cycles: " << seed << endl;
	//	cout << "chrono time: " << seed2 << endl;
	//	cout << "time: " << heh << endl;
	//}

	cout << ford.getBrand() << " " << ford.getModel() << endl;
	cout << vehicle->getBrand() << " " << vehicle->getModel() << endl;
	cout << vehicle->getClass() << endl;

	fs::path p = fs::path("data");
	p /= "heh";
	fs::create_directories(p);

	return 0;

}
