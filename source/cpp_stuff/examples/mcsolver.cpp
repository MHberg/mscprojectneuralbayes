#include "../projectConfig.h"
#include <algorithm>
#include <cstdint>
#include <iostream>
#include <armadillo>
#include <random>
#include <sstream>
#include <cstdlib>
#include <tuple>
#include <string>
#include <chrono>
#include <experimental/filesystem>
#include "sysRand.h"

namespace fs = std::experimental::filesystem;

using namespace std;
using namespace arma;

int delta = 0; // Detuning
double Gamma = 1; // Decay rate
int fast_eval = 0; // Determines if we want fast evaluation (assuming constant Rabi frequency)
int add_noise = 0; // Determines if we want to add noise
int MAX_DELAY = 0; // Determines the maximum amount of time steps a signal can be delayed when noise in the form of a delay is added
bool USE_ARMA_RNG = false;
bool save_psi = false;

// Function to get total pseudo-cycles since processor was powered on
// Used as source of random seed for armadillo
// Tip by Mysticial as answer on question on stackoverflow:
// Mysticial answered Oct 1 '11 at 1:44
// https://stackoverflow.com/questions/7617587/is-there-an-alternative-to-using-time-to-seed-a-random-number-generation
unsigned long long rdtsc(){
	unsigned int lo,hi;
	__asm__ __volatile__ ("rdtsc" : "=a" (lo), "=d" (hi));
	return ((unsigned long long)hi << 32) | lo;
}

class DetectionStrategy {
	private:

	public:
		DetectionStrategy() {};
		virtual ~DetectionStrategy() {}; // virtual destructor
		virtual std::tuple<cx_mat,rowvec> getMCTrajectory(double t0, double T, double nt, rowvec omegas, std::mt19937_64* generator) = 0;
		
		virtual string getDetectionMethod() = 0;

};

class HomodyneDetection: public DetectionStrategy {
	private:
		string detectionMethod = "Homodyne";
		double Phi = datum::pi/2.0;
		//double Phi = 0;

	public:
		HomodyneDetection()
			: DetectionStrategy{}
		{
			//cout << "Homodyne detection strategy created." << endl;
		}

		string getDetectionMethod() override {
			return detectionMethod;
		}

		std::tuple<cx_mat,Row<double>> getMCTrajectory(double t0, double T, double nt, rowvec omegas, std::mt19937_64* generator) override {
			double dt = (T-t0)/nt; // time step size
			vec times = linspace(t0,T,nt); // vector of time steps

			// Ground state
			cx_vec psi_gs = { cx_double(0,0), cx_double(1,0) }; // on the form [c_e; c_g]
			
			// Initial state
			cx_vec psi0 = { cx_double(0,0), cx_double(1,0) };

			// Operators:
			// System Hamiltonian
			cx_mat Hsys = { { cx_double(-delta/2.0,0), cx_double(omegas(0)/2.0,0)},
							{ cx_double(omegas(0)/2.0,0), cx_double(delta/2.0,0) } };
			// Lowering operator
			cx_mat C = { { cx_double(0,0), cx_double(0,0) },
						 { cx_double(1,0), cx_double(0,0) } };
			C = sqrt(Gamma)*C;

			// Identity.
			mat I = { { 1, 0 },
				      { 0, 1 } };
			
			// Non-hermitian effective Hamiltonian
			cx_mat Heff = Hsys - cx_double(0,1)*(C.t()*C)/2.0;

			// Time evolution operator
			cx_mat U = expmat(-cx_double(0,1)*Heff*dt);

			// Generic quadrature operator, Phi=0 (in-phase) and Phi=pi/2.0 (quadrature) components of system dipole
			cx_mat cPhi = C*exp(-cx_double(0,1)*Phi); //
			cx_mat xPhi = cPhi + cPhi.t(); 

			// Generate normal distribution with zero mean, unit variance.
			std::normal_distribution<double> r_normal(0,1);

			// Generate vector of Wiener increments i.e. a vector of 
			// normal distributed elements with mean 0 and variance dt.
			arma::vec dW(nt);
			if (!USE_ARMA_RNG) {
				for (int i=0; i<nt; i++) {
					dW(i) = r_normal(*generator);
					// For recording and plotting random numbers.
					//auto dW_str = std::to_string(dW(i)) + "\n";
					//cout << dW_str << endl;
				}
				dW *= sqrt(dt);
			} else {
				dW = sqrt(dt)*randn<vec>(nt);
			}
			
			cx_mat psi = cx_mat(2,nt); // Allocate memory for state
			psi.col(0) = psi0; // Initial state
			rowvec dY = zeros<rowvec>(nt); // Memory for homodyne signal
			vec temp; // Temporary placeholder for matrix-vector multiplication

			// Assumes Rabi frequency is constant over whole trajectory
			if (fast_eval) {
				for (int i=1; i<nt; i++) {
					// Taking real part since there seem to be a tiny imaginary part
					// probably due to numerics which should not be there.
					// The signal is a sum of a random real number and
					// <psi|X_Phi|psi> which is also real.
					temp = real(psi.col(i-1).t()*xPhi*psi.col(i-1)*dt);
					dY(i-1) = temp(0) + dW(i-1);
					
					// Direct exponentiation of Heff part and attempt at Millstein's method for stochastic part
					// It seems like it cPhi should be squared but this makes the correction 0.
					//psi.col(i) = (U + dY(i-1)*cPhi + 0.5*(pow(dW(i-1),2)-dt)*cPhi) * psi.col(i-1);
					//cout << "Milstein correction: " << 0.5*(pow(dW(i-1),2)-dt)*cPhi << endl;

					// Direct exponentiation of Heff part and Euler-Maruyama for stochastic part
					psi.col(i) = (U + dY(i-1)*cPhi) * psi.col(i-1);
					// First order expansion of exponential (identical to first order Euler method)
					//psi.col(i) = (I-cx_double(0,1)*Heff*dt + dY(i-1)*cPhi) * psi.col(i-1);
					psi.col(i) = psi.col(i)/norm(psi.col(i));
				}
				temp = real(psi.col(nt-1).t()*xPhi*psi.col(nt-1)*dt);
				dY(nt-1) = temp(0) + dW(nt-1);
			} else {
				// Re-evaluates the time evolution operator at every time step and
				// can be used with varying Rabi frequency (is slower).
				for (int i=1; i<nt; i++) {
					Hsys(0,1) = omegas(i)/2.0;
					Hsys(1,0) = omegas(i)/2.0;
					Heff = Hsys - cx_double(0,1)*(C.t()*C)/2.0;
					U = expmat(-cx_double(0,1)*Heff*dt);
					// Taking real part since there seem to be a tiny imaginary part
					// probably due to numerics which should not be there
					temp = real(psi.col(i-1).t()*xPhi*psi.col(i-1)*dt);
					dY(i-1) = temp(0) + dW(i-1);
					// Direct exponentiation of Heff part and Euler-Maruyama for stochastic part
					psi.col(i) = (U + dY(i-1)*cPhi) * psi.col(i-1);
					psi.col(i) = psi.col(i)/norm(psi.col(i));
				}
				temp = real(psi.col(nt-1).t()*xPhi*psi.col(nt-1)*dt);
				dY(nt-1) = temp(0) + dW(nt-1);
			}
			return std::make_tuple(psi,dY);
		}
};

// Conting detection class.
class CountingDetection: public DetectionStrategy {
	private:
		string detectionMethod = "Counting";
	public:
		CountingDetection()
		{
			//cout << "Counting strategy created." << endl;
		}

		string getDetectionMethod() override {
			return detectionMethod;
		}

		std::tuple<cx_mat,Row<double>> getMCTrajectory(double t0, double T, double nt, rowvec omegas, std::mt19937_64* generator) override {

			double dt = (T-t0)/nt;
			vec times = linspace(t0,T,nt);

			cx_vec psi_gs = { cx_double(0,0), cx_double(1,0) };

			// Initial state
			cx_vec psi0 = { cx_double(0,0), cx_double(1,0) };

			// System Hamiltonian in frame rotating with laser frequency.
			cx_mat Hsys = { { cx_double(-delta/2.0,0), cx_double(omegas(0)/2.0,0)},
							{ cx_double(omegas(0)/2.0,0), cx_double(delta/2.0,0) } };
			// Dipole lowering operator (eg. sigma_- for 2-level atom).
			cx_mat C = { { cx_double(0,0), cx_double(0,0) },
						 { cx_double(1,0), cx_double(0,0) } };
			C = sqrt(Gamma)*C;
			
			// Non-Hermitian effective Hamiltonian.
			cx_mat Heff = Hsys - cx_double(0,1)*(C.t()*C)/2.0;
			cx_mat U = expmat(-cx_double(0,1)*Heff*dt);

			cx_mat psi = cx_mat(2,nt);
			psi.col(0) = psi0;
			rowvec dN = zeros<rowvec>(nt);

			// Generate uniform random number r.
			double r;
			std::uniform_real_distribution<double> r_uniform(0.0,1.0);
			if (!USE_ARMA_RNG) {
				r = r_uniform(*generator);
				// For recording and plotting random numbers.
				//auto r_str = std::to_string(r) + "\n";
				//cout << r_str << endl;
			} else {
 				r = randu<double>();
			}

			// Only works if the Rabi frequency is constant i.e. omegas vector 
			// is constant - uses the first entry in the omegas vector.
			if (fast_eval) {
				cx_mat Upsi = cx_mat(psi);
				vec Upsi_norm = vec(nt);
				Upsi.col(0) = psi0;
				Upsi_norm(0) = 1;
				
				// Precompute non-Hermitian evolution and norm.
				for (int i=1; i<nt; i++){
					Upsi.col(i) = U * Upsi.col(i-1);
					Upsi.col(i-1) = Upsi.col(i-1)/norm(Upsi.col(i-1));
					Upsi_norm(i) = norm(Upsi.col(i));
				}
				Upsi.col(nt-1) = Upsi.col(nt-1)/norm(Upsi.col(nt-1));

				vec Upsi_norm_squared = square(Upsi_norm);

				int t_idx = 0; // Keep track of time.
				while (t_idx <= nt-1) { // While there is still remaning time.
					// Find first index where square of norm is less than r.
					uvec idx_vec = find(Upsi_norm_squared <= r,1);
					if (idx_vec.is_empty()) { // Norm does not become <= r, break
						psi.cols(t_idx,nt-1) = Upsi.cols(0,nt-1-t_idx);
						break;
					} else {
						int new_idx = idx_vec[0];
						
						// Jump would happen after duration T is reached.
						// Fill out remaining part of trajectory and break.
						if (t_idx + new_idx > nt-1) {
							psi.cols(t_idx,nt-1) = Upsi.cols(0,nt-1-t_idx);
							break;
						} else { // A jump happens.
							psi.cols(t_idx,t_idx+new_idx-1) = Upsi.cols(0,new_idx-1);
							// Generate uniform random number r using PRNG.
							if (!USE_ARMA_RNG) {
								r = r_uniform(*generator);
								// For plotting random numbers
								//auto r_str = std::to_string(r) + "\n";
								//cout << r_str << endl;
							} else {
								r = randu<double>();
							}
							// Record jump and advance time.
							dN(t_idx+new_idx) = 1;
							t_idx = t_idx+new_idx;
						}
					}
				}
				
			// Evaluates every time step and can be used with varying Rabi frequency.
			} else {
				// Constants for comparing floats.
				//double abstol = 1e-2*dt;
				//double reltol = 1e-1*dt;
				
				// Specifies if method of finding idx where norm^2 = r should be used. 
				// Otherwise checks dp at every time step and compares with random number.
				bool use_new_method = true;

				for (int i=1; i<nt; i++) {
					
					// Insert artificial clicks every x detection.
					//dN(i) = (i%8 == 0) ? 1 : 0; 
					//
					// For using dynamic Rabi frequency.
					//Hsys(0,1) = omegas(i)/2.0;
					//Hsys(1,0) = omegas(i)/2.0;
					//Heff = Hsys - cx_double(0,1)*(C.t()*C)/2.0;
					//Heff = -cx_double(0,1)*Heff*dt;
					//U = expmat(Heff);

					psi.col(i) = U * psi.col(i-1);
					
					// Check for the first time square of norm is approximately equal to r.
					//if ((abs(pow(norm(psi.col(i-1)),2)-r) <= abstol || abs(pow(norm(psi.col(i-1)),2)-r) / max(abs(pow(norm(psi.col(i-1)),2)),abs(r)) <= reltol || pow(norm(psi.col(i-1)),2) < r) && use_new_method) { 
					
					// Check for the first time square of norm is equal to or less than r.
					if (use_new_method) {
						if ((pow(norm(psi.col(i)),2) <= r)) {
					
							// Alternative to putting state in ground state - gives global phase we dont care about.
							//psi.col(i) = C * psi.col(i-1)/sqrt((psi.col(i-1).t() * (C.t() * C) * psi.col(i-1)).eval()(0,0)); 
							// Reset state to ground state and record detection.
							psi.col(i) = psi_gs;
							dN(i) = 1;
							// Generate new random uniform number r.
							if (!USE_ARMA_RNG) {
								r = r_uniform(*generator);
							} else {
								r = randu<double>();
							}
						} 
						psi.col(i-1) = psi.col(i-1)/norm(psi.col(i-1));
					} else { // Use old method (new random number each time step).
						psi.col(i) = psi.col(i)/norm(psi.col(i));
						if (!USE_ARMA_RNG) {
							r = r_uniform(*generator);
						} else {
							r = randu<double>();
						}
						if (Gamma*dt*pow(abs(psi(0,i)),2) > r) {
							psi.col(i) = psi_gs;
							dN(i) = 1;
							continue;
						}
					}
				}
				if (use_new_method) {
					psi.col(nt-1) = psi.col(nt-1)/norm(psi.col(nt-1));
				}
			}
			return std::make_tuple(psi,dN);
		}
};

// simple function for getting the correct detection strategy
DetectionStrategy* getDetectionStrategy(int use_homodyne) {
	if (use_homodyne) {
		return new HomodyneDetection();
	}
	return new CountingDetection();
}

std::tuple<cx_cube,mat> mcsolve(int N, double t0, double T, double nt, mat omegas, bool use_homodyne, uint_least64_t manual_seed, bool use_manual_seed) {
	cx_cube psi_total;
	if (save_psi) {
		cx_cube psi_total = cx_cube(2,nt,N,fill::none);
	}
	mat signal_total = mat(N,nt);

	////!! COPY OF CODE IN LOOP - TO BE USED IF A SINGLE INSTANCE OF A MT19937 SHOULD BE USED ACROSS 
	////!! TRAJECTORIES - PROBABLY FAILS WITH CONCURRENCY ISSUES IF MULTITHREADED
	////!! Edited to use copies of the same generator at different nt displaced starting points
	// Set seed to be value generated by calling linux function "getrandom" which
	// tries to fetch random bytes from the urandom source (the same source as the /dev/urandom device).
	// The getrandom call will be blocked if the urandom source has not yet been initialized.
	// According to https://github.com/Microsoft/WSL/issues/1789, in WSL BCryptGenRandom is used
	// in the driver to generate random bytes for the getrandom syscall and /dev/random and 
	// /dev/urandom. Link: https://docs.microsoft.com/en-us/windows/win32/api/bcrypt/nf-bcrypt-bcryptgenrandom?redirectedfrom=MSDN (checked 18/02/2020)
	// An array of 312 unsigned 64 bit integers (state size of MT19937_64) is filled with random
	// numbers from this source. This array is then used to create a seed sequence which
	// will be used to generate the initial state of the 64 bit Mersenne Twister 
	// PRNG algorithm. This is probably overkill, different sources say giving seed sequence
	// a few random seeds is enough and some argue that seed sequence itself is bad. Some
	// say a single random number is enough.
	std::mt19937_64 gen;
	std::mt19937_64* gen_ptr;
	//int n_discards = 70000;
	if (!USE_ARMA_RNG) {
		if (use_manual_seed) {
			gen.seed(manual_seed);
			cout << "mcsolve: Manual seed set for MT19937 generator." << endl;
		} else {
			std::uint_least64_t seeds[gen.state_size];
			for (int i=0; i<gen.state_size; i++) {
				sysrandom(&seeds[i], sizeof(seeds[i]));
			}
			std::seed_seq state(std::begin(seeds),std::end(seeds));
			gen.seed(state);
			cout << "mcsolve: Random seed set for MT19937 generator." << endl;
		}
		//gen.discard(n_discards);
	} else {
		if (use_manual_seed) {
			arma_rng::set_seed(manual_seed);
			cout << "mcsolve: Manual seed set in Arma." << endl;
		} else {
			std::uint_least32_t seed;
			sysrandom(&seed, sizeof(seed));
			arma_rng::set_seed(seed);
			cout << "mcsolve: Random seed set in Arma." << endl;
		}
		//vec discardedVals = randn(n_discards);
	}
	gen_ptr = &gen;
	//cout << "gen before loop: " << gen() << endl;

	// Make N-1 copies of the generator starting at different places and a list of pointers
	// to them.
	std::mt19937_64* gen_ptrs[N];
	gen_ptrs[0] = gen_ptr;
	for (int i=1; i<N; i++) {
		//auto start = std::chrono::high_resolution_clock::now();
		gen.discard(nt+100);
		//auto stop= std::chrono::high_resolution_clock::now();
		//auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop-start);
		std::mt19937_64 gen_i = gen;
		gen_ptrs[i] = &gen;
		//cout << " i: " <<i << " duration : " << duration.count() << endl;
	}

	#pragma omp parallel for // do CPU multithreading
	for (int i=0; i<N; i++) {
		DetectionStrategy* detectionStrategy = getDetectionStrategy(use_homodyne);
		
		// Make a copy of the initial MT and discard i*nt values
		//std::mt19937_64 gen_i = gen;
		//gen_i.discard(i*nt);
		//std::mt19937_64* gen_ptr_i = &gen_i;
		
		// Get pointer to MT with i*nt discarded values which is initalized outside loop
		std::mt19937_64* gen_ptr_i = gen_ptrs[i];

		//if (i == 0) {
		//	std::mt19937_64 gen_i = gen;
		//	cout << "gen inside loop: " << gen_i() << endl;
		//	std::mt19937_64 gen_a(28);
		//	cout << "new gen seed 28 inside loop: " << gen_a() << endl;
		//	cout << "new gen seed 28 inside loop 2nd number: " << gen_a() << endl;
		//}
		//if (i == 1) {
		//	std::mt19937_64 gen_i = gen;
		//	cout << "gen inside loop: " << gen_i() << endl;
		//	std::mt19937_64 gen_a(28);
		//	cout << "new gen seed 28 inside loop: " << gen_a() << endl;
		//	cout << "new gen seed 28 inside loop 2nd number: " << gen_a() << endl;
		//}

		// Set seed to be pseudo-cycles of processor since power on.
		// Changes at a rate faster than calling time(NULL) which only changes 
		// once per second - does not specify a full state for the MT (requires 624*32 bits or 312*64 bits).
		// long long seed = rdtsc();
		// std::mt19937 rng(seed);
		
		// Set seed for MT to be time since epoch (high precision time).
		// Does not specify full state for MT (requires 624*32 bits or 312*64 bits).
		// std::mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());

		// Set seed to be value obtained from std::random_device which is either 
		// a true RNG or in some implementations just a pseudo-RNG. 
		// Does not specify full state for MT (requires 624*32 bits or 312*64 bits).
		// std::random_device rd;
		// std::mt19937 rng(rd());

		// Set seed to be value generated by calling linux function "getrandom" which
		// tries to fetch random bytes from the urandom source (the same source as the /dev/urandom device).
		// The getrandom call will be blocked if the urandom source has not yet been initialized.
		// According to https://github.com/Microsoft/WSL/issues/1789, in WSL BCryptGenRandom is used
		// in the driver to generate random bytes for the getrandom syscall and /dev/random and 
		// /dev/urandom. Link: https://docs.microsoft.com/en-us/windows/win32/api/bcrypt/nf-bcrypt-bcryptgenrandom?redirectedfrom=MSDN (checked 18/02/2020)
		// An array of 312 unsigned 64 bit integers (state size of MT19937_64) is filled with random
		// numbers from this source. This array is then used to create a seed sequence which
		// will be used to generate the initial state of the 64 bit Mersenne Twister 
		// PRNG algorithm. This is probably overkill, different sources say giving seed sequence
		// a few random seeds is enough and some argue that seed sequence itself is bad. Some
		// say a single random number is enough.
		//std::mt19937_64 gen;
		//std::mt19937_64* gen_ptr;
		//int n_discards = 70000;
		//if (!USE_ARMA_RNG) {
		//	std::uint_least64_t seeds[gen.state_size];
		//	for (int j=0; j<gen.state_size; j++) {
		//		sysrandom(&seeds[j], sizeof(seeds[j]));
		//	}
		//	std::seed_seq state(std::begin(seeds),std::end(seeds));
		//	//gen.seed(state);
		//	gen.seed(i);
		//	gen.discard(n_discards);
		//} else {
		//	std::uint_least32_t seed;
		//	sysrandom(&seed, sizeof(seed));
		//	//cout << "Seed :" << seed << endl;
		//	arma_rng::set_seed(seed);
		//	vec discardedVals = randn(n_discards);
		//}
		//gen_ptr = &gen;

		// Setting seed for the RNG used by Armadillo. Dropped this since there is little transparency
		// as to what PRNG algorithm is used (according to some people on the net, in C++11 and above
		// it uses the default of the <random> library.
		// long long seed = rdtsc()
		//arma_rng::set_seed(seed);
		// Throw away first random value.
		// According to a guy on stackexchange the first value of similar seeds
		// might look alike but the series diverge quickly.
		//vec dummy = randn<vec>(1);

		//cout << "New trajectory number i: "<< i << endl;
		std::tuple<cx_mat,rowvec> traj_i = detectionStrategy->getMCTrajectory(t0, T, nt, omegas.row(i), gen_ptr_i);
		if (save_psi) {
			psi_total.slice(i) = std::get<0>(traj_i);
		}
		signal_total.row(i) = std::get<1>(traj_i);
	}
	// check for nan values
	if (psi_total.has_nan() || signal_total.has_nan()) {
		cout << "Data has NaN values. Exiting..." << endl;
		exit(EXIT_FAILURE);
	}
	// check for inf values
	if (psi_total.has_inf() || signal_total.has_inf()) {
		cout << "Data has INF values. Exiting..." << endl;
		exit(EXIT_FAILURE);
	}
	return std::make_tuple(psi_total,signal_total);
}

// function for adding noise to signal
// returns: signal with some noise added
mat addNoise(mat signal) {
	vec non_zero_elements = nonzeros(signal);
	int n_discards = 70000;
	std::uint_least32_t seed;
	sysrandom(&seed, sizeof(seed));
	arma_rng::set_seed(seed);
	vec discardedVals = randn(n_discards);
	vec rand_delay = randi<vec>(non_zero_elements.n_elem, distr_param(0,MAX_DELAY));
	mat signal_noisy = zeros<mat>(arma::size(signal));
	int k = 0;
	for (int i=0; i<signal.n_rows; i++) {
		for (int j=0; j<signal.n_cols; j++) {
			if (signal(i,j)) {
				signal_noisy(i,j) = 0;
				if (j+rand_delay(k) < signal.n_cols) {
					signal_noisy(i,j+rand_delay(k)) = 1;
					k++;
				}
			}
		}
	}
	//signal.row(0).print("first traj before noise:\n");
	//signal_noisy.row(0).print("first traj after noise:\n");
	//rand_delay.t().print("Delays:\n");
	return signal_noisy;
}

std::tuple<cx_cube, mat, vec, mat> get_binned_data(cx_cube psi, mat signal, vec times, mat omegas, int N_bins) {
	int bin_factor = floor(times.size()/N_bins);

	cout << "Binning factor: " << times.size()/N_bins << endl;
	cx_cube psi_binned;
	psi_binned.set_size(psi.n_rows,N_bins,psi.n_slices);
	mat signal_binned;
    signal_binned.set_size(signal.n_rows,N_bins);
	vec times_binned = linspace<vec>(times.front(),times.back(),N_bins);
	mat omegas_binned;
	omegas_binned.set_size(omegas.n_rows,N_bins);

	for (int i = 0; i<N_bins; i++) {
		signal_binned.col(i) = arma::mean(signal(span::all,span((i*bin_factor),((i+1)*bin_factor-1))),1);
	}
	return std::make_tuple(psi_binned, signal_binned, times_binned, omegas_binned);

}

// Method for formatting a number into a string with no trailing zeros or
// periods for use in path specification.
template <class T>
string customToString(T val) {
	string result = to_string(val);
	int offset = 1;
	if (result.find_last_not_of('0') == result.find('.')) {
		offset = 0;
	}
	result.erase(result.find_last_not_of('0') + offset, string::npos);
	transform(result.begin(), result.end(), result.begin(), [](char ch) {
			return ch == '.' ? '_' : ch;
			});
	return result;
}

int main(int argc, char** argv) {
	if (argc < 10) {
		//report version
		cout << argv[0] << " Version " << FirstProject_VERSION_MAJOR << "." << FirstProject_VERSION_MINOR << endl;
		cout << "Usage: " << argv[0] << " number_of_mc_trajectories<int> fast_eval<int> add_noise<int> use_homodyne<int> bin_output<int> Omega<int> trajectory_length<double> dt<double> filename<string> (optional)manual_seed<uint_least64_t>" << endl;
		cout << "If Omega=n where n is an integer, simulate with that frequency only. If Omega == -1, use predefined hypotheses in c++ file (for classification). If Omega==-2, use predefined range of hypotheses in c++ file (for regression)." << endl;
		return 1;
	}
    double t0 = 0; // initial time
	double T = 10; // final time

	int use_homodyne = true; // use homodyne detection
	
	int N; // number of trajectories

	int Omega; // Rabi frequency

	double dt = 0.001;

	int bin_output = 0;

	// assign command line arguments to variables
	const int NUM_INT_ARGS = 6;
	int* intCMDLineArguments[NUM_INT_ARGS] = {&N, &fast_eval, &add_noise, &use_homodyne, &bin_output, &Omega};
	for (int i = 1; i<NUM_INT_ARGS+1; i++) {
		istringstream ss(argv[i]);
		if (!(ss >> *intCMDLineArguments[i-1])) {
			cerr << "Invalid number: " << argv[i] << endl;
			return 1;
		} else if(!ss.eof()) {
			cerr << "Trailing characters after number: " << argv[i] << endl;
			return 1;
		}
	}
	const int NUM_DOUBLE_ARGS = 2;
	double* doubleCMDLineArguments[NUM_DOUBLE_ARGS] = {&T, &dt};
	for (int i = NUM_INT_ARGS+1; i<NUM_INT_ARGS+NUM_DOUBLE_ARGS+1; i++) {
		istringstream ss(argv[i]);
		if (!(ss >> *doubleCMDLineArguments[i-NUM_INT_ARGS-1])) {
			cerr << "Invalid number: " << argv[i] << endl;
			return 1;
		} else if(!ss.eof()) {
			cerr << "Trailing characters after number: " << argv[i] << endl;
			return 1;
		}
	}

		
	int NUM_ARGS = NUM_INT_ARGS + NUM_DOUBLE_ARGS;

	// Handle paths and filenames - separate files for data with and w/o noise.
	string filename = argv[NUM_ARGS+1];
	NUM_ARGS++;
	filename += "_T" + customToString<double>(T);
	fs::path p = fs::path("data");

	if (use_homodyne) {
		p /= "homodyne";
	} else {
		p /= "counting";
	}

	cout << "Time resolution dt = " << dt << " chosen." << endl;
	if (add_noise) {
		cout << "Noise will be added." << endl;
	} else {
		cout << "Noise will not be added." << endl;
	}
	if (bin_output) {
		cout << "A binned version of the signal will be saved along with the default data." << endl;
	} else {
		cout << "The signal will not be binned." << endl;
	}

	p /= "dt_" + customToString<double>(dt);

	if (Omega == -2) {
		p /= "linspaced";
	} else if (Omega == -1) {
		p /= "hyp2class";
	} else {
		p /= "singleOmega";
	}

	// set number of time steps
	int nt = floor(T/dt)+1; // number of time steps

	filename += ".h5";

	// Rabi frequency of system
	vec hypotheses;
	if (Omega == -1) {
		hypotheses = { 2, 6 };
		//vec hypotheses = { 1, 2, 4, 6};
	} else if (Omega == -2) {
		hypotheses = linspace<vec>(2,14,200);
	}
   	else {
		hypotheses = { (double) Omega };
	}

	// Code for taking manual seed from command line for reproducibility
	bool USE_MANUAL_SEED = false;
	std::uint_least64_t manual_seed;
	if (argc == NUM_ARGS + 2) {
		USE_MANUAL_SEED = true;
		istringstream ss(argv[NUM_ARGS+1]);
		if (!(ss >> manual_seed)) {
			cerr << "Invalid number: " << argv[NUM_ARGS+1] << endl;
			return 1;
		} else if(!ss.eof()) {
			cerr << "Trailing characters after number: " << argv[NUM_ARGS+1] << endl;
			return 1;
		}
		cout << "Manual seed specified succesfully. Seed is: " << manual_seed << endl;
	}

	if (USE_MANUAL_SEED) {
		arma_rng::set_seed(manual_seed);
		cout << "main: Manual seed set in Arma." << endl;
	} else {
		std::uint_least32_t seed;
		sysrandom(&seed, sizeof(seed));
		arma_rng::set_seed(seed);
		cout << "main: Random seed set in Arma." << endl;
	}

	// create vector of uniformly distributed random numbers in the range of the 
	// number of hypotheses
	//vec omega_rand = randi<vec>(N, distr_param(0,hypotheses.n_elem-1));
	vec omega_rand = randi<vec>(N, distr_param(0,hypotheses.n_elem-1));
	
	// For constant Rabi frequency throughout each trajectory - only save one element 
	// to save memory.
	mat omegas(N,1);
	for (int i = 0; i<N; i++) {
		omegas(i,0) = hypotheses(omega_rand(i));
	}
	
	// For filling matrix of omegas
	//mat omegas(N,nt); // initialize matrix of Rabi frequencies for each trajectory
	// assign to each trajectory the Rabi frequency corresponding to the i'th random number
	// in omega_rand
	//for (int i=0; i<N; i++) {
	//	// for constant Rabi frequency throughout each trajectory
	//	omegas(i,0) = hypotheses(omega_rand(i));
	//	omegas.row(i).fill(omegas(i,0));

	//	// varying Rabi frequency
	//	//for (int j=0; j<nt; j++) {
	//	//	omegas(j) = ?? //TBD how dynamic Rabi frequencies should be assigned to each 
	//	//					 trajectory
	//	//}
	//}

	cout << "Simulating " << N << " trajectories using " << (fast_eval ? "fast evaluation":"standard simulations") << ". Using dt = " << dt << " and T = " << T << ". Storing data in " << filename << endl;
	std::tuple<cx_cube,mat> psiAndSignal = mcsolve(N, t0, T, nt, omegas, use_homodyne, manual_seed, USE_MANUAL_SEED);
	cx_cube psi = std::get<0>(psiAndSignal); // get state
	mat signal = std::get<1>(psiAndSignal); // get signal - either homodyne signal or "click" record
	// Create folders if they do not exist.
	fs::create_directories(p);
	// Save data to hdf5 file.
	fs::path file_path = p;
	file_path /= filename;
	if (save_psi) {
		psi.save(hdf5_name(file_path,"trajectories"));
		signal.save(hdf5_name(file_path,"clicks",hdf5_opts::append));
	} else {
		signal.save(hdf5_name(file_path,"clicks"));
	}
	vec times = linspace(t0,T,nt);
	times.save(hdf5_name(file_path,"times",hdf5_opts::append));
	omegas.save(hdf5_name(file_path,"frequencies",hdf5_opts::append));
	hypotheses.save(hdf5_name(file_path,"hypotheses",hdf5_opts::append));
	cout << "Filepath used: " << file_path << endl;


	if (bin_output) {
		int N_bins = floor(nt/10);
		std::tuple<cx_cube,mat,vec,mat> binned_data = get_binned_data(psi, signal, times, omegas, N_bins);
		cx_cube psi_binned = std::get<0>(binned_data);
		mat signal_binned = std::get<1>(binned_data);
		vec times_binned = std::get<2>(binned_data);
		mat omegas_binned = std::get<3>(binned_data);
		signal_binned.save(hdf5_name(file_path,"signal_binned",hdf5_opts::append));
		times_binned.save(hdf5_name(file_path,"times_binned",hdf5_opts::append));
	}

	// if add_noise=1, add noise to signal and save hdf5 file containing noisy data
	if (add_noise) {
		fs::path file_path_noisy = p;
		file_path_noisy /= "noisy";
		fs::create_directories(file_path_noisy);
		file_path_noisy /= filename;
		mat signal_noisy = addNoise(signal);
		psi.save(hdf5_name(file_path_noisy,"trajectories"));
		signal_noisy.save(hdf5_name(file_path_noisy,"clicks",hdf5_opts::append));
		times.save(hdf5_name(file_path_noisy,"times",hdf5_opts::append));
		omegas.save(hdf5_name(file_path_noisy,"frequencies",hdf5_opts::append));
	}

	return 0;
}
