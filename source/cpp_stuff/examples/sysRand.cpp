// some of these can probably be removed
#include <cstdint>
#include <string>
#include <iostream>
#include <armadillo>
#include <random>
#include <cstdlib>
#include <ctime>
#include <chrono>

using namespace std;
using namespace arma;

// Function created by user Alexander Huszagh on stackoverflow: https://stackoverflow.com/questions/45069219/how-to-succinctly-portably-and-thoroughly-seed-the-mt19937-prng (checked 18/02/2020)
#if defined(__linux__) || defined(linux) || defined(__linux)
    // Check the kernel version. `getrandom` is only Linux 3.17 and above.
#   include <linux/version.h>
#   if LINUX_VERSION_CODE >= KERNEL_VERSION(3,17,0)
#       define HAVE_GETRANDOM
#   endif
#endif

// also requires glibc 2.25 for the libc wrapper
#if defined(HAVE_GETRANDOM)
#   include <sys/syscall.h>
#   include <linux/random.h>


size_t sysrandom(void* dst, size_t dstlen)
{
    int bytes = syscall(SYS_getrandom, dst, dstlen, 0);
    if (bytes != dstlen) {
    	throw std::runtime_error("Unable to read N bytes from CSPRNG.");
    }
    return dstlen;
}

#elif defined(_WIN32)

// Windows sysrandom here.

#else

// POSIX sysrandom here.

#endif

