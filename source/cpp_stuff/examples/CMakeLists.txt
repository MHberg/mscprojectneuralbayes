cmake_minimum_required(VERSION 3.10)

# project name and version
project(examples VERSION 1.0)

# specify the C++ standard
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED True)

set(GCC_LINK_FLAGS "-lstdc++fs")
# add executable
add_executable(mcsolver mcsolver.cpp sysRand.cpp)
set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${GCC_LINK_FLAGS}")
if(USE_MKL)
	#target_link_libraries(mcsolver ${ARMADILLO_LIBRARIES} ${HDF5_LIBRARIES} ${MKL_LIBRARIES})
	if(OpenMP_CXX_FOUND)
		target_link_libraries(mcsolver PUBLIC ${HDF5_LIBRARIES} ${ARMADILLO_LIBRARIES} ${MKL_LIBRARIES} OpenMP::OpenMP_CXX)
	else()
		target_link_libraries(mcsolver PUBLIC ${HDF5_LIBRARIES} ${ARMADILLO_LIBRARIES} ${MKL_LIBRARIES})
	endif()
else()
	if(OpenMP_CXX_FOUND)
		target_link_libraries(mcsolver PUBLIC ${ARMADILLO_LIBRARIES} ${BLAS_LIBRARIES} ${HDF5_LIBRARIES} OpenMP::OpenMP_CXX)
	else()
		target_link_libraries(mcsolver PUBLIC ${ARMADILLO_LIBRARIES} ${BLAS_LIBRARIES} ${HDF5_LIBRARIES})
	endif()
endif()
target_link_libraries(mcsolver PUBLIC ${GCC_LINK_FLAGS})
target_include_directories(mcsolver PUBLIC "${PROJECT_BINARY_DIR}")
