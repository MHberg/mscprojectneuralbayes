# Fix RNG seed in NumPy and Tensorflow for reproducibility
from numpy.random import seed
seed(1)
import tensorflow as tf
tf.random.set_seed(2)
kfold_seed = 1 # fix seed for rng

import os
import datetime
import sys
from pathlib import Path
import time
import numpy as np
import math
from tensorflow.keras.models import load_model
from tensorflow.keras.callbacks import ModelCheckpoint, TensorBoard, EarlyStopping
from tensorflow.keras import optimizers
from tensorflow.keras import metrics
from tensorflow.keras.utils import to_categorical
from sklearn.model_selection import KFold
from sklearn.metrics import ConfusionMatrixDisplay
from sklearn.metrics import confusion_matrix
import sklearn
import pickle
from mpl_toolkits.axes_grid1 import make_axes_locatable

np.set_printoptions(threshold=sys.maxsize)

# custom functions and classes
import KerasBatchSequence
import KerasBatchGenerators
import KerasModels
from plottingTools import *
from FileManagement import *
import bayes
import sgdr
import mesolver

### Configuration for hypothesis testing
# {

# Separate folder for testing stuff - used for noisy trajectories right now
TESTING = True

# Boolean specifying if homodyne detection or photon counting data should be loaded 
USE_HOMODYNE = True

ADD_NOISE_INSTEAD_OF_BINNING = True

# Global variable RECALCULATE_BAYES is used to determine whether or not to recalculate 
# Bayes on the trajectories when load_accs=False
# This allows retraining and testing the network on the same data without also running Bayes again
# If Bayes have not been run on a trajectory or new trajectories are simulated, RECALCULATE_BAYES
# should be set to True
RECALCULATE_BAYES = False

# Resolution of time in units of 1/gamma
dt = 0.001

if (TESTING):
    #save_path += 'testing/'
    #save_path += 'onlyNoise/'
    save_path += 'noiseAndWhiteNoise/'
    #save_path += 'onlyWhiteNoise/'
    #save_path += 'LSTM/'

if (USE_HOMODYNE):
    data_path += 'homodyne/'
    save_path += 'homodyne/'
else:
    data_path += 'counting/'
    save_path += 'counting/'

dt_str = str(dt)
dt_str = dt_str.replace('.','_')

data_path += 'dt_' + dt_str + '/'
save_path += 'dt_' + dt_str + '/'

# save path and hypotheses chosen
hypotheses = np.array((2,6))
#hypotheses = np.array((1,2,4,6))

single_omega_data_path = data_path + 'singleOmega/'
data_path += 'hyp' + str(hypotheses.size) + 'class/'
save_path += 'hyp' + str(hypotheses.size) + 'class/'

# Create directories on save path if they do not exist.
Path(save_path).mkdir(parents=True, exist_ok=True)

# }


def plotConfusionMatrix(pred, actual, filename):
    #display_labels = np.array(['$\Omega=' + str(hypotheses[0]) + '$','$\Omega=' + str(hypotheses[1]) + '$'])
    display_labels = np.array(['$h_0$','$ h_1 $'])
    sample_weight = None
    normalize = 'all'
    include_values = True
    xticks_rotation = 'horizontal'
    values_format = '.2f'
    cmap = 'Blues'
    fig = plt.figure()
    ax = fig.gca()
    #ax = None


    cm = confusion_matrix(actual, pred, labels=None, sample_weight=sample_weight, normalize=normalize)
    disp = ConfusionMatrixDisplay(confusion_matrix=cm, display_labels=display_labels)
    disp.plot(include_values=include_values, cmap=cmap, ax=ax, xticks_rotation=xticks_rotation, values_format=values_format)
    im = ax.images
    cb = im[-1].colorbar
    cb.remove()
    disp.im_.set_clim(vmin=0,vmax=0.5)

    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    plt.colorbar(disp.im_, cax=cax)
    #fig = disp.figure_
    fig.set_size_inches(2.5, 2.5)
    plt.tight_layout(rect=[0,0,0.85,1])
    fig.savefig(save_path + filename + '.pdf',dpi=100)
    plt.close(fig)

def calcPredictionNN(signal, signal_name, model, T_str, save=True):
    pred = hypotheses[np.argmax(model.predict(signal),axis=1)]
    if save:
        np.save(save_path + "pred_NN" + signal_name + T_str, pred)
    return pred

def calcPredictionBayes(psi, signal, signal_name, times, Delta, gamma, Phi, use_binned_data, binningFactor, save, T_str, bayesProbs = [], bayesSignal = []):
    if (use_binned_data):
        print("Calculating Bayes predictions for T=",T_str," and binning factor =", binningFactor, "...")
    else:
        print("Calculating Bayes predictions for T=",T_str)
    #T_str = T_ToFileNameString(times[-1])
    #print('T_str in calcPredictionBayes: ', T_str)
    #print('times[-1] in calcPredictionBayes: ', times[-1])

    if (len(bayesProbs) == 0):
        print('Bayes was not already calculated for this record length. Calculating Bayes for length: ', times[-1])
        bayesProbability, bayesSignal = bayes.bayesPredict(psi, signal, times, hypotheses, Delta, gamma, USE_HOMODYNE, Phi, use_binned_data, binningFactor)
    else:
        #print('bayesprobs = ',bayesProbs)
        print('bayesprobs.shape = ',bayesProbs.shape)
        bayesProbability = bayesProbs
    end_idx = get_end_idx(times[-1],dt)
    print('Times vector in calcPredictionBayes:', times[-1])
    print('end_idx for bayes calculation: ', end_idx)
    bayesSignal = bayesSignal[:,:end_idx]
    print('bayesSignal shape: ', bayesSignal.shape)
    print('bayesProbability shape: ', bayesProbability.shape)
    print('Fetching argmax of column %.i of bayesProbability'%end_idx)
    pred_bayes = hypotheses[np.argmax(bayesProbability[:,:,end_idx-1],axis=1)]
    if save:
        np.save(save_path + "pred_bayes" + signal_name + T_str, pred_bayes)
    print("Bayes prediction calculation done, results saved as ",save_path + "pred_bayes" + signal_name + T_str)
    return pred_bayes, bayesSignal

def calcAccuracy(pred, actual, filename, save):
    acc = sklearn.metrics.accuracy_score(pred, actual)
    if save:
        np.save(save_path + filename, acc)
    return acc

def calc_Qe_from_accs(acc0, acc1):
    return 1-(acc0+acc1)/2

def plot_acc_vs_T(acc0, acc1, bayes_acc0, bayes_acc1, Ts):
    fig,axes = plt.subplots(2)
    axes[0].plot(Ts, acc0, label='NN accuracy')
    axes[1].plot(Ts, acc1, label='NN accuracy')
    axes[0].plot(Ts, bayes_acc0, label='Bayes accuracy')
    axes[1].plot(Ts, bayes_acc1, label='Bayes accuracy')
    for ax in axes:
        ax.set(xlabel="Trajectory length $\gamma T$", ylabel="Accuracy")
        ax.legend()
    axes[0].set(title='Accuracy versus trajectory length $\Omega =' + str(hypotheses[0]) + '$')
    axes[1].set(title='Accuracy versus trajectory length $\Omega =' + str(hypotheses[1]) + '$')
    fig.tight_layout()
    fig.savefig(save_path + 'acc_vs_T.pdf')
    plt.close(fig)

def plot_saved_results_Ts(Ts):
    acc0, bayes_acc0, acc1, bayes_acc1 = list(), list(), list(), list()
    for T in Ts:
        T_str = T_ToFileNameString(T)
        acc0.append(np.load(save_path + "nn_acc" + str(hypotheses[0]) + T_str + '.npy'))
        acc1.append(np.load(save_path + "nn_acc" + str(hypotheses[1]) + T_str + '.npy'))
        bayes_acc0.append(np.load(save_path + "bayes_acc" + str(hypotheses[0]) + T_str + '.npy'))
        bayes_acc1.append(np.load(save_path + "bayes_acc" + str(hypotheses[1]) + T_str + '.npy'))

    acc0_T9 = np.load(save_path + 'nn_acc' + str(hypotheses[0]) + '_bestT9.npy')
    acc1_T9 = np.load(save_path + 'nn_acc' + str(hypotheses[1]) + '_bestT9.npy')

    bayes_acc0 = np.array(bayes_acc0)
    bayes_acc1 = np.array(bayes_acc1)
    acc0 = np.array(acc0)
    acc1 = np.array(acc1)
    plot_acc_vs_T(acc0, acc1, bayes_acc0, bayes_acc1, Ts)
    Qe_nn = calc_Qe_from_accs(acc0, acc1)
    Qe_nn_T9 = calc_Qe_from_accs(acc0_T9, acc1_T9)
    Qe_bayes = calc_Qe_from_accs(bayes_acc0, bayes_acc1)
    plot_Qe_vs_T(Qe_nn, Qe_bayes, Ts, [(9, Qe_nn_T9)])

def plot_Qe_vs_T(Qe_nn, Qe_bayes, Ts, extra = []):
    fig = plt.figure()
    ax = plt.gca()
    ax.plot(Ts, Qe_nn, label='Neural network')
    if TESTING:
        ax.plot(Ts, Qe_bayes, label="Poor man's Bayes")
    else:
        ax.plot(Ts, Qe_bayes, label='Bayes')
    if not (len(extra) == 0):
        for x,y in extra:
            ax.plot(x,y,'o',label='Optimized NN',color='C0')

    ax.set(xlabel="$\gamma t$", ylabel="$Q_e$")
    enchantAx(ax)
    ax.set_ylim([0,0.5])
    ax.set_xlim([0,15])
    ax.legend()
    #ax.set(title='Error probability versus trajectory length with $\Omega = ' + str(hypotheses[0]) + '$ and $\Omega = ' + str(hypotheses[1]) + '$')
    fig.set_size_inches(5.2, 3.5)
    fig.tight_layout()
    fig.savefig(save_path + 'Qe_vs_T.pdf')
    return fig,ax

def test_saved_models_binned(Ts, binningFactors, load_accs=False):
    global save_path
    save = True
    models = list()
    acc0_lst, bayes_acc0_lst, acc1_lst, bayes_acc1_lst = list(), list(), list(), list()
    temp_save_path = save_path
    for binningFactor in binningFactors:
        if not ADD_NOISE_INSTEAD_OF_BINNING:
            # Binned
            save_path += "binned_" + str(binningFactor) + "/"
        else:
            # Noise
            save_path += "noisy_" + str(binningFactor) + "/"
        # Create directories on save path if they do not exist.
        Path(save_path).mkdir(parents=True, exist_ok=True)
        acc0, acc1, bayes_acc0, bayes_acc1 = test_saved_models(Ts, True, binningFactor, load_accs)

        acc0_lst.append(acc0)
        acc1_lst.append(acc1)
        bayes_acc0_lst.append(bayes_acc0)
        bayes_acc1_lst.append(bayes_acc1)
        save_path = temp_save_path

    # Turn lists into numpy arrays.
    acc0 = np.array(acc0_lst)
    acc1 = np.array(acc1_lst)
    bayes_acc0 = np.array(bayes_acc0_lst)
    bayes_acc1 = np.array(bayes_acc1_lst)
    print(acc0.shape)
    print(acc1.shape)
    print(bayes_acc0.shape)
    print(bayes_acc1.shape)

    plot_acc_vs_binfactor(acc0, acc1, bayes_acc0, bayes_acc1, binningFactors)
    Qe_nn = calc_Qe_from_accs(acc0, acc1)
    Qe_bayes = calc_Qe_from_accs(bayes_acc0, bayes_acc1)
    plot_Qe_vs_binfactor(Qe_nn, Qe_bayes, binningFactors)

def plot_acc_vs_binfactor(acc0, acc1, bayes_acc0, bayes_acc1, binfactors):
    fig,axes = plt.subplots(2)
    axes[0].plot(binfactors, acc0, label='NN accuracy')
    axes[1].plot(binfactors, acc1, label='NN accuracy')
    axes[0].plot(binfactors, bayes_acc0, label='Bayes accuracy')
    axes[1].plot(binfactors, bayes_acc1, label='Bayes accuracy')
    for ax in axes:
        ax.set(xlabel="Binning factor", ylabel="Accuracy")
        ax.legend()
    axes[0].set(title='Accuracy versus binning factor with $\Omega = ' + str(hypotheses[0]) + ' $')
    axes[1].set(title='Accuracy versus binning factor with $\Omega = ' + str(hypotheses[1]) + '$')
    fig.tight_layout()
    fig.savefig(save_path + 'acc_vs_binfactor.pdf')
    plt.close(fig)

def plot_Qe_vs_binfactor(Qe_nn, Qe_bayes, binfactors):
    fig = plt.figure()
    ax = plt.gca()
    ax.plot(binfactors, Qe_nn, label='Neural network')
    ax.plot(binfactors, Qe_bayes, label="Poor man's Bayes")
    print('Qe bayes: ',Qe_bayes)
    print('Qe nn: ',Qe_nn)
    if ADD_NOISE_INSTEAD_OF_BINNING:
        ax.set(xlabel=r"$ \alpha $", ylabel="$Q_e$")
    else:
        ax.set(xlabel="Binning factor", ylabel="$Q_e$")
    ax.legend()
    #ax.set(title='Error probability versus binning factor with $\Omega = ' + str(hypotheses[0]) + '$ and $\Omega = ' + str(hypotheses[1]) + '$')
    enchantAx(ax)
    ax.set_ylim([0,0.5])
    ax.set_xlim([binfactors[0],binfactors[-1]])
    ax.legend()
    fig.set_size_inches(5.2, 3.5)
    fig.tight_layout()
    fig.savefig(save_path + 'Qe_vs_binfactor.pdf')
    plt.clf()

def test_saved_models(Ts, use_binned_data, binningFactor, load_accs=False):
    save = True
    Delta = 0
    gamma = 1
    Phi = np.pi/2

    models = list()
    acc0_lst, bayes_acc0_lst, acc1_lst, bayes_acc1_lst = list(), list(), list(), list()

    if not load_accs:
        # Load single omega test data (4096 trajectories simulated at same Rabi frequency).
        max_T_str = T_ToFileNameString(15)
        end_idx = 0
        if (use_binned_data):
            #end_idx = get_end_idx(Ts[-1],dt)
            end_idx = get_end_idx(Ts[-1]-dt,dt)
        psi_test0_all, test_signal_Omega0_all, times_test0_all, omegas_test0_all = read_data(single_omega_data_path + 'test_Omega' + str(hypotheses[0]) + max_T_str + '.h5',np.arange(0), np.arange(end_idx))
        psi_test1_all, test_signal_Omega1_all, times_test1_all, omegas_test1_all = read_data(single_omega_data_path + 'test_Omega' + str(hypotheses[1]) + max_T_str + '.h5',np.arange(0), np.arange(end_idx))

        # If testing binned data, bin signal
        if (use_binned_data):
            test_signal_Omega0_all, times_all = binSignal(test_signal_Omega0_all, times_test0_all, binningFactor)
            test_signal_Omega1_all, _ = binSignal(test_signal_Omega1_all, times_test1_all, binningFactor)
            print("In hyptest: signal0 after binning: ",test_signal_Omega0_all.shape)
            print("In hyptest: signal1 after binning: ",test_signal_Omega1_all.shape)
            print("In hyptest: binned times : ",times_all.shape)
        else:
            # Signals and state trajectories have same resolution
            times_all = times_test0_all

        # Calculate bayes probabilities for each hypothesis using all test data.
        if (RECALCULATE_BAYES):
            print("Calculating Bayes predictions for T=",max_T_str," and binning factor =", binningFactor, "...")
            print('This record has length: ', times_test0_all[-1])
            print("In hyptest: signal0 just before bayes prediction: ",test_signal_Omega0_all.shape)
            bayesProb0, bayesSignal0_all = bayes.bayesPredict(psi_test0_all, test_signal_Omega0_all, times_test0_all, hypotheses, Delta, gamma, USE_HOMODYNE, Phi, use_binned_data, binningFactor)
            print("In hyptest: bayesSignal0_all just after bayes prediction: ",bayesSignal0_all.shape)
            bayesProb1, bayesSignal1_all = bayes.bayesPredict(psi_test1_all, test_signal_Omega1_all, times_test1_all, hypotheses, Delta, gamma, USE_HOMODYNE, Phi, use_binned_data, binningFactor)

    for idx,T in enumerate(Ts):
        T_str = T_ToFileNameString(T)
        print('T_str: ',T_str)

        if not load_accs:
            scores = np.load(save_path + 'hypTestingScores' + T_str + '.npy')
            if (scores.size > 1):
                print(scores[0,:])
                best_model_idx = np.argmax(scores[0,:],axis=0)
            else:
                best_model_idx = 0
            print(best_model_idx)
            print(save_path + 'best_model_' + str(best_model_idx) + T_str +'.h5')
            model = load_model(save_path + 'best_model_' + str(best_model_idx) + T_str +'.h5')
            #model = load_model(save_path + 'model_0' + T_str +'.h5')
            models.append(model)

            if (use_binned_data):
                end_idx = get_end_idx(T-dt,dt)
            else:
                end_idx = get_end_idx(T,dt)

            print('End idx for this record: ',end_idx)
            # Use only data up to the given T.
            print("In hyptest: signal0 just before taking [:,:end_idx]: ",test_signal_Omega0_all.shape)
            psi_test0, test_signal_Omega0, times_test0, omegas_test0 = psi_test0_all[:,:end_idx], test_signal_Omega0_all[:,:end_idx], times_test0_all[:end_idx], omegas_test0_all[:,:end_idx]
            print("In hyptest: signal0 just after taking [:,:end_idx]: ",test_signal_Omega0.shape)
            psi_test1, test_signal_Omega1, times_test1, omegas_test1 = psi_test1_all[:,:end_idx], test_signal_Omega1_all[:,:end_idx], times_test1_all[:end_idx], omegas_test1_all[:,:end_idx]
            times = times_all[:end_idx]

            # Calculate NN predictions and accuracy.
            pred0 = calcPredictionNN(test_signal_Omega0, str(hypotheses[0]), model, T_str, save)
            actual0 = omegas_test0[:,0]
            pred1 = calcPredictionNN(test_signal_Omega1, str(hypotheses[1]), model, T_str, save)
            actual1 = omegas_test1[:,0]
            acc0 = calcAccuracy(pred0, actual0, "nn_acc" + str(hypotheses[0]) + T_str, save)
            acc1 = calcAccuracy(pred1, actual1, "nn_acc" + str(hypotheses[1]) + T_str, save)
            print('NN accuracy hyp0: ', acc0)
            print('NN accuracy hyp1: ', acc1)

            N_data_plot = 50
            #bayesSignal0 = bayesSignal0_all[:N_data_plot]
            #bayesSignal1 = bayesSignal1_all[:N_data_plot]

            #if (RECALCULATE_BAYES):
            #    bayesSignal0 = bayesSignal0_all[:,:end_idx]
            #    bayesSignal1 = bayesSignal1_all[:,:end_idx]
            #psi_test0 = read_data_psi(single_omega_data_path + 'test_Omega0' + T_str + '.h5', np.arange(N_data_plot), np.arange(end_idx))
            #psi_test1 = read_data_psi(single_omega_data_path + 'test_Omega1' + T_str + '.h5', np.arange(N_data_plot), np.arange(end_idx))
            averageAndPlotInput(psi_test0, times_test0, test_signal_Omega0[:N_data_plot], times, 'nn' + str(hypotheses[0]))
            averageAndPlotInput(psi_test1, times_test1, test_signal_Omega1[:N_data_plot], times, 'nn' + str(hypotheses[1]))

            # Calculate Bayes predictions
            #pred_bayes0, bayesSignal0 = calcPredictionBayes(psi_test0, test_signal_Omega0, str(hypotheses[0]), times_test0, T, use_binned_data, binningFactor, save)
            #pred_bayes1, bayesSignal1 = calcPredictionBayes(psi_test1, test_signal_Omega1, str(hypotheses[1]), times_test1, T, use_binned_data, binningFactor, save)

            # Global variable RECALCULATE_BAYES is used to determine whether or not to recalculate 
            # Bayes on the trajectories when load_accs=False
            # This allows retraining and testing the network on the same data without also running Bayes again
            # If Bayes have not been run on a trajectory or new trajectories are simulated, RECALCULATE_BAYES
            # should be set to True
            if (RECALCULATE_BAYES):
                pred_bayes0, bayesSignal0 = calcPredictionBayes(psi_test0, test_signal_Omega0, str(hypotheses[0]), times_test0, Delta, gamma, Phi, use_binned_data, binningFactor, save, T_str, bayesProb0, bayesSignal0_all)
                pred_bayes1, bayesSignal1 = calcPredictionBayes(psi_test1, test_signal_Omega1, str(hypotheses[1]), times_test1, Delta, gamma, Phi, use_binned_data, binningFactor, save, T_str, bayesProb1, bayesSignal1_all)
            else:
                pred_bayes0 = np.load(save_path + 'pred_bayes' + str(hypotheses[0]) + T_str + '.npy')
                print('Loaded Bayes predictions from disk: ',save_path + 'pred_bayes' + str(hypotheses[0]) + T_str + '.npy')

                pred_bayes1 = np.load(save_path + 'pred_bayes' + str(hypotheses[1]) + T_str + '.npy')
                print('Loaded Bayes predictions from disk: ',save_path + 'pred_bayes' + str(hypotheses[1]) + T_str + '.npy')


            if (RECALCULATE_BAYES):
                averageAndPlotInput(psi_test0, times_test0, bayesSignal0[:N_data_plot], times_test0, 'bayes' + str(hypotheses[0]))
                averageAndPlotInput(psi_test1, times_test1, bayesSignal1[:N_data_plot], times_test1, 'bayes' + str(hypotheses[1]))

            plotConfusionMatrix(np.concatenate((pred0,pred1)), np.concatenate((actual0,actual1)),'CM_NN' + T_str)
            plotConfusionMatrix(np.concatenate((pred_bayes0,pred_bayes1)), np.concatenate((actual0,actual1)),'CM_bayes' + T_str)

            # Load predictions from file
            #pred_bayes0 = np.load(save_path + 'pred_bayes' + str(hypotheses[0]) + T_str + '.npy')
            #pred_bayes1 = np.load(save_path + 'pred_bayes' + str(hypotheses[1]) + T_str + '.npy')

            # Calculate Bayes accuracy
            bayes_acc0 = calcAccuracy(pred_bayes0, actual0, "bayes_acc" + str(hypotheses[0]) + T_str, save)
            bayes_acc1 = calcAccuracy(pred_bayes1, actual1, "bayes_acc" + str(hypotheses[1]) + T_str, save)

            print("Bayes accuracy hyp0: ", bayes_acc0)
            print("Bayes accuracy hyp1: ", bayes_acc1)

            # Calculate best model performance on T=9, only activate this if best model for T=9 has been saved
            #if T==9:
            #    model = load_model("C:/Users/Henneberg/speciale_local/testing/homodyne/dt_0_001/hyp2class/best_model_0_T9.h5")
            #    pred0_T9 = calcPredictionNN(test_signal_Omega0, str(hypotheses[0]) + '_bestT9', model, T_str, save)
            #    pred1_T9 = calcPredictionNN(test_signal_Omega1, str(hypotheses[1]) + '_bestT9', model, T_str, save)
            #    acc0_T9 = calcAccuracy(pred0_T9, omegas_test0, 'nn_acc' + str(hypotheses[0]) + '_bestT9', save)
            #    acc1_T9 = calcAccuracy(pred1_T9, omegas_test1, 'nn_acc' + str(hypotheses[1]) + '_bestT9', save)
        else:
            # Load predictions from file
            pred0 = np.load(save_path + 'pred_NN' + str(hypotheses[0]) + T_str + '.npy')
            pred1 = np.load(save_path + 'pred_NN' + str(hypotheses[1]) + T_str + '.npy')
            print('Bayes prediction0 loaded from path: \n',save_path + 'pred_bayes' + str(hypotheses[0]) + T_str + '.npy')
            print('Bayes prediction1 loaded from path: \n',save_path + 'pred_bayes' + str(hypotheses[1]) + T_str + '.npy')
            pred_bayes0 = np.load(save_path + 'pred_bayes' + str(hypotheses[0]) + T_str + '.npy')
            pred_bayes1 = np.load(save_path + 'pred_bayes' + str(hypotheses[1]) + T_str + '.npy')
            #pred_bayes0 = np.load(save_path + 'pred_bayes' + str(hypotheses[0]) + T_ToFileNameString(times_+ '.npy')
            #pred_bayes1 = np.load(save_path + 'pred_bayes' + str(hypotheses[1]) + T_str + '.npy')

            #pred0_T9 = np.load(save_path + 'pred_NN' + str(hypotheses[0]) + '_bestT9_T9' + '.npy')
            #pred1_T9 = np.load(save_path + 'pred_NN' + str(hypotheses[1]) + '_bestT9_T9' + '.npy')

            plotConfusionMatrix(np.concatenate((pred0,pred1)), np.concatenate((hypotheses[0]*np.ones(pred0.shape),hypotheses[1]*np.ones(pred1.shape))),'CM_NN' + T_str)
            plotConfusionMatrix(np.concatenate((pred_bayes0,pred_bayes1)),np.concatenate((hypotheses[0]*np.ones(pred0.shape),hypotheses[1]*np.ones(pred1.shape))), 'CM_bayes' + T_str)

            # Load accuracies from file
            acc0 = np.load(save_path + 'nn_acc' + str(hypotheses[0]) + T_str + '.npy')
            acc1 = np.load(save_path + 'nn_acc' + str(hypotheses[1]) + T_str + '.npy')
            bayes_acc0 = np.load(save_path + 'bayes_acc' + str(hypotheses[0]) + T_str + '.npy')
            bayes_acc1 = np.load(save_path + 'bayes_acc' + str(hypotheses[1]) + T_str + '.npy')

            # Activate below to load accuracies by model specific to T9 data
            #acc0_T9 = np.load(save_path + 'nn_acc' + str(hypotheses[0]) + '_bestT9.npy')
            #acc1_T9 = np.load(save_path + 'nn_acc' + str(hypotheses[1]) + '_bestT9.npy')


        acc0_lst.append(acc0)
        acc1_lst.append(acc1)
        bayes_acc0_lst.append(bayes_acc0)
        bayes_acc1_lst.append(bayes_acc1)

    # Turn lists into numpy arrays.
    acc0 = np.array(acc0_lst)
    acc1 = np.array(acc1_lst)
    bayes_acc0 = np.array(bayes_acc0_lst)
    bayes_acc1 = np.array(bayes_acc1_lst)

    # Calculate and plot probability of error.
    Qe_nn = calc_Qe_from_accs(acc0, acc1)
    Qe_bayes = calc_Qe_from_accs(bayes_acc0, bayes_acc1)
    plot_Qe_vs_T(Qe_nn, Qe_bayes, Ts)

    # Only activate this for plot of best model performance on T=9 data
    #Qe_nn_T9 = calc_Qe_from_accs(acc0_T9, acc1_T9)
    #plot_Qe_vs_T(Qe_nn, Qe_bayes, Ts, [(9,Qe_nn_T9)])

    return acc0, acc1, bayes_acc0, bayes_acc1

def test_bestModelT9(Ts, save):
    T = 9
    max_T_str = T_ToFileNameString(Ts[-1])
    T_str = T_ToFileNameString(T)
    test_signal_Omega0, omegas_test0 = read_data_signalsAndFrequencies(single_omega_data_path + 'test_Omega' + str(hypotheses[0]) + max_T_str + '.h5', np.arange(0), np.arange(get_end_idx(T,dt)))
    test_signal_Omega1, omegas_test1 = read_data_signalsAndFrequencies(single_omega_data_path + 'test_Omega' + str(hypotheses[1]) + max_T_str + '.h5', np.arange(0), np.arange(get_end_idx(T,dt)))
    model = load_model(save_path + "best_model_0_T9_bestT9.h5")
    pred0_T9 = calcPredictionNN(test_signal_Omega0, str(hypotheses[0]) + '_bestT9', model, T_str, save)
    pred1_T9 = calcPredictionNN(test_signal_Omega1, str(hypotheses[1]) + '_bestT9', model, T_str, save)
    acc0_T9 = calcAccuracy(pred0_T9, omegas_test0[:,0], 'nn_acc' + str(hypotheses[0]) + '_bestT9', save)
    acc1_T9 = calcAccuracy(pred1_T9, omegas_test1[:,0], 'nn_acc' + str(hypotheses[1]) + '_bestT9', save)
    print(acc0_T9)
    print(acc1_T9)
    return pred0_T9, pred1_T9, acc0_T9, acc1_T9


def main(batch_size, num_epochs, use_dropout, use_kfold, Ts):
    print(save_path)
    models, scores, histories = list(), list(), list()
    for T in Ts:
        T_str = T_ToFileNameString(T)
        model, score, history = hypothesisTest(batch_size, num_epochs, use_dropout, use_kfold, T)
        models.append(model)
        scores.append(score)
        histories.append(history)
        np.save(save_path+'/hypTestingScores' + T_str, score, allow_pickle=False)
        # Reset tensorflow graph and create new one
        tf.keras.backend.clear_session()
    models = np.array(models)
    scores = np.array(scores)
    histories = np.array(histories)

def hypothesisTestBinned(batch_size, num_epochs, use_dropout, use_kfold, Ts, binningFactors):
    global save_path
    temp_save_path = save_path
    #models, scores, histories = list(), list(), list()
    for binningFactor in binningFactors:
        # Binning
        if not ADD_NOISE_INSTEAD_OF_BINNING:
            save_path += "binned_" + str(binningFactor) + "/"
        # Noisy
        else:
            save_path += "noisy_" + str(binningFactor) + "/"
        # Create directories on save path if they do not exist.
        Path(save_path).mkdir(parents=True, exist_ok=True)
        for T in Ts:
            T_str = T_ToFileNameString(T)
            model, score, history = hypothesisTest(batch_size, num_epochs, use_dropout, use_kfold, T, True, binningFactor)
            # Reset tensorflow graph and create new one
            tf.keras.backend.clear_session()
            np.save(save_path+'/hypTestingScores' + T_str,score, allow_pickle=False)
        save_path = temp_save_path

def checkDuplicatesTrainValidTest(T):
    """
    Checks training, validation and test sets for duplicates.

    Uses configurations specified in the top of this file.
    
    Params:
        T: Trajectory length.
    """
    T_str = T_ToFileNameString(T)

    filename_suffix = T_str + '.h5'

    training_data_path = data_path + 'training' + filename_suffix
    valid_data_path = data_path + 'valid' + filename_suffix
    test_data_path = data_path + 'test' + filename_suffix

    # clean data - load all
    dN_train,omegas_train = read_data_signalsAndFrequencies(training_data_path)
    dN_valid,omegas_valid = read_data_signalsAndFrequencies(valid_data_path)
    dN_test,omegas_test = read_data_signalsAndFrequencies(test_data_path)

    # For checking for duplicates - inserting fake duplicates
    #dN_train[94] = dN_train[3]
    #dN_test[2345] = dN_train[43]
    #dN_valid[1932] = dN_train[87]

    # check for duplicates
    checkDuplicates(dN_train[4096:8192], dN_valid, dN_test)

def hypothesisTest(batch_size, num_epochs, use_dropout, use_kfold, T, use_binned_data=False, binningFactor=1):
    """
    Runs hypothesis testing with specified arguments.

    Params:
        batch_size:     Batch size used by mini-batch optimization algorithm.
        num_epochs:     Number of epochs to run optimization for. One epoch means 
                        the whole data set has been used.
        use_dropout:    Boolean specifying if the model should use dropout or not.
        use_kfold:      Boolean specifying if the kfold cross validation should be used.
        T:              Trajectory length that should be tested.
        use_binned_data: Boolean specifying if binned data should be used.
        binningFactor:  The binned signal will be divided in size by this factor.

    Returns:
        members:        A list of the trained models. Only contains one model if use_kfold
                        is false.
        scores:         A list of the scores achieved by the models on the test data.
        histories:      A list of the training histories.
    """

    t0 = time.time()
    hidden_size = 0
    T_str = T_ToFileNameString(T)

    # New method. Just using the same trajectories for different T by only loading parts of
    # long trajectories.
    end_idx = get_end_idx(T,dt)
    
    # Drop last time step for binned data.
    if (use_binned_data):
        end_idx = get_end_idx(T-dt,dt)
        print('end_idx for loading data to be binned: ',end_idx)
    max_T_str = T_ToFileNameString(15)
    filename_suffix = max_T_str + '.h5'

    # Old method. Using different trajectories for each new T.
    #filename_suffix = T_str + '.h5'

    training_data_path = data_path + 'training' + filename_suffix
    valid_data_path = data_path + 'valid' + filename_suffix
    test_data_path = data_path + 'test' + filename_suffix

    times = read_data_times(training_data_path,np.arange(end_idx))
    print("times shape before binning: ",times.shape)
    print(times[0])
    print(times[-1])

    # Clean data
    N_data_load = 0 # Number of trajectories to load - 0 means all
    dN_train, omegas_train = read_data_signalsAndFrequencies(training_data_path,np.arange(N_data_load), np.arange(end_idx))
    dN_valid, omegas_valid = read_data_signalsAndFrequencies(valid_data_path,np.arange(N_data_load), np.arange(end_idx))
    dN_test, omegas_test = read_data_signalsAndFrequencies(test_data_path,np.arange(N_data_load), np.arange(end_idx))

    if (use_binned_data):
        times_temp = times
        dN_train, times = binSignal(dN_train, times_temp, binningFactor)
        print("times_temp after first binning(should be same shape as times before binning): ",times_temp.shape)
        dN_valid, _ = binSignal(dN_valid, times_temp, binningFactor)
        dN_test, _ = binSignal(dN_test, times_temp, binningFactor)
        print("times shape after binning: ",times.shape)

    # old method, reading ALL data
    #psi_train,dN_train,times_train,omegas_train = read_data(training_data_path,np.arange(N_data_plot))
    #psi_valid,dN_valid,times_valid,omegas_valid = read_data(valid_data_path, np.arange(N_data_plot))
    #psi_test,dN_test,times_test,omegas_test = read_data(test_data_path, np.arange(N_data_plot))

    #print("First test trajectory has no duplicates in training data as expected." if not (np.any(np.equal(dN_test[0,:],dN_train))) else "WARNING: Duplicates of test trajectory 0 was found in training data.")
    #print("First test trajectory has no duplicates in validation data as expected." if not (np.any(np.equal(dN_test[0,:],dN_valid))) else "WARNING: Duplicates of test trajectory 0 was found in validation data.")


    # test shuffling labels on training data to see generalization error falls apart
    # but maybe training acc is still high because of complexity of NN
    #np.random.shuffle(omegas_train[:,0])

    # noisy data - random delays
    #noisy_data_path = data_path + 'noisy/'
    #psi_train,dN_train,times_train,omegas_train = read_data(noisy_data_path + 'training' + filename_suffix)
    #psi_valid,dN_valid,times_valid,omegas_valid = read_data(noisy_data_path + 'valid' + filename_suffix)
    #psi_test,dN_test,times_test,omegas_test = read_data(noisy_data_path + 'test' + filename_suffix)

    N_trajs_train = dN_train.shape[0]
    nt = len(times)
    timesteps = nt
    if (T != times[-1] and not use_binned_data):
        print(T)
        print(times[-1])
        print("T not equal to end of times array. Exiting...")
        exit()
    #timesteps = 1000
    skip_step = timesteps

    t1 = time.time()
    tDataLoad = t1-t0

    # Turned this off to allow for training on bigger data sets where psi is not saved
    PLOT_SAMPLES = False
    if (PLOT_SAMPLES):
        N_data_plot = 50 # Number of trajectories to load for plotting
        #idx_hypotheses = np.zeros((dN_train.shape[0],hypotheses.size),dtype='bool')
        idx_hypotheses = np.zeros((N_data_plot,hypotheses.size),dtype='bool')

        psi_train = read_data_psi(training_data_path,np.arange(N_data_plot), np.arange(end_idx))
        times_train = read_data_times(training_data_path,np.arange(end_idx))

        # Create matrix with booleans where rows are trajectories and columns are frequencies
        # the (i,j)'th element thus is 1 if the i'th trajectory has frequency j
        for idx,hypothesis in enumerate(hypotheses):
            idx_hypotheses[:,idx] = omegas_train[:N_data_plot,0] == hypothesis

        for i in range(idx_hypotheses.shape[1]):
            if (psi_train[idx_hypotheses[:,i],:,:].shape[0] == 0):
                print('Hypothesis frequencies did not match any frequencies in data set. Exiting...')
                exit()
            averageAndPlotInput(psi_train[idx_hypotheses[:,i],:,:],
                    times_train, dN_train[:N_data_plot][idx_hypotheses[:,i],:],
                                times, i)
    
    t2 = time.time()
    tFirstPlots = t2-t1

    print("Time for execution of different units:")
    print("tDataLoad",tDataLoad)
    print("tFirstPlots",tFirstPlots)

    # encoding Rabi frequencies of system into one-hot representations
    omegasEncoded_train = np.empty((omegas_train.shape[0],nt))
    omegasEncoded_valid = np.empty((omegas_valid.shape[0],nt))
    omegasEncoded_test = np.empty((omegas_test.shape[0],nt))
    for omegas_i,omegasEncoded_i in zip([omegas_train, omegas_valid,
                         omegas_test],[omegasEncoded_train,
                                       omegasEncoded_valid,
                                       omegasEncoded_test]):
        omegas_i = omegas_i[:,0] # using first element while frequencies are constant
        for i in range(hypotheses.size):
            omega_idx = np.where(omegas_i == hypotheses[i])
            omegasEncoded_i[omega_idx] = i


    # choose model we want to use (NN architecture)
    modelStrategy = KerasModels.ParameterCNNModel(hidden_size, nt, timesteps,
                                                   use_dropout, hypotheses)

    # encode input as alternating between 0 and 1 at each click
    #batchGenerator = KerasBatchGenerators.SignalToConstantParameterFromHypotheses

    # take raw clicks as input using generator
    #batchGenerator = KerasBatchGenerators.SingleClickToConstantParameterFromHypotheses

    # take raw clicks as input using Keras Sequence
    batchSequence = KerasBatchSequence.SingleClickToConstantParameterFromHypotheses

    #train_data_generator = batchGenerator(dN_train, timesteps, batch_size,
    #        skip_step, hypotheses, omegasEncoded_train, modelStrategy, True)
    #valid_data_generator = batchGenerator(dN_valid, timesteps, batch_size,
    #        skip_step, hypotheses, omegasEncoded_valid, modelStrategy, False)


    # using optimizers from tf.keras.optimizers required for cuDNN LSTM
    my_optimizer = tf.keras.optimizers.Nadam()
    #my_optimizer = tf.keras.optimizers.SGD()

    #my_optimizer = optimizers.Nadam()

    # evaluation metric
    my_metric = "accuracy" # accuracy defaults to categorical_accuracy
    
    # callback to save model after every epoch
    checkpointer = ModelCheckpoint(filepath=save_path + '/param_esti_model-{epoch:02d}.h5', verbose=1)
    
    # callbacks for tensorboard
    logdir = Path(log_path) / datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    tensorboard_callback = TensorBoard(log_dir=os.fspath(logdir))

    # set minimum change in val_accuracy before early stopping
    MIN_DELTA = 0.001
    PATIENCE = 10

    class OnEpochEnd(tf.keras.callbacks.Callback):
        def __init__(self,dataGenerator,model):
            super().__init__()
            self.dataGenerator = dataGenerator
            self.model = model
            self.initial_weights = self.model.get_weights()

        # function written by 183amir in comment https://github.com/keras-team/keras/issues/341#issuecomment-539198392 on 07/10/2019 extended by AZippelius in comment https://github.com/keras-team/keras/issues/341#issuecomment-547833394 on 30/10/2019 - not guaranteed to work for all layer types. Should work for Dense, Conv2D, Conv1D, SimpleRNN, GRU, LSTM, ConvLSTM2D according to source.
        def reset_weights(self, model):
            for layer in model.layers:
                if isinstance(layer, tf.keras.Model): #if you're using a model as a layer
                    self.reset_weights(layer) #apply function recursively
                    continue

                #where are the initializers?
                if hasattr(layer, 'cell'):
                    init_container = layer.cell
                else:
                    init_container = layer

                for key, initializer in init_container.__dict__.items():
                    if "initializer" not in key: #is this item an initializer?
                        continue #if no, skip it

                    # find the corresponding variable, like the kernel or the bias
                    if key == 'recurrent_initializer': #special case check
                        var = getattr(init_container, 'recurrent_kernel')
                    else:
                        var = getattr(init_container, key.replace("_initializer", ""))

                    var.assign(initializer(var.shape, var.dtype))
                    #use the initializer

        def on_epoch_end(self, epoch, logs):
            self.dataGenerator.on_epoch_end()
            if (epoch > 1 and epoch % 2 == 0 and logs.get('accuracy',epoch) < 0.51):
                self.reset_weights(self.model)
                print('Weights reinitialized.')


    # Trains model for a given initial amount of epochs, evaluates it on test data and returns the
    # model and test_accuracy. For use in e.g. K-fold cross-validation.
    # train_data_generator is generating data from training data set, except a hold out part
    # valid_data_generator is generating data from hold out part of training data set
    # test_data_generator is generating data from validation data set
    def auto_fit_model(model, train_data_generator, valid_data_generator, test_data_generator, num_epochs, idx):
        # callback to implement early stopping and save the best model found during training
        earlyStopping = EarlyStopping(monitor='val_accuracy',mode='max', verbose=1, patience=PATIENCE, min_delta=MIN_DELTA, restore_best_weights=True)
        modelCheckpoint = ModelCheckpoint(filepath=save_path + 'best_model_' + str(idx) + T_str + '.h5', monitor='val_accuracy', mode='max', save_best_only=True, verbose=1)
        #modelCheckpoint = ModelCheckpoint(filepath=save_path + 'best_model_' + str(idx) + T_str +'.h5', monitor='val_accuracy', mode='max', save_best_only=True, verbose=1)
        # Callback for controlling what happens at the end of every epoch.
        onEpochEnd = OnEpochEnd(train_data_generator,model)
        #SGDR = sgdr.SGDRScheduler(1e-5,1e-2,train_data_generator.__len__(),0.9,5)

        print('Fitting of model started...')
        hist = model.fit(train_data_generator,
                epochs=num_epochs, validation_data = valid_data_generator,
                use_multiprocessing=False, shuffle=False,
                initial_epoch=0, callbacks=[earlyStopping, modelCheckpoint, onEpochEnd])#, SGDR])
                #callbacks=[checkpointer]) # checkpointer saves model after each epoch
                #callbacks=[checkpointer,tensorboard_callback])

        del model
        # Reset tensorflow graph and create new one
        tf.keras.backend.clear_session()
        print('Fitting of model finished.')
        model = load_model(save_path + 'best_model_' + str(idx) + T_str +'.h5')
        print('Evaluating model on test data (not the same as val_acc)...')
        _, test_acc = model.evaluate(test_data_generator,
                use_multiprocessing=False,
                callbacks=[]) 
        print('Evaluation of model completed.')
        return model, test_acc, hist

    # Trains model for a given initial amount of epochs and then asks if more training epochs should be
    # added. If not, evaluates model on test data and returns model and test_accuracy.
    # train_data_generator is generating data from training data set, except a hold out part
    # valid_data_generator is generating data from hold out part of training data set
    # test_data_generator is generating data from validation data set
    def fit_model(model, train_data_generator, valid_data_generator, test_data_generator, num_epochs):
        # callback to implement early stopping and save the best model found during training
        earlyStopping = EarlyStopping(monitor='val_accuracy',mode='max', verbose=1, patience=PATIENCE, min_delta=MIN_DELTA)
        # For standard saving of best models
        modelCheckpoint = ModelCheckpoint(filepath=save_path + 'best_model_0' + T_str +'.h5', monitor='val_accuracy', mode='max', save_best_only=True, verbose=1)
        # For T=9 best model training using ParameterCNNT9 model in KerasModels
        #modelCheckpoint = ModelCheckpoint(filepath=save_path + 'best_model_0' + T_str + '_bestT9' +'.h5', monitor='val_accuracy', mode='max', save_best_only=True, verbose=1)
        onEpochEnd = OnEpochEnd(train_data_generator, model)
        #SGDR = sgdr.SGDRScheduler(1e-5,1e-2,train_data_generator.__len__(),0.9,3)

        remaining_epochs = num_epochs
        total_epochs = 0

        print('Fitting of model started...')
        while (remaining_epochs>0):
            hist = model.fit(train_data_generator, epochs=total_epochs+remaining_epochs, steps_per_epoch=None, validation_data = valid_data_generator, use_multiprocessing=False, shuffle=False, initial_epoch=total_epochs, callbacks=[modelCheckpoint,onEpochEnd, earlyStopping])#, SGDR])#, workers=12) #callbacks=[checkpointer,tensorboard_callback])
            total_epochs += remaining_epochs
            remaining_epochs = 0
            try:
                #remaining_epochs = int(input("Enter number of epochs to run (default 0):\n"))
                remaining_epochs = 0
            except ValueError:
                print('Input was not an integer. Ending the program normally.')
            except:
                print('Something went wrong with integer conversion of input. Ending the program normally.')

        model = load_model(save_path + 'best_model_0' + T_str + '.h5')
        # For testing T9 optimized model
        #model = load_model(save_path + 'best_model_0' + T_str + '_bestT9' +'.h5')

        print('Fitting of model finished.')
        print('Evaluating model on test data...')
        _, test_acc = model.evaluate(test_data_generator,
                use_multiprocessing=False,
                callbacks=[])
        print('Evaluation of model completed.')
        print("\nTotal amount of epochs run: ", total_epochs)
        return model, test_acc, hist

    # K-fold cross-validation
    def do_KFold_CV(n_folds):
        #kfold = KFold(n_folds, True)
        kfold = KFold(n_folds, True, kfold_seed)

        # not using test set atm - splitting training data into training and validation data using kfold and using the c++ simulated validation data as the test set
        test_data_generator = batchSequence(dN_valid, omegasEncoded_valid, hypotheses, batch_size, timesteps, skip_step, modelStrategy, False)
        scores, members, histories = list(), list(), list()
        for idx, (train_idx, test_idx) in enumerate(kfold.split(dN_train)):
            if 'model' in locals():
                del model
            model = modelStrategy.defineModel()
            model.compile(loss='categorical_crossentropy',
                    optimizer=my_optimizer, metrics=[my_metric])
            train_data_generator = batchSequence(dN_train[train_idx,:,:], omegasEncoded_train[train_idx,:], hypotheses,batch_size, timesteps, skip_step, modelStrategy, True)
            valid_data_generator = batchSequence(dN_train[test_idx,:,:], omegasEncoded_train[test_idx,:], hypotheses, batch_size, timesteps, skip_step, modelStrategy, False)
            model, test_acc, hist = auto_fit_model(model, train_data_generator, valid_data_generator, test_data_generator, num_epochs, idx)
            scores.append(test_acc)
            members.append(model)
            histories.append(hist)

            # Reset tensorflow graph and create new one
            tf.keras.backend.clear_session()

        for idx,(history,model) in enumerate(zip(histories,members)):
            with open(save_path + 'history_' + str(idx) + T_str, 'wb') as pickle_file:
                pickle.dump(history.history, pickle_file)
            #model.save(save_path + 'model_' + str(idx) + T_str + '.h5')
        print('Estimated accuracy: %.3f (%.3f)' % (np.mean(scores), np.std(scores)))
        print('Estimated accuracy is based on the validation data set whereas the validation loss is calculated from a hold-out part of the training data. Thus estimated accuracy is not expected to be equal to val_loss')

        # generates prediction of ensemble of models on test data set
        # doesnt currently use the dataGenerator and instead accesses data directly through dN_test and omegas_test
        def getEnsemblePrediction(members, dataGenerator):
            #predictions = [model.predict(dataGenerator) for model in members]
            predictions = np.empty((dN_test.shape[0], hypotheses.size, len(members)))
            #dataGenerator.reset()
            for idx,model in enumerate(members):
            #    pred = model.predict(dataGenerator)# predict on data
                pred = model.predict(dN_test)
                predictions[:,:,idx] = pred
            actual_values = omegas_test[:,0]

            # return mean of predicted probability by every model in members
            # converts one-hot rep. to frequency
            mean_predictions = hypotheses[np.argmax(np.mean(predictions,axis=2),axis=1)]

            return mean_predictions,actual_values

        def evaluate_n_members(members, n_members, dataGenerator):
            subset = members[:n_members]
            mean_predictions, actual_values = getEnsemblePrediction(subset, dataGenerator)
            acc = sklearn.metrics.accuracy_score(mean_predictions, actual_values)
            return acc

        ### Evaluating models on test data set
        test_data_generator = batchSequence(dN_test, omegasEncoded_test, hypotheses, batch_size, timesteps, skip_step, modelStrategy, False)
        single_scores, ensemble_scores, result_list = list(), list(), list()
        for i in range(1, n_folds+1):
            ensemble_score = evaluate_n_members(members, i, test_data_generator)
            #test_data_generator.reset()
            #_, single_score = members[i-1].evaluate(test_data_generator, verbose=0)
            _, single_score = members[i-1].evaluate(dN_test, to_categorical(omegasEncoded_test[:,0]), verbose=0)
            result_list.append('> %d: single=%.3f, ensemble=%.3f' % (i, single_score, ensemble_score))
            ensemble_scores.append(ensemble_score)
            single_scores.append(single_score)
        for result in result_list:
            print(result)
        print('Accuracy %.3f (%.3f)' % (np.mean(single_scores), np.std(single_scores)))
        print('Accuracy was obtained using the test data set. So are the single scores and the ensemble scores.')

        ### Plotting result of evaluation
        xs = [i for i in range(1, n_folds+1)]
        fig = plt.figure()
        plt.plot(xs, single_scores, marker='o', linestyle='None',label='Single')
        plt.plot(xs, ensemble_scores, marker='o', label='Ensemble predictions')
        fig.savefig(save_path + 'ensemble_vs_single' + T_str + '.pdf')
        plt.close(fig)

        scores = np.array([single_scores, ensemble_scores])
        return members, scores, histories

    def getSingleModel():
        # not using test set atm - splitting training data into training and validation data using kfold and using the c++ simulated validation data as the test set
        test_data_generator = batchSequence(dN_valid, omegasEncoded_valid, hypotheses, batch_size, timesteps, skip_step, modelStrategy, False)
        scores, members, histories = list(), list(), list()
        model = modelStrategy.defineModel()
        model.compile(loss='categorical_crossentropy',
                optimizer=my_optimizer, metrics=[my_metric])
        alpha = 9/10
        idx_fold = int(alpha*N_trajs_train) # takes a fraction 1-alpha of the training data as validation data
        train_data_generator = batchSequence(dN_train[:idx_fold,:,:], omegasEncoded_train[:idx_fold,:], hypotheses, batch_size, timesteps, skip_step, modelStrategy, True)
        valid_data_generator = batchSequence(dN_train[idx_fold:,:,:], omegasEncoded_train[idx_fold:,:], hypotheses, batch_size, timesteps, skip_step, modelStrategy, False)
        model, test_acc, hist = fit_model(model, train_data_generator, valid_data_generator,
                test_data_generator, num_epochs)

        # for loading the best model
        #model = load_model(save_path + 'temp_best_model.h5')
        #_, test_acc = model.evaluate(test_data_generator,
        #        use_multiprocessing=False,
        #        callbacks=[])
        #hist = 0 # there is no history if we just load the best model

        #save_path + 'history' + T_str + '.npy', np.array(hist.history))
        with open(save_path + 'history' + T_str, 'wb') as pickle_file:
            pickle.dump(hist.history, pickle_file)
        members.append(model)
        histories.append(hist)

        print('Estimated accuracy: %.3f ' % (test_acc))
        print('Estimated accuracy is based on the validation data set whereas the validation loss is calculated from a hold-out part of the training data. Thus estimated accuracy is not expected to be equal to val_loss')

        ### Evaluating models on test data set
        test_data_generator = batchSequence(dN_test, omegasEncoded_test, hypotheses, batch_size, timesteps, skip_step, modelStrategy, False)
        scores = list()
        _, test_acc = model.evaluate(dN_test, to_categorical(omegasEncoded_test[:,0]), verbose=0)

        scores.append(test_acc)

        print('Accuracy %.3f' % (test_acc))
        print('Accuracy was obtained using the test data set.')


        return members, scores, histories

    n_folds = 10
    if use_kfold:
        members, scores, histories = do_KFold_CV(n_folds)
    else:
        members, scores, histories = getSingleModel()

    fig,axes = plt.subplots(len(histories))
    if (len(histories) == 1):
        axes.plot(histories[0].history['accuracy'], label=r'$ \mathcal{D}_{\mathrm{train}}$')
        axes.plot(histories[0].history['val_accuracy'], label=r'$ \mathcal{D}_{\mathrm{valid}}$')
        enchantAx(axes)
        axes.set(xlabel='Epoch', ylabel='Accuracy')
        #axes.set_ylim(0,5)
        axes.legend()
    else:
        for idx,hist in enumerate(histories):
            axes[idx].plot(hist.history['accuracy'], label=r'$ \mathcal{D}_{\mathrm{train}}$')
            axes[idx].plot(hist.history['val_accuracy'], label=r'$ \mathcal{D}_{\mathrm{valid}}$')
            axes[idx].set(ylabel='Accuracy')
            #axes[idx].set_ylim(0,5)
            axes[idx].legend()

        axes[axes.shape[0]-1].set(xlabel='Epoch')
    fig.tight_layout()
    fig.savefig(save_path + '/training_history' + T_str + '.pdf')
    plt.close(fig)

    def get_test_predictions(num_batches_predict, data_generator, members, dummy_iters = 0):
        """
        Returns array of predictions.

        """
        dummy_iters = 0 # controls how many batches to skip
        num_predict = num_batches_predict # how many batches of test trajectories to do the prediction for

        mean_pred = np.zeros((num_predict,))
        pred = np.empty((num_predict,len(hypotheses),len(members)))

        print('Predicting on test data started...')
        actual_values = np.empty(num_predict)
        for i in range(num_predict):
            data = data_generator.__getitem__(i+dummy_iters)
            actual_values[i] = hypotheses[np.argmax(data[1])]
            for idx,model in enumerate(members):
                pred[i,:,idx] = model.predict(data[0])

        #return mean of predicted probability by every model in members
        pred_arr = np.mean(np.array(pred),axis=2)
        # converts one-hot rep. to frequency
        pred_arr = hypotheses[np.argmax(pred_arr,axis=1)]
        return pred_arr, actual_values

    # data generator on test set with batch size 1, so we have fine control over how many trajectories to use for prediction
    test_data_generator = batchSequence(dN_test, omegasEncoded_test, hypotheses, 1, timesteps, skip_step, modelStrategy, False)

    # having initialized the data generator with 1 batch size, num_predict will equal number of trajectories to do the prediction for
    #num_predict = dN_test.shape[0]
    num_predict = 100
    pred_arr, actual_values = get_test_predictions(num_predict, test_data_generator, members)
    comparePredictedToTest = np.array((actual_values,pred_arr)).T
    print('Predicting on test data finished.')
    print('actual frequencies:, predicted frequencies:\n',comparePredictedToTest)
    pred_acc = np.mean(np.equal(comparePredictedToTest[:,0],comparePredictedToTest[:,1]))
    print('Accuracy in shown predictions: ',pred_acc)
    return members, scores, histories

def averageAndPlotInput(psi, times_psi, dN, times_dN, dataIndex):
    T_str = T_ToFileNameString(times_dN[-1])
    # plot first four individual trajectories
    for i in range(2):
        fig,axs = plt.subplots(2,1)
        axs[0] = plotPopulations(axs[0],times_psi,(np.abs(psi[i,:,:])**2).T)
        axs[1] = plotClicks(axs[1],times_dN,dN[i,:], USE_HOMODYNE)
        fig.tight_layout()
        plt.savefig(save_path + "data_traj" + T_str + "_" + str(dataIndex) + "_" + str(i) + ".pdf",dpi=100)
        plt.close(fig)

    #### Fourier transform of signal
    #traj_idx = 0
    #fig, axes = plt.subplots(2)
    #T = times[-1]
    #N = dN[traj_idx,:].size
    #print("N: ", N)
    #fft_times = 2*np.pi*np.linspace(-0.5*N/T,0.5*N/T,N)
    #fft_dN = 2.0/N*np.fft.fftshift(np.fft.fft(dN[traj_idx,:]))
    #axes[0].plot(times, dN[traj_idx,:])
    #axes[1].plot(fft_times, np.abs(fft_dN))
    #fig.savefig(save_path + 'spectrum' + T_str + '_' + str(dataIndex) + '_traj0.pdf')
    #plt.close(fig)

    #fig, axes = plt.subplots(2)
    #result = 0
    #L = N-1
    #C = np.empty((dN.shape[0],L))
    #for l in range(1,L):
    #    t_l = l
    #    print((1.0/(N-l)*np.sum([np.multiply(dN[:,t_i],dN[:,t_i+t_l]) for t_i in range(1,N-l)],axis=0)).shape)
    #    print((1.0/(N-l)*np.sum([np.multiply(dN[:,t_i],dN[:,t_i+t_l]) for t_i in range(1,N-l)],axis=0)).reshape(dN.shape[0]).shape)
    #    print("l:", l)

    #    C[:,l] = (1.0/(N-l)*np.sum([np.multiply(dN[:,t_i],dN[:,t_i+t_l]) for t_i in range(1,N-l)],axis=0)).reshape(dN.shape[0])
    #C_avg = np.mean(C,axis=0)
    #print(C_avg)
    #ls = np.arange(0,L)
    #axes[0].plot(ls,C_avg)
    #plt.show()



    psi_squared_avg = np.mean(np.abs(psi)**2,axis=0).T
    dN_avg = np.mean(dN,axis=0)

    fig,axs = plt.subplots(2,1)
    axs[0] = plotPopulations(axs[0],times_psi,psi_squared_avg)
    axs[1] = plotClicks(axs[1],times_dN,dN_avg,USE_HOMODYNE)
    fig.tight_layout()
    plt.savefig(save_path + "data" + T_str + "_" + str(dataIndex) + ".pdf")
    plt.close(fig)
    # For plotting 000111 transformed signal
    if not (USE_HOMODYNE):
        input_signal = np.zeros(dN.shape)
        cumsum = np.cumsum(dN,axis=1)
        np.mod(cumsum,2,input_signal)
        input_signal = np.mean(input_signal,axis=0)
        fig,axs = plt.subplots(2,1)
        axs[0] = plotClicks(axs[0],times_dN,dN_avg,USE_HOMODYNE)
        axs[1].plot(times_dN, input_signal)
        fig.tight_layout()
        plt.savefig(save_path + "clicksAndSignal" + T_str + str(dataIndex) + ".pdf")
        plt.close(fig)


if __name__=='__main__':
    # For testing the best model on T=9 data
    Ts = [0.25, 0.5, 1, 2, 3, 5, 7, 9, 12, 15]
    save = True
    test_bestModelT9(Ts, save)
    plot_saved_results_Ts(Ts)

