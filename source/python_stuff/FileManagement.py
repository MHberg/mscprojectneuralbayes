import sys
import h5py
import numpy as np
import time
import math
from scipy.stats import cauchy
import matplotlib.pyplot as plt
from plottingTools import enchantAx

import multiprocessing
from joblib import Parallel, delayed
NUM_LOGICAL_CORES = multiprocessing.cpu_count()

# system path for loading/saving stuff
data_path = "../cpp_stuff/build/data/"
if (sys.platform.startswith('win')):
    save_path = "C:/Users/Henneberg/speciale_local/"
    log_path = "C:/Users/Henneberg/speciale_local/logs/fit/"
elif (sys.platform.startswith('linux')):
    save_path = "./"
    log_path = "./logs/fit/"


# read data from c++ simulations into python using the h5py module
# parameters:
#   filename: string containing path to file to read
#   traj_idx: trajectory indices to read - if empty, read all
# returns:
#   wave function, detector measurements, times, rabi frequencies
def read_data_signalsAndFrequencies(filename, traj_idx = [], T_idx = []):
    # Temporarily putting T_idx = [] no matter what for omega since we 
    # only deal with constant frequency trajectories
    f = h5py.File(filename, 'r')
    if (len(traj_idx) == 0 and len(T_idx) == 0):
        dN = f['clicks']
        frequencies = f['frequencies']
    elif (len(T_idx) == 0):
        dN = f['clicks'][:,traj_idx]
        frequencies = f['frequencies'][:,traj_idx]
    elif (len(traj_idx) == 0):
        dN = f['clicks'][T_idx,:]
        frequencies = f['frequencies'][:,:]
    else:
        dN = f['clicks'][T_idx,traj_idx]
        #frequencies = f['frequencies'][T_idx,traj_idx]
        frequencies = f['frequencies'][:,traj_idx]

    # Transposing to get shapes (traj_idx, timesteps) for
    # frequencies and dN
    # Seems like creating an enclosing numpy array and accessing the first (0th) element
    # improves speed of checking duplicates compared to just using the raw
    # arrays - not sure why.
    dN = np.array([dN[...].T])[0]
    frequencies = np.array([frequencies[...].T])[0]

    # 3d input - last dimension is feature number
    dN = np.expand_dims(dN, 2) # last dimension indicates we only have one feature i.e. whether a photon was detected or not - binary signal - this dimension is needed by the LSTM layer which expects 3 dimensions - (batch size, time steps, number of features)
    print('dn shape: ',dN.shape)
    print('frequencies shape: ',frequencies.shape)
    return dN,frequencies

# read data from c++ simulations into python using the h5py module
# parameters:
#   filename: string containing path to file to read
#   traj_idx: trajectory indices to read - if empty, read all
# returns:
#   times
def read_data_times(filename, T_idx = []):
    f = h5py.File(filename, 'r')
    if (len(T_idx) == 0):
        times = f['times'][0]
    else:
        times = f['times'][0,T_idx]
    # Transposing to get shape (timesteps, 1) for times.
    # Seems like creating an enclosing numpy array and accessing the first (0th) element
    # improves speed of checking duplicates compared to just using the raw
    # arrays - not sure why.
    times = np.array([times[...].T])[0]
    print('times shape: ',times.shape)
    return times

def read_data_hypotheses(filename):
    f = h5py.File(filename, 'r')
    hypotheses = np.array(f['hypotheses'])
    return hypotheses

# read data from c++ simulations into python using the h5py module
# parameters:
#   filename: string containing path to file to read
#   traj_idx: trajectory indices to read - if empty, read all
# returns:
#   psi
def read_data_psi(filename, traj_idx = [], T_idx = []):
    f = h5py.File(filename, 'r')
    if (len(traj_idx) == 0 and len(T_idx) == 0):
        dset = f['trajectories']
        psi = np.empty(dset.shape, dtype="complex_")
        psi.real = dset['real']; psi.imag = dset['imag']
    elif (len(T_idx) == 0):
        dset = f['trajectories'][traj_idx]
        psi = np.empty(dset.shape, dtype="complex_")
        psi.real = dset['real']; psi.imag = dset['imag']
    elif (len(traj_idx) == 0):
        dset = f['trajectories'][:,T_idx]
        psi = np.empty(dset.shape, dtype="complex_")
        psi.real = dset['real']; psi.imag = dset['imag']
    else:
        dset = f['trajectories'][traj_idx]
        print(dset.shape)
        dset = dset[:,T_idx]
        psi = np.empty(dset.shape, dtype="complex_")
        psi.real = dset['real']; psi.imag = dset['imag']
    print('psi shape: ', psi.shape)
    return psi

# read data from c++ simulations into python using the h5py module
# parameters:
#   filename: string containing path to file to read
#   traj_idx: trajectory indices to read - if empty, read all
# returns:
#   wave function, detector measurements, times, rabi frequencies
def read_data(filename, traj_idx = [], T_idx = []):
    psi = read_data_psi(filename,traj_idx, T_idx)
    dN,frequencies = read_data_signalsAndFrequencies(filename,traj_idx, T_idx)
    times = read_data_times(filename, T_idx)
    return psi,dN,times,frequencies

def checkDuplicates(arr1, arr2, arr3):
    """
    Method for checking if arrays contains in-set or out-of-set duplicates.
    Assumes three arrays are given (training, validation, test) data sets.
    Assumes training data set is at least as big as validation and test.
    Params:
      arr1: Array to check for in-set duplicates and out-of-set duplicates.
            Needs to have shape[0] >= arr2 and arr3.
      arr2: Array to check for in-set duplicates and out-of-set duplicates.
      arr3: Array to check for in-set duplicates and out-of-set duplicates.
      T: Trajectory length to investigate
    Returns:
      Nothing.
    """

    t_check1 = time.time()
    def checkArray_i(i):
        if (i%100 == 0):
            print("i: ",i)
        # Check if a duplicate of a training trajectory exists in the validation or
        # test sets.
        if (np.any(np.all(np.equal(arr1[i,:], arr2), axis=1))):
            print("WARNING: Duplicates of training trajectory " + str(i) + " found in validation data.")
        if (np.any(np.all(np.equal(arr1[i,:], arr3), axis=1))):
            print("WARNING: Duplicates of training trajectory " + str(i) + " found in test data.")

        # Check if there exists a duplicate of the i'th row anywhere in the matrix excluding
        # the i'th row itself.
        if (np.any(np.all(np.equal(arr1[i,:],arr1[np.arange(arr1.shape[0]) != i]),axis=1))):
            print("WARNING: Duplicates of training trajectory " + str(i) + " found in training data.")
        if (i<arr2.shape[0]):
            if (np.any(np.all(np.equal(arr2[i,:],arr2[np.arange(arr2.shape[0]) != i]),axis=1))):
                print("WARNING: Duplicates of validation trajectory " + str(i) + " found in validation data.")
        if (i<arr3.shape[0]):
            if (np.any(np.all(np.equal(arr3[i,:],arr3[np.arange(arr3.shape[0]) != i]),axis=1))):
                print("WARNING: Duplicates of test trajectory " + str(i) + " found in test data.")
        if (i<arr2.shape[0] or i<arr3.shape[0]):
            if (arr2.shape[0] >= arr3.shape[0]):
                if (np.any(np.all(np.equal(arr2[i,:], arr3), axis=1))):
                    print("WARNING: Duplicates of validation trajectory " + str(i) + " found in test data.")
            else:
                if (np.any(np.all(np.equal(arr3[i,:], arr2), axis=1))):
                    print("WARNING: Duplicates of test trajectory " + str(i) + " found in validation data.")

    # Not sure if https://stackoverflow.com/questions/29545605/why-is-it-important-to-protect-the-main-loop-when-using-joblib-parallel is relevant here and code should be restructured. Seems to work.
    result = Parallel(n_jobs=NUM_LOGICAL_CORES, backend="threading")(delayed(checkArray_i)(i) for i in range(arr1.shape[0]))
    #result = Parallel(n_jobs=1, backend="threading")(delayed(checkArray_i)(i) for i in range(arr1.shape[0]))
    t_check4 = time.time()
    t_check_total = t_check4 - t_check1
    print("Total check time: ", t_check_total)


def T_ToFileNameString(T):
    result = str(T)
    result = result.replace('.','_')
    return '_T' + result

def get_end_idx(T,dt):
    """
    Finds the amount of time steps needed to reach T

    Params:
        T:  Trajectory length.

    Returns:
        end_idx:    Amount of time steps of size dt needed to reach T.

    """
    # Old stable method
    #if not ((T/dt).is_integer()):
    #    print("Cannot resolve trajectory length " + str(T) + " into an integer number of time steps of size dt=" + str(dt) + ". Exiting...")
    #    exit()
    # New unstable method for fixing binning
    end_idx = int(np.around(T/dt))+1
    # Unstable depending on T and dt
    #end_idx = math.floor(T/dt)+1
    print(T)
    print(dt)
    print(end_idx)
    return end_idx

# Remember to set the defaults of this function correctly depending on the type of noise wanted
def binSignal(signal, times, binningFactor, add_noise=True, plotting=False, test_trajectory=0, add_whiteNoise = False, onlyWhiteNoise = False):


    if add_noise:
        print('Add white noise? ', add_whiteNoise)
        #t = np.linspace(0, 0.5, 500)
        #signal = np.sin(40 * 2 * np.pi * t) + 0.5 * np.sin(90 * 2 * np.pi * t)
        #N = dN[traj_idx,:].size
        #print("N: ", N)
        #fft_times = 2*np.pi*np.linspace(-0.5*N/T,0.5*N/T,N)
        #fft_dN = 2.0/N*np.fft.fftshift(np.fft.fft(dN[traj_idx,:]))
        #axes[0].plot(times, dN[traj_idx,:])
        #axes[1].plot(fft_times, np.abs(fft_dN))
        nt = times.size
        #Add noise here, use binningFactor for noise
        noise_factor = binningFactor
        dt = times[1]-times[0]

        if not onlyWhiteNoise:
            print('Adding symmetrical convolution (smoothing) of signal, noise_factor = %.2f '%noise_factor)
            
            print(signal.shape)
            print(signal[0].shape)
            fft_signal = np.fft.fft(signal[:,:,0],axis=1)
            fft_signal = np.fft.fftshift(fft_signal)
            print(fft_signal.shape)
            T = times[-1]
            print('dt=',dt)
            fft_times = 2*np.pi*np.fft.fftfreq(nt,dt)
            fft_times = np.fft.fftshift(fft_times)
            print(fft_times.shape)
            alpha = binningFactor

            #mu = 0
            #rv = cauchy(mu, alpha)
            #print(rv.pdf(fft_times).shape)
            #fft_noisy_signal = rv.pdf(fft_times)*fft_signal

            #lorentzScaled = (alpha/2)*alpha/(np.pi*(alpha**2+fft_times**2))
            # np.fft has: norm = 1, oscillatory factor = -1 so
            # fourier transform of a/2*exp(-a*|t|)= a**2/(a**2+w**2)
            lorentzScaled = (alpha**2)/(alpha**2+fft_times**2)
            fft_noisy_signal = lorentzScaled*fft_signal
            print(fft_noisy_signal.shape)
            noisy_signal = np.fft.ifftshift(fft_noisy_signal)
            noisy_signal = np.fft.ifft(noisy_signal)
        else:
            print('No smoothing of signal')
            noisy_signal = signal[:,:,0]
        if add_whiteNoise:
            white_noise_power = 1
            print('Adding white noise with variance %.5f to signal'%(white_noise_power*dt))
            white_noise = np.sqrt(white_noise_power*dt)*np.random.randn(noisy_signal.shape[0],noisy_signal.shape[1])
            preWhiteNoise_signal = np.copy(noisy_signal)
            noisy_signal += white_noise


        if plotting:
            figure_save_path = "C:/Users/Henneberg/speciale_local/thesisFigures/"
            def plotNoise(test_trajectory):
                #fig,axs = plt.subplots(5)
                fig = plt.figure(constrained_layout=True)

                from matplotlib.gridspec import GridSpec

                gs = GridSpec(6, 2, figure=fig)
                ax1 = fig.add_subplot(gs[0:3, 0])
                # identical to ax1 = plt.subplot(gs.new_subplotspec((0, 0), colspan=3))
                ax2 = fig.add_subplot(gs[0:2, 1])
                ax3 = fig.add_subplot(gs[2:4, 1])
                ax4 = fig.add_subplot(gs[4:6, 1])
                ax5 = fig.add_subplot(gs[3:6, 0])
                axs = [ax1, ax2, ax3, ax4, ax5]

                for ax in axs:
                    enchantAx(ax)
                    ax.tick_params(labelleft=False)

                # Test scaling is right
                #f_check = 0.00001*np.sin(50*times)
                #ax4.plot(fft_times, np.abs(np.fft.fftshift(np.fft.fft(f_check))))

                # cauchy is exactly alpha/(pi*(alpha**2+omega**2))

                # Test that transformation does nothing to constant signal
                #fake_constant_signal = 0.1*np.ones(times.size)
                #fft_fake = np.fft.fft(fake_constant_signal)
                #fft_fake = np.fft.fftshift(fft_fake)
                #fft_fake_lorentz4 = lorentzScaled*fft_fake
                #fake_lorentz_signal4 = np.fft.ifftshift(fft_fake_lorentz4)
                #fake_lorentz_signal4 = np.fft.ifft(fake_lorentz_signal4)
                #ax5.plot(times, fake_lorentz_signal4)
                #ax1.plot(times,fake_constant_signal)

                # These transformations change the signal amplitude
                #fft_fake_lorentz1 = rv.pdf(fft_times)*fft_fake
                #fft_fake_lorentz2 = (alpha/2)*rv.pdf(fft_times)*fft_fake
                #fft_fake_lorentz3 = (alpha**2)/(2*np.pi*(alpha**2+fft_times**2))*fft_fake
                #fake_lorentz_signal1 = np.fft.ifftshift(fft_fake_lorentz1)
                #fake_lorentz_signal2 = np.fft.ifftshift(fft_fake_lorentz2)
                #fake_lorentz_signal3 = np.fft.ifftshift(fft_fake_lorentz3)
                #fake_lorentz_signal1 = np.fft.ifft(fake_lorentz_signal1)
                #fake_lorentz_signal2 = np.fft.ifft(fake_lorentz_signal2)
                #fake_lorentz_signal3 = np.fft.ifft(fake_lorentz_signal3)
                #ax5.plot(times, fake_lorentz_signal1)
                #ax5.plot(times, fake_lorentz_signal2)
                #ax5.plot(times, fake_lorentz_signal3)
                #ax3.plot(fft_times,(alpha**2)/(2*np.pi*(alpha**2+fft_times**2)),'b-*')
                #ax3.plot(fft_times,(alpha)/(np.pi*(alpha**2+fft_times**2)),'g-*')

                lw = 0.5
                ax1.plot(times, signal[test_trajectory].real,lw=lw)
                # Plot mean of signal - matches transformed signal closely for alpha->0
                # Transformed signal -> original signal as alpha -> infty
                #ax5.plot(times, np.mean(signal[test_trajectory].real)*np.ones(times.size),lw=lw)

                ax1.set(ylabel=r'$ \mathrm{d} Y_t $')
                ax1.set_xlim([0,8])
                ax1.tick_params(labelbottom=False)
                # Show absolute value sqrt(a^2+b^2) for complex number a+ib
                ax2.plot(fft_times, np.abs(fft_signal[test_trajectory]),lw=lw)
                ax2.tick_params(labelbottom=False)
                ax2.set_xlim([-200,200])
                ax2.set(ylabel=r'$ |\tilde{Y}_t(\omega)| $')
                #ax3.plot(fft_times, rv.pdf(fft_times),'r--',label='cauchy pdf')
                #ax3.plot(fft_times, (alpha/2)*rv.pdf(fft_times),'g--',label='cauchy pdf')
                ax3.plot(fft_times, lorentzScaled,'r-',label='lorentzScaled')
                ax3.tick_params(labelbottom=False)
                ax3.set_xlim([-200,200])
                ax3.set(ylabel=r'$ f( \omega ) $')
                ax4.plot(fft_times, np.abs(fft_noisy_signal[test_trajectory]),'m-',lw=lw)
                ax4.set_xlim([-200,200])
                print(noisy_signal.shape)
                ax4.set(xlabel=r'$\omega / \gamma $ ',ylabel=r'$ |f( \omega) \tilde{Y}_t(\omega)| $')
                ax5.plot(times, noisy_signal[test_trajectory].real,'m-',lw=lw)
                ax5.set(ylabel=r'$ \mathrm{d} Q_t $',xlabel=r'$ \gamma t$')
                ax5.set_xlim([0,8])
                
                ax1.annotate(r"(a)", xy=(-0.22,0.92), xycoords='axes fraction')
                ax2.annotate(r"(b)", xy=(-0.22,0.92), xycoords='axes fraction')
                ax3.annotate(r"(c)", xy=(-0.22,0.92), xycoords='axes fraction')
                ax4.annotate(r"(d)", xy=(-0.22,0.92), xycoords='axes fraction')
                ax5.annotate(r"(e)", xy=(-0.22,0.92), xycoords='axes fraction')
                fig.set_size_inches(7.2, 5)
                plt.show()
                fig.savefig(figure_save_path + 'noise.pdf',dpi=200)

            if not add_whiteNoise:
                plotNoise(test_trajectory)
            else:
                fig,axs = plt.subplots(3)
                lw = 0.5
                axs[0].plot(times, signal[test_trajectory].real,lw=lw)
                axs[1].plot(times, preWhiteNoise_signal[test_trajectory].real,'m-',lw=lw)
                axs[2].plot(times, noisy_signal[test_trajectory].real,'C1',lw=lw)
                #axs[0].plot(times, signal[test_trajectory].imag)
                axs[0].set(ylabel=r'$ \mathrm{d} Y_t $')
                axs[1].set(ylabel=r'$ \mathrm{d} Q_t $')
                axs[2].set(ylabel=r'$ \mathrm{d} Q_t + \mathrm{d} W_t$')
                axs[0].annotate(r"(a)", xy=(-0.22,0.92), xycoords='axes fraction')
                axs[1].annotate(r"(b)", xy=(-0.22,0.92), xycoords='axes fraction')
                axs[2].annotate(r"(c)", xy=(-0.22,0.92), xycoords='axes fraction')

                for ax in axs.flat:
                    enchantAx(ax)
                    ax.set_xlim([0,8])

                axs[0].tick_params(labelbottom=False)
                axs[1].tick_params(labelbottom=False)
                axs[2].set(xlabel=r'$ \gamma t$')
                plt.tight_layout()
                plt.show()
                fig.savefig(figure_save_path + 'noisyTrajs' + T_ToFileNameString(alpha) + '.pdf',dpi=200)

        noisy_signal = noisy_signal.reshape((noisy_signal.shape[0], noisy_signal.shape[1], 1))
        print('final noisy signal shape: ', noisy_signal.shape)
        return noisy_signal, times

    # Lose the last data point so bins have equal size for binningfactors 2-64
    #if (binningFactor == 1):
        #return signal[:,:-1,:], times[:-1]
    # Keep the last index when e.g. nt=8001
    #N_bins = signal.shape[1]//binningFactor + 1
    # If signal is divisible by 2
    N_bins = signal.shape[1]//binningFactor
    print('N_bins: ',N_bins)
    #binned_signal = np.empty((signal.shape[0],N_bins,signal.shape[2]))
    # old method
    #for i in range(N_bins):
    #    if ((i+1)*binningFactor < signal.shape[1]):
    #        binned_signal[:,i,:] = np.sum(signal[:,i*binningFactor:(i+1)*binningFactor,:],axis=1)
    #    else:
    #        binned_signal[:,-1,:] = np.sum(signal[:,i*binningFactor:,:],axis=1)
    split = np.array_split(signal,N_bins,axis=1)
    splitSum = [np.sum(split[idx], axis=1) for idx in np.arange(len(split))]
    binned_signal = np.stack(splitSum,axis=1)

    binned_times = np.linspace(times[0], times[-1],N_bins)

    print("pre-bin shape: \n",signal.shape)
    print("post-bin shape: \n",binned_signal.shape)
    print("pre-bin: \n",signal[0,:20,0])
    print("post-bin: \n",binned_signal[0,:2,0])
    return binned_signal, binned_times

#def removeClicks


