import numpy as np
import scipy as sp
from plottingTools import *
from scipy.linalg import expm

from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
## for Palatino and other serif fonts use:
#rc('font',**{'family':'serif','serif':['Palatino']})
rc('text', usetex=True)


Omega = 2 # Rabi frequency
Delta = 0 # detuning
gamma = 1 # decay rate
# phase of local oscillator - changing this allows us to measure an arbitrary
# quadrature of the signal field - denoted phi in Bolund/KM
Phi = np.pi/2 

# TODO: Look at article from KM and Bolund and look at Ito calculus+Euler
# method (generalization of same method on ODEs to SDEs (stochastic
# differential equations))

def mcsolve(t0 = 0, T = 10, nt = 200):
    dt = (T-t0)/nt

    # Initial state
    psi0 = np.array([[0],[1]],dtype='complex_') #[[c_e],[c_g]]

    # Operators
    H = np.array([[0, Omega/2],[Omega/2, -Delta]]) # Hamiltonian
    C = np.sqrt(gamma)*np.array([[0,0],[1,0]])
    Heff = H-1j*(C.conj().T @ C)/2
    cPhi = C*np.exp(-1j*Phi)
    xPhi = cPhi + cPhi.conj().T
    U = expm(-1j*Heff*dt)

    dW = np.sqrt(dt)*np.random.randn(nt)

    psi = np.zeros((2,nt),dtype='complex_')
    dY = np.zeros((nt),dtype='complex_')
    psi[:,0] = psi0.reshape((2,))
    for i in range(1,nt):
        dY[i-1] = psi[:,i-1].conj().T@xPhi@psi[:,i-1]*dt + dW[i-1]
        psi[:,i] = (U + dY[i-1]*cPhi)@psi[:,i-1]
        psi[:,i] = psi[:,i]/np.linalg.norm(psi[:,i])
    dY[-1] = psi[:,-1].conj().T@xPhi@psi[:,-1]*dt + dW[-1]
    return psi,dY


if __name__ == '__main__':
    t0 = 0
    T = 9
    nt = 9000
    times = np.linspace(t0,T,nt)
    psi,dY = mcsolve(t0, T, nt)
    plt.figure()
    plt.plot(times,np.real(dY))
    plt.plot(times,np.imag(dY))
    plt.xlabel('Time')
    plt.ylabel('dY')


    fig = plt.figure()
    ax = plt.gca()
    psi_pop = np.abs(np.power(psi,2))
    plotPopulations(ax, times, psi_pop)
    plt.show()
    #fig = plotBlochSphere()
    #R = getBlochComponents(rho)
    #animateBlochVector(fig,times,rho,0)

    #fig2 = plotXYZ(times,R[0],R[1],R[2])
