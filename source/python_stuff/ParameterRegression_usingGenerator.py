import os
import datetime
import sys
from pathlib import Path
import h5py
import time
import tensorflow as tf
tf.get_logger().setLevel('ERROR') # suppress anything but errors (info and warnings)
import numpy as np
from tensorflow.keras.models import load_model
from tensorflow.keras.callbacks import ModelCheckpoint, TensorBoard, EarlyStopping
from tensorflow.keras import optimizers
from tensorflow.keras import metrics

from keras.wrappers.scikit_learn import KerasRegressor
from sklearn.pipeline import Pipeline
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.preprocessing import StandardScaler
import sklearn


# set the threshold of printed numpy stuff to maximum possible
# specify printed np results as 2 digits precision and suppress scientific notation for small numbers
np.set_printoptions(threshold=sys.maxsize,precision=2,suppress=True) 

seed = 1
np.random.seed(seed)

# custom functions and classes
import KerasBatchGenerators
import KerasModels
from plottingTools import *
from FileManagement import *

save_path += 'regression/'

def estimateParameter(batch_size, num_epochs, use_dropout):
    hidden_size = 0 # not used anymore
    t0 = time.time()

    # clean data
    psi_train,dN_train,times_train,omegas_train = read_data('training.h5')
    psi_valid,dN_valid,times_valid,omegas_valid = read_data('valid.h5')
    psi_test,dN_test,times_test,omegas_test = read_data('test.h5')

    # noisy data - random delays
    #psi_train,dN_train,times_train,omegas_train = read_data('training_noisy.h5')
    #psi_valid,dN_valid,times_valid,omegas_valid = read_data('valid_noisy.h5')
    #psi_test,dN_test,times_test,omegas_test = read_data('test_noisy.h5')

    times = times_train # the time steps must be equal for the different data sets

    N_trajs_train = dN_train.shape[0]
    nt = len(times)
    timesteps = nt
    #timesteps = 500
    skip_step = timesteps

    t1 = time.time()
    tDataLoad = t1-t0

    # plot some trajectories
    NUM_PLOTS = 3
    fig1,axes1 = plt.subplots(NUM_PLOTS)
    fig2,axes2 = plt.subplots(NUM_PLOTS)
    for i in range(NUM_PLOTS):
        plotPopulations(axes1[i], times, np.power(np.abs(psi_train[i,:,:].T),2))
        axes2[i].plot(times, dN_train[i,:], label=r'$\Omega = $'+str(round(omegas_train[i,0],2)))
        axes2[i].legend()
        fig2.suptitle('Signal versus time')
    fig1.tight_layout()
    fig2.tight_layout()
    fig1.savefig(save_path + 'regression_' + 'signal_' + str(NUM_PLOTS) + 'trajs' + '.pdf')
    fig2.savefig(save_path + 'regression_' + 'clicks_' + str(NUM_PLOTS) + 'trajs' + '.pdf')
    t2 = time.time()
    tFirstPlots = t2-t1

    print("Time for execution of different units:")
    print("tDataLoad",np.around(tDataLoad,2))
    print("tFirstPlots",np.around(tFirstPlots,2))

    # callback to save model after every epoch
    checkpointer = ModelCheckpoint(filepath=save_path + '/param_esti_model-{epoch:02d}.h5', verbose=1)


    # callback for using Tensorboard
    logdir = Path(log_path) / datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    tensorboard_callback = TensorBoard(log_dir=os.fspath(logdir))

    # Trains model for a given initial amount of epochs, evaluates it on test data and returns the
    # model and test_accuracy. For use in e.g. K-fold cross-validation
    def auto_fit_model(model, train_data_generator, valid_data_generator, test_data_generator, num_epochs):
        print('Fitting of model started...')
        # callback to implement early stopping and save the best model found during training
        earlyStopping = EarlyStopping(monitor='val_loss',mode='min', verbose=1, patience=10, min_delta=0.5)
        modelCheckpoint = ModelCheckpoint(filepath=save_path + '/best_model.h5', monitor='val_loss', mode='min', save_best_only=True, verbose=1)
        with tf.device('/GPU:0'):
            hist = model.fit(train_data_generator.generate(),
                    steps_per_epoch=train_data_generator.getNStepsPerEpoch(),
                    epochs=num_epochs, validation_data =
                    valid_data_generator.generate(),
                    validation_steps=valid_data_generator.getNStepsPerEpoch(),
                    use_multiprocessing=False, shuffle=True,
                    initial_epoch=0, callbacks=[earlyStopping, modelCheckpoint])
                    #callbacks=[checkpointer]) # checkpointer saves model after each epoch
                    #callbacks=[checkpointer,tensorboard_callback])
        print('Fitting of model finished.')
        model = load_model(save_path + '/best_model.h5')
        print('Evaluating model on test data (not the same as val_acc)...')
        _, test_acc = model.evaluate(test_data_generator.generate(),
                steps = test_data_generator.getNStepsPerEpoch(), use_multiprocessing=False,
                callbacks=[])
        print('Evaluation of model completed.')
        return model, test_acc, hist

    # Trains model for a given initial amount of epochs and then asks if more training epochs should be
    # added. If not, evaluates model on test data and returns model and test_accuracy
    def fit_model(model, train_data_generator, valid_data_generator, test_data_generator, num_epochs):
        remaining_epochs = num_epochs
        total_epochs = 0
        # callback to implement early stopping and save the best model found during training
        earlyStopping = EarlyStopping(monitor='val_loss',mode='min', verbose=1, patience=10, min_delta=0.5)
        modelCheckpoint = ModelCheckpoint(filepath=save_path + '/best_model.h5', monitor='val_loss', mode='min', save_best_only=True, verbose=1)
        print('Fitting of model started...')
        #model = load_model(save_path + '/param_esti_model-15.h5')
        while (remaining_epochs>0):
            with tf.device('/GPU:0'):
                hist = model.fit(train_data_generator.generate(),
                        steps_per_epoch=train_data_generator.getNStepsPerEpoch(),
                        epochs=total_epochs+remaining_epochs, validation_data =
                        valid_data_generator.generate(),
                        validation_steps=valid_data_generator.getNStepsPerEpoch(),
                        use_multiprocessing=False, shuffle=True,
                        initial_epoch=total_epochs,
                        callbacks=[modelCheckpoint])
                        #callbacks=[checkpointer,tensorboard_callback])
            total_epochs += remaining_epochs
            remaining_epochs = 0
            try:
                remaining_epochs = int(input("Enter number of epochs to run (default 0):\n"))
            except ValueError:
                print('Input was not an integer. Ending the program normally.')
            except:
                print('Something went wrong with integer conversion of input. Ending the program normally.')

        #model = load_model(save_path + '/param_esti_model-15.h5')
        model = load_model(save_path + '/best_model.h5')
        print('Fitting of model finished.')
        print('Evaluating model on test data...')
        _, test_acc = model.evaluate(test_data_generator.generate(),
                steps = test_data_generator.getNStepsPerEpoch(), use_multiprocessing=False,
                callbacks=[])
        print('Evaluation of model completed.')
        print("\nTotal amount of epochs run: ", total_epochs)
        return model, test_acc, hist

    # choose model we want to use (NN architecture)
    modelStrategy = KerasModels.RegressionParameterConvLSTMModel(hidden_size, nt, timesteps, use_dropout)

    # using optimizers from tf.keras.optimizers required for cuDNN LSTM
    my_optimizer = tf.keras.optimizers.Adam()

    # optimizer from Keras library directly
    #my_optimizer = optimizers.Nadam()

    # could use others???
    my_metric = "mae" # mean-squared error for regression model
    
    # callback to save model after every epoch
    checkpointer = ModelCheckpoint(filepath=save_path + '/param_esti_model-{epoch:02d}.h5', verbose=1)

    # callback to implement early stopping and save the best model found during training
    earlyStopping = EarlyStopping(monitor='val_loss',mode='min', verbose=1, patience=10, min_delta=0.05)
    modelCheckpoint = ModelCheckpoint(filepath=save_path + '/best_model.h5', monitor='val_loss', mode='min', save_best_only=True, verbose=1)

    # callback for using Tensorboard
    #logdir = Path(log_path) / datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    #tensorboard_callback = TensorBoard(log_dir=os.fspath(logdir))

    # take raw clicks as input
    batchGenerator = KerasBatchGenerators.SingleClickToConstantParameterRegression

    # K-fold cross-validation
    def do_KFold_CV(n_folds):
        kfold = KFold(n_folds, True, seed)

        # not using test set atm - splitting training data into training and validation data using kfold and using the c++ simulated validation data as the test set
        test_data_generator = batchGenerator(dN_valid, timesteps, batch_size,
                skip_step, omegas_valid, modelStrategy, True)
        scores, members, histories = list(), list(), list()
        for train_idx, test_idx in kfold.split(dN_train):
            model = modelStrategy.defineModel()
            model.compile(loss='mean_absolute_error',
                    optimizer=my_optimizer, metrics=[my_metric])
            train_data_generator = batchGenerator(dN_train[train_idx,:,:], timesteps, batch_size,
                    skip_step, omegas_train[train_idx,:], modelStrategy, True)
            valid_data_generator = batchGenerator(dN_train[test_idx,:,:], timesteps, batch_size,
                    skip_step, omegas_train[test_idx,:], modelStrategy, True)
            model, test_acc, hist = auto_fit_model(model, train_data_generator, valid_data_generator,
                    test_data_generator, num_epochs)
            scores.append(test_acc)
            members.append(model)
            histories.append(hist)
        print('Estimated accuracy: %.3f (%.3f)' % (np.mean(scores), np.std(scores)))
        print('Estimated accuracy is based on the validation data set whereas the validation loss is calculated from a hold-out part of the training data. Thus estimated accuracy is not expected to be equal to val_loss')

        def getEnsemblePrediction(members, dataGenerator):
            #predictions = [model.predict(dataGenerator.generate()) for model in members]
            predictions = np.empty((dN_test.shape[0],len(members)))
            #dataGenerator.reset()
            for idx,model in enumerate(members):
            #    pred = model.predict(dataGenerator.generate(),steps=dataGenerator.getNStepsPerEpoch()) # predict on data
                pred = model.predict(dN_test)
                predictions[:,idx] = pred[:,0]
            actual_values = omegas_test[:,0]
            predictions = np.array(predictions)
            mean_predictions = np.mean(predictions,axis=1)
            return mean_predictions,actual_values

        def evaluate_n_members(members, n_members, dataGenerator):
            subset = members[:n_members]
            mean_predictions, actual_values = getEnsemblePrediction(subset, dataGenerator)

            # MSE
            #acc = sklearn.metrics.mean_squared_error(actual_values,mean_predictions)

            # MAE
            acc = sklearn.metrics.mean_absolute_error(actual_values,mean_predictions)
            return acc

        test_data_generator = batchGenerator(dN_test, timesteps, batch_size,
                skip_step, omegas_test, modelStrategy, True)

        single_scores, ensemble_scores, result_list = list(), list(), list()
        for i in range(1, n_folds+1):
            ensemble_score = evaluate_n_members(members, i, test_data_generator)
            #test_data_generator.reset()
            #_, single_score = members[i-1].evaluate(test_data_generator.generate(), steps=test_data_generator.getNStepsPerEpoch(), verbose=0)
            _, single_score = members[i-1].evaluate(dN_test, omegas_test[:,0], verbose=0)
            result_list.append('> %d: single=%.3f, ensemble=%.3f' % (i, single_score, ensemble_score))
            ensemble_scores.append(ensemble_score)
            single_scores.append(single_score)

        for result in result_list:
            print(result)
        print('Accuracy %.3f (%.3f)' % (np.mean(single_scores), np.std(single_scores)))
        print('Accuracy was obtained using the test data set. So are the single scores and the ensemble scores.')

        xs = [i for i in range(1, n_folds+1)]
        fig = plt.figure()
        plt.plot(xs, single_scores, marker='o', linestyle='None',label='Single')
        plt.plot(xs, ensemble_scores, marker='o', label='Ensemble predictions')
        fig.savefig(save_path + 'ensemble_vs_single.pdf')

        # return the list of models
        return members, histories

    def getSingleModel():
        # not using test set atm - splitting training data into training and validation data using kfold and using the c++ simulated validation data as the test set
        test_data_generator = batchGenerator(dN_valid, timesteps, batch_size,
                skip_step, omegas_valid, modelStrategy, True)
        scores, members, histories = list(), list(), list()
        model = modelStrategy.defineModel()
        model.compile(loss='mean_absolute_error',
                optimizer=my_optimizer, metrics=[my_metric])
        alpha = 8/10
        idx_fold = int(alpha*N_trajs_train) # takes a fraction 1-alpha of the training data as validation data
        train_data_generator = batchGenerator(dN_train[:idx_fold,:,:], timesteps, batch_size,
                skip_step, omegas_train[:idx_fold,:], modelStrategy, True)
        valid_data_generator = batchGenerator(dN_train[idx_fold:,:,:], timesteps, batch_size,
                skip_step, omegas_train[idx_fold:,:], modelStrategy, True)
        model, test_acc, hist = fit_model(model, train_data_generator, valid_data_generator,
                test_data_generator, num_epochs)

        # for loading the best model
        #model = load_model(save_path + '/best_model.h5')
        #_, test_acc = model.evaluate(test_data_generator.generate(),
        #        steps = test_data_generator.getNStepsPerEpoch(), use_multiprocessing=False,
        #        callbacks=[])

        #hist = 0
        scores.append(test_acc)
        members.append(model)
        histories.append(hist)

        print('Estimated accuracy: %.3f (%.3f)' % (np.mean(scores), np.std(scores)))
        return members, histories

    n_folds = 5
    #members, histories = do_KFold_CV(n_folds)
    members, histories = getSingleModel()

#    fig,axes = plt.subplots(len(histories))
#    if (len(histories) == 1):
#        #axes.plot(histories[0].history['mse'], label='Training set')
#        #axes.plot(histories[0].history['val_mse'], label='Validation set')
#        #axes.set(xlabel='Epoch number', ylabel='Mean-squared error')
#
#        axes.plot(histories[0].history['mae'], label='Training set')
#        axes.plot(histories[0].history['val_mae'], label='Validation set')
#        axes.set(xlabel='Epoch number', ylabel='Mean-absolute error')
#        axes.set_ylim(0,5)
#        axes.legend()
#    else:
#        for idx,hist in enumerate(histories):
#            #axes[idx].plot(hist.history['mse'], label='Training set')
#            #axes[idx].plot(hist.history['val_mse'], label='Validation set')
#            #axes[idx].set(xlabel='Epoch number', ylabel='Mean-squared error')
#
#            axes[idx].plot(hist.history['mae'], label='Training set')
#            axes[idx].plot(hist.history['val_mae'], label='Validation set')
#            axes[idx].set(ylabel='MAE')
#            axes[idx].set_ylim(0,5)
#            axes[idx].legend()
#
#        axes[axes.shape[0]-1].set(xlabel='Epoch number')
#    fig.tight_layout()
#    fig.savefig(save_path + '/training_history.pdf')

    # returns array of predictions
    def get_test_predictions(num_batches_predict, data_generator, members, dummy_iters = 0):
        dummy_iters = 0 # controls where to start from in a single trajectory
        num_predict = num_batches_predict # how many batches of test trajectories to do the prediction for

        mean_pred = np.zeros((num_predict,))
        pred = np.empty((num_predict,len(members)))

        print('Predicting on test data started...')
        for i in range(dummy_iters):
            dummy = next(data_generator.generate())
        for i in range(num_predict):
            data = next(data_generator.generate())
            for idx,model in enumerate(members):
                pred[i,idx] = model.predict(data[0])

        #return mean of predictions by every model in members
        pred_arr = np.mean(np.array(pred),axis=1) 
        return pred_arr

    # data generator on test set with batch size 1, so we have fine control over how many trajectories to use for prediction
    test_data_generator = batchGenerator(dN_test, timesteps, 1,
        skip_step, omegas_test, modelStrategy, True)

    # having initialized the data generator with 1 batch size, num_predict will equal number of trajectories to do the prediction for
    num_predict = 50
    pred_arr = get_test_predictions(num_predict, test_data_generator, members)
    comparePredictedToTest = np.around(np.array([omegas_test[:num_predict,0],pred_arr,np.abs(omegas_test[:num_predict,0]-pred_arr)]),2).T
    print('Predicting on test data finished.')
    print('actual frequencies:, predicted frequencies:\n',comparePredictedToTest)
    print('MAE in shown predictions: %0.3f' % (np.mean(np.abs(comparePredictedToTest[:,0]-comparePredictedToTest[:,1]),axis=0)))
    #print('MAE double check on test data: %0.3f' % (np.mean(np.abs(omegas_test[:,0]-get_test_predictions(omegas_test.shape[0],test_data_generator,members)))))

