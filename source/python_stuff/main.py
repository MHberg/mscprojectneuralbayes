import time
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' # disable debugging logs
#tf.get_logger().setLevel('ERROR')
import sys
import getopt

#from matplotlib import rc
#rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
## for Palatino and other serif fonts use:
#rc('font',**{'family':'serif','serif':['Palatino']})
#rc('text', usetex=True)

# custom functions
import qjpred
import HypothesisTesting
import ParameterRegression

# make Tensorflow utilize all logical CPUs
#NUM_PARALLEL_EXEC_UNITS = os.cpu_count()
#config = tf.ConfigProto(intra_op_parallelism_threads=NUM_PARALLEL_EXEC_UNITS, inter_op_parallelism_threads = 2, allow_soft_placement=True, device_count = {'CPU': NUM_PARALLEL_EXEC_UNITS})
#session = tf.Session(config=config)

# Required when using CuDNNLSTM - might slow down gpu processing a bit
#config = tf.compat.v1.ConfigProto()
#config.gpu_options.allow_growth = True
#session = tf.compat.v1.Session(config=config)

def main(argv):

    batch_size = 32
    #batch_size = 1024
    use_dropout = True
    use_kfold = False
    use_binned_data = False
    num_epochs = 100
    load_accs = True# Load accuracies from disk instead of calculating them

    # trajectory lengths
    #Ts = [0.25, 0.5, 1, 1.5, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 15]
    #Ts = [0.25]
    Ts = [8]
    #Ts = [15]
    #Ts = [50]


    binningFactors = [2**x for x in range(0,7)]
    #binningFactors = [2**x for x in range(7)] + [100, 200, 400, 800]
    #binningFactors = [100, 200, 400]
    #binningFactors = [800]
    #binningFactors = [1600]
    # For adding noise
    #binningFactors = [0.25,0.5,0.75,1,1.25,1.5]
    #binningFactors = [0.25,0.5,0.75,1,1.25,1.5]
    #binningFactors = [0.75]
    binningFactors = [2**x for x in range(1,7)]
    binningFactors = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 14, 16, 32, 64, 128]
    #binningFactors = [10, 12, 14]
    #binningFactors = [1, 3, 5, 6, 7, 9]
    # Use this for onlyWhiteNoise signals, remember to set the correct folder in Hypothesis Testing
    #binningFactors = [0]

    # check for duplicates
    #HypothesisTesting.checkDuplicatesTrainValidTest(15)
    #exit()
    #ParameterRegression.checkDuplicatesTrainValidTest(Ts)


    helpstr = 'main.py -r <program>["hyptrain", "hyptest", "regtrain", "regtest", "qjpred"]'
    if (argv == []):
        print('No argument specified. The syntax is:')
        print(helpstr)
        sys.exit(2)

    try:
        opts, args = getopt.getopt(argv,"hr:", ["run="])
    except getopt.GetoptError:
        print(helpstr)
        sys.exit(2)
    for opt, arg in opts:
        if (opt == '-h'):
            print(helpstr)
            sys.exit()
        elif (opt in ("-r", "--run")):
            if (arg == 'hyptrain'):
                # start hypothesis testing using classification
                HypothesisTesting.main(batch_size, num_epochs, use_dropout, use_kfold, Ts)
            elif (arg == 'hyptest'):
                # test previously fit models on classification problem
                HypothesisTesting.test_saved_models(Ts,use_binned_data, binningFactors[0], load_accs)
            elif (arg == 'hyptrainbinned'):
                HypothesisTesting.hypothesisTestBinned(batch_size, num_epochs, use_dropout, use_kfold, Ts, binningFactors)
            elif (arg == 'hyptestbinned'):
                HypothesisTesting.test_saved_models_binned(Ts, binningFactors, load_accs)
            elif (arg == 'regtrain'):
                # start parameter estimation using regression
                ParameterRegression.main(batch_size, num_epochs, use_dropout, use_kfold, Ts)
            elif (arg == 'regtest'):
                # test previously fit models on regression problem
                ParameterRegression.test_saved_models(Ts, False, 1, load_accs)
            elif (arg == 'qjpred'):
                # run quantum jump prediction
                qjpred.main(batch_size, num_epochs, use_dropout)
            else:
                print('No valid program specified. Exiting...')
                sys.exit(2)

if __name__=='__main__':
    t_begin = time.time()
    


    main(sys.argv[1:])
    t_end = time.time()
    t_final = t_end-t_begin
    print("Total time: ", round(t_final,2))
