# Fix RNG seed in NumPy and Tensorflow for reproducibility
from numpy.random import seed
seed(1)
import tensorflow as tf
tf.random.set_seed(2)
kfold_seed = 1 # fix seed for rng

import os
import datetime
import sys
from pathlib import Path
import time
import numpy as np
from tensorflow.keras.models import load_model
from tensorflow.keras.callbacks import ModelCheckpoint, TensorBoard, EarlyStopping
from tensorflow.keras import optimizers
from tensorflow.keras import metrics
from tensorflow.keras.utils import to_categorical
from sklearn.model_selection import KFold
from sklearn.metrics import plot_confusion_matrix
import sklearn
import pickle

import multiprocessing
from joblib import Parallel, delayed
NUM_LOGICAL_CORES = multiprocessing.cpu_count()

np.set_printoptions(threshold=sys.maxsize)

# custom functions and classes
import KerasBatchSequence2
import KerasBatchGenerators
import KerasModels
from plottingTools import *
from FileManagement import *
import bayes
import sgdr

TESTING = True
USE_HOMODYNE = True
dt = 0.001


if (TESTING):
    save_path += 'testing/'

if (USE_HOMODYNE):
    data_path += 'homodyne/'
    save_path += 'homodyne/'
else:
    data_path += 'counting/'
    save_path += 'counting/'

dt_str = str(dt)
dt_str = dt_str.replace('.','_')

data_path += 'dt_' + dt_str + '/'
save_path += 'dt_' + dt_str + '/'

# save path and hypotheses chosen
hypotheses = np.array((2,6))
#hypotheses = np.array((1,2,4,6))

single_omega_data_path = data_path + 'singleOmega/'
data_path += 'hyp' + str(hypotheses.size) + 'class/'
save_path += 'hyp' + str(hypotheses.size) + 'class/'

# Create directories on save path if they do not exist.
Path(save_path).mkdir(parents=True, exist_ok=True)


#def plotConfusionMatrix():


def test_saved_models(Ts):
    # Needed for Bayes
    Delta = 0
    gamma = 1

    models = list()
    acc2 = list()
    bayes_acc2 = list()
    acc6 = list()
    bayes_acc6 = list()
    for idx,T in enumerate(Ts):
        T_str = str(T)
        T_str = T_str.replace('.','_')
        scores = np.load(save_path + 'hypTestingScores_T' + T_str + '.npy')
        if (scores.size > 1):
            print(scores[0,:])
            best_model_idx = np.argmax(scores[0,:],axis=0)
        else:
            best_model_idx = 0
        print(best_model_idx)
        model = load_model(save_path + 'best_model_' + str(best_model_idx) + '_T' + T_str +'.h5')
        #model = load_model(save_path + 'model_0_T' + str(int(T)) +'.h5')
        models.append(model)
        psi_test2, test_signal_Omega2, times_test2, omegas_test2 = read_data(single_omega_data_path + 'test_Omega2_T' + T_str + '.h5')
        pred2 = hypotheses[np.argmax(models[idx].predict(test_signal_Omega2),axis=1)]
        psi_test6, test_signal_Omega6, times_test6, omegas_test6 = read_data(single_omega_data_path + 'test_Omega6_T' + T_str + '.h5')
        pred6 = hypotheses[np.argmax(models[idx].predict(test_signal_Omega6),axis=1)]
        actual2 = omegas_test2[:,0]
        actual6 = omegas_test6[:,0]
        acc2.append(sklearn.metrics.accuracy_score(pred2, actual2))
        acc6.append(sklearn.metrics.accuracy_score(pred6, actual6))
        #num_predict = 50
        #compare_pred_actual = np.array(([pred,actual])).T
        #print(compare_pred_actual)

        bayesProbability2 = bayes.bayesPredict(psi_test2, test_signal_Omega2, times_test2, hypotheses, T, Delta, gamma, USE_HOMODYNE, -np.pi/2)
        bayesProbability6 = bayes.bayesPredict(psi_test6, test_signal_Omega6, times_test6, hypotheses, T, Delta, gamma, USE_HOMODYNE, -np.pi/2)
        #print('P6: \n',bayesProbability[6,:,:])
        #print('P: \n',bayesProbability[:50,:,-1])
        pred_bayes2 = hypotheses[np.argmax(bayesProbability2[:,:,-1],axis=1)]
        pred_bayes6 = hypotheses[np.argmax(bayesProbability6[:,:,-1],axis=1)]
        bayes_acc2.append(sklearn.metrics.accuracy_score(pred_bayes2,actual2))
        bayes_acc6.append(sklearn.metrics.accuracy_score(pred_bayes6,actual6))

        #compare_bayes_actual2 = np.array(([pred_bayes2,omegas_test2[:,0]])).T
        #print('predicted, actual:')
        #print(compare_bayes_actual)

    #print("Accuracy on test data with Omega=2: \n",acc2)
    #print("Accuracy on test data with Omega=2: \n",acc2)
    fig,axes = plt.subplots(2)
    axes[0].plot(Ts, acc2, label='NN accuracy')
    axes[1].plot(Ts, acc6, label='NN accuracy')
    axes[0].plot(Ts, bayes_acc2, label='Bayes accuracy')
    axes[1].plot(Ts, bayes_acc6, label='Bayes accuracy')
    for ax in axes:
        ax.set(xlabel="Trajectory length T", ylabel="Accuracy")
        ax.legend()
    axes[0].set(title='Accuracy versus trajectory length $\Omega = 2$')
    axes[1].set(title='Accuracy versus trajectory length $\Omega = 6$')
    fig.tight_layout()
    fig.savefig(save_path + 'acc_vs_T.pdf')
    plt.clf()

    # For sanity checking manually that trajectories with same Omega differ
    #averageAndPlotInput(psi_test2, test_signal_Omega2, times_test2, 2)
    #averageAndPlotInput(psi_test6, test_signal_Omega6, times_test6, 6)

def main(batch_size, num_epochs, use_dropout, use_kfold, Ts):
    models, scores, histories = list(), list(), list()
    for T in Ts:
        T_str = str(T)
        T_str = T_str.replace('.','_')
        model, score, history = hypothesisTest(batch_size, num_epochs, use_dropout, use_kfold, T)
        models.append(model)
        scores.append(score)
        histories.append(history)
        np.save(save_path+'/hypTestingScores_T' + T_str,score, allow_pickle=False)
        # Reset tensorflow graph and create new one
        tf.keras.backend.clear_session()
    models = np.array(models)
    scores = np.array(scores)
    histories = np.array(histories)

def checkDuplicates(arr1, arr2, arr3):
    """
    Method for checking if arrays contains in-set or out-of-set duplicates.
    Assumes three arrays are given (training, validation, test) data sets.
    Assumes training data set is at least as big as validation and test.
    Params:
      arr1: Array to check for in-set duplicates and out-of-set duplicates.
            Needs to have shape[0] >= arr2 and arr3.
      arr2: Array to check for in-set duplicates and out-of-set duplicates.
      arr3: Array to check for in-set duplicates and out-of-set duplicates.
      T: Trajectory length to investigate
    Returns:
      Nothing.
    """

    t_check1 = time.time()
    def checkArray_i(i):
        # Check if a duplicate of a training trajectory exists in the validation or
        # test sets.
        if (np.any(np.all(np.equal(arr1[i,:], arr2), axis=1))):
            print("WARNING: Duplicates of training trajectory " + str(i) + " found in validation data.")
        if (np.any(np.all(np.equal(arr1[i,:], arr3), axis=1))):
            print("WARNING: Duplicates of training trajectory " + str(i) + " found in test data.")

        # Check if there exists a duplicate of the i'th row anywhere in the matrix excluding
        # the i'th row itself.
        if (np.any(np.all(np.equal(arr1[i,:],arr1[np.arange(arr1.shape[0]) != i]),axis=1))):
            print("WARNING: Duplicates of training trajectory " + str(i) + " found in training data.")
        if (i<arr2.shape[0]):
            if (np.any(np.all(np.equal(arr2[i,:],arr2[np.arange(arr2.shape[0]) != i]),axis=1))):
                print("WARNING: Duplicates of validation trajectory " + str(i) + " found in validation data.")
        if (i<arr3.shape[0]):
            if (np.any(np.all(np.equal(arr3[i,:],arr3[np.arange(arr3.shape[0]) != i]),axis=1))):
                print("WARNING: Duplicates of test trajectory " + str(i) + " found in test data.")
        if (i<arr2.shape[0] or i<arr3.shape[0]):
            if (arr2.shape[0] >= arr3.shape[0]):
                if (np.any(np.all(np.equal(arr2[i,:], arr3), axis=1))):
                    print("WARNING: Duplicates of validation trajectory " + str(i) + " found in test data.")
            else:
                if (np.any(np.all(np.equal(arr3[i,:], arr2), axis=1))):
                    print("WARNING: Duplicates of test trajectory " + str(i) + " found in validation data.")

    # Not sure if https://stackoverflow.com/questions/29545605/why-is-it-important-to-protect-the-main-loop-when-using-joblib-parallel is relevant here and code should be restructured. Seems to work.
    result = Parallel(n_jobs=NUM_LOGICAL_CORES, backend="threading")(delayed(checkArray_i)(i) for i in range(arr1.shape[0]))
    t_check4 = time.time()
    t_check_total = t_check4 - t_check1
    print("Total check time: ", t_check_total)

def checkDuplicatesTrainValidTest(T):
    T_str = str(T[0])
    T_str = T_str.replace('.','_')

    filename_suffix = '_T' + T_str + '.h5'

    training_data_path = data_path + 'training' + filename_suffix
    valid_data_path = data_path + 'valid' + filename_suffix
    test_data_path = data_path + 'test' + filename_suffix

    # clean data - load all
    dN_train,omegas_train = read_data_signalsAndFrequencies(training_data_path,np.arange(0))
    dN_valid,omegas_valid = read_data_signalsAndFrequencies(valid_data_path, np.arange(0))
    dN_test,omegas_test = read_data_signalsAndFrequencies(test_data_path, np.arange(0))

    # For checking for duplicates - inserting fake duplicates
    #dN_train[94] = dN_train[3]
    #dN_test[2345] = dN_train[43]
    #dN_valid[1932] = dN_train[87]

    # check for duplicates
    checkDuplicates(dN_train, dN_valid, dN_test)

def hypothesisTest(batch_size, num_epochs, use_dropout, use_kfold, T):
    t0 = time.time()
    hidden_size = 0
    T_str = str(T)
    T_str = T_str.replace('.','_')

    filename_suffix = '_T' + T_str + '.h5'

    training_data_path = data_path + 'training' + filename_suffix
    valid_data_path = data_path + 'valid' + filename_suffix
    test_data_path = data_path + 'test' + filename_suffix

    N_trajs_train = 20000
    N_trajs_valid = 4096
    N_trajs_test = 4096

    times = read_data_times(training_data_path)

    # clean data - load
    N_data_plot = 1000 # Number of trajectories to load for plotting
    psi_train,dN_train,times_train,omegas_train = read_data(training_data_path,np.arange(N_data_plot))
    #psi_valid,dN_valid,times_valid,omegas_valid = read_data(valid_data_path, np.arange(1000))
    #psi_test,dN_test,times_test,omegas_test = read_data(test_data_path, np.arange(1000))

    # For checking for duplicates - inserting fake duplicates
    #dN_train[94] = dN_train[3]
    #dN_test[2345] = dN_train[43]
    #dN_valid[1932] = dN_train[87]

    #checkDuplicates(dN_train)

    #print("First test trajectory has no duplicates in training data as expected." if not (np.any(np.equal(dN_test[0,:],dN_train))) else "WARNING: Duplicates of test trajectory 0 was found in training data.")
    #print("First test trajectory has no duplicates in validation data as expected." if not (np.any(np.equal(dN_test[0,:],dN_valid))) else "WARNING: Duplicates of test trajectory 0 was found in validation data.")

    # test shuffling labels on training data to see generalization error falls apart
    # but maybe training acc is still high because of complexity of NN
    #np.random.shuffle(omegas_train[:,0])

    # noisy data - random delays
    #noisy_data_path = data_path + 'noisy/'
    #psi_train,dN_train,times_train,omegas_train = read_data(noisy_data_path + 'training' + filename_suffix)
    #psi_valid,dN_valid,times_valid,omegas_valid = read_data(noisy_data_path + 'valid' + filename_suffix)
    #psi_test,dN_test,times_test,omegas_test = read_data(noisy_data_path + 'test' + filename_suffix)

    #times = times_train # the time steps must be equal for the different data sets

    nt = len(times)
    timesteps = nt
    if (T != times[-1]):
        print("T not equal to end of times array. Exiting...")
        exit()
    #timesteps = 500
    skip_step = timesteps

    t1 = time.time()
    tDataLoad = t1-t0

    
    idx_hypotheses = np.zeros((N_data_plot,hypotheses.size),dtype='bool')

    for idx,hypothesis in enumerate(hypotheses):
        idx_hypotheses[:,idx] = omegas_train[:,0] == hypothesis

    #for i in range(idx_hypotheses.shape[1]):
    #    if (psi_train[idx_hypotheses[:,i],:,:].shape[0] == 0):
    #        print('Hypothesis frequencies did not match any frequencies in data set. Exiting...')
    #        exit()
    #    averageAndPlotInput(psi_train[idx_hypotheses[:,i],:,:],
    #                        dN_train[idx_hypotheses[:,i],:],
    #                        times_train, i)
    
    t2 = time.time()
    tFirstPlots = t2-t1

    print("Time for execution of different units:")
    print("tDataLoad",tDataLoad)
    print("tFirstPlots",tFirstPlots)

    
    # choose model we want to use (NN architecture)
    modelStrategy = KerasModels.ParameterCNNModel(hidden_size, nt, timesteps,
                                                   use_dropout, hypotheses)

    # encode input as alternating between 0 and 1 at each click
    #batchGenerator = KerasBatchGenerators.SignalToConstantParameterFromHypotheses

    # take raw clicks as input using generator
    #batchGenerator = KerasBatchGenerators.SingleClickToConstantParameterFromHypotheses

    # take raw clicks as input using Keras Sequence
    batchSequence = KerasBatchSequence2.SingleClickToConstantParameterFromHypotheses

    #train_data_generator = batchGenerator(dN_train, timesteps, batch_size,
    #        skip_step, hypotheses, omegasEncoded_train, modelStrategy, True)
    #valid_data_generator = batchGenerator(dN_valid, timesteps, batch_size,
    #        skip_step, hypotheses, omegasEncoded_valid, modelStrategy, False)


    # using optimizers from tf.keras.optimizers required for cuDNN LSTM
    my_optimizer = tf.keras.optimizers.Nadam()
    #my_optimizer = tf.keras.optimizers.SGD()

    #my_optimizer = optimizers.Nadam()

    # evaluation metric
    my_metric = "accuracy" # accuracy defaults to categorical_accuracy
    
    # callback to save model after every epoch
    checkpointer = ModelCheckpoint(filepath=save_path + '/param_esti_model-{epoch:02d}.h5', verbose=1)
    
    # callbacks for tensorboard
    logdir = Path(log_path) / datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    tensorboard_callback = TensorBoard(log_dir=os.fspath(logdir))

    # set minimum change in val_accuracy before early stopping
    MIN_DELTA = 0.001

    class OnEpochEnd(tf.keras.callbacks.Callback):
        def __init__(self,dataGenerator,model):
            super().__init__()
            self.dataGenerator = dataGenerator
            self.model = model
            self.initial_weights = self.model.get_weights()

        # function written by 183amir in comment https://github.com/keras-team/keras/issues/341#issuecomment-539198392 on 07/10/2019 extended by AZippelius in comment https://github.com/keras-team/keras/issues/341#issuecomment-547833394 on 30/10/2019 - not guaranteed to work for all layer types. Should work for Dense, Conv2D, Conv1D, SimpleRNN, GRU, LSTM, ConvLSTM2D according to source.
        def reset_weights(self, model):
            for layer in model.layers:
                if isinstance(layer, tf.keras.Model): #if you're using a model as a layer
                    self.reset_weights(layer) #apply function recursively
                    continue

                #where are the initializers?
                if hasattr(layer, 'cell'):
                    init_container = layer.cell
                else:
                    init_container = layer

                for key, initializer in init_container.__dict__.items():
                    if "initializer" not in key: #is this item an initializer?
                        continue #if no, skip it

                    # find the corresponding variable, like the kernel or the bias
                    if key == 'recurrent_initializer': #special case check
                        var = getattr(init_container, 'recurrent_kernel')
                    else:
                        var = getattr(init_container, key.replace("_initializer", ""))

                    var.assign(initializer(var.shape, var.dtype))
                    #use the initializer

        def on_epoch_end(self, epoch, logs):
            self.dataGenerator.on_epoch_end()
            if (epoch > 1 and epoch % 2 == 0 and logs.get('accuracy',epoch) < 0.51):
                self.reset_weights(self.model)
                print('Weights reinitialized.')

    # Trains model for a given initial amount of epochs, evaluates it on test data and returns the
    # model and test_accuracy. For use in e.g. K-fold cross-validation.
    # train_data_generator is generating data from training data set, except a hold out part
    # valid_data_generator is generating data from hold out part of training data set
    # test_data_generator is generating data from validation data set
    def auto_fit_model(model, train_data_generator, valid_data_generator, test_data_generator, num_epochs, idx):
        # callback to implement early stopping and save the best model found during training
        earlyStopping = EarlyStopping(monitor='val_accuracy',mode='max', verbose=1, patience=10, min_delta=MIN_DELTA, restore_best_weights=True)
        modelCheckpoint = ModelCheckpoint(filepath=save_path + 'best_model_' + str(idx) + '_T' + T_str + '.h5', monitor='val_accuracy', mode='max', save_best_only=True, verbose=1)
        # Callback for controlling what happens at the end of every epoch.
        onEpochEnd = OnEpochEnd(train_data_generator,model)
        #SGDR = sgdr.SGDRScheduler(1e-5,1e-2,train_data_generator.__len__(),0.9,5)

        print('Fitting of model started...')
        hist = model.fit(train_data_generator,
                epochs=num_epochs, validation_data = valid_data_generator,
                use_multiprocessing=False, shuffle=False,
                initial_epoch=0, callbacks=[earlyStopping, modelCheckpoint, onEpochEnd])

        del model
        # Reset tensorflow graph and create new one
        tf.keras.backend.clear_session()
        print('Fitting of model finished.')
        model = load_model(save_path + 'best_model_' + str(idx) + '_T' + T_str +'.h5')
        print('Evaluating model on test data (not the same as val_acc)...')
        _, test_acc = model.evaluate(test_data_generator,
                use_multiprocessing=False,
                callbacks=[]) 
        print('Evaluation of model completed.')
        return model, test_acc, hist

    # Trains model for a given initial amount of epochs and then asks if more training epochs should be
    # added. If not, evaluates model on test data and returns model and test_accuracy.
    # train_data_generator is generating data from training data set, except a hold out part
    # valid_data_generator is generating data from hold out part of training data set
    # test_data_generator is generating data from validation data set
    def fit_model(model, train_data_generator, valid_data_generator, test_data_generator, num_epochs):
        # callback to implement early stopping and save the best model found during training
        earlyStopping = EarlyStopping(monitor='val_accuracy',mode='max', verbose=1, patience=10, min_delta=MIN_DELTA)
        modelCheckpoint = ModelCheckpoint(filepath=save_path + 'best_model_0_T' + T_str +'.h5', monitor='val_accuracy', mode='max', save_best_only=True, verbose=1)
        onEpochEnd = OnEpochEnd(train_data_generator, model)
        #SGDR = sgdr.SGDRScheduler(1e-5,1e-2,train_data_generator.__len__(),0.9,3)

        remaining_epochs = num_epochs
        total_epochs = 0

        print('Fitting of model started...')
        while (remaining_epochs>0):
            hist = model.fit(train_data_generator, epochs=total_epochs+remaining_epochs, steps_per_epoch=None, validation_data = valid_data_generator, use_multiprocessing=False, shuffle=False, initial_epoch=total_epochs, callbacks=[modelCheckpoint,onEpochEnd, earlyStopping])
            total_epochs += remaining_epochs
            remaining_epochs = 0
            try:
                #remaining_epochs = int(input("Enter number of epochs to run (default 0):\n"))
                remaining_epochs = 0
            except ValueError:
                print('Input was not an integer. Ending the program normally.')
            except:
                print('Something went wrong with integer conversion of input. Ending the program normally.')

        model = load_model(save_path + 'best_model_0_T' + T_str + '.h5')
        print('Fitting of model finished.')
        print('Evaluating model on test data...')
        _, test_acc = model.evaluate(test_data_generator,
                use_multiprocessing=False,
                callbacks=[])
        print('Evaluation of model completed.')
        print("\nTotal amount of epochs run: ", total_epochs)
        return model, test_acc, hist

    # K-fold cross-validation
    def do_KFold_CV(n_folds):
        #kfold = KFold(n_folds, True)
        kfold = KFold(n_folds, True, kfold_seed)

        # not using test set atm - splitting training data into training and validation data using kfold and using the c++ simulated validation data as the test set
        test_data_generator = batchSequence(valid_data_path, np.arange(N_trajs_valid), nt, hypotheses, batch_size, timesteps, skip_step, modelStrategy, False)
        scores, members, histories = list(), list(), list()
        for idx, (train_idx, test_idx) in enumerate(kfold.split(dN_train)):
            if 'model' in locals():
                del model
            model = modelStrategy.defineModel()
            model.compile(loss='categorical_crossentropy',
                    optimizer=my_optimizer, metrics=[my_metric])
            train_data_generator = batchSequence(training_data_path, train_idx, nt, hypotheses, batch_size, timesteps, skip_step, modelStrategy, True)
            valid_data_generator = batchSequence(training_data_path, test_idx, nt, hypotheses, batch_size, timesteps, skip_step, modelStrategy, False)
            model, test_acc, hist = auto_fit_model(model, train_data_generator, valid_data_generator, test_data_generator, num_epochs, idx)
            scores.append(test_acc)
            members.append(model)
            histories.append(hist)

            # Reset tensorflow graph and create new one
            tf.keras.backend.clear_session()

        for idx,(history,model) in enumerate(zip(histories,members)):
            with open(save_path + 'history_' + str(idx) + '_T' + T_str, 'wb') as pickle_file:
                pickle.dump(history.history, pickle_file)
        print('Estimated accuracy: %.3f (%.3f)' % (np.mean(scores), np.std(scores)))
        print('Estimated accuracy is based on the validation data set whereas the validation loss is calculated from a hold-out part of the training data. Thus estimated accuracy is not expected to be equal to val_loss')

        # generates prediction of ensemble of models on test data set
        # doesnt currently use the dataGenerator and instead accesses data directly through dN_test and omegas_test
        def getEnsemblePrediction(members, dataGenerator):
            #predictions = [model.predict(dataGenerator) for model in members]
            predictions = np.empty((dN_test.shape[0], hypotheses.size, len(members)))
            #dataGenerator.reset()
            for idx,model in enumerate(members):
            #    pred = model.predict(dataGenerator)# predict on data
                pred = model.predict(dN_test)
                predictions[:,:,idx] = pred
            actual_values = omegas_test[:,0]

            # return mean of predicted probability by every model in members
            # converts one-hot rep. to frequency
            mean_predictions = hypotheses[np.argmax(np.mean(predictions,axis=2),axis=1)]

            return mean_predictions,actual_values

        def evaluate_n_members(members, n_members, dataGenerator):
            subset = members[:n_members]
            mean_predictions, actual_values = getEnsemblePrediction(subset, dataGenerator)
            acc = sklearn.metrics.accuracy_score(mean_predictions, actual_values)
            return acc

        ### Evaluating models on test data set
        test_data_generator = batchSequence(test_data_path, np.arange(N_trajs_test), nt, hypotheses, batch_size, timesteps, skip_step, modelStrategy, False)
        single_scores, ensemble_scores, result_list = list(), list(), list()
        for i in range(1, n_folds+1):
            ensemble_score = evaluate_n_members(members, i, test_data_generator)
            #test_data_generator.reset()
            #_, single_score = members[i-1].evaluate(test_data_generator, verbose=0)
            _, single_score = members[i-1].evaluate(dN_test, to_categorical(omegasEncoded_test[:,0]), verbose=0)
            result_list.append('> %d: single=%.3f, ensemble=%.3f' % (i, single_score, ensemble_score))
            ensemble_scores.append(ensemble_score)
            single_scores.append(single_score)
        for result in result_list:
            print(result)
        print('Accuracy %.3f (%.3f)' % (np.mean(single_scores), np.std(single_scores)))
        print('Accuracy was obtained using the test data set. So are the single scores and the ensemble scores.')

        ### Plotting result of evaluation
        xs = [i for i in range(1, n_folds+1)]
        fig = plt.figure()
        plt.plot(xs, single_scores, marker='o', linestyle='None',label='Single')
        plt.plot(xs, ensemble_scores, marker='o', label='Ensemble predictions')
        fig.savefig(save_path + 'ensemble_vs_single_T' + T_str + '.pdf')
        plt.close(fig)

        scores = np.array([single_scores, ensemble_scores])
        return members, scores, histories

    def getSingleModel():
        # not using test set atm - splitting training data into training and validation data using kfold and using the c++ simulated validation data as the test set
        test_data_generator = batchSequence(valid_data_path, np.arange(N_trajs_valid), nt, hypotheses, batch_size, timesteps, skip_step, modelStrategy, False)
        scores, members, histories = list(), list(), list()
        model = modelStrategy.defineModel()
        model.compile(loss='categorical_crossentropy',
                optimizer=my_optimizer, metrics=[my_metric])
        alpha = 9/10
        idx_fold = int(alpha*N_trajs_train) # takes a fraction 1-alpha of the training data as validation data
        train_data_generator = batchSequence(training_data_path, np.arange(idx_fold), nt, hypotheses, batch_size, timesteps, skip_step, modelStrategy, True)
        valid_data_generator = batchSequence(training_data_path, np.arange(idx_fold,N_trajs_train), nt, hypotheses, batch_size, timesteps, skip_step, modelStrategy, False)
        model, test_acc, hist = fit_model(model, train_data_generator, valid_data_generator,
                test_data_generator, num_epochs)

        # for loading the best model
        #model = load_model(save_path + 'temp_best_model.h5')
        #_, test_acc = model.evaluate(test_data_generator,
        #        use_multiprocessing=False,
        #        callbacks=[])
        #hist = 0 # there is no history if we just load the best model

        members.append(model)
        histories.append(hist)

        print('Estimated accuracy: %.3f ' % (test_acc))
        print('Estimated accuracy is based on the validation data set whereas the validation loss is calculated from a hold-out part of the training data. Thus estimated accuracy is not expected to be equal to val_loss')

        ### Evaluating models on test data set
        test_data_generator = batchSequence(test_data_path, np.arange(N_trajs_test), nt, hypotheses, batch_size, timesteps, skip_step, modelStrategy, False)
        scores = list()
        _, test_acc = model.evaluate(dN_test, to_categorical(omegasEncoded_test[:,0]), verbose=0)
        scores.append(test_acc)
        print('Accuracy %.3f' % (test_acc))
        print('Accuracy was obtained using the test data set.')

        return members, scores, histories

    n_folds = 10
    if use_kfold:
        members, scores, histories = do_KFold_CV(n_folds)
    else:
        members, scores, histories = getSingleModel()

    fig,axes = plt.subplots(len(histories))
    if (len(histories) == 1):
        axes.plot(histories[0].history['accuracy'], label='Training set')
        axes.plot(histories[0].history['val_accuracy'], label='Validation set')
        axes.set(xlabel='Epoch number', ylabel='Accuracy')
        #axes.set_ylim(0,5)
        axes.legend()
    else:
        for idx,hist in enumerate(histories):
            axes[idx].plot(hist.history['accuracy'], label='Training set')
            axes[idx].plot(hist.history['val_accuracy'], label='Validation set')
            axes[idx].set(ylabel='Accuracy')
            #axes[idx].set_ylim(0,5)
            axes[idx].legend()

        axes[axes.shape[0]-1].set(xlabel='Epoch number')
    fig.tight_layout()
    fig.savefig(save_path + '/training_history_T' + T_str + '.pdf')
    plt.close(fig)

    def get_test_predictions(num_batches_predict, data_generator, members, dummy_iters = 0):
        """
        Returns array of predictions.

        """
        dummy_iters = 0 # controls how many batches to skip
        num_predict = num_batches_predict # how many batches of test trajectories to do the prediction for

        mean_pred = np.zeros((num_predict,))
        pred = np.empty((num_predict,len(hypotheses),len(members)))

        print('Predicting on test data started...')
        actual_values = np.empty(num_predict)
        for i in range(num_predict):
            data = data_generator.__getitem__(i+dummy_iters)
            actual_values[i] = hypotheses[np.argmax(data[1])]
            for idx,model in enumerate(members):
                pred[i,:,idx] = model.predict(data[0])

        #return mean of predicted probability by every model in members
        pred_arr = np.mean(np.array(pred),axis=2)
        # converts one-hot rep. to frequency
        pred_arr = hypotheses[np.argmax(pred_arr,axis=1)]
        return pred_arr, actual_values

    # data generator on test set with batch size 1, so we have fine control over how many trajectories to use for prediction
    test_data_generator = batchSequence(test_data_path, np.arange(N_trajs_test), nt, hypotheses, 1, timesteps, skip_step, modelStrategy, False)

    # having initialized the data generator with 1 batch size, num_predict will equal number of trajectories to do the prediction for
    #num_predict = dN_test.shape[0]
    num_predict = 100
    pred_arr, actual_values = get_test_predictions(num_predict, test_data_generator, members)
    comparePredictedToTest = np.array((actual_values,pred_arr)).T
    print('Predicting on test data finished.')
    print('actual frequencies:, predicted frequencies:\n',comparePredictedToTest)
    pred_acc = np.mean(np.equal(comparePredictedToTest[:,0],comparePredictedToTest[:,1]))
    print('Accuracy in shown predictions: ',pred_acc)
    return members, scores, histories

def averageAndPlotInput(psi, dN, times, dataIndex):
    T_str = str(times[-1,0])
    T_str = T_str.replace('.','_')
    # plot first four individual trajectories
    for i in range(2):
        fig,axs = plt.subplots(2,1)
        axs[0] = plotPopulations(axs[0],times,(np.abs(psi[i,:,:])**2).T)
        axs[1] = plotClicks(axs[1],times,dN[i,:])
        fig.tight_layout()
        plt.savefig(save_path + "data_traj_T" + T_str + "_" + str(dataIndex) + "_" + str(i) + ".pdf")
        plt.close(fig)

    #### Fourier transform of signal
    #traj_idx = 0
    #fig, axes = plt.subplots(2)
    #T = times[-1]
    #N = dN[traj_idx,:].size
    #print("N: ", N)
    #fft_times = 2*np.pi*np.linspace(-0.5*N/T,0.5*N/T,N)
    #fft_dN = 2.0/N*np.fft.fftshift(np.fft.fft(dN[traj_idx,:]))
    #axes[0].plot(times, dN[traj_idx,:])
    #axes[1].plot(fft_times, np.abs(fft_dN))
    #fig.savefig(save_path + 'spectrum_T' + str(int(times[-1])) + '_' + str(dataIndex) + '_traj0.pdf')
    #plt.close(fig)

    #fig, axes = plt.subplots(2)
    #result = 0
    #L = N-1
    #C = np.empty((dN.shape[0],L))
    #for l in range(1,L):
    #    t_l = l
    #    print((1.0/(N-l)*np.sum([np.multiply(dN[:,t_i],dN[:,t_i+t_l]) for t_i in range(1,N-l)],axis=0)).shape)
    #    print((1.0/(N-l)*np.sum([np.multiply(dN[:,t_i],dN[:,t_i+t_l]) for t_i in range(1,N-l)],axis=0)).reshape(dN.shape[0]).shape)
    #    print("l:", l)

    #    C[:,l] = (1.0/(N-l)*np.sum([np.multiply(dN[:,t_i],dN[:,t_i+t_l]) for t_i in range(1,N-l)],axis=0)).reshape(dN.shape[0])
    #C_avg = np.mean(C,axis=0)
    #print(C_avg)
    #ls = np.arange(0,L)
    #axes[0].plot(ls,C_avg)
    #plt.show()

    psi_squared_avg = np.mean(np.abs(psi)**2,axis=0).T
    dN_avg = np.mean(dN,axis=0)

    fig,axs = plt.subplots(2,1)
    axs[0] = plotPopulations(axs[0],times,psi_squared_avg)
    axs[1] = plotClicks(axs[1],times,dN_avg)
    fig.tight_layout()
    plt.savefig(save_path + "data_T" + T_str + "_" + str(dataIndex) + ".pdf")
    plt.close(fig)
    if not (USE_HOMODYNE):
        input_signal = np.zeros(dN.shape)
        cumsum = np.cumsum(dN,axis=1)
        np.mod(cumsum,2,input_signal)
        input_signal = np.mean(input_signal,axis=0)
        fig,axs = plt.subplots(2,1)
        axs[0] = plotClicks(axs[0],times,dN_avg)
        axs[1].plot(times, input_signal)
        fig.tight_layout()
        plt.savefig(save_path + "clicksAndSignal" + "_T" + T_str + str(dataIndex) + ".pdf")
        plt.close(fig)


if __name__=='__main__':
    main()
