import numpy as np
import scipy as sp
from plottingTools import *
from scipy.linalg import expm

Delta = 0 # detuning
gamma = 1 # decay rate

def mesolve(t0 = 0, T = 10, nt = 10001, Omega=2, Delta = 0):
    dt = (T-t0)/(nt-1)

    # Initial state
    psi0 = np.array([[0],[1]],dtype='complex_') #[[c_e],[c_g]]
    rho0 = getDensityMatrixFromStateVector(psi0) # initial density matrix
    #rho0 = psi0 @ psi0.conj().T 
    #print(rho0)
    trace = np.array([[1],[0],[0],[1]]) # vectorized trace

    # Operators
    H = np.array([[-Delta/2, Omega/2],[Omega/2, Delta/2]]) # Hamiltonian
    C = np.sqrt(gamma)*np.array([[0,0],[1,0]])
    I = np.eye(2)

    CdagC = C.conj().T @ C
    #print('CdagC\n',CdagC)

    def getLindblad(rho,C):
        # np.kron gives kronecker product of two arrays
        # resulting in composite array with blocks of 
        # the second array scaled by the first
        return np.kron(C,C.conj()) - 0.5*(np.kron(CdagC,I) + np.kron(I,C.T@C.conj()))

    lindblad = getLindblad(rho0,C)

    def getLiouvillian(rho, H, lindblad):
        return -1j*(np.kron(H,I) - np.kron(I,H.T)) + lindblad # possibly sign on first term according to 1504.05266 on arxiv, something to do with us wanting the reverse order kronecker product

    liouvillian = getLiouvillian(rho0, H, lindblad)
    expLdt = expm(liouvillian*dt)
    #print('lindblad\n', lindblad)
    #print('liouvillian\n', liouvillian)
    #print('expLdt\n',expLdt)

    rho = np.zeros((4,nt),dtype='complex_')
    rho[:,0] = np.reshape(rho0,(4,))
    for i in range(1,nt):
        rho[:,i] = expLdt @ rho[:,i-1]
    #print('rho\n',rho[:,:2])

    rhoSS = sp.linalg.null_space(liouvillian)

    ### Attempt to calculate average over many independent samples of the homodyne current
    ### by using the Quantum Regression Theorem
    ### Not sure how to do this

    #Phi = np.pi/2
    #cPhi = C*np.exp(-1j*Phi)
    #xPhi = cPhi + cPhi.conj().T
    #xPhi = np.kron(xPhi,I) - np.kron(I,xPhi.T)
    #eta = 1
    #print(xPhi.shape)
    #print(expLdt.shape)
    #F = np.empty(shape=(times.size))
    #taus = np.linspace(0,12,100)
    #print("xPhi: ",xPhi)
    #for idx,tau in enumerate(taus):
    #    L_exp_tau = expm(getLiouvillian(rho[:,idx], H, getLindblad(rho[:,idx], C))*tau)
    #    print("L_exp_tau: ", L_exp_tau)
    #    print("(xPhi @ L_exp_tau @ xPhi): ", (xPhi @ L_exp_tau @ xPhi))
    #    print("rhoSS", rhoSS)
    #    print("((xPhi @ L_exp_tau @ xPhi) @ rhoSS): ", ((xPhi @ L_exp_tau @ xPhi) @ rhoSS))
    #    print("trace.T @ ((xPhi @ L_exp_tau @ xPhi) @ rhoSS): ", trace.T @ ((xPhi @ L_exp_tau @ xPhi) @ rhoSS))
    #    F[idx] = eta * (trace.T @ ((xPhi @ L_exp_tau @ xPhi) @ rhoSS))[0,0] + Delta
    #    print("F: ", F[idx])

    #fig = plt.Figure()
    #plt.plot(times, F)
    #plt.show()


    ### Attempt to implement bad estimator - need to ask Klaus about this.
    # Taking real part since some tiny imaginary part is present which should not
    # be there (populations should be real).
    #rhoSS_ee = np.real(rhoSS[0,0])
    #rhoSS = rhoSS/(trace.T@np.real(rhoSS))
    #print(rhoSS)
    #print('rhoSS_ee:',rhoSS_ee)

    #n_clicks = rhoSS_ee*gamma*T
    #print('#clicks',n_clicks)
    #print(rhoSS_ee/(1-2*rhoSS_ee))
    #print('estimated rabi frequency: ',np.sqrt(rhoSS_ee/(1-2*rhoSS_ee)))


    return rho

if __name__ == '__main__':
    t0 = 0
    T = 9
    nt = 9000
    Omega2 = 2 # Rabi frequency
    Omega6 = 6 # Rabi frequency
    times = np.linspace(t0,T,nt)
    rho2 = mesolve(t0, T, nt, Omega2)
    rho6 = mesolve(t0, T, nt, Omega6)
    fig = plt.figure()
    ax = plt.gca()
    plotPopulations(ax, times, rho2)
    plotPopulations(ax, times, rho6)
    plt.show()
    #fig = plotBlochSphere()
    #R = getBlochComponents(rho)
    #animateBlochVector(fig,times,rho,0)

    #fig2 = plotXYZ(times,R[0],R[1],R[2])
