from plottingTools import *
from FileManagement import *
import bayes
import mesolver
from sklearn.metrics import ConfusionMatrixDisplay
from sklearn.metrics import confusion_matrix
from mpl_toolkits.axes_grid1 import make_axes_locatable
from scipy.stats import cauchy
import pickle

from matplotlib.gridspec import GridSpec



use_clip_on = False

### Configuration for hypothesis testing
# {
TESTING = False
USE_HOMODYNE = True
dt = 0.001

save_path = "C:/Users/Henneberg/speciale_local/thesisFigures/"
saved_results_path = "C:/Users/Henneberg/speciale_local/"

if (TESTING):
    saved_results_path += 'testing/'

if (USE_HOMODYNE):
    data_path += 'homodyne/'
    saved_results_path += 'homodyne/'
else:
    data_path += 'counting/'
    saved_results_path += 'counting/'

dt_str = str(dt)
dt_str = dt_str.replace('.','_')

data_path += 'dt_' + dt_str + '/'
saved_results_path += 'dt_' + dt_str + '/'

# save path and hypotheses chosen
hypotheses = np.array((2,6))

single_omega_data_path = data_path + 'singleOmega/'
data_path += 'hyp' + str(hypotheses.size) + 'class/'
saved_results_path += 'hyp' + str(hypotheses.size) + 'class/'

# }

# Plot trajectory with detection record
def plotTrajAndDt(times, psi, D):
    fig,axs = plt.subplots(2,1)
    axs[0].set(xlabel='$\gamma t$',ylabel='$|c_e|^2$')
    axs[0].set_ylim([0,1])
    if USE_HOMODYNE:
        axs[0].plot(times,(np.abs(psi[:,0])**2))
        axs[1].plot(times,D,clip_on=use_clip_on,lw=0.5)
        axs[1].set(xlabel='$\gamma t$',ylabel=(('$\mathrm{d}Y_t$') ))
        axs[1].set_ylim([-0.2,0.2])
    else:
        axs[0].plot(times,(np.abs(psi[:,0])**2))
        axs[1].plot(times,D,clip_on=use_clip_on)
        axs[1].set(xlabel='$\gamma t$',ylabel=(('$\mathrm{d}N_t$') ))
        axs[1].set_ylim([0,1])

    #ax.tick_params(which='both',top=True,right=True)
    #axs[0].set_title(r"(a)",pad=13,left=True)
    #axs[1].set_title(r"(b)",pad=13,left=True)
    axs[0].set(xlabel=None,xticklabels=[None])

    axs[0].annotate(r"(a)", xy=(-0.22,0.92), xycoords='axes fraction')
    axs[1].annotate(r"(b)", xy=(-0.22,0.92), xycoords='axes fraction')
    axs[0].set_xlim([0,times[-1]])
    axs[1].set_xlim([0,times[-1]])
    enchantAx(axs[0])
    enchantAx(axs[1])
    fig.set_size_inches(5.2, 3)
    plt.tight_layout()
    #axs[1].set(clip_on=False)
    if (USE_HOMODYNE):
        plt.savefig(save_path + 'homodyneTrajDetection.pdf',dpi=200)
    else:
        plt.savefig(save_path + 'countingTrajDetection.pdf',dpi=200)

def plotMasterEquation():
    t0 = 0
    T = 8
    nt = 8001
    times = np.linspace(t0,T,nt)
    Delta = [0, 1, 2]
    markers = ['-', '--', ':']
    labels = ['$ \delta=0$', '$ \delta=\Omega $', '$ \delta=2 \Omega $']

    fig,axs = plt.subplots(2,1)
    for i in range(hypotheses.size):
        #axs[i].set(xlabel='$\gamma t$')
        axs[i].set(ylabel=r'$\rho_{ee}$')
        enchantAx(axs[i])
        axs[i].set_xlim(0,8)
        axs[i].set_ylim(0,1)
        for j in range(len(Delta)):
            rho = mesolver.mesolve(t0,T,nt,hypotheses[i],Delta[j]*hypotheses[i])
            axs[i].plot(times,np.real(rho[0]),markers[j],label=labels[j])

    #axs[0].set(ylabel=r'$\rho_{ee}$')
    #axs[0].set_title(r"(a)",pad=13)
    #axs[1].set_title(r"(b)",pad=13)
    #axs[1].set(ylabel=None, yticklabels=[None])
    #axs[1].set(xlabel='$\gamma t$')

    axs[0].annotate(r"(a)", xy=(-0.22,0.92), xycoords='axes fraction')
    axs[1].annotate(r"(b)", xy=(-0.22,0.92), xycoords='axes fraction')
    axs[0].set(xlabel=None, xticklabels=[None])
    axs[1].set(xlabel='$\gamma t$')

    #l4 = axs[0].legend(bbox_to_anchor=(0,1.15,1,0.2), loc="lower right", borderaxespad=0, ncol=3)
    axs[0].legend(ncol=3,frameon=False)
    fig.set_size_inches(5.4, 3.5)
    plt.tight_layout()
    plt.savefig(save_path + 'masterEquation.pdf')
    plt.show()


# Omega=6
def plotMasterEqComparison(times,psi):

    def plotShort(ax, times, psi, trueRho, max_idx):
        psi_squared_avg = np.mean(np.abs(psi[:max_idx,:,0])**2,axis=0)
        ax.plot(times,psi_squared_avg,label=r'$\mathbf{E}(| \psi _{ N_t } \rangle \langle \psi _{ N_t } |)$')
        ax.plot(times,np.real(trueRho[0]),'--',label=r'$\rho$')
        ax.set_xlim([0,times[-1]])
        ax.set_ylim([0,1])
        enchantAx(ax)

    
    rho = mesolver.mesolve(0,times[-1],times.size,hypotheses[1])
    fig,axs = plt.subplots(3,1)
    
    N = [10, 100, 1000]
    text = ['(a)','(b)','(c)']
    for i in range(3):
        plotShort(axs[i],times,psi,rho,N[i])
        axs[i].annotate(text[i], xy=(-0.22,0.92), xycoords='axes fraction')
        axs[i].set(ylabel='$|c_e|^2$')

    axs[0].set(xlabel=None,xticklabels=[None])
    axs[1].set(xlabel=None,xticklabels=[None])
    #axs[-1].legend
    #l1 = axs[0].legend(bbox_to_anchor=(1.04,1), borderaxespad=0)
    #l5 = plt.legend(bbox_to_anchor=(1,0), loc="lower right", bbox_transform=fig.transFigure, ncol=2)
    #l3 = plt.legend(bbox_to_anchor=(1.04,0.5), loc="center left", borderaxespad=0)
    #l4 = axs[0].legend(bbox_to_anchor=(0,1.15,1,0.2), loc="lower left", mode="expand", borderaxespad=0, ncol=2)
    l4 = axs[0].legend(bbox_to_anchor=(0,1.15,1,0.2), loc="lower right", borderaxespad=0, ncol=2)
    axs[-1].set(xlabel='$\gamma t$')
    fig.set_size_inches(5.2, 4)
    plt.tight_layout()
    plt.savefig(save_path + 'masterEqComparison.pdf',dpi=100)
    plt.show()

def plotMollowTriplet(times,signal):
    #print(signal.shape)
    #signal_avg = np.mean(signal,axis=0)
    ##### Fourier transform of signal
    ##traj_idx = 0
    ##fig, axes = plt.subplots(2)
    #consider last half of signal
    nt = times.size
    #idx_cut = int(times.size/2)
    idx_cut = 0
    #idx_cut = 3000
    times = times[idx_cut:-1]
    signal = signal[:9000,idx_cut:-1]
    T = times[-1]
    nt = times.size
    print(nt)
    
    #print("nt: ", nt)
    #fft_times = 2*np.pi*np.linspace(-0.5*nt/T,0.5*nt/T,N)
    #fft_dN = 2.0/nt*np.fft.fftshift(np.fft.fft(signal_avg))
    #axes[0].plot(times, signal_avg)
    #axes[1].plot(fft_times, np.abs(fft_dN))
    ##fig.savefig(save_path + 'spectrum' + T_str + '_' + str(dataIndex) + '_traj0.pdf')
    ##plt.close(fig)
    #plt.show()

    fig, axes = plt.subplots(2)
    result = 0
    L = nt-1
    C = np.empty((signal.shape[0],L))
    for l in range(1,L):
        t_l = l
        #print((1.0/(nt-l)*np.sum([np.multiply(signal[:,t_i],signal[:,t_i+t_l]) for t_i in range(1,nt-l)],axis=0)).shape)
        #print((1.0/(nt-l)*np.sum([np.multiply(signal[:,t_i],signal[:,t_i+t_l]) for t_i in range(1,nt-l)],axis=0)).reshape(signal.shape[0]).shape)
        #print("l:", l)
        print(np.array([np.multiply(signal[:,t_i],signal[:,t_i+t_l]) for t_i in range(1,nt-l)]).shape)

        C[:,l] = ((1.0/((nt-l)*dt**2))*np.sum([np.multiply(signal[:,t_i],signal[:,t_i+t_l]) for t_i in range(1,nt-l)],axis=0)).reshape(signal.shape[0])
    C_avg = np.mean(C,axis=0)
    C_avg[0] = 1
    np.save('C_avg',C_avg)
    #print(C_avg)
    ls = np.arange(0,L)
    axes[0].plot(ls*dt,C_avg)
    #print("nt: ", nt)
    nt = nt-1
    fft_times = 2*np.pi*np.linspace(-0.5*nt/T,0.5*nt/T,nt)
    fft_dN = 2.0/nt*np.fft.fftshift(np.fft.fft(C_avg))
    #axes[0].plot(times, C_avg)
    axes[1].plot(fft_times, np.abs(fft_dN))
    #fig.savefig(save_path + 'spectrum' + T_str + '_' + str(dataIndex) + '_traj0.pdf')
    #plt.close(fig)
    #plt.show()
    plt.show()

def plotBayes2hyp(times, psi, signal, use_coarse_grained):
    Delta = 0
    gamma = 1
    Phi = np.pi/2
    hypotheses = np.array([2,6])

    times, psi, signal, P = bayes.bayesGeneralPredict(times, psi, signal, hypotheses, Delta, gamma, Phi, USE_HOMODYNE, use_coarse_grained)

    fig,axs = plt.subplots(2,1)
    #populations = np.square(np.abs(psi))
    #axes[1,0] = plotPopulations(axes[1,0], times, populations)
    axs[1].plot(times, P[0], label='$h_0$', clip_on=use_clip_on, color='C2')
    axs[1].plot(times, P[1], label='$h_1$', clip_on=use_clip_on, color='C1')
    if USE_HOMODYNE:
        axs[0].plot(times, signal[0], clip_on=use_clip_on, color='C0',lw=0.5)
        axs[0].set(ylabel=(('$\mathrm{d}Y_t$') ))
        axs[0].set_ylim([-0.2,0.2])
        axs[1].set(ylabel='$p_t(h_i | Y_t)$')
    else:
        axs[0].plot(times, signal[0], clip_on=use_clip_on, color='C0')
        axs[0].set(ylabel=(('$\mathrm{d}N_t$') ))
        axs[0].set_ylim([0,1])
        axs[1].set(ylabel='$p_t(h_i | N_t)$')

    axs[1].set(xlabel='$\gamma t$')
    axs[0].set(xlabel=None,xticklabels=[None])
    axs[0].annotate(r"(a)", xy=(-0.22,0.92), xycoords='axes fraction')
    axs[1].annotate(r"(b)", xy=(-0.22,0.92), xycoords='axes fraction')

    axs[1].legend(frameon=False,loc='center right')


    axs[1].set_ylim([0,1])
    axs[0].set_xlim([0,times[-1]])
    axs[1].set_xlim([0,times[-1]])
    enchantAx(axs[0])
    enchantAx(axs[1])
    fig.set_size_inches(5.2, 3)
    plt.tight_layout()

    if (USE_HOMODYNE):
        plt.savefig(save_path + 'bayesHomodyne2Hyp.pdf',dpi=200)
    else:
        plt.savefig(save_path + 'bayesCounting2Hyp.pdf',dpi=200)

    plt.show()

def plotBayesColorHomodyne(times, psi, signal):
    use_coarse_grained = False
    Delta = 0
    gamma = 1
    Phi = np.pi/2
    #hypotheses = np.linspace(2,14,61)
    hypotheses = np.linspace(2,14,241)
    true_idx = 80
    print('true hypotheses: ', hypotheses[true_idx])
    print('true hypotheses -1 : ', hypotheses[true_idx-1])
    print('true hypotheses +1: ', hypotheses[true_idx+1])

    times, psi, signal, P = bayes.bayesGeneralPredict(times, psi, signal, hypotheses, Delta, gamma, Phi, USE_HOMODYNE, use_coarse_grained)
    fig,axs = plt.subplots(2,1)
    bayes.plot_bayesTest(axs[0], signal, times, P, hypotheses, true_idx)

    Phi = 0
    phi0_data_path = '../cpp_stuff/build/data/homodyne/dt_0_001/singleOmega/test_Phi0_seed33_T15.h5'
    psi_phi0,signal_phi0,times_phi0,omegas_phi0 = read_data(phi0_data_path, np.arange(0), np.arange(end_idx))

    times_phi0, psi_phi0, signal_phi0, P_phi0 = bayes.bayesGeneralPredict(times_phi0, psi_phi0[1], signal_phi0[1], hypotheses, Delta, gamma, Phi, USE_HOMODYNE, use_coarse_grained)
    bayes.plot_bayesTest(axs[1], signal_phi0, times_phi0, P_phi0, hypotheses, true_idx)

    axs[0].set(xlabel=None,xticklabels=[None])
    axs[0].annotate(r"(a)", xy=(-0.22,0.92), xycoords='axes fraction')
    axs[1].annotate(r"(b)", xy=(-0.22,0.92), xycoords='axes fraction')

    fig.set_size_inches(5.2, 4.5)
    plt.tight_layout()
    plt.savefig(save_path + 'bayesColor.pdf',dpi=100)

    plt.show()

def plotBayesColorCounting(times, psi, signal):
    use_coarse_grained = False
    Delta = 0
    gamma = 1
    Phi = np.pi/2
    hypotheses = np.linspace(2,14,241)
    true_idx = 80
    print('True hyp: ', hypotheses[true_idx])

    times, psi, signal, P = bayes.bayesGeneralPredict(times, psi, signal, hypotheses, Delta, gamma, Phi, USE_HOMODYNE, use_coarse_grained)
    fig = plt.figure()
    ax = plt.gca()
    bayes.plot_bayesTest(ax, signal, times, P, hypotheses, true_idx)

    fig.set_size_inches(5.2, 2.7)
    plt.tight_layout()
    plt.savefig(save_path + 'bayesColorCounting.pdf',dpi=100)

    plt.show()

def plotConfusionMatrices():
    fig,axs = plt.subplots(2,4)
    Ts = [1, 3, 8, 15]
    #Ts = [1]

    def plotConfusionMatrix(ax, pred, actual):
        display_labels = np.array(['$h_0$','$ h_1 $'])
        sample_weight = None
        normalize = 'all'
        include_values = True
        xticks_rotation = 'horizontal'
        values_format = '.2f'
        cmap = 'Blues'
        #ax = None

        cm = confusion_matrix(actual, pred, labels=None, sample_weight=sample_weight, normalize=normalize)
        disp = ConfusionMatrixDisplay(confusion_matrix=cm, display_labels=display_labels)
        disp.plot(include_values=include_values, cmap=cmap, ax=ax, xticks_rotation=xticks_rotation, values_format=values_format)
        im = ax.images
        cb = im[-1].colorbar
        cb.remove()
        disp.im_.set_clim(vmin=0,vmax=0.5)

        #if i==3:
            #divider = make_axes_locatable(ax)
            #cax = divider.append_axes("right", size="5%", pad=0.05)
            #plt.colorbar(disp.im_, cax=cax)
        #fig = disp.figure_
        #fig.set_size_inches(2.5, 2.5)
        #plt.tight_layout(rect=[0,0,0.85,1])
        return ax, disp.im_

    ims_nn = list()
    ims_bayes = list()
    for i,T in enumerate(Ts):
        T_str = T_ToFileNameString(T)
        pred0 = np.load(saved_results_path + 'pred_NN' + str(hypotheses[0]) + T_str + '.npy')
        pred1 = np.load(saved_results_path + 'pred_NN' + str(hypotheses[1]) + T_str + '.npy')
        pred_bayes0 = np.load(saved_results_path + 'pred_bayes' + str(hypotheses[0]) + T_str + '.npy')
        pred_bayes1 = np.load(saved_results_path + 'pred_bayes' + str(hypotheses[1]) + T_str + '.npy')
        #pred_bayes0 = np.load(save_path + 'pred_bayes' + str(hypotheses[0]) + T_ToFileNameString(times_+ '.npy')
        #pred_bayes1 = np.load(save_path + 'pred_bayes' + str(hypotheses[1]) + T_str + '.npy')

        #pred0_T9 = np.load(save_path + 'pred_NN' + str(hypotheses[0]) + '_bestT9_T9' + '.npy')
        #pred1_T9 = np.load(save_path + 'pred_NN' + str(hypotheses[1]) + '_bestT9_T9' + '.npy')

        axs[0,i], im_nn = plotConfusionMatrix(axs[0,i],np.concatenate((pred0,pred1)), np.concatenate((hypotheses[0]*np.ones(pred0.shape),hypotheses[1]*np.ones(pred1.shape))))
        axs[0,i].set(xlabel=None,xticklabels=[None])
        axs[1,i], im_bayes = plotConfusionMatrix(axs[1,i],np.concatenate((pred_bayes0,pred_bayes1)),np.concatenate((hypotheses[0]*np.ones(pred0.shape),hypotheses[1]*np.ones(pred1.shape))))
        axs[0,i].set(title='$T= $'+str(T))
        axs[1,i].set(xlabel=r'$h_{\mathrm{pred}}$')
        ims_nn.append(im_nn)
        ims_bayes.append(im_bayes)
        if i>0:
            #axs[0,i].tick_params(axis='x',which='both',bottom=False,left=False)
            #axs[0,i].tick_params(axis='y',which='both',left=False)
            axs[0,i].set(ylabel=None,yticklabels=[None])
            #axs[1,i].tick_params(axis='x',which='both',bottom=True,left=False)
            #axs[1,i].tick_params(axis='y',which='both',left=False)
            axs[1,i].set(ylabel=None,yticklabels=[None])


    axs[0,0].annotate(r"(a)", xy=(-0.32,1.1), xycoords='axes fraction')
    axs[1,0].annotate(r"(b)", xy=(-0.32,1.1), xycoords='axes fraction')
    axs[0,0].set(ylabel=r'$h_{\mathrm{true}}$')
    axs[1,0].set(ylabel=r'$h_{\mathrm{true}}$')

    #fig.colorbar(ims_nn[3], ax=axs.ravel().tolist())
    cbar_ax = fig.add_axes([0.85, 0.22, 0.025, 0.575])
    fig.colorbar(ims_nn[3], cax=cbar_ax)
    
    #plt.tight_layout(h_pad=-2)
    plt.tight_layout(w_pad= 1.59,h_pad=-2,rect=[0,0,0.85,1])
    #fig.subplots_adjust(right=0.95)
    if USE_HOMODYNE:
        fig.savefig(save_path + 'confusion_matrix.pdf',dpi=100)
    else:
        fig.savefig(save_path + 'confusion_matrix_counting.pdf',dpi=100)
    plt.show()

def plotBinnedTrajAndRecord(times, psi, D, true_idx):
    hypotheses = np.array([2,6])
    Delta = 0
    Phi = np.pi/2
    gamma = 1
    times = times[:-1]
    nt = times.size
    psi = psi[:nt,:]
    D = D[:nt,:]

    use_coarse_grained = False
    binningFactor = 1
    signal_nobin, psi_nobin, times, times, P_nobin = bayes.bayesTestNoSim(times, psi, D, hypotheses, USE_HOMODYNE, use_coarse_grained, binningFactor)

    fig,axs = plt.subplots(3,2)

    axs[0,0].plot(times,(np.abs(psi_nobin[true_idx,0,:])**2), clip_on=use_clip_on)
    axs[1,0].plot(times, signal_nobin[0,:,0], clip_on=use_clip_on, lw=0.5)
    # These are equivalent to the above
    #axs[0,0].plot(times,(np.abs(psi[:,0])**2),'--')
    #axs[1,0].plot(times, D, clip_on=use_clip_on)

    axs[2,0].plot(times, P_nobin[0,:], label='$h_0$', color='C1', clip_on=use_clip_on)
    axs[2,0].plot(times, P_nobin[1,:], label='$h_1$', color='C2', clip_on=use_clip_on)

    ### Binned data
    use_coarse_grained = True
    binningFactor = 64
    signal_binned, psi_binned, times, times_binned, P_binned = bayes.bayesTestNoSim(times, psi, D, hypotheses, USE_HOMODYNE, use_coarse_grained, binningFactor)
    
    axs[0,1].plot(times,(np.abs(psi_binned[true_idx,0,:])**2), clip_on=use_clip_on)
    axs[1,1].plot(times, signal_binned[0,:,0], clip_on=use_clip_on, lw=0.5)
    axs[2,1].plot(times, P_binned[0,:], label='$h_0$', color='C1', clip_on=use_clip_on)
    axs[2,1].plot(times, P_binned[1,:], label='$h_1$', color='C2', clip_on=use_clip_on)

    for i in range(2):
        for j in range(2):
            axs[i,j].set_xlim([0,8])
            axs[i,j].set(xlabel=None,xticklabels=[None])
            enchantAx(axs[i,j])

    axs[2,1].set_xlim([0,8])
    axs[2,0].set_xlim([0,8])
    enchantAx(axs[2,0])
    enchantAx(axs[2,1])

    axs[0,0].set_ylim([0,1])
    axs[0,1].set_ylim([0,1])
    axs[0,0].set(ylabel='$|c_e|^2$')

    if USE_HOMODYNE:
        axs[1,0].set(ylabel=(('$\mathrm{d}Y_t$') ))
        axs[1,0].set_ylim([-0.2,0.2])
        axs[1,1].set_ylim([-0.013,0.013])
        axs[2,0].set(ylabel='$p_t(h_i | Y_t)$')

    axs[2,0].set_ylim([0,1])
    axs[2,1].set_ylim([0,1])

    axs[2,0].set(xlabel='$\gamma t$')
    axs[2,1].set(xlabel='$\gamma t$')

    axs[0,0].set_title(r"(a)",pad=13)
    axs[0,1].set_title(r"(b)",pad=13)

    axs[2,0].legend(frameon=False)
    axs[2,1].legend(frameon=False)

    fig.set_size_inches(5.2, 4)
    plt.tight_layout()

    if (USE_HOMODYNE):
        plt.savefig(save_path + 'homodyneTrajDetectionBinned.pdf',dpi=200)
    else:
        plt.savefig(save_path + 'countingTrajDetectionBinned.pdf',dpi=200)

    # Make another plot with even further binning 400 and 800

    use_coarse_grained = True
    binningFactor = 400
    #signal_nobin, psi_nobin, times, times, P_nobin = bayes.bayesTestNoSim(times, psi, D, hypotheses, USE_HOMODYNE, use_coarse_grained, binningFactor)

    fig,axs = plt.subplots(3,2)

    signal_binned, psi_binned, times, times_binned, P_binned = bayes.bayesTestNoSim(times, psi, D, hypotheses, USE_HOMODYNE, use_coarse_grained, binningFactor)
    
    axs[0,0].plot(times,(np.abs(psi_binned[true_idx,0,:])**2), clip_on=use_clip_on)
    axs[1,0].plot(times, signal_binned[0,:,0], clip_on=use_clip_on, lw=0.5)
    axs[2,0].plot(times, P_binned[0,:], label='$h_0$', color='C1', clip_on=use_clip_on)
    axs[2,0].plot(times, P_binned[1,:], label='$h_1$', color='C2', clip_on=use_clip_on)

    #axs[0,0].plot(times,(np.abs(psi_nobin[true_idx,0,:])**2), clip_on=use_clip_on)
    #axs[1,0].plot(times, signal_nobin[0,:,0], clip_on=use_clip_on, lw=0.5)

    #axs[2,0].plot(times, P_nobin[0,:], label='$h_0$', color='C1', clip_on=use_clip_on)
    #axs[2,0].plot(times, P_nobin[1,:], label='$h_1$', color='C2', clip_on=use_clip_on)

    binningFactor = 800
    signal_binned, psi_binned, times, times_binned, P_binned = bayes.bayesTestNoSim(times, psi, D, hypotheses, USE_HOMODYNE, use_coarse_grained, binningFactor)
    
    axs[0,1].plot(times,(np.abs(psi_binned[true_idx,0,:])**2), clip_on=use_clip_on)
    axs[1,1].plot(times, signal_binned[0,:,0], clip_on=use_clip_on, lw=0.5)
    axs[2,1].plot(times, P_binned[0,:], label='$h_0$', color='C1', clip_on=use_clip_on)
    axs[2,1].plot(times, P_binned[1,:], label='$h_1$', color='C2', clip_on=use_clip_on)

    for i in range(2):
        for j in range(2):
            axs[i,j].set_xlim([0,8])
            axs[i,j].set(xlabel=None,xticklabels=[None])
            enchantAx(axs[i,j])

    axs[2,1].set_xlim([0,8])
    axs[2,0].set_xlim([0,8])
    enchantAx(axs[2,0])
    enchantAx(axs[2,1])

    axs[0,0].set_ylim([0,1])
    axs[0,1].set_ylim([0,1])
    axs[0,0].set(ylabel='$|c_e|^2$')

    if USE_HOMODYNE:
        axs[1,0].set(ylabel=(('$\mathrm{d}Y_t$') ))
        axs[1,0].set_ylim([-0.004,0.004])
        axs[1,1].set_ylim([-0.004,0.004])
        axs[2,0].set(ylabel='$p_t(h_i | Y_t)$')

    axs[2,0].set_ylim([0,1])
    axs[2,1].set_ylim([0,1])

    axs[2,0].set(xlabel='$\gamma t$')
    axs[2,1].set(xlabel='$\gamma t$')

    axs[0,0].set_title(r"(a)",pad=13)
    axs[0,1].set_title(r"(b)",pad=13)

    axs[0,1].set(yticklabels=[None])
    axs[1,1].set(yticklabels=[None])
    axs[2,1].set(yticklabels=[None])

    axs[2,0].legend(frameon=False,loc='right', bbox_to_anchor=(1.07, 0.46))
    axs[2,1].legend(frameon=False,loc='right', bbox_to_anchor=(1.07, 0.46))
    #axs[2,1].legend(frameon=False,loc='best')
    #axs[2,1].legend(frameon=False,loc='right', bbox_to_anchor=(0.5, 0.5))

    fig.set_size_inches(5.2, 4)
    plt.tight_layout()

    if (USE_HOMODYNE):
        plt.savefig(save_path + 'homodyneTrajDetectionBinnedMore.pdf',dpi=200)

    plt.show()

def plotBayesBinnedVsNNbinned(times, signal):
    times = times[:-1]
    nt = times.size
    signal = signal[:-1]
    signal = signal.reshape((1,nt,1))
    binningFactor = 64
    fig, axs = plt.subplots(2,1)
    signal_binned, times_binned = binSignal(signal, times, binningFactor)
    axs[0].plot(times_binned, signal_binned[0,:,0], clip_on=use_clip_on)
    signal_binned_bayes = bayes.bayes_binning(signal_binned, times, binningFactor)
    print(signal_binned_bayes.shape)
    axs[1].plot(times, signal_binned_bayes[0,:,0], clip_on=use_clip_on)

    axs[0].set_xlim([0,8])
    axs[1].set_xlim([0,8])
    axs[0].set(xticklabels=[None])

    axs[0].annotate(r"(a)", xy=(-0.22,0.92), xycoords='axes fraction')
    axs[1].annotate(r"(b)", xy=(-0.22,0.92), xycoords='axes fraction')
    enchantAx(axs[0])
    enchantAx(axs[1])
    axs[1].set(xlabel='$ \gamma t$')
    axs[0].set(ylabel='$ \mathrm{d} Y_t $')
    axs[1].set(ylabel='$ \mathrm{d} Y_t $')

    fig.set_size_inches(5.2, 3)
    plt.tight_layout()

    if (USE_HOMODYNE):
        plt.savefig(save_path + 'homodyneBinnedBayesVsNN.pdf',dpi=100)
    else:
        plt.savefig(save_path + 'countingBinnedBayesVsNN.pdf',dpi=100)

    plt.show()

def plotScatter():
    from tensorflow.keras.models import load_model
    from ParameterRegression import calcAccuracy
    from sklearn.metrics import mean_squared_error
    Ts = [1, 3, 8, 15]
    Ts = [1, 8, 15, 50]

    if USE_HOMODYNE:
        scatter_data_path_T50 = "../cpp_stuff/build/data/homodyne/dt_0_001/linspaced/final_regression_test_T50.h5"
        scatter_data_path = "../cpp_stuff/build/data/homodyne/dt_0_001/linspaced/final_regression_test_T15.h5"
        saved_model_path = "C:/Users/Henneberg/speciale_local/homodyne/dt_0_001/regression/"
    else:
        scatter_data_path_T50 = "../cpp_stuff/build/data/counting/dt_0_001/linspaced/final_regression_test_T50.h5"
        scatter_data_path = "../cpp_stuff/build/data/counting/dt_0_001/linspaced/final_regression_test_T15.h5"
        saved_model_path = "C:/Users/Henneberg/speciale_local/counting/dt_0_001/regression/"

    dN_test_allT,omegas_test_allT = read_data_signalsAndFrequencies(scatter_data_path, np.arange(0), np.arange(0))

    actual_lst, pred_lst, mae_lst, rmse_list = list(), list(), list(), list()

    for idx,T in enumerate(Ts):
        T_str = T_ToFileNameString(T)
        max_T_str = T_ToFileNameString(Ts[-1])

        # New method. Just using the same trajectories for different T by only loading parts of
        # long trajectories.
        end_idx = get_end_idx(T,dt)
        max_T_str = T_ToFileNameString(15)
        filename_suffix = max_T_str + '.h5'

        scores = np.load(saved_model_path + 'RegressionScores' + T_str + '.npy')
        if (scores.size > 1):
            print(scores[0,:])
            best_model_idx = np.argmin(scores[0,:],axis=1)
            print(best_model_idx)
        else:
            best_model_idx = 0
        # Use list of scores to choose best model (requires all T's to have been run
        # in a single instance of the program - otherwise the RegressionScores array
        # does not contain the scores for the Ts excluded in that run)
        model = load_model(saved_model_path + 'best_model_' + str(best_model_idx) + T_str +'.h5')

        if (T==50):
            dN_test, omegas_test = read_data_signalsAndFrequencies(scatter_data_path_T50, np.arange(0), np.arange(0))
        else:
            dN_test, omegas_test = dN_test_allT[:,:end_idx],omegas_test_allT[:,:end_idx]
        actual = omegas_test[:,0]
        print(dN_test.shape)
        print(omegas_test.shape)

        pred = model.predict(dN_test)
        mae = calcAccuracy(pred, actual, '', False)

        # compute standard deviation
        rmse = mean_squared_error(actual, pred, squared=False)
        actual_lst.append(actual)
        pred_lst.append(pred)
        mae_lst.append(mae)
        print(T_str,', MAE: ', mae)
        print(T_str,', rmse: ', rmse)
        #plt.errorbar(actual,pred,yerr=scores[idx,0,best_indices[idx]],fmt='.')


    fig, axs = plt.subplots(2,2)
    #plt.xlim(1.9,14.1)
    #plt.ylim(1.9,14.1)
    axs[0,0].set(ylabel='$\Omega_{\mathrm{true}}$')
    axs[1,0].set(xlabel='$\Omega_{\mathrm{est}}$', ylabel='$\Omega_{\mathrm{true}}$')
    axs[1,1].set(xlabel='$\Omega_{\mathrm{est}}$')
    axs[0,0].set(xticklabels=[None])
    axs[0,1].set(xticklabels=[None])
    axs[0,1].set(yticklabels=[None])
    axs[1,1].set(yticklabels=[None])
    for i,ax in enumerate(axs.flat):
        ax.plot(pred_lst[i],actual_lst[i],'k,',label='T='+str(Ts[i]))
        enchantAx(ax)
        ax.set_xlim([1,17])
        ax.set_ylim([1,17])
        #ax.legend()
    axs[0,0].annotate(r"(a)", xy=(-0.32,0.92), xycoords='axes fraction')
    axs[0,1].annotate(r"(b)", xy=(-0.22,0.92), xycoords='axes fraction')
    axs[1,0].annotate(r"(c)", xy=(-0.32,0.92), xycoords='axes fraction')
    axs[1,1].annotate(r"(d)", xy=(-0.22,0.92), xycoords='axes fraction')
    fig.set_size_inches(5.2, 4)
    plt.tight_layout()
    plt.savefig(save_path + 'scatter.pdf')
    plt.show()

def plotLorentz(times):
    #alphas = [0.5,0.75,1,1.25,1.5]
    alphas = [2, 8, 32, 128, 512, 2048]
    linestyles = ['-', '--', '-.', ':','-','--','-.']
    dt = times[1]-times[0]
    nt = times.size
    print('dt=',dt)
    #fft_times = 2*np.pi*np.linspace(0,nt/T, nt)
    #fft_times = np.linspace(0,1/dt, nt)
    fft_times = np.fft.fftfreq(nt,dt)
    fft_times = 2*np.pi*np.fft.fftshift(fft_times)
    print(fft_times.shape)
    

    fig = plt.figure()
    ax = plt.gca()
    #mu = 0
    for i,alpha in enumerate(alphas):
        scaledLorentz = (alpha**2)/(alpha**2+fft_times**2)
        #rv = cauchy(mu,alpha)
        #ax.plot(fft_times,rv.pdf(fft_times),label=r'$\alpha = %.2f$'%alpha,linestyle=linestyles[i])
        ax.plot(fft_times,scaledLorentz,label=r'$\alpha / \gamma = %.i$'%alpha,linestyle=linestyles[i])
        enchantAx(ax)
    ax.set(xlabel=r'$\omega / \gamma$ ',ylabel=r'$ f( \omega)  $')

    ax.set_xlim([-500,500])

    l4 = ax.legend(bbox_to_anchor=(0,1.15,1,0.2), loc="lower right", borderaxespad=0, ncol=3)
    #ax.legend()
    fig.set_size_inches(5.5, 3.6)
    plt.tight_layout()
    plt.savefig(save_path + 'lorentz.pdf')
    plt.show()

def plotHistories():
    # Plot training history for hyptesting
    Ts = [0.25, 1, 3, 5, 8, 15]
    annotations = ['(a)', '(b)', '(c)', '(d)', '(e)', '(f)']
    fig,axs = plt.subplots(3,2)
    histories = list()
    for i,T in enumerate(Ts):
        with open(saved_results_path + 'history' + T_ToFileNameString(T), 'rb') as pickle_file:
            hist = pickle.load(pickle_file)

        histories.append(hist)
        axs.flat[i].plot(hist['accuracy'], label=r'$ \mathcal{D}_{\mathrm{train}}$')
        axs.flat[i].plot(hist['val_accuracy'], label=r'$ \mathcal{D}_{\mathrm{valid}}$')
        axs.flat[i].annotate(annotations[i], xy=(-0.45,0.98), xycoords='axes fraction')
        max_acc_idx = np.argmax(hist['val_accuracy'])
        axs.flat[i].plot(max_acc_idx,hist['val_accuracy'][max_acc_idx],'kx')
        if (i == 2 or i==3):
            axs.flat[i].text(0.05,0.95,r'$T=%.i$'%Ts[i],transform=axs.flat[i].transAxes,horizontalalignment='left',verticalalignment='top')
        elif (i==0):
            axs.flat[i].text(0.95,0.01,r'$T=%.2f$'%Ts[i],transform=axs.flat[i].transAxes,horizontalalignment='right',verticalalignment='bottom')
        else:
            axs.flat[i].text(0.95,0.01,r'$T=%.i$'%Ts[i],transform=axs.flat[i].transAxes,horizontalalignment='right',verticalalignment='bottom')
        enchantAx(axs.flat[i])
        #axs.flat[i].set(xlabel='Epoch', ylabel=r'$A_{\mathcal{D}}$')
        #axes.set_ylim(0,5)

    axs[0,0].set(ylabel=r'$A_{\mathcal{D}}$')
    axs[1,0].set(ylabel=r'$A_{\mathcal{D}}$')
    axs[2,0].set(xlabel='Epoch', ylabel=r'$A_{\mathcal{D}}$')
    axs[2,1].set(xlabel='Epoch')
    #l1 = axs[0].legend(bbox_to_anchor=(1.04,1), borderaxespad=0)
    #l5 = plt.legend(bbox_to_anchor=(1,0), loc="lower right", bbox_transform=fig.transFigure, ncol=2)
    #l3 = plt.legend(bbox_to_anchor=(1.04,0.5), loc="center left", borderaxespad=0)
    #l4 = axs[0].legend(bbox_to_anchor=(0,1.15,1,0.2), loc="lower left", mode="expand", borderaxespad=0, ncol=2)
    l4 = axs[0,1].legend(bbox_to_anchor=(0,1.15,1,0.2), loc="lower right", borderaxespad=0, ncol=2)

    fig.set_size_inches(6, 6.2)
    plt.tight_layout()
    if (USE_HOMODYNE):
        plt.savefig(save_path + 'training_hyp2_homodyne.pdf')
    else:
        plt.savefig(save_path + 'training_hyp2_counting.pdf')
    #plt.show()

    # Plot training history for regression
    Ts = [0.25, 3, 7, 8, 15, 50]
    fig,axs = plt.subplots(3,2)
    histories = list()
    if USE_HOMODYNE:
        saved_results_path_regression = "C:/Users/Henneberg/speciale_local/homodyne/dt_0_001/regression/"
    else:
        saved_results_path_regression = "C:/Users/Henneberg/speciale_local/counting/dt_0_001/regression/"
    for i,T in enumerate(Ts):
        with open(saved_results_path_regression + 'history' + T_ToFileNameString(T), 'rb') as pickle_file:
            hist = pickle.load(pickle_file)

        histories.append(hist)
        axs.flat[i].plot(hist['mae'], label=r'$ \mathcal{D}_{\mathrm{train}}$')
        axs.flat[i].plot(hist['val_mae'], label=r'$ \mathcal{D}_{\mathrm{valid}}$')
        min_MAE_idx = np.argmin(hist['val_mae'])
        axs.flat[i].plot(min_MAE_idx,hist['val_mae'][min_MAE_idx],'kx')
        axs.flat[i].annotate(annotations[i], xy=(-0.45,0.98), xycoords='axes fraction')
        if (i==0):
            axs.flat[i].text(0.95,0.92,r'$T=%.2f$'%Ts[i],transform=axs.flat[i].transAxes,horizontalalignment='right',verticalalignment='top')
        else:
            axs.flat[i].text(0.95,0.92,r'$T=%.i$'%Ts[i],transform=axs.flat[i].transAxes,horizontalalignment='right',verticalalignment='top')
        enchantAx(axs.flat[i])
        #axs.flat[i].set(xlabel='Epoch', ylabel=r'$A_{\mathcal{D}}$')
        #axes.set_ylim(0,5)

    axs[0,0].set(ylabel=r'MAE$_{\mathcal{D}}$')
    axs[1,0].set(ylabel=r'MAE$_{\mathcal{D}}$')
    axs[2,0].set(xlabel='Epoch', ylabel=r'MAE$_{\mathcal{D}}$')
    axs[2,1].set(xlabel='Epoch')
    #l1 = axs[0].legend(bbox_to_anchor=(1.04,1), borderaxespad=0)
    #l5 = plt.legend(bbox_to_anchor=(1,0), loc="lower right", bbox_transform=fig.transFigure, ncol=2)
    #l3 = plt.legend(bbox_to_anchor=(1.04,0.5), loc="center left", borderaxespad=0)
    #l4 = axs[0].legend(bbox_to_anchor=(0,1.15,1,0.2), loc="lower left", mode="expand", borderaxespad=0, ncol=2)
    l4 = axs[0,1].legend(bbox_to_anchor=(0,1.15,1,0.2), loc="lower right", borderaxespad=0, ncol=2)

    fig.set_size_inches(6, 6.2)
    plt.tight_layout()
    if (USE_HOMODYNE):
        plt.savefig(save_path + 'training_reg_homodyne.pdf')
    else:
        plt.savefig(save_path + 'training_reg_counting.pdf')

    # Plot training history for hyptesting with binning
    Ts = [8]
    binningFactors = [1, 8, 32, 64, 200, 400]
    fig,axs = plt.subplots(3,2)
    histories = list()
    annotations = ['(a)', '(b)', '(c)', '(d)', '(e)', '(f)']
    for T in Ts:
        for i,binningFactor in enumerate(binningFactors):
            with open(saved_results_path + 'binned_' + str(binningFactor) + '/history' + T_ToFileNameString(T), 'rb') as pickle_file:
                hist = pickle.load(pickle_file)

            histories.append(hist)
            axs.flat[i].plot(hist['accuracy'], label=r'$ \mathcal{D}_{\mathrm{train}}$')
            axs.flat[i].plot(hist['val_accuracy'], label=r'$ \mathcal{D}_{\mathrm{valid}}$')
            axs.flat[i].annotate(annotations[i], xy=(-0.45,0.98), xycoords='axes fraction')
            max_acc_idx = np.argmax(hist['val_accuracy'])
            axs.flat[i].plot(max_acc_idx,hist['val_accuracy'][max_acc_idx],'kx')
            #if (i == 2 or i==3):
                #axs.flat[i].text(0.05,0.95,r'$\beta=%.i$'%binningFactors[i],transform=axs.flat[i].transAxes,horizontalalignment='left',verticalalignment='top')
            #elif (i==0):
                #axs.flat[i].text(0.95,0.01,r'$\beta=%.2f$'%Ts[0],transform=axs.flat[i].transAxes,horizontalalignment='right',verticalalignment='bottom')
            #else:
            axs.flat[i].text(0.95,0.05,r'$\beta=%.i$'%binningFactors[i],transform=axs.flat[i].transAxes,horizontalalignment='right',verticalalignment='bottom')
            enchantAx(axs.flat[i])

    axs[0,0].set(ylabel=r'$A_{\mathcal{D}}$')
    axs[1,0].set(ylabel=r'$A_{\mathcal{D}}$')
    axs[2,0].set(xlabel='Epoch', ylabel=r'$A_{\mathcal{D}}$')
    axs[2,1].set(xlabel='Epoch')
    #l1 = axs[0].legend(bbox_to_anchor=(1.04,1), borderaxespad=0)
    #l5 = plt.legend(bbox_to_anchor=(1,0), loc="lower right", bbox_transform=fig.transFigure, ncol=2)
    #l3 = plt.legend(bbox_to_anchor=(1.04,0.5), loc="center left", borderaxespad=0)
    #l4 = axs[0].legend(bbox_to_anchor=(0,1.15,1,0.2), loc="lower left", mode="expand", borderaxespad=0, ncol=2)
    l4 = axs[0,1].legend(bbox_to_anchor=(0,1.15,1,0.2), loc="lower right", borderaxespad=0, ncol=2)

    fig.set_size_inches(6, 6.2)
    plt.tight_layout()
    if (USE_HOMODYNE):
        plt.savefig(save_path + 'training_hyp2_homodyne_binned.pdf')
    else:
        plt.savefig(save_path + 'training_hyp2_counting_binned.pdf')
    plt.show()


def plotAutocorrelation(times, signal):
    #from scipy.signal import fftconvolve
    #autocor = fftconvolve(signal,signal[::-1],axis=1)
    signal = signal[:20]
    autocor = np.empty(shape=(signal.shape))
    for i in range(signal.shape[0]):
        autocor_i = np.correlate(signal[i,:,0],signal[i,:,0], mode='full')
        autocor_i /= autocor_i.max()
        autocor[i,:,0] =autocor_i[autocor_i.size//2:]
    autocor_mean = np.mean(autocor,axis=0)
    dt = times[1]-times[0]
    fft_times = np.fft.fftfreq(times.size,dt)
    fft_times = np.fft.fftshift(fft_times)
    autocor_fft = np.fft.fft(autocor_mean)
    autocor_fft = np.fft.fftshift(autocor_fft)
    fig, axs = plt.subplots(3)
    axs[0].plot(times,np.mean(signal[:,:,0],axis=0))
    axs[1].plot(np.arange(times.size),autocor_mean)
    axs[2].plot(fft_times, np.abs(autocor_fft))
    plt.show()

def plot2Dt(times, signal0, signal1, test_trajectory):
    fig,axs = plt.subplots(2,1)
    if USE_HOMODYNE:
        axs[0].plot(times,signal0[test_trajectory],clip_on=use_clip_on, lw=0.5)
        axs[0].set(ylabel=(('$\mathrm{d}Y_t$') ))
        axs[0].set_ylim([-0.2,0.2])
        axs[1].plot(times,signal1[test_trajectory],clip_on=use_clip_on, lw=0.5)
        axs[1].set(xlabel='$\gamma t$',ylabel=(('$\mathrm{d}Y_t$') ))
        axs[1].set_ylim([-0.2,0.2])
    else:
        axs[0].plot(times,signal0[test_trajectory],clip_on=use_clip_on)
        axs[0].set(ylabel=(('$\mathrm{d}N_t$') ))
        axs[0].set_ylim([0,1])
        axs[1].plot(times,signal1[test_trajectory],clip_on=use_clip_on)
        axs[1].set(xlabel='$\gamma t$',ylabel=(('$\mathrm{d}N_t$') ))
        axs[1].set_ylim([0,1])

    #ax.tick_params(which='both',top=True,right=True)
    #axs[0].set_title(r"(a)",pad=13,left=True)
    #axs[1].set_title(r"(b)",pad=13,left=True)
    axs[0].set(xlabel=None,xticklabels=[None])

    axs[0].annotate(r"(a)", xy=(-0.22,0.92), xycoords='axes fraction')
    axs[1].annotate(r"(b)", xy=(-0.22,0.92), xycoords='axes fraction')
    axs[0].set_xlim([0,times[-1]])
    axs[1].set_xlim([0,times[-1]])
    enchantAx(axs[0])
    enchantAx(axs[1])
    fig.set_size_inches(5.2, 3)
    plt.tight_layout()
    #axs[1].set(clip_on=False)
    if (USE_HOMODYNE):
        plt.savefig(save_path + 'homodyneDetectionComp.pdf',dpi=200)
    else:
        plt.savefig(save_path + 'countingDetectionComp.pdf',dpi=200)

def plotFeatureMaps(times, signal0, signal1, test_trajectory):
    import tensorflow as tf
    tf.random.set_seed(2)
    from tensorflow.keras.models import Model
    from tensorflow.keras.models import load_model
    from tensorflow.compat.v1 import ConfigProto
    from tensorflow.compat.v1 import InteractiveSession
    config = ConfigProto()
    config.gpu_options.allow_growth = True
    session = InteractiveSession(config=config)


    def plotOneSignal(times,signal, test_trajectory):
        fig = plt.figure(constrained_layout=True)

        gs = GridSpec(12, 2, figure=fig)
        axs = list()
        axs.append(fig.add_subplot(gs[0:4, 0]))
        axs.append(fig.add_subplot(gs[4:8, 0]))
        axs.append(fig.add_subplot(gs[8:12, 0]))
        axs.append(fig.add_subplot(gs[0:3, 1]))
        axs.append(fig.add_subplot(gs[3:6, 1]))
        axs.append(fig.add_subplot(gs[6:9, 1]))
        axs.append(fig.add_subplot(gs[9:12, 1]))

       

        model = load_model(saved_results_path + 'best_model_0_T8.h5')
        pred = model.predict(signal[test_trajectory].reshape(1,signal.shape[1],signal.shape[2]))
        print('Model prediction: ', pred)
        indices = [0,2]
        outputs = [model.layers[i].output for i in indices]
        #outputs = model.layers[0].output
        model = Model(inputs=model.inputs, outputs=outputs)
        featureMaps = model.predict(signal[test_trajectory].reshape(1,signal.shape[1],signal.shape[2]))
        #print(featureMaps.shape)
        featureMap0 = featureMaps[0]
        featureMap1 = featureMaps[1]
        xs0 = np.arange(featureMaps[0].shape[1])
        xs1 = np.arange(featureMaps[1].shape[1])
        #print(xs.shape)
        #axs[0].imshow(featureMaps[:,0],cmap='gray')
        #X1 = np.array([xs,featureMaps[0,:,0]])
        #X1 = np.array([featureMaps[0,:,0],featureMaps[0,:,0]])
        #X2 = np.array([xs,featureMaps[0,:,1]])
        #axs[0].imshow(X1,aspect='auto', origin='lower',cmap='gray')
        if (USE_HOMODYNE):
            axs[0].plot(times,signal[test_trajectory],lw=0.5,clip_on=use_clip_on)
            axs[0].set_ylim([-0.2,0.2])
        else:
            axs[0].plot(times,signal[test_trajectory],clip_on=use_clip_on)
            axs[0].set_ylim([0,1])
        axs[1].plot(xs0,featureMap0[0,:,0])
        #axs[1].imshow(np.array([featureMap0[0,:,0],featureMap0[0,:,0]]),aspect='auto',origin='lower')
        axs[2].plot(xs0,featureMap0[0,:,1])
        annotations = ['(a)', '(b)', '(c)' ,'(d)' ,'(e)', '(f)','(g)']
        for i,ax in enumerate(axs):
            ax.annotate(annotations[i], xy=(-0.13,0.83), xycoords='axes fraction')
            # enchantAx(ax)
            if not (i==0):
                ax.set(xticklabels=[None],yticklabels=[None])
                ax.tick_params(axis='both',which='both',bottom=False,top=False,left=False,right=False)
            else:
                ax.set_xlim([0,8])
                ax.set(yticklabels=[None])
        #axs[1].imshow(X2,cmap='gray')

        axs[3].plot(xs1,featureMap1[0,:,0])
        axs[4].plot(xs1,featureMap1[0,:,1])
        axs[5].plot(xs1,featureMap1[0,:,2])
        axs[6].plot(xs1,featureMap1[0,:,3])
        
        axs[0].set(xlabel=r'$\gamma t$')
        axs[0].set_xlim([0,8])
        if (USE_HOMODYNE):
            axs[0].set(ylabel=r'$ \mathrm{d} Y_t$')
        else:
            axs[0].set(ylabel=r'$ \mathrm{d} N_t$')

        fig.set_size_inches(5, 4)
        plt.tight_layout()

        return fig

    fig = plotOneSignal(times, signal0, test_trajectory)

    if (USE_HOMODYNE):
        plt.savefig(save_path + 'featureVecsHomodyne2.pdf',dpi=200)
    else:
        plt.savefig(save_path + 'featureVecsCounting2.pdf',dpi=200)

    fig = plotOneSignal(times, signal1, test_trajectory)

    if (USE_HOMODYNE):
        plt.savefig(save_path + 'featureVecsHomodyne6.pdf',dpi=200)
    else:
        plt.savefig(save_path + 'featureVecsCounting6.pdf',dpi=200)

    plt.show()


if __name__=='__main__':


    # Plot master equation solution
    #plotMasterEquation()

    # Plot confusion matrices
    #plotConfusionMatrices()

    # Plot scatter plots
    #plotScatter()

    # Plot histories
    #plotHistories()

    ### Load data
    # maxT denotes the maximum value of T
    maxT = 15
    max_T_str = T_ToFileNameString(maxT)

    # How much of the trajectories do we want.
    T = 8
    filename_suffix = max_T_str + '.h5'
    end_idx = get_end_idx(T,dt)

    # Old method. Using different trajectories for each new T.
    #filename_suffix = T_str + '.h5'

    training_data_path = data_path + 'training' + filename_suffix
    print(training_data_path)
    valid_data_path = data_path + 'valid' + filename_suffix
    test_data_path = data_path + 'test' + filename_suffix

    # reading ALL data
    N_data_plot = 4096
    psi_train,dN_train,times_train,omegas_train = read_data(training_data_path, np.arange(0), np.arange(end_idx))
    #psi_valid,dN_valid,times_valid,omegas_valid = read_data(valid_data_path, np.arange(N_data_plot))
    #psi_test,dN_test,times_test,omegas_test = read_data(test_data_path, np.arange(N_data_plot))

    # Create matrix with booleans where rows are trajectories and columns are frequencies
    # the (i,j)'th element thus is 1 if the i'th trajectory has frequency j
    idx_hypotheses = np.zeros((omegas_train.shape[0],hypotheses.size),dtype='bool')
    for idx,hypothesis in enumerate(hypotheses):
        idx_hypotheses[:,idx] = omegas_train[:,0] == hypothesis

    if (psi_train[idx_hypotheses[:,0],:,:].shape[0] == 0 or psi_train[idx_hypotheses[:,1],:,:].shape[0] == 0):
        print('Hypothesis frequencies did not match any frequencies in data set. Exiting...')
        exit()

    psi = psi_train
    dN = dN_train
    times = times_train

    #checkDuplicates(dN_train,dN_valid,dN_test)

    test_trajectory = 3
    #test_trajectory = 4

    # Separate
    psi2 = psi[idx_hypotheses[:,0]]
    signal2 = dN[idx_hypotheses[:,0]]
    psi6 = psi[idx_hypotheses[:,1]]
    signal6 = dN[idx_hypotheses[:,1]]
    a = omegas_train[idx_hypotheses[:,1]]
    print(omegas_train[idx_hypotheses[:,1]])
    print(a[test_trajectory+3])


    # Plot detection records and trajectories
    #plotTrajAndDt(times,psi6[test_trajectory],signal6[test_trajectory])

    # Comparison of average with master eq
    #plotMasterEqComparison(times[:6001],psi6[:,:6001,:])

    # Mollow triplet
    #plotMollowTriplet(times,signal6[:,:,:])

    # Plot Bayes 2 hypotheses
    #use_coarse_grained = False
    #plotBayes2hyp(times, psi6[test_trajectory], signal6[test_trajectory], use_coarse_grained)

    # Plot Bayes color
    #plotBayesColorHomodyne(times, psi6[test_trajectory], signal6[test_trajectory])

    # Plot Bayes color
    #plotBayesColorCounting(times, psi6[test_trajectory], signal6[test_trajectory])

    # Plot transition from no binning to binned signals
    #plotBinnedTrajAndRecord(times, psi6[test_trajectory], signal6[test_trajectory], 1)

    # Plot Bayes vs NN binned trajectory
    #plotBayesBinnedVsNNbinned(times, signal6[test_trajectory])

    # maybe have alpha = 50 maximum
    # Try noisy signal without white noise
    #binSignal(signal6[:,:8000], times[:8000], 32, True, True, test_trajectory, False)

    # Plot noisy signal with and without white noise
    #binSignal(signal6[:,:8000], times[:8000], 8, True, True, test_trajectory, True)
    #binSignal(signal6[:,:8000], times[:8000], 256, True, True, test_trajectory, True)
    #binSignal(signal6[:,:8000], times[:8000], 1000, True, True, test_trajectory, False)

    # Plot Lorentz distribution for different alpha
    plotLorentz(times[:8000])

    # Plot autocorrelation function
    #plotAutocorrelation(times, signal6)

    # Plot two trajectories with different Rabi frequency for comparison
    # For counting test_trajectory+3 is used. For homodyne either.
    #plot2Dt(times, signal2, signal6, test_trajectory+3)

    # Plot feature maps - test_trajectory for counting, test_trajectory+3 for homodyne
    #plotFeatureMaps(times, signal2, signal6, test_trajectory)
