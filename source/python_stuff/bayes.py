import numpy as np
import scipy as sp
from scipy.linalg import expm
import matplotlib.pyplot as plt
import matplotlib
from scipy.interpolate import griddata

from plottingTools import *
from FileManagement import *

fig_path = "C:/Users/Henneberg/speciale_local/"


def getEffectiveHamiltonian(H,C):
    return H - 1j*(C.conj().T @ C)/2

def getEffectiveTimeEvolOp(H,dt):
    return expm(-1j*H*dt)

def normalize(psi):
    #print(psi.shape)
    if (psi.ndim > 2):
        norm1 = np.linalg.norm(psi,axis=1)
        for k in range(psi.shape[0]):
            for i in range(psi.shape[2]):
                a = psi[k,:,i]/norm1[k,i]
                psi[k,:,i] = np.divide(psi[k,:,i],norm1[k,i])
        return psi
    else:
        return psi/np.linalg.norm(psi)

def bayesTest(times=np.linspace(0,9,9001), hypotheses = np.linspace(2,14,200), true_idx=0, use_homodyne=False, use_coarse_grained=False, binningFactor=1):

    Delta = 0 # detuning
    gamma = 1 # decay rate
    Phi = np.pi/2

    I = np.eye(2)

    t0 = times[0]
    T = times[-1]
    nt = times.size

    dt = (T-t0)/(nt-1)
    times = np.linspace(t0,T,nt)

    psi_gs = np.array([[0],[1]],dtype='complex_') # on the form [[e],[g]]
    psi0 = np.array([[0],[1]],dtype='complex_')

    # Operators.
    H = np.empty(shape=(hypotheses.size,2,2))
    for k in range(hypotheses.size):
        H[k,:,:] = np.array([[-Delta/2, hypotheses[k]/2],[hypotheses[k]/2, Delta/2]])

    C = np.sqrt(gamma)*np.array([[0,0],[1,0]])

    cPhi = C*np.exp(-1j*Phi)

    # Effective Hamiltonians.
    #Heff = getEffectiveHamiltonian(H, C)
    # TESTING 
    Heff = H - 1j*(C.conj().T @ C - I)/2

    # Effective time evolution operators.
    U = np.empty(shape=(H.shape),dtype='complex_')
    for k in range(hypotheses.size):
        U[k] = getEffectiveTimeEvolOp(Heff[k], dt)
    
    psi_true = np.zeros([2,nt],dtype='complex_')
    psi_true[:,0] = psi0.reshape(2,)

    if (use_homodyne):
        cPhi = C*np.exp(-1j*Phi)
        xPhi = cPhi + cPhi.conj().T

        dW = np.sqrt(dt)*np.random.randn(nt)

        dY = np.zeros(nt)
        for i in range(1,nt):
            dY[i-1] = np.real(psi_true[:,i-1].conj().T@xPhi@psi_true[:,i-1])*dt + dW[i-1]
            psi_true[:,i] = (U[true_idx] + dY[i-1]*cPhi)@psi_true[:,i-1]
            psi_true[:,i] = normalize(psi_true[:,i])
        dY[-1] = np.real(psi_true[:,-1].conj().T@xPhi@psi_true[:,-1])*dt + dW[-1]
    else:
        I = np.eye(2)
        dN = np.zeros(nt)
        for i in range(1,nt):
            r = np.random.uniform(0)
            psi_true[:,i] = U[true_idx] @ psi_true[:,i-1]
            if (dt > r):
                print(dt)
                print(r)
                psi_true[:,i] = psi_gs.reshape(2,)
                dN[i] = 1
            else:
                psi_true[:,i-1] = normalize(psi_true[:,i-1])
        psi_true[:,-1] = normalize(psi_true[:,-1])


        #for i in range(1,nt):
        #    psi_true[:,i] = U[true_idx] @ psi_true[:,i-1]
        #    if (np.linalg.norm(psi_true[:,i])**2 <= r):
        #        psi_true[:,i] = psi_gs.reshape(2,)
        #        dN[i] = 1
        #        r = np.random.uniform(0)
        #    else:
        #        psi_true[:,i-1] = normalize(psi_true[:,i-1])
        #psi_true[:,-1] = normalize(psi_true[:,-1])

    psi = np.zeros([hypotheses.size,2,nt],dtype='complex_')
    psi[:,:,0] = psi0.reshape(1,2,)

    # Propagate unnormalized candidate states conditioned on true signal.
    P = np.empty(shape=(hypotheses.size, nt))
    for k in range(hypotheses.size):
        if (use_homodyne):
            for i in range(1,nt):
                psi[k,:,i] = (U[k] + dY[i-1]*cPhi) @ psi[k,:,i-1] # Step propagate candidate state.
        else:
            for i in range(1,nt):
                psi[k,:,i] = (U[k] + dN[i-1]*(C-I)) @ psi[k,:,i-1] # Step propagate candidate state.
        # Calculate Bayesian probabilities.
        P[k,:] = np.sum(np.square(np.abs(psi[k,:,:])),axis=0) # Probability for the k'th hypothesis to be the true hypothesis.
    psi = normalize(psi)

    # Normalize probabilities.
    P_temp = np.empty(shape=P.shape)
    for k in range(hypotheses.size):
        P_temp[k] = P[k]/np.sum(P,axis=0)
    P = P_temp

    if (use_homodyne):
        signal = dY
    else:
        signal = dN

    signal = signal.reshape((1,signal.size,1))

    if use_coarse_grained:
        P = np.empty(shape=(hypotheses.size, nt))
        signal, times_binned = binSignal(signal, times, binningFactor)
        psi_binned = np.empty(shape=(hypotheses.size, psi_true.shape[0], nt),dtype='complex_')
        signal = bayes_binning(signal, times, binningFactor)
        #signal = signal[:,:-1,:]

        for k in range(hypotheses.size):
            psi_binned[k,:,0] = psi0.reshape((2,))
            # Use last data point (doesnt get averaged)
            #for i in range(1,nt-1):
            # Ignore last data point
            for i in range(1,nt):
                #print(signal[0,i-1])
                psi_binned[k,:,i] = (U[k] + signal[0,i-1]*cPhi)@psi_binned[k,:,i-1]
            # Calculate Bayesian probabilities.
            P[k,:] = np.sum(np.square(np.abs(psi_binned[k,:,:])),axis=0) # Probability for the k'th hypothesis to be the true hypothesis.
        # Normalize probabilities.
        P_temp = np.empty(shape=P.shape)
        for k in range(hypotheses.size):
            P_temp[k] = P[k]/np.sum(P,axis=0)
        P = P_temp

        psi_binned = normalize(psi_binned)
        psi = psi_binned
        times = times_binned
        print('psibinned' , psi_binned.shape)
        print("signal shape: ",signal.shape)

    return signal, psi, times, P # Return photon signal and probabilities.

def bayesTestNoSim(times, psi, signal, hypotheses, use_homodyne=False, use_coarse_grained=False, binningFactor=1):
    Delta = 0 # detuning
    gamma = 1 # decay rate
    Phi = np.pi/2

    t0 = times[0]
    T = times[-1]
    nt = times.size

    dt = (T-t0)/(nt-1)

    signal = signal.reshape((1,nt,1))

    # Operators.
    H = np.empty(shape=(hypotheses.size,2,2))
    for k in range(hypotheses.size):
        H[k,:,:] = np.array([[-Delta/2, hypotheses[k]/2],[hypotheses[k]/2, Delta/2]])

    C = np.sqrt(gamma)*np.array([[0,0],[1,0]])

    cPhi = C*np.exp(-1j*Phi)
    xPhi = cPhi + cPhi.conj().T
    I = np.eye(2)

    # Effective Hamiltonians.
    Heff = getEffectiveHamiltonian(H, C)

    # Effective time evolution operators.
    U = np.empty(shape=(H.shape),dtype='complex_')
    for k in range(hypotheses.size):
        U[k] = getEffectiveTimeEvolOp(Heff[k], dt)
    
    psi = np.zeros([hypotheses.size,2,nt],dtype='complex_')
    psi0 = np.array([[0],[1]],dtype='complex_')
    psi[:,:,0] = psi0.reshape(1,2,)
    P = np.empty(shape=(hypotheses.size, nt))
    if (use_coarse_grained):
        signal, times_binned = binSignal(signal, times, binningFactor)
        #psi = np.empty(shape=(hypotheses.size, psi.shape[0], nt),dtype='complex_')
        signal = bayes_binning(signal, times, binningFactor)
        # For setting the signal equal to zeros
        #signal = np.zeros(shape=(signal.shape))
    else:
        times_binned = times

    # Propagate unnormalized candidate states conditioned on true signal.
    for k in range(hypotheses.size):
        if (use_homodyne):
            for i in range(1,nt):
                psi[k,:,i] = (U[k] + signal[0,i-1,0]*cPhi) @ psi[k,:,i-1] # Step propagate candidate state.
        else:
            for i in range(1,nt):
                psi[k,:,i] = (U[k] + signal[0,i-1,0]*(C-I)) @ psi[k,:,i-1] # Step propagate candidate state.
        # Calculate Bayesian probabilities.
        P[k,:] = np.sum(np.square(np.abs(psi[k,:,:])),axis=0) # Probability for the k'th hypothesis to be the true hypothesis.
    psi = normalize(psi)

    # Normalize probabilities.
    P_temp = np.empty(shape=P.shape)
    for k in range(hypotheses.size):
        P_temp[k] = P[k]/np.sum(P,axis=0)
    P = P_temp

    signal = signal.reshape((1,signal.size,1))
    print('psibinned' , psi.shape)
    print("signal shape: ",signal.shape)

    return signal, psi, times, times_binned, P # Return photon signal and probabilities.


def bayesGeneralPredict(times, psi, signal, hypotheses, Delta, gamma, Phi, use_homodyne, use_coarse_grained):
    t0 = times[0]
    T = times[-1]
    nt = times.size

    dt = (T-t0)/(nt-1)
    times = np.linspace(t0,T,nt)

    psi_gs = np.array([[0],[1]],dtype='complex_') # on the form [[e],[g]]
    psi0 = np.array([[0],[1]],dtype='complex_')

    # Operators.
    H = np.empty(shape=(hypotheses.size,2,2))
    for k in range(hypotheses.size):
        H[k,:,:] = np.array([[-Delta/2, hypotheses[k]/2],[hypotheses[k]/2, Delta/2]])

    C = np.sqrt(gamma)*np.array([[0,0],[1,0]])
    cPhi = C*np.exp(-1j*Phi)
    xPhi = cPhi + cPhi.conj().T
    I = np.eye(2)

    # Effective Hamiltonians.
    Heff = getEffectiveHamiltonian(H, C)

    # Effective time evolution operators.
    U = np.empty(shape=(H.shape),dtype='complex_')
    for k in range(hypotheses.size):
        U[k] = getEffectiveTimeEvolOp(Heff[k], dt)

    psi = np.zeros([hypotheses.size,2,nt],dtype='complex_')
    psi[:,:,0] = psi0.reshape(1,2,)

    # Propagate unnormalized candidate states conditioned on true signal.
    P = np.empty(shape=(hypotheses.size, nt))
    for k in range(hypotheses.size):
        if (use_homodyne):
            for i in range(1,nt):
                psi[k,:,i] = (U[k] + signal[i-1]*cPhi) @ psi[k,:,i-1] # Step propagate candidate state.
        else:
            for i in range(1,nt):
                psi[k,:,i] = (U[k] + signal[i-1]*(C-I)) @ psi[k,:,i-1] # Step propagate candidate state.
        # Calculate Bayesian probabilities.
        P[k,:] = np.sum(np.square(np.abs(psi[k,:,:])),axis=0) # Probability for the k'th hypothesis to be the true hypothesis.
    psi = normalize(psi)

    # Normalize probabilities.
    P_temp = np.empty(shape=P.shape)
    for k in range(hypotheses.size):
        P_temp[k] = P[k]/np.sum(P,axis=0)
    P = P_temp

    signal = signal.reshape((1,signal.size,1))

    if use_coarse_grained:
        signal, times_binned = binSignal(signal, times, binningFactor)
        psi_binned = np.empty(shape=(hypotheses.size, psi_true.shape[0], psi_true.shape[1]),dtype='complex_')
        signal = bayes_binning(signal, times, binningFactor)

        for k in range(hypotheses.size):
            psi_binned[k,:,0] = psi0.reshape((2,))
            for i in range(1,nt):
                psi_binned[k,:,i] = (U[k] + signal[0,i-1]*cPhi)@psi_binned[k,:,i-1]
                psi_binned[k,:,i] = normalize(psi_binned[k,:,i])
        psi = psi_binned
        times = times_binned

    return times, psi, signal, P # Return photon signal and probabilities.


def bayesPredict(psi, dN, times, hypotheses, Delta, gamma, use_homodyne, Phi=np.pi/2, use_coarse_grained=False, binningFactor=1):

    t0 = times[0]
    T = times[-1]
    nt = len(times)
    dt = (T-t0)/(nt-1)
    print('dt: ', dt)

    # Operators
    H0 = np.array([[-Delta/2, hypotheses[0]/2],[hypotheses[0]/2, Delta/2]]) # Hamiltonian 0
    H1 = np.array([[-Delta/2, hypotheses[1]/2],[hypotheses[1]/2, Delta/2]]) # Hamiltonian 1
    C = np.sqrt(gamma)*np.array([[0,0],[1,0]])
    if (use_homodyne):
        CPhi = C * np.exp(-1j*Phi)
    I = np.eye(2)

    # Effective Hamiltonians
    Heff0 = getEffectiveHamiltonian(H0, C)
    Heff1 = getEffectiveHamiltonian(H1, C)

    # Effective time evolution operators
    U0 = getEffectiveTimeEvolOp(Heff0, dt)
    U1 = getEffectiveTimeEvolOp(Heff1, dt)


    # Can only be outside trajectory loop as long as initial state is constant
    # for every trajectory.
    #psi0 = np.array([[0],[1]],dtype='complex_') # on the form [[e],[g]]
    #psi_init = psi[0,0,:]

    # Change to make function independent of psi since we start in ground state always
    psi_init = np.array([[0],[1]],dtype='complex_') # on the form [[e],[g]]

    temp_dN = dN[0,:64]
    print(dN.shape)

    if use_coarse_grained:
        # Temporarily disabled for adding noise where it is not needed
        #dN = bayes_binning(dN, times, binningFactor)
        print("nt after coarse graining: ",nt)
        print(dN.shape)

    print("true dN ; dN after average: \n",np.array([temp_dN,dN[0,:64]]).T)

    P = np.empty([dN.shape[0],hypotheses.size,nt])

    print('Predicting hypothesis using Bayes...')
    for traj_idx in range(dN.shape[0]):
        psi0 = np.zeros([2,nt],dtype='complex_')
        psi0[:,0] = psi_init.reshape(2,)
        psi1 = np.zeros([2,nt],dtype='complex_')
        psi1[:,0] = psi_init.reshape(2,)
        
        # propagate unnormalized candidate states conditioned on true signal
        if (use_homodyne):
            for i in range(1,nt):
                #print('bayes signal [i-1]', dN[traj_idx,i-1])
                psi0[:,i] = (U0 + dN[traj_idx,i-1]*CPhi) @ psi0[:,i-1] # step propagate state 1
                psi1[:,i] = (U1 + dN[traj_idx,i-1]*CPhi) @ psi1[:,i-1] # step propagate state 2
        else:
            for i in range(1,nt):
                psi0[:,i] = (U0 + dN[traj_idx,i-1]*(C-I)) @ psi0[:,i-1] # step propagate state 1
                psi1[:,i] = (U1 + dN[traj_idx,i-1]*(C-I)) @ psi1[:,i-1] # step propagate state 2
        
        # calculate Bayesian probabilities
        P0_temp = np.sum(np.square(np.abs(psi0)),axis=0) # Probability for hypotheses[0] to be the true Rabi frequency
        P1_temp = np.sum(np.square(np.abs(psi1)),axis=0) # Probability for hypotheses[1] to be the true Rabi frequency 
        # normalize probabilities
        P[traj_idx,0,:] = P0_temp/(P0_temp+P1_temp)
        P[traj_idx,1,:] = 1-P[traj_idx,0,:]
        #print('P[traj_idx,0]', np.around(P[traj_idx,0,-1],2))
        #print('P[traj_idx,1]', np.around(P[traj_idx,1,-1],2))


    # trying to vectorize stuff
    #psi0 = np.empty(shape=(dN.shape[0],2,nt),dtype='complex_')
    #psi1 = np.empty(shape=(dN.shape[0],2,nt),dtype='complex_')
    ## propagate unnormalized candidate states conditioned on true signal
    #if (use_homodyne):
    #    for i in range(1,nt):
    #        psi0[:,:,i] = (U0 + np.tensordot(dN[:,i-1,0],CPhi,axes=0)) * psi0[:,:,i-1] # step propagate state 1
    #        psi1[:,:,i] = (U1 + dN[traj_idx,i-1]*CPhi) @ psi1[:,i-1] # step propagate state 2
    #else:
    #    for i in range(1,nt):
    #        psi0[:,i] = (U0 + dN[traj_idx,i-1]*(C-I)) @ psi0[:,i-1] # step propagate state 1
    #        psi1[:,i] = (U1 + dN[traj_idx,i-1]*(C-I)) @ psi1[:,i-1] # step propagate state 2

    print('Prediction by Bayes complete.')
    

    return P, dN

def bayes_binning(signal, times, binningFactor):
    if (binningFactor == 1):
        return signal
    #signal_avg = np.empty(shape=(signal.shape[0],times.size,signal.shape[2]))
    # If binned signal is not reduced to be divisible by 2
    #N_bins = times.size//binningFactor + 1
    # If binned signal is divisble by 2
    N_bins = times.size//binningFactor
    # old method
    #if (N_bins <= 1):
    #    print("N_bins <= 1, passing")
    #    pass
    #else:
    #    for i in range(N_bins):
    #        if ((i+1)*binningFactor < times.size):
    #            signal_avg[:,i*binningFactor:(i+1)*binningFactor] = signal[:,i]/binningFactor
    #        else:
    #            signal_avg[:,i*binningFactor:(times.size)] = signal[:,i]/(times.size-i*binningFactor)

    # new method - only works for binning factors that are multiples of 2
    signal_avg = signal / binningFactor
    #signal_avg = signal[:,:-1,:] / binningFactor
    signal_avg = np.repeat(signal_avg, binningFactor, axis=1)
    #signal_avg = np.concatenate((signal_avg, signal[:,-1,:].reshape((signal.shape[0],1,1))), axis=1)
    print("signal after bayes binning: ", signal_avg.shape)
    return signal_avg

def bayesCounting(times=np.linspace(0,9,9001), use_homodyne=False, use_coarse_grained=False, binningFactor=1):
    Omega0 = 2
    Omega1 = 6

    Delta = 0 # detuning
    gamma = 1 # decay rate
    Phi = np.pi/2

    t0 = times[0]
    T = times[-1]
    nt = times.size
    dt = (T-t0)/(nt-1)
    times = np.linspace(t0,T,nt)
    
    psi_gs = np.array([[0],[1]],dtype='complex_') # on the form [[e],[g]]
    psi0 = np.array([[0],[1]],dtype='complex_')

    # Operators
    H_true = np.array([[-Delta/2, Omega0/2],[Omega0/2, Delta/2]]) # True Hamiltonian
    H_false = np.array([[-Delta/2, Omega1/2],[Omega1/2, Delta/2]]) # False Hamiltonian
    C = np.sqrt(gamma)*np.array([[0,0],[1,0]])

    # effective Hamiltonians
    Heff_true = getEffectiveHamiltonian(H_true, C)
    Heff_false = getEffectiveHamiltonian(H_false, C)

    # effective time evolution operators
    U_true = getEffectiveTimeEvolOp(Heff_true, dt)
    U_false = getEffectiveTimeEvolOp(Heff_false, dt)

    psi = np.zeros([2,len(times)],dtype='complex_')
    psi[:,0] = psi0.reshape(2,)
    
    if (use_homodyne):
        cPhi = C*np.exp(-1j*Phi)
        xPhi = cPhi + cPhi.conj().T

        dW = np.sqrt(dt)*np.random.randn(nt)

        dY = np.zeros(nt)
        psi[:,0] = psi0.reshape((2,))
        for i in range(1,nt):
            dY[i-1] = np.real(psi[:,i-1].conj().T@xPhi@psi[:,i-1])*dt + dW[i-1]
            psi[:,i] = (U_true + dY[i-1]*cPhi)@psi[:,i-1]
            psi[:,i] = normalize(psi[:,i])
        dY[-1] = np.real(psi[:,-1].conj().T@xPhi@psi[:,-1])*dt + dW[-1]
    else:

        I = np.eye(2)
        
        dN = np.zeros(len(times))
        r = np.random.uniform(0)
        for i in range(1,len(times)):
            #print("i: ",i)
            #print('random number: ',r)
            #print('psi: ',psi[:,i-1])
            #print('norm: ',np.linalg.norm(psi[:,i-1]))
            if (np.linalg.norm(psi[:,i-1]) <= r):
                #psi[:,i] = C @ psi[:,i-1]/np.sqrt(psi[:,i-1].conj().T @ (C.conj().T @ C) @ psi[:,i-1])
                psi[:,i] = psi_gs.reshape(2,)
                dN[i] = 1
                r = np.random.uniform(0)
            else:
                psi[:,i] = U_true @ psi[:,i-1]
                psi[:,i-1] = normalize(psi[:,i-1])
        psi[:,-1] = normalize(psi[:,-1])

    psi_true = np.zeros([2,len(times)],dtype='complex_')
    psi_true[:,0] = psi0.reshape(2,)
    psi_false = np.zeros([2,len(times)],dtype='complex_')
    psi_false[:,0] = psi0.reshape(2,)

    # propagate unnormalized candidate states conditioned on true signal
    if (use_homodyne):
        for i in range(1,len(times)):
            psi_true[:,i] = (U_true + dY[i-1]*(cPhi)) @ psi_true[:,i-1] # step propagate true state
            psi_false[:,i] = (U_false + dY[i-1]*(cPhi)) @ psi_false[:,i-1] # step propagate false state
    else:
        for i in range(1,len(times)):
            psi_true[:,i] = (U_true + dN[i-1]*(C-I)) @ psi_true[:,i-1] # step propagate true state
            psi_false[:,i] = (U_false + dN[i-1]*(C-I)) @ psi_false[:,i-1] # step propagate false state
    
    # calculate Bayesian probabilities
    P0 = np.sum(np.square(np.abs(psi_true)),axis=0) # Probability for Omega0 to be the true state
    P1 = np.sum(np.square(np.abs(psi_false)),axis=0) # Probability for Omega1 to be the true state
    # normalize probabilities
    P0 = P0/(P0+P1)
    P1 = 1-P0
    if (use_homodyne):
        signal = dY
    else:
        signal = dN

    signal = signal.reshape((1,signal.size,1))

    if use_coarse_grained:
        signal, times_binned = binSignal(signal, times, binningFactor)
        psi_binned = np.empty(shape=(psi.shape),dtype='complex_')
        signal = bayes_binning(signal, times, binningFactor)
        psi_binned[:,0] = psi0.reshape((2,))
        for i in range(1,nt):
            psi_binned[:,i] = (U_true + signal[0,i-1]*cPhi)@psi_binned[:,i-1]
            psi_binned[:,i] = normalize(psi_binned[:,i])
        psi = psi_binned
        times = times_binned
    return signal, psi, times, P0, P1 # return photon signal and probabilities

def plot_bayesCounting(times, use_homodyne, hypotheses, true_idx):
    use_coarse_grained = False
    binningFactor = 1
    #dN, psi, times, P0, P1 = bayesCounting(times, use_homodyne, use_coarse_grained, binningFactor)
    signal, psi, times, P = bayesTest(times, hypotheses, true_idx, use_homodyne, use_coarse_grained, binningFactor)

    fig,axes = plt.subplots(4,2)
    print(psi.shape)
    print(psi[true_idx].shape)
    populations = np.square(np.abs(psi[true_idx]))
    print(populations.shape)
    axes[0,0] = plotClicks(axes[0,0], times, signal[0,:,0], use_homodyne)
    axes[1,0] = plotPopulations(axes[1,0], times, populations)
    axes[2,0].plot(times, P[0], label='$\Omega_0$')
    axes[2,0].plot(times, P[1], label='$\Omega_1$')


    axes[2,0].legend()
    axes[2,0].set(ylabel='P')

    #probs = np.array([P0,P1])
    #cmap = plt.get_cmap('viridis')
    #axes[3,0].set(xlabel='$\gamma t$',ylabel='$\Omega / \gamma$')
    axes[3,0] = plot_bayesTest(axes[3,0], signal, times, P, hypotheses, true_idx)
    #plt.yticks(np.array([0.5,1.5]),['$\Omega_0$','$\Omega_1$'])
    #plt.yticks(hypotheses,['$\Omega_0$','$\Omega_1$'])
    #plt.xticks(np.arange(0,nt,nt//10),np.array(np.arange(0,nt,nt//10,dtype='int32')*T/nt,dtype='int32'))

    #im = axes[3,0].pcolormesh(times, hypotheses, P, cmap=cmap)
    #ax.plot(times,hypotheses[np.argmax(P,axis=0)],'r',lw=0.5)
    #im = axes[3,0].pcolormesh(probs)

    ### Binned data
    #use_coarse_grained = True
    #binningFactor = 64
    #times = times[:-1]
    ##dN, psi, times_binned, P0, P1 = bayesCounting(times, use_homodyne, use_coarse_grained, binningFactor)
    #signal, psi, times_binned, P = bayesTest(times, hypotheses, true_idx, use_homodyne, use_coarse_grained, binningFactor)

    #axes[0,1] = plotClicks(axes[0,1], times, signal[0,:,0], use_homodyne)
    #populations = np.square(np.abs(psi[true_idx]))
    #axes[1,1] = plotPopulations(axes[1,1], times, populations)
    #axes[1,1].set(ylabel='')
#(np#.arange(populations), " "))
    #axes[2,1].plot(times, P.T, label='P')
    ##axes[2,1].plot(times, P0, label='$\Omega_0$ binned')
    ##axes[2,1].plot(times, P1, label='$\Omega_1$ binned')
    ##axes[2,1].legend()

    ##probs = np.array([P0,P1])

    ##axes[3,1].set(xlabel='$\gamma t$',ylabel='')
    ##plt.yticks(np.array([0.5,1.5]),['$\Omega_0$','$\Omega_1$'])
    ##plt.xticks(np.arange(0,nt+1,nt//10),np.array(np.arange(0,nt+1,nt//10,dtype='int32')*T/nt,dtype='int32'))
    ##im = axes[3,1].pcolormesh(times, hypotheses, P, cmap=cmap)

    #axes[3,1] = plot_bayesTest(axes[3,1],signal,times,P,hypotheses, true_idx)


    ##cbar = plt.colorbar(im,ax=axes[3,1], label='Probability')
    ##cbar.minorticks_on()

    #plt.tight_layout()
    ##plt.savefig(fig_path + "data" +"homodyne"+ ".pdf")
    #plt.show()

def plot_bayesTest(ax, signal, times, P, hypotheses, true_idx):
    nt = times.size
    T = times[-1]

    plt.sca(ax) # Set current axes instance to ax.
    ax.set(xlabel='$\gamma t$',ylabel='$\Omega / \gamma$')

    yticks = np.arange(0,hypotheses.size,1) if (hypotheses.size <= 10) else np.arange(0,hypotheses.size,(hypotheses.size-1)//6)
    yticklabels = np.around(hypotheses[yticks],2)
    plt.yticks(yticks, yticklabels)

    xticks = np.arange(0,nt+1,nt//(1/2*T))
    xticklabels = np.around(xticks*T/(nt-1),1)

    plt.xticks(xticks,xticklabels)

    cmap = plt.get_cmap('viridis')

    #im = plt.imshow(P, aspect='auto',origin='lower',interpolation='spline36',cmap=cmap)
    im = plt.imshow(P, aspect='auto',origin='lower',interpolation=None,cmap=cmap)
    ax.plot(np.arange(times.size),np.argmax(P,axis=0),color=[0.75,0,0],lw=0.5)
    ax.plot([0,nt-1],[true_idx,true_idx],'k--',lw=0.5)
    enchantAx(ax)

    cbar = plt.colorbar(im, ax=ax)
    cbar.minorticks_on()

    return ax

if __name__ == '__main__':
    t0 = 0
    T = 8
    nt = 8001
    use_homodyne = False
    use_coarse_grained = False
    binningFactor = 64
    times = np.linspace(t0,T,nt)

    hypotheses = np.array([0,6])
    true_idx = 0
    plot_bayesCounting(times, use_homodyne, hypotheses, true_idx)

    plt.show()


    #hypotheses = np.linspace(2,14,50)
    #hypotheses = np.array([2,6])
    #true_idx = 7
    #fig = plt.Figure()
    #ax = plt.gca()
    #print('True Rabi frequency: ', hypotheses[true_idx])
    #signal, psi, times, P = bayesTest(times, hypotheses, true_idx, use_homodyne, use_coarse_grained, binningFactor)
    #ax = plot_bayesTest(ax, signal, times, P)
    #enchantAx(ax)
    #plt.show()

    #fig = plt.figure()
    #ax = plt.gca()

    #P = np.array([[0.1,0.5],[0.8,0.2]])
    #print(P.shape)
    #ax.imshow(P)
    #plt.show()

