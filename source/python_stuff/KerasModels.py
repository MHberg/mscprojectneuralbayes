from abc import ABC, abstractmethod
import tensorflow as tf

#access Keras through tensorflow
from tensorflow.keras.models import Sequential, load_model
from tensorflow.keras.layers import Dense, Activation, Dropout, TimeDistributed, LSTM, RepeatVector, Flatten, Conv1D, AveragePooling1D, ConvLSTM2D, MaxPooling1D, MaxPooling2D, Attention, GlobalAveragePooling1D
from tensorflow.compat.v1.keras.layers import CuDNNLSTM

#access Keras directly
#from keras.models import Sequential, load_model
#from keras.layers import Dense, Activation, Dropout, TimeDistributed, LSTM, RepeatVector, Flatten, Conv1D, AveragePooling1D, ConvLSTM2D, MaxPooling1D, MaxPooling2D
#from tensorflow.compat.v1.keras.layers import CuDNNLSTM

class KerasModel(ABC):
    def __init__(self,hidden_size,timesteps,use_dropout):
        self.hidden_size = hidden_size
        self.timesteps = timesteps
        self.use_dropout = use_dropout

    @abstractmethod
    def defineModel(self):
        raise NotImplementedError("Method has to be implemented in subclass")

    @abstractmethod
    def getDimsRequired(self):
        raise NotImplementedError("Method has to be implemented in subclass")

class EncoderDecoderLSTM(KerasModel):
    """
    Encoder-decoder LSTM for multistep forecasting.
    """

    def __init__(self,hidden_size,timesteps,use_dropout,n_out):
        super().__init__(hidden_size,timesteps,use_dropout)
        self.n_out = n_out

    def defineModel(self):
        model = Sequential()
        model.add(LSTM(self.hidden_size, activation='relu', input_shape=(self.timesteps,1)))
        model.add(RepeatVector(self.n_out))
        model.add(LSTM(self.hidden_size, activation='relu', return_sequences=True)) # second LSTM layer
        if self.use_dropout:
            model.add(Dropout(0.5)) # add dropout to prevent overfitting
        model.add(TimeDistributed(Dense(1)))
        model.add(Activation('sigmoid'))
        model.summary()
        return model

    def getDimsRequired(self):
        return 3

class SingleStepLSTMModel(KerasModel):
    """
    LSTM model for forecasting a click one step into the future.
    """

    def __init__(self,hidden_size,timesteps,use_dropout):
        super().__init__(hidden_size,timesteps,use_dropout)

    def defineModel(self):
        model = Sequential()
        model.add(LSTM(self.hidden_size, activation='relu', return_sequences=True, input_shape=(self.timesteps,1)))
        model.add(LSTM(self.hidden_size, activation='relu', return_sequences=True)) # second LSTM layer
        if self.use_dropout:
            model.add(Dropout(0.2)) # add dropout to prevent overfitting
        model.add(TimeDistributed(Dense(1)))
        model.add(Activation('sigmoid'))
        model.summary()
        return model

    def getDimsRequired(self):
        return 3

class VectorLSTM(KerasModel):
    """
    Vector output LSTM for multistep forecasting - does not work.
    """
    def __init__(self,hidden_size,timesteps,use_dropout,n_out):
        super().__init__(self,hidden_size,timesteps,use_dropout)
        self.n_out = n_out
        
    def defineModel(self):
        raise NotImplementedError('This method does not work yet')
        model = Sequential()
        model.add(LSTM(self.hidden_size, activation='relu', return_sequences=True, input_shape=(self.timesteps,1)))
        model.add(LSTM(self.hidden_size, activation='relu'))
        if use_dropout:
            model.add(Dropout(0.5)) # add dropout to prevent overfitting
        model.add(Dense(self.n_out))
        model.add(Activation('sigmoid'))
        model.summary()
        return model

    def getDimsRequired(self):
        return 3

class ParameterLSTMModel(KerasModel):
    """
    A model for parameter estimation from hypothesis set using LSTM.
    """

    def __init__(self,hidden_size,nt,timesteps,use_dropout,hypotheses):
        super().__init__(hidden_size,timesteps,use_dropout)
        self.num_classes = hypotheses.size
        self.nt = nt

    def defineModel(self):
        # LSTM return seq. model - able to reach around 0.75 val_acc - hits odd
        # spikes in loss (maybe where things sometimes become NaN), takes
        # forever to train (~11 minutes per epoch with batchsize=64,
        # timesteps=200 and 50000 training trajs, 4096 valid) - also seems
        # unstable (for some initialization of weights it does not learn)
        # the reason it is unstable could be that I used relu in the LSTM unit
        #model = Sequential()
        #model.add(LSTM(64, activation='relu',
            #return_sequences=True,
            #input_shape=(self.timesteps,1)))
        #if self.use_dropout:
            #model.add(Dropout(0.5)) # add dropout to prevent overfitting
        #model.add(TimeDistributed(Dense(1,activation='relu')))
        #model.add(Flatten())
        #model.add(Dense(3,activation='relu'))
        #model.add(Dense(self.num_classes,activation='softmax'))

        # LSTM with return sequences=true and stateful, hit 0.8940 on
        # t0=0,T=4,nt=100, homodyne signal
        #model = Sequential()
        #model.add(LSTM(3, activation='relu',
        #    return_sequences=True,
        #    stateful=True,
        #    batch_input_shape=(64,self.timesteps,1)))
        #if self.use_dropout:
        #    model.add(Dropout(0.4)) # add dropout to prevent overfitting
        ##model.add(TimeDistributed(Dense(1,activation='relu')))
        #model.add(Flatten())
        #model.add(Dense(1024,activation='relu'))
        #model.add(Dense(self.num_classes,activation='softmax'))

        # hit 0.98 val_acc with t0=0,T=4,nt=100, homodyne signal with this arch
        # EDIT: acc 0.625, val_acc 0.63550 with batch size 1024 and time steps = trajectory length (8001)
        # took 6580 seconds 22 epochs, hit 0.63550 after 17
       # model = Sequential()
       # model.add(LSTM(1, activation='relu',
       #     return_sequences=True,
       #     input_shape=(self.timesteps,1)))
       # #model.add(TimeDistributed(Dense(1,activation='relu')))
       # model.add(Flatten())
       # model.add(Dense(16,activation='relu'))
       # if self.use_dropout:
       #     model.add(Dropout(0.4)) # add dropout to prevent overfitting
       # model.add(Dense(self.num_classes,activation='softmax'))

        model = Sequential()
        model.add(AveragePooling1D(pool_size=32,input_shape=(self.timesteps,1)))
        #model.add(LSTM(2, activation='relu', return_sequences=True, input_shape=(self.timesteps,1)))
        model.add(LSTM(8, return_sequences=True, activation='relu'))
        #model.add(LSTM(64, return_sequences=True, activation='relu'))
        #model.add(TimeDistributed(Dense(1,activation='relu')))
        model.add(Flatten())
        model.add(Dense(128,activation='relu'))
        if self.use_dropout:
            model.add(Dropout(0.3)) # add dropout to prevent overfitting
        model.add(Dense(self.num_classes,activation='softmax'))

        # LSTM CNN Hybrid
        #model = Sequential()
        #model.add(Conv1D(2, 40, strides=2, input_shape=(self.timesteps,1), activation='relu',kernel_initializer='he_uniform',bias_initializer=tf.keras.initializers.Constant(0.1)))
        #model.add(Conv1D(4, 80, strides=2, activation='relu',kernel_initializer='he_uniform',bias_initializer=tf.keras.initializers.Constant(0.1)))
        #model.add(AveragePooling1D(pool_size=2))
        #model.add(Conv1D(4, 80, strides=2, activation='relu',kernel_initializer='he_uniform',bias_initializer=tf.keras.initializers.Constant(0.1)))
        #model.add(Conv1D(4, 120, strides=2, activation='relu',kernel_initializer='he_uniform',bias_initializer=tf.keras.initializers.Constant(0.1)))
        #model.add(AveragePooling1D(pool_size=2))
        ##model.add(LSTM(2, activation='relu', return_sequences=True, input_shape=(self.timesteps,1)))
        #model.add(LSTM(32, return_sequences=True, activation='relu'))
        ###model.add(LSTM(64, return_sequences=True, activation='relu'))
        ###model.add(TimeDistributed(Dense(1,activation='relu')))
        #model.add(Flatten())
        ##model.add(Dense(128,activation='relu'))
        #if self.use_dropout:
        #    model.add(Dropout(0.3)) # add dropout to prevent overfitting
        #model.add(Dense(self.num_classes,activation='softmax'))

        model.summary()
        return model

    def getDimsRequired(self):
        return 3

class ParameterConvLSTMModel(KerasModel):
    """
    A model for parameter estimation from hypothesis set using LSTM
    but with convolution operations in LSTM cells.

    ConvLSTM replaces matrix multiplication with convolution operation at each
    gate in LSTM cell in both input-to-state and state-to-state transition.
    """

    def __init__(self,hidden_size,nt,timesteps,use_dropout,hypotheses):
        super().__init__(hidden_size,timesteps,use_dropout)
        self.num_classes = hypotheses.size
        self.nt = nt

    def defineModel(self):
        # ConvLSTM approach - experimental - has special demands for input dims
        # (5D) - can reach upwards of 0.97 val_acc
        model = Sequential()
        model.add(ConvLSTM2D(filters=2, kernel_size=(1,100), activation='relu',
            input_shape=(self.nt//self.timesteps, 1, self.timesteps, 1)))
        model.add(Flatten())
        model.add(Dense(8,activation='relu'))
        model.add(Dense(3,activation='relu'))
        model.add(Dense(self.num_classes,activation='softmax'))
        model.summary()
        return model

    def getDimsRequired(self):
        return 5

class ParameterMLPModel(KerasModel):
    """
    A model for parameter estimation from hypothesis set using MLP.
    """
    def __init__(self,hidden_size,nt,timesteps,use_dropout,hypotheses):
        super().__init__(hidden_size,timesteps,use_dropout)
        self.num_classes = hypotheses.size
        self.nt = nt

    def defineModel(self):
        # Simple architecture Multilayer Perceptron (MLP) reaching around 0.7 val_acc
        model = Sequential()
        model.add(Dense(1, input_shape=(self.timesteps,1), activation='relu'))
        model.add(Flatten())
        model.add(Dense(2,activation='relu'))
        model.add(Dense(3,activation='relu'))
        #model.add(Dense(5,activation='relu'))
        model.add(Dense(1024,activation='relu'))
        if self.use_dropout:
            model.add(Dropout(0.3)) # add dropout to prevent overfitting
        #model.add(Dense(1024,activation='relu'))
        model.add(Dense(self.num_classes,activation='softmax'))

        # Simple architecture Multilayer Perceptron (MLP) reaching around 0.7 val_acc
        # EDIT: reaches no higher than ~0.546 acc
        #model = Sequential()
        #model.add(Dense(1, input_shape=(self.timesteps,1), activation='relu'))
        #model.add(Flatten())
        #if self.use_dropout:
        #    model.add(Dropout(0.3)) # add dropout to prevent overfitting
        #model.add(Dense(20,activation='relu'))
        #model.add(Dense(1024,activation='relu'))
        ##model.add(Dense(1024,activation='relu'))
        #model.add(Dense(self.num_classes,activation='softmax'))

        model.summary()
        return model

    def getDimsRequired(self):
        return 3

class ParameterCNNModel(KerasModel):
    """
    A model for parameter estimation from hypothesis set using CNN.
    """
    def __init__(self,hidden_size,nt,timesteps,use_dropout,hypotheses):
        super().__init__(hidden_size,timesteps,use_dropout)
        self.num_classes = hypotheses.size
        self.nt = nt

    def defineModel(self):
        # Convolutional neural network - able to reach around 0.95 val_acc
        # pretty good model I could use that is not identical to Eliska's model
        # MAIN MODEL for dt=0.05, good params: avgpool(2,2,2), conv((4,10),(8,5),(16,3)) - cant handle increasing resolution of time to dt=0.005, maybe because the model params increase a lot with T in this case
        #model = Sequential()
        #model.add(Conv1D(4,10, input_shape=(self.timesteps,1),
        #    activation='relu'))
        #model.add(AveragePooling1D(pool_size=2))
        #model.add(Conv1D(8,5, activation='relu'))
        #model.add(AveragePooling1D(pool_size=2))
        #model.add(Conv1D(16,3, activation='relu'))
        #model.add(AveragePooling1D(pool_size=2))
        #model.add(Flatten())
        #model.add(Dense(1024,activation='relu'))
        #if self.use_dropout:
        #    model.add(Dropout(0.4)) # add dropout to prevent overfitting
        #model.add(Dense(self.num_classes,activation='softmax'))

        # Convolutional neural network - experimental - hit 0.9861 val_acc with
        # this setup - seems unstable
        # 09/02/2020: Seem to have hit val_acc=1 with this network on homodyne,dt=0.005 signal with T=50
        #model = Sequential()
        #model.add(Conv1D(1,100, input_shape=(self.timesteps,1),
        #    activation='relu'))
        #model.add(AveragePooling1D())
        #model.add(Conv1D(1,50, activation='relu'))
        #model.add(AveragePooling1D())
        #model.add(Conv1D(1,5, activation='relu'))
        #model.add(AveragePooling1D())
        ##if self.use_dropout:
        ##    model.add(Dropout(0.5)) # add dropout to prevent overfitting
        #model.add(Flatten())
        #model.add(Dense(2,activation='relu'))
        #model.add(Dense(3,activation='relu'))
        #model.add(Dense(self.num_classes,activation='softmax'))

        # Convolutional neural network - experimental
        # trying scaling of kernels with input size (T)
        #first_kernel_width = self.timesteps//10
        #print("first kernel width: ",first_kernel_width)
        #model = Sequential()
        #model.add(Conv1D(8,first_kernel_width, input_shape=(self.timesteps,1),
        #    activation='relu',kernel_initializer='he_uniform',bias_initializer=tf.keras.initializers.Constant(0)))
        #model.add(MaxPooling1D(pool_size=5))
        #model.add(Conv1D(16,first_kernel_width//5, activation='relu', kernel_initializer='he_uniform',bias_initializer=tf.keras.initializers.Constant(0)))
        #model.add(MaxPooling1D(pool_size=5))
        ##model.add(Conv1D(8,first_kernel_width//5, activation='relu', kernel_initializer='he_uniform',bias_initializer=tf.keras.initializers.Constant(0)))
        ##model.add(MaxPooling1D(pool_size=2))
        ##model.add(Flatten())
        ##model.add(Dense(1024,activation='relu', input_shape=(self.timesteps,1)))
        #model.add(Flatten())
        #model.add(Dense(1024,activation='relu'))
        #if self.use_dropout:
        #    model.add(Dropout(0.3)) # add dropout to prevent overfitting
        ##model.add(Dense(2,activation='relu'))
        ##model.add(Dense(3,activation='relu'))
        ##model.add(Dense(3,activation='relu'))
        #model.add(Dense(self.num_classes,activation='softmax'))

        # BOTH COUNTING AND HOMODYNE FINAL MODELS USED THIS MODEL WITHOUT SGDR
        # Convolutional neural network 
        # Trying scaling of kernels with input size (T).
        # Hit val_acc > 0.9 consistently (n_folds=10) on T=50, dt=0.005, homodyne, batchsize=32,
        # using this model and with
        # SGDR = sgdr.SGDRScheduler(1e-5,1e-2,train_data_generator.__len__(),0.9,3) and
        # my_optimizer = tf.keras.optimizers.SGD()
        # but the training is very slow and seems to slow down over time.
        # Dont know if this happens generally or only with the scheduler.
        # {
        # The if statements concern really small records such as those generated by heavy binning
        first_kernel_width = self.timesteps//10 if self.timesteps//10 >= 2 else 2
        second_kernel_width = first_kernel_width//4 if first_kernel_width//4 >= 2 else 2
        print("first kernel width: ",first_kernel_width)
        print("second kernel width: ",second_kernel_width)
        pool_size = 5 if self.timesteps >= 250 else 2 #self.timesteps//1000
        print("pool_size: ",pool_size)
        model = Sequential()
        model.add(Conv1D(2, first_kernel_width, input_shape=(self.timesteps,1),
            activation='relu',kernel_initializer='he_uniform',bias_initializer=tf.keras.initializers.Constant(0.01)))
        model.add(MaxPooling1D(pool_size=pool_size))
        model.add(Conv1D(4,second_kernel_width, activation='relu', kernel_initializer='he_uniform',bias_initializer=tf.keras.initializers.Constant(0.01)))
        model.add(MaxPooling1D(pool_size=pool_size))
        model.add(Flatten())
        model.add(Dense(1024,activation='relu'))
        if self.use_dropout:
            model.add(Dropout(0.3)) # add dropout to prevent overfitting
        model.add(Dense(self.num_classes,activation='softmax'))
        # }

        # experiment with strides
        #first_kernel_width = 2
        #second_kernel_width = 4
        #print("first kernel width: ",first_kernel_width)
        #print("second kernel width: ",second_kernel_width)
        #pool_size = 2 
        #print("pool_size: ",pool_size)
        #model = Sequential()
        #model.add(Conv1D(2, first_kernel_width, input_shape=(self.timesteps,1),
        #    activation='relu',kernel_initializer='he_uniform', strides= first_kernel_width, bias_initializer=tf.keras.initializers.Constant(0.01)))
        #model.add(Conv1D(4,second_kernel_width, strides=second_kernel_width, activation='relu', kernel_initializer='he_uniform',bias_initializer=tf.keras.initializers.Constant(0.01)))
        #model.add(MaxPooling1D(pool_size=pool_size))
        #model.add(Conv1D(4,second_kernel_width, strides=second_kernel_width, activation='relu', kernel_initializer='he_uniform',bias_initializer=tf.keras.initializers.Constant(0.01)))
        #model.add(Conv1D(4,second_kernel_width*2, strides=second_kernel_width*2, activation='relu', kernel_initializer='he_uniform',bias_initializer=tf.keras.initializers.Constant(0.01)))
        #model.add(MaxPooling1D(pool_size=pool_size))
        #model.add(Flatten())
        #model.add(Dense(1024,activation='relu'))
        #if self.use_dropout:
        #    model.add(Dropout(0.3)) # add dropout to prevent overfitting
        #model.add(Dense(self.num_classes,activation='softmax'))
        # }

        # Eliska inspired model - hit over 0.99 val_acc on homodyne signal
        #model = Sequential()
        #model.add(Conv1D(filters=16, kernel_size=9, activation='relu',
        #    input_shape=(self.timesteps, 1)))
        #model.add(Conv1D(filters=32, kernel_size=9, activation='relu',
        #    input_shape=(self.timesteps, 1)))
        #model.add(AveragePooling1D(pool_size=2))
        #model.add(Flatten())
        #model.add(Dense(1024,activation='relu'))
        #if self.use_dropout:
        #    model.add(Dropout(0.4))
        #model.add(Dense(self.num_classes, activation='softmax'))

        model.summary()
        return model

    def getDimsRequired(self):
        return 3

class ParameterCNNModelT9(KerasModel):
    """
    A model for parameter estimation from hypothesis set using CNN
    optimized for T=9, dt=0.001 homodyne data.
    """

    def __init__(self,hidden_size,nt,timesteps,use_dropout,hypotheses):
        super().__init__(hidden_size,timesteps,use_dropout)
        self.num_classes = hypotheses.size
        self.nt = nt

    def defineModel(self):
        # Convolutional neural network - experimental
        model = Sequential()
        model.add(Conv1D(filters=10,kernel_size=1000, input_shape=(self.timesteps,1),
            activation='relu', kernel_initializer='he_uniform',bias_initializer=tf.keras.initializers.Constant(0.01)))
        model.add(MaxPooling1D(pool_size=2))
        model.add(Conv1D(filters=10,kernel_size=100, activation='relu', kernel_initializer='he_uniform',bias_initializer=tf.keras.initializers.Constant(0.01)))
        model.add(MaxPooling1D(pool_size=2))
        model.add(Conv1D(filters=10,kernel_size=10, activation='relu', kernel_initializer='he_uniform',bias_initializer=tf.keras.initializers.Constant(0.01)))
        model.add(MaxPooling1D(pool_size=2))
        model.add(Conv1D(filters=10,kernel_size=3, activation='relu', kernel_initializer='he_uniform',bias_initializer=tf.keras.initializers.Constant(0.01)))
        #model.add(GlobalAveragePooling1D())
        model.add(Flatten())
        model.add(Dense(64,activation='relu'))
        if self.use_dropout:
            model.add(Dropout(0.3)) # add dropout to prevent overfitting
        #model.add(Dense(3,activation='relu'))
        model.add(Dense(self.num_classes,activation='softmax'))
        
        # Convolutional neural network - hit 0.86 test acc and outperformed Qe of adaptive model
        #model = Sequential()
        #model.add(Conv1D(filters=10,kernel_size=1000, input_shape=(self.timesteps,1),
        #    activation='relu', kernel_initializer='he_uniform',bias_initializer=tf.keras.initializers.Constant(0.01)))
        #model.add(MaxPooling1D(pool_size=2))
        #model.add(Conv1D(filters=10,kernel_size=100, activation='relu', kernel_initializer='he_uniform',bias_initializer=tf.keras.initializers.Constant(0.01)))
        #model.add(MaxPooling1D(pool_size=2))
        #model.add(Conv1D(filters=10,kernel_size=10, activation='relu', kernel_initializer='he_uniform',bias_initializer=tf.keras.initializers.Constant(0.01)))
        #model.add(MaxPooling1D(pool_size=2))
        #model.add(Conv1D(filters=10,kernel_size=3, activation='relu', kernel_initializer='he_uniform',bias_initializer=tf.keras.initializers.Constant(0.01)))
        #model.add(Flatten())
        #model.add(Dense(64,activation='relu'))
        #if self.use_dropout:
        #    model.add(Dropout(0.3)) # add dropout to prevent overfitting
        #model.add(Dense(self.num_classes,activation='softmax'))

        # Convolutional neural network - hit test acc 0.850
        #model = Sequential()
        #model.add(Conv1D(filters=10,kernel_size=2000, input_shape=(self.timesteps,1),
        #    activation='relu'))
        #model.add(MaxPooling1D(pool_size=5))
        #model.add(Conv1D(filters=4,kernel_size=10, activation='relu'))
        #model.add(Conv1D(filters=4,kernel_size=3, activation='relu'))
        #model.add(GlobalAveragePooling1D())
        #model.add(Flatten())
        #model.add(Dense(256,activation='relu'))
        #if self.use_dropout:
        #    model.add(Dropout(0.3)) # add dropout to prevent overfitting
        #model.add(Dense(self.num_classes,activation='softmax'))


        # Convolutional neural network - hit 0.846 test acc
        #model = Sequential()
        #model.add(Conv1D(filters=10,kernel_size=2000, input_shape=(self.timesteps,1),
        #    activation='relu'))
        #model.add(Conv1D(filters=10,kernel_size=100, activation='relu'))
        #model.add(MaxPooling1D(pool_size=5))
        #model.add(Conv1D(filters=4,kernel_size=10, activation='relu'))
        #model.add(Conv1D(filters=4,kernel_size=3, activation='relu'))
        #model.add(GlobalAveragePooling1D())
        #model.add(Flatten())
        #model.add(Dense(128,activation='relu'))
        #if self.use_dropout:
        #    model.add(Dropout(0.3)) # add dropout to prevent overfitting
        #model.add(Dense(self.num_classes,activation='softmax'))



        # Convolutional neural network - experimental - hit 0.856 acc with this setup
        #model = Sequential()
        #model.add(Conv1D(2,1000, input_shape=(self.timesteps,1),
        #    activation='relu'))
        #model.add(MaxPooling1D(pool_size=2))
        #model.add(Conv1D(2,100, activation='relu'))
        #model.add(MaxPooling1D(pool_size=2))
        ##model.add(Conv1D(2,5, activation='relu'))
        ##model.add(MaxPooling1D(pool_size=2))
        #model.add(Flatten())
        #model.add(Dense(1024,activation='relu'))
        #if self.use_dropout:
        #    model.add(Dropout(0.3)) # add dropout to prevent overfitting
        ##model.add(Dense(3,activation='relu'))
        #model.add(Dense(self.num_classes,activation='softmax'))


        # Convolutional neural network - experimental
        # 0.853 acc
        #model = Sequential()
        #model.add(Conv1D(2,1000, input_shape=(self.timesteps,1),
        #    activation='relu'))
        #model.add(MaxPooling1D(pool_size=5))
        #model.add(Conv1D(2,100, activation='relu'))
        #model.add(MaxPooling1D(pool_size=5))
        ##model.add(Conv1D(2,50, activation='relu'))
        ##model.add(MaxPooling1D(pool_size=5))
        #model.add(Flatten())
        #model.add(Dense(1024,activation='relu'))
        #if self.use_dropout:
        #    model.add(Dropout(0.4)) # add dropout to prevent overfitting
        ##model.add(Dense(3,activation='relu'))
        #model.add(Dense(self.num_classes,activation='softmax'))

        # Convolutional neural network - experimental
        # trying scaling of kernels with input size (T)
        #first_kernel_width = self.timesteps//10
        #print("first kernel width: ",first_kernel_width)
        #model = Sequential()
        #model.add(Conv1D(8,first_kernel_width, input_shape=(self.timesteps,1),
        #    activation='relu',kernel_initializer='he_uniform',bias_initializer=tf.keras.initializers.Constant(0)))
        #model.add(MaxPooling1D(pool_size=5))
        #model.add(Conv1D(16,first_kernel_width//5, activation='relu', kernel_initializer='he_uniform',bias_initializer=tf.keras.initializers.Constant(0)))
        #model.add(MaxPooling1D(pool_size=5))
        ##model.add(Conv1D(8,first_kernel_width//5, activation='relu', kernel_initializer='he_uniform',bias_initializer=tf.keras.initializers.Constant(0)))
        ##model.add(MaxPooling1D(pool_size=2))
        ##model.add(Flatten())
        ##model.add(Dense(1024,activation='relu', input_shape=(self.timesteps,1)))
        #model.add(Flatten())
        #model.add(Dense(1024,activation='relu'))
        #if self.use_dropout:
        #    model.add(Dropout(0.3)) # add dropout to prevent overfitting
        ##model.add(Dense(2,activation='relu'))
        ##model.add(Dense(3,activation='relu'))
        ##model.add(Dense(3,activation='relu'))
        #model.add(Dense(self.num_classes,activation='softmax'))

        # Convolutional neural network
        # Trying scaling of kernels with input size (T).
        # Hit val_acc > 0.9 consistently (n_folds=10) on T=50, dt=0.005, homodyne, batchsize=32,
        # using this model and with
        # SGDR = sgdr.SGDRScheduler(1e-5,1e-2,train_data_generator.__len__(),0.9,3) and
        # my_optimizer = tf.keras.optimizers.SGD()
        # but the training is very slow and seems to slow down over time.
        # Dont know if this happens generally or only with the scheduler.
        # EDIT: With new fixed data, this achieves around 0.846 accuracy on test data.
        # {
        #first_kernel_width = self.timesteps//10 if self.timesteps//10 >= 2 else 2
        #second_kernel_width = first_kernel_width//4 if first_kernel_width//4 >= 2 else 2
        #print("first kernel width: ",first_kernel_width)
        #print("second kernel width: ",second_kernel_width)
        #pool_size = 5 #self.timesteps//1000
        #print("pool_size: ",pool_size)
        #model = Sequential()
        #model.add(Conv1D(2, first_kernel_width, input_shape=(self.timesteps,1),
        #    activation='relu',kernel_initializer='he_uniform',bias_initializer=tf.keras.initializers.Constant(0.01)))
        #model.add(MaxPooling1D(pool_size=pool_size))
        #model.add(Conv1D(4,second_kernel_width, activation='relu', kernel_initializer='he_uniform',bias_initializer=tf.keras.initializers.Constant(0.01)))
        #model.add(MaxPooling1D(pool_size=pool_size))
        #model.add(Flatten())
        #model.add(Dense(1024,activation='relu'))
        #if self.use_dropout:
        #    model.add(Dropout(0.3)) # add dropout to prevent overfitting
        #model.add(Dense(self.num_classes,activation='softmax'))
        # }

        # Eliska inspired model - way too many parameters for the current setup - OOM error
        #model = Sequential()
        #model.add(Conv1D(filters=16, kernel_size=9, activation='relu',
        #    input_shape=(self.timesteps, 1)))
        #model.add(Conv1D(filters=32, kernel_size=9, activation='relu',
        #    input_shape=(self.timesteps, 1)))
        #model.add(AveragePooling1D(pool_size=2))
        #model.add(Flatten())
        #model.add(Dense(1024,activation='relu'))
        #if self.use_dropout:
        #    model.add(Dropout(0.4))
        #model.add(Dense(self.num_classes, activation='softmax'))


        model.summary()
        return model

    def getDimsRequired(self):
        return 3

class ParameterCuDNN_CNN_LSTMModel(KerasModel):
    """
    A model for parameter estimation from hypothesis set using CNN
    but with an LSTM interpreting the features extracted by the CNN.
    Uses CUDA Deep Neural Network library LSTM which should run faster
    on GPU.
    """

    def __init__(self,hidden_size,nt,timesteps,use_dropout,hypotheses):
        super().__init__(hidden_size,timesteps,use_dropout)
        self.num_classes = hypotheses.size
        self.nt = nt

    def defineModel(self):
        # CNN-LSTM with CuDNNLSTM - does not work yet - problem with input
        # shape
        model = tf.keras.Sequential()
        model.add(tf.keras.layers.TimeDistributed(tf.keras.layers.Conv1D(filters=64,
            kernel_size=1, activation='relu'), input_shape=(None,self.timesteps,1)))
        model.add(tf.keras.layers.TimeDistributed(tf.keras.layers.MaxPooling1D(pool_size=2)))
        model.add(tf.keras.layers.Flatten())
        model.add(CuDNNLSTM(50))
        model.add(tf.keras.layers.Dense(self.num_classes,activation='softmax'))
        model.summary()
        return model

    def getDimsRequired(self):
        return 4

class ParameterCuDNN_LSTMModel(KerasModel):
    """
    A model for parameter estimation from hypothesis set using LSTM.
    Uses CUDA Deep Neural Network library LSTM which should run faster
    on GPU.
    """

    def __init__(self,hidden_size,nt,timesteps,use_dropout,hypotheses):
        super().__init__(hidden_size,timesteps,use_dropout)
        self.num_classes = hypotheses.size
        self.nt = nt

    def defineModel(self):
        # CuDNN LSTM - optimized for GPU - has a hard time learning - only hits
        # around 0.54 val_acc - needs to be a tf.keras model (optimizer thus
        # also needs to come from tf package) - unstable (for some 
        # initialization of weights it does not learn)
        model = tf.keras.Sequential()
        model.add(CuDNNLSTM(64, return_sequences=True, input_shape=(self.timesteps,1)))
        model.add(tf.keras.layers.Activation(tf.keras.activations.relu))
        model.add(tf.keras.layers.TimeDistributed(tf.keras.layers.Dense(3,activation='relu')))
        model.add(tf.keras.layers.Flatten())
        #model.add(tf.keras.layers.Dense(1024,activation='relu'))
        #if self.use_dropout:
        #    model.add(tf.keras.layers.Dropout(0.4)) # add dropout to prevent overfitting
        model.add(tf.keras.layers.Dense(self.num_classes,activation='softmax'))

        # stacking CuDNNLSTM layers - does not learn
        #model = tf.keras.Sequential()
        #model.add(CuDNNLSTM(200, input_shape=(self.timesteps,1),
        #    return_sequences=True))
        #model.add(CuDNNLSTM(100, input_shape=(self.timesteps,1)))
        #if self.use_dropout:
        #    model.add(Dropout(0.5)) # add dropout to prevent overfitting
        #model.add(tf.keras.layers.Dense(3,activation='relu'))
        #model.add(tf.keras.layers.Dense(self.num_classes,activation='softmax'))

        model.summary()
        return model

    def getDimsRequired(self):
        return 3

class ParameterAttentionModel(KerasModel):
    """
    A model for parameter estimation from hypothesis set using Attention.
    I cannot get this model to learn anything.
    """

    def __init__(self,hidden_size,nt,timesteps,use_dropout,hypotheses):
        super().__init__(hidden_size,timesteps,use_dropout)
        self.num_classes = hypotheses.size
        self.nt = nt

    def defineModel(self):

        input1 = tf.keras.Input(shape=(self.timesteps,1))#,batch_size=64)
        cnn1 = Conv1D(4,50, activation='relu',padding='same')
        flatten1 = Flatten()
        flatten2 = Flatten()
        flatten3 = Flatten()
        maxpool1 = MaxPooling1D(pool_size=4)
        cnn2 = Conv1D(60,30, activation='relu',padding='same')
        maxpool2 = MaxPooling1D(pool_size=15)
        cnn3 = Conv1D(6,5, activation='relu',padding='same')
        maxpool3 = MaxPooling1D(pool_size=1)
        dense = Dense(self.timesteps,activation='relu')
        dense2 = Dense(3,activation='relu')
        dropout = Dropout(0.4)
        dense_out = Dense(self.num_classes,activation='softmax')

        query1 = cnn1(input1)
        maxpooled1 = maxpool1(query1)
        query2 = cnn2(maxpooled1)
        maxpooled2 = maxpool2(query2)
        last_cnn = cnn3(maxpooled2)
        maxpooled3 = maxpool3(last_cnn)
        value = dropout(dense(flatten1(maxpooled3)))

        #attention1 = tf.keras.layers.AdditiveAttention()([flatten2(maxpooled1),value])
        #attention2 = tf.keras.layers.AdditiveAttention()([flatten3(maxpooled2),value])

        attention1 = Attention()([flatten2(maxpooled1),value])
        attention2 = Attention()([flatten3(maxpooled2),value])

        output = tf.keras.layers.Concatenate()([attention1, attention2])
        #print(tf.keras.backend.shape(output))
        #output = value
        output_dense = (dense2(output))
        output_softmax = dense_out(output_dense)
        #output = tf.keras.layers.Concatenate()([query1, attention1, attention2])

        model = Sequential()
        model = tf.keras.models.Model(inputs=input1,outputs = output_softmax)
        model.summary()
        return model

    def getDimsRequired(self):
        return 3

class RegressionParameterCNNModel(KerasModel):
    """
    A regression model for parameter estimation using CNN.
    """

    def __init__(self,hidden_size,nt,timesteps,use_dropout):
        super().__init__(hidden_size,timesteps,use_dropout)
        self.nt = nt

    def defineModel(self):
        # Convolutional neural network with three convolutional+pooling layers
        # hit 0.574 val_mae with some previous setup
        # this one hit 1.23774
     
        #model = Sequential()
        ##model.add(Conv1D(filters=32,kernel_size=16, input_shape=(self.timesteps,1), activation='relu'))
        #model.add(Conv1D(filters=4,kernel_size=16, input_shape=(self.timesteps,1),kernel_initializer='glorot_uniform',bias_initializer=tf.keras.initializers.Constant(0.0)))
        ##model.add(tf.keras.layers.LeakyReLU(alpha=0.3))
        ##model.add(tf.keras.layers.BatchNormalization())
        #model.add(tf.keras.layers.ReLU())
        ##model.add(MaxPooling1D(pool_size=2))
        #model.add(MaxPooling1D(pool_size=2))

        ##model.add(Conv1D(filters=32,kernel_size=8, activation='relu'))
        #model.add(Conv1D(filters=8,kernel_size=8,kernel_initializer='glorot_uniform',bias_initializer=tf.keras.initializers.Constant(0.0)))
        ##model.add(tf.keras.layers.LeakyReLU(alpha=0.3))
        ##model.add(tf.keras.layers.BatchNormalization())
        #model.add(tf.keras.layers.ReLU())
        #model.add(MaxPooling1D(pool_size=2))
        ##model.add(MaxPooling1D(pool_size=2))

        #model.add(Conv1D(filters=16,kernel_size=4,kernel_initializer='glorot_uniform',bias_initializer=tf.keras.initializers.Constant(0.0)))
        ##model.add(tf.keras.layers.BatchNormalization())
        ##model.add(tf.keras.layers.LeakyReLU(alpha=0.3))
        #model.add(tf.keras.layers.ReLU())
        ##model.add(MaxPooling1D(pool_size=2))
        #model.add(MaxPooling1D(pool_size=2))
        #model.add(Flatten())
        ##model.add(Dense(1024))
        ###model.add(tf.keras.layers.BatchNormalization())
        ###model.add(tf.keras.layers.LeakyReLU(alpha=0.3))
        #model.add(tf.keras.layers.ReLU())
        #model.add(Dense(2048,activation='relu'))
        #if self.use_dropout:
        #    model.add(Dropout(0.5)) # add dropout to prevent overfitting
        #model.add(Dense(1))

        
#        # testing
#        model = Sequential()
#        model.add(Conv1D(filters=16,kernel_size=16, input_shape=(self.timesteps,1), activation='relu'))
#        #model.add(Conv1D(filters=32,kernel_size=16, input_shape=(self.timesteps,1),kernel_initializer='glorot_uniform',bias_initializer=tf.keras.initializers.Constant(0.0)))
#        #model.add(tf.keras.layers.LeakyReLU(alpha=0.3))
#        #model.add(tf.keras.layers.BatchNormalization())
#        #model.add(tf.keras.layers.ReLU())
#        #model.add(MaxPooling1D(pool_size=2))
#        model.add(MaxPooling1D(pool_size=2))
#
#        model.add(Conv1D(filters=32,kernel_size=8, activation='relu'))
#        #model.add(Conv1D(filters=8,kernel_size=8,kernel_initializer='glorot_uniform',bias_initializer=tf.keras.initializers.Constant(0.0)))
#        #model.add(tf.keras.layers.LeakyReLU(alpha=0.3))
#        #model.add(tf.keras.layers.BatchNormalization())
#        #model.add(tf.keras.layers.ReLU())
#        model.add(MaxPooling1D(pool_size=2))
#        #model.add(MaxPooling1D(pool_size=2))
#
#        model.add(Conv1D(filters=32,kernel_size=8, activation='relu'))
#        #model.add(Conv1D(filters=16,kernel_size=4,kernel_initializer='glorot_uniform',bias_initializer=tf.keras.initializers.Constant(0.0)))
#        #model.add(tf.keras.layers.BatchNormalization())
#        #model.add(tf.keras.layers.LeakyReLU(alpha=0.3))
#        #model.add(tf.keras.layers.ReLU())
#        model.add(MaxPooling1D(pool_size=2))
#        model.add(Flatten())
#        #model.add(Dense(1024))
#        ##model.add(tf.keras.layers.BatchNormalization())
#        ##model.add(tf.keras.layers.LeakyReLU(alpha=0.3))
#        #model.add(tf.keras.layers.ReLU())
#        model.add(Dense(1024,activation='relu'))
#        if self.use_dropout:
#            model.add(Dropout(0.4)) # add dropout to prevent overfitting
#        model.add(Dense(1))

        # Same model as used for hypothesis testing 05/02/2020
        #model = Sequential()
        #model.add(Conv1D(4,10, input_shape=(self.timesteps,1),
        #    activation='relu'))
        #model.add(AveragePooling1D(pool_size=2))
        #model.add(Conv1D(8,5, activation='relu'))
        #model.add(AveragePooling1D(pool_size=2))
        #model.add(Conv1D(16,3, activation='relu'))
        #model.add(AveragePooling1D(pool_size=2))
        #model.add(Flatten())
        #model.add(Dense(1024,activation='relu'))
        #if self.use_dropout:
        #    model.add(Dropout(0.4)) # add dropout to prevent overfitting
        #model.add(Dense(1))

        # CNN hit 2.273 mae on T=9,dt=0.001 data
        #first_kernel_width = self.timesteps//10
        #print("first kernel width: ",first_kernel_width)
        #pool_size = 5 #self.timesteps//1000
        #print("pool_size: ",pool_size)
        #model = Sequential()
        #model.add(Conv1D(2,first_kernel_width, input_shape=(self.timesteps,1),
        #    activation='relu',kernel_initializer='he_uniform',bias_initializer=tf.keras.initializers.Constant(0.01)))
        #model.add(MaxPooling1D(pool_size=pool_size))
        #model.add(Conv1D(4,first_kernel_width//4, activation='relu', kernel_initializer='he_uniform',bias_initializer=tf.keras.initializers.Constant(0.01)))
        #model.add(MaxPooling1D(pool_size=pool_size))
        ##model.add(Conv1D(2,first_kernel_width//4, activation='relu', kernel_initializer='he_uniform',bias_initializer=tf.keras.initializers.Constant(0.01)))
        ##model.add(MaxPooling1D(pool_size=pool_size))
        #model.add(Flatten())
        #model.add(Dense(1024,activation='relu'))
        #if self.use_dropout:
        #    model.add(Dropout(0.3)) # add dropout to prevent overfitting
        #model.add(Dense(1))

        # USING THIS MODEL AS FINAL MODEL (NO SGDR)
        # Convolutional neural network
        # Trying scaling of kernels with input size (T).
        # Hit val_acc > 0.9 consistently (n_folds=10) on T=50, dt=0.005, homodyne, batchsize=32,
        # using this model and with
        # SGDR = sgdr.SGDRScheduler(1e-5,1e-2,train_data_generator.__len__(),0.9,3) and
        # my_optimizer = tf.keras.optimizers.SGD()
        # but the training is very slow and seems to slow down over time.
        # Dont know if this happens generally or only with the scheduler.
        # {
        first_kernel_width = self.timesteps//10
        print("first kernel width: ",first_kernel_width)
        pool_size = 5 #self.timesteps//1000
        print("pool_size: ",pool_size)
        model = Sequential()
        model.add(Conv1D(2,first_kernel_width, input_shape=(self.timesteps,1),
            activation='relu',kernel_initializer='he_uniform',bias_initializer=tf.keras.initializers.Constant(0.01)))
        model.add(MaxPooling1D(pool_size=pool_size))
        model.add(Conv1D(4,first_kernel_width//4, activation='relu', kernel_initializer='he_uniform',bias_initializer=tf.keras.initializers.Constant(0.01)))
        model.add(MaxPooling1D(pool_size=pool_size))
        #model.add(Conv1D(2,first_kernel_width//4, activation='relu', kernel_initializer='he_uniform',bias_initializer=tf.keras.initializers.Constant(0.01)))
        #model.add(MaxPooling1D(pool_size=pool_size))
        model.add(Flatten())
        model.add(Dense(1024,activation='relu'))
        if self.use_dropout:
            model.add(Dropout(0.3)) # add dropout to prevent overfitting
        model.add(Dense(1))
        # }



        # Eliska inspired model - hit over 0.99 val_acc on homodyne signal
        #model = Sequential()
        #model.add(Conv1D(filters=16, kernel_size=9, activation='relu',
        #    input_shape=(self.timesteps, 1)))
        #model.add(Conv1D(filters=32, kernel_size=9, activation='relu',
        #    input_shape=(self.timesteps, 1)))
        #model.add(AveragePooling1D(pool_size=2))
        #model.add(Flatten())
        #model.add(Dense(1024,activation='relu'))
        #if self.use_dropout:
        #    model.add(Dropout(0.4))
        #model.add(Dense(1))

        model.summary()
        return model

    def getDimsRequired(self):
        return 3

class RegressionParameterCNNModelT50(KerasModel):
    """
    A regression model for parameter estimation using CNN specialized for T=50.
    """

    def __init__(self,hidden_size,nt,timesteps,use_dropout):
        super().__init__(hidden_size,timesteps,use_dropout)
        self.nt = nt

    def defineModel(self):
        # USING THIS MODEL AS FINAL MODEL (NO SGDR)
        # Convolutional neural network
        # Trying scaling of kernels with input size (T).
        # Hit val_acc > 0.9 consistently (n_folds=10) on T=50, dt=0.005, homodyne, batchsize=32,
        # using this model and with
        # SGDR = sgdr.SGDRScheduler(1e-5,1e-2,train_data_generator.__len__(),0.9,3) and
        # my_optimizer = tf.keras.optimizers.SGD()
        # but the training is very slow and seems to slow down over time.
        # Dont know if this happens generally or only with the scheduler.
        # {
        first_kernel_width = self.timesteps//10
        first_kernel_width = 100
        print("first kernel width: ",first_kernel_width)
        pool_size = 5 #self.timesteps//1000
        print("pool_size: ",pool_size)
        model = Sequential()
        model.add(Conv1D(4,50, input_shape=(self.timesteps,1),
            activation='relu',kernel_initializer='he_uniform',bias_initializer=tf.keras.initializers.Constant(0.01)))
        model.add(MaxPooling1D(pool_size=pool_size))
        model.add(Conv1D(8,20, activation='relu', kernel_initializer='he_uniform',bias_initializer=tf.keras.initializers.Constant(0.01)))
        model.add(MaxPooling1D(pool_size=pool_size))
        model.add(Conv1D(16,10, activation='relu', kernel_initializer='he_uniform',bias_initializer=tf.keras.initializers.Constant(0.01)))
        model.add(MaxPooling1D(pool_size=pool_size))
        #model.add(Conv1D(2,first_kernel_width//4, activation='relu', kernel_initializer='he_uniform',bias_initializer=tf.keras.initializers.Constant(0.01)))
        #model.add(MaxPooling1D(pool_size=pool_size))
        model.add(Flatten())
        model.add(Dense(256,activation='relu'))
        if self.use_dropout:
            model.add(Dropout(0.3)) # add dropout to prevent overfitting
        model.add(Dense(1))
        # }

        model.summary()
        return model

    def getDimsRequired(self):
        return 3

class RegressionParameterConvLSTMModel(KerasModel):
    """
    A regression model for parameter estimation using LSTM
    but with convolution operations in LSTM cells.

    ConvLSTM replaces matrix multiplication with convolution operation at each
    gate in LSTM cell in both input-to-state and state-to-state transition.
    """

    def __init__(self,hidden_size,nt,timesteps,use_dropout):
        super().__init__(hidden_size,timesteps,use_dropout)
        self.nt = nt

    def defineModel(self):
        # ConvLSTM approach - experimental - has special demands for input dims
        # (5D)
        # for some reason it seems like this model can reach 0.0592 val_mae but still have bad generalization
        # something is very wrong
        model = Sequential()
        model.add(ConvLSTM2D(filters=1, kernel_size=(1,50), activation='relu',
            input_shape=(self.nt//self.timesteps, 1, self.timesteps, 1)))
        #model.add(ConvLSTM2D(filters=1, kernel_size=(1,10),
            #input_shape=(self.nt//self.timesteps, 1, self.timesteps, 1)))
        model.add(Flatten())
        model.add(Dense(2,activation='relu'))
        model.add(Dense(3,activation='relu'))
        model.add(Dense(1))
        model.summary()
        return model

    def getDimsRequired(self):
        return 5

class RegressionParameterMLPModel(KerasModel):
    """
    A regression model for parameter estimation using a simple
    multilayer perceptron (MLP).
    """

    def __init__(self,hidden_size,nt,timesteps,use_dropout):
        super().__init__(hidden_size,timesteps,use_dropout)
        self.nt = nt

    def defineModel(self):
        # hit val_mae=1.03 with this setup
        #model = Sequential()
        #model.add(Dense(1, input_shape=(self.timesteps,1),activation='relu'))
        #model.add(Flatten())
        #model.add(Dense(64,activation='relu'))
        #model.add(Dense(64,activation='relu'))
        #if self.use_dropout:
        #    model.add(Dropout(0.4)) # add dropout to prevent overfitting
        #model.add(Dense(1))


        # Simple architecture Multilayer Perceptron (MLP) reaching around 0.7 val_acc
        model = Sequential()
        #model.add(Dense(1, input_shape=(self.timesteps,1), activation='relu'))
        model.add(Dense(1, input_shape=(self.timesteps,1)))
        model.add(tf.keras.layers.LeakyReLU(alpha=0.3))
        model.add(Flatten())
        model.add(Dense(3,activation='relu'))
        model.add(Dense(2,activation='relu'))
        if self.use_dropout:
            model.add(Dropout(0.4)) # add dropout to prevent overfitting
        model.add(Dense(1))

        model.summary()
        return model

    def getDimsRequired(self):
        return 3

