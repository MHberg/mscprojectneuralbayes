Things to try:

decrease amount of layers to 1
decrease batch size to 128 instead of 1024
think



Fixing loss becoming nans: (nans likely cause be exploding gradients problem)
Use LeakyReLU
Use clipnorm=1


    Normalize your outputs by quantile normalizing or z scoring. To be rigorous, compute this transformation on the training data, not on the entire dataset. For example, with quantile normalization, if an example is in the 60th percentile of the training set, it gets a value of 0.6. (You can also shift the quantile normalized values down by 0.5 so that the 0th percentile is -0.5 and the 100th percentile is +0.5).

    Add regularization, either by increasing the dropout rate or adding L1 and L2 penalties to the weights. L1 regularization is analogous to feature selection, and since you said that reducing the number of features to 5 gives good performance, L1 may also.

    If these still don't help, reduce the size of your network. This is not always the best idea since it can harm performance, but in your case you have a large number of first-layer neurons (1024) relative to input features (35) so it may help.

    Increase the batch size from 32 to 128. 128 is fairly standard and could potentially increase the stability of the optimization.

