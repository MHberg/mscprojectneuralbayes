import numpy as np
from scipy.linalg import expm
import matplotlib.pyplot as plt

fig = plt.figure()
ax = plt.gca()
ts = np.linspace(0,10,100)
Omega = 2
Delta = 0
gamma = 1
dt = 0.001
T = 10
nt = int(T/dt+1)
print(nt)

Hsys = np.array([[0, Omega/2],[Omega/2, -Delta]],dtype='complex_') # single photon atom-field system
C = np.sqrt(gamma)*np.array([[0,0],[1,0]],dtype='complex_')
Phi = np.pi/2
CPhi = C*np.exp(-1j*Phi)
Heff = Hsys - 1j*(CPhi.conj().T @ CPhi)/2
U = expm(-1j*Heff*dt)
psi0 = np.array([[0],[1]],dtype='complex_').reshape(2)
psi = np.zeros([2,nt],dtype='complex_')
psi2 = np.zeros([2,nt],dtype='complex_')
psi[:,0] = psi0
psi2[:,0] = psi0

times = np.linspace(0,T,nt)
print(times)

a = np.array([[1,0],[0,1]])-1j*Heff*dt
print('U:',U)
print('a:', a)

for i in range(1,nt):
    dtActual = times[i-1]-times[i]
    #print(dtActual)
    psi[:,i] = U@psi[:,i-1]
    psi2[:,i] = a@psi2[:,i-1]
print(psi[0,:10])
print(psi2[0,:10])
ax.plot(times,np.sqrt(np.power(np.abs(psi[0,:]),2)),'r--')
#ax.plot(times,np.real(np.sqrt(psi[0,:].conj()*psi[0,:])))
#print(np.sqrt(psi[0,:].conj()*psi[0,:]))

ax.plot(times,np.sqrt(np.power(np.abs(psi2[0,:]),2)),'b--')
#ax.plot(times,np.real(np.sqrt(psi2[0,:].conj()*psi2[0,:])))
plt.show()
