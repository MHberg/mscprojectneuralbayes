import matplotlib.pyplot as plt
from matplotlib import rc
#plt.rcParams.update({'font.size': 16})
import pandas as pd
import sys
# insert at 1, 0 is the script path (or '' in REPL)
#sys.path.insert(1, '/home/henneberg/speciale/first_project/python_stuff/')
from plottingTools import *

# With manual seeding and dt=0.05 and T=5

# counting random numbers : 1096193
# homodyne random numbers : 2847392
data_path = '../cpp_stuff/build/'

# Counting - 500.000 training, 4096 validation, 4096 test
headers = ['Numbers']
data = pd.read_csv(data_path+'checkCounting2.txt', header=None, names=headers)
print('Number of random values: ',data.size)

fig,axs = plt.subplots(1,2)
axs[0].hist(data['Numbers'],1000)
axs[0].set(xlabel = 'Random value')
axs[0].set(ylabel='Number of occurrences')

# Homodyne - 20000 training, 4096 validation, 4096 test
headers = ['Numbers']
data = pd.read_csv(data_path+'checkHomodyne.txt', header=None, names=headers)
print('Number of random values: ',data.size)

axs[1].hist(data['Numbers'],1000)
axs[1].set(xlabel = 'Random value')

#print(data)
#axs[0].annotate(r"(a)", xy=(0.50,1.07), xycoords='axes fraction')
#axs[1].annotate(r"(b)", xy=(0.50,1.07), xycoords='axes fraction')
#axs[0].annotate(r"(a)", xy=(0.82,0.92), xycoords='axes fraction')
#axs[1].annotate(r"(b)", xy=(0.82,0.92), xycoords='axes fraction')
axs[0].set_title(r"(a)",pad=13)
axs[1].set_title(r"(b)",pad=13)
axs[0].set_xlim([0,1])
axs[0].set_ylim([0,1400])
axs[1].set_xlim([-5,5])
axs[1].set_ylim([0,14000])
enchantAx(axs[0])
enchantAx(axs[1])
fig.set_size_inches(6, 3)
plt.tight_layout()

save_path = "C:/Users/Henneberg/speciale_local/thesisFigures/"
plt.savefig(save_path + 'randomDistributions.pdf',dpi=100)


plt.show()
