import matplotlib.pyplot as plt
import matplotlib.animation as animation
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter,
                                       AutoMinorLocator)
import numpy as np
from matplotlib import rc
#rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
#rc('font',**{'family':'serif','serif':['Palatino']})
#rc('text', usetex=True)

def getDensityMatrixFromStateVector(state):
    if state.ndim == 3: #chunky implementation to allow evaluation of density matrices for multiple trajectories described by wave functions
        rho_vec = np.zeros((state.shape[0],4,state.shape[2]),dtype='complex_')
        for i in range(state.shape[0]):
            rho_vec[i,:,:] = getDensityMatrixFromStateVector(state[i,:,:])
    else:
        rho_vec = np.zeros((4,state.shape[1]),dtype='complex_')
        i = 0
        state_0 = state[0,:] #TODO:there has to be a better way to do this
        state_1 = state[1,:]
        for state_0_i,state_1_i in zip(state_0,state_1):
            state_i = np.array([[state_0_i],[state_1_i]])
            rho = np.outer(state_i, state_i.conj())
            rho_vec[:,i] = rho.reshape([4,])
            i += 1
    return rho_vec

# t is time index, not actual time
def getBlochComponents(state):
    if (state.shape[0] == 4):
        rho = state
        u = np.real(rho[2,:] + rho[1,:])
        v = np.real(complex(1j)*(rho[1,:] - rho[2,:]))
        w = np.real(rho[0,:] - rho[3,:])
        R = np.array((u,v,w))
    elif (state.shape[0] == 2):
        rho = getDensityMatrixFromStateVector(state)
        R = getBlochComponents(rho)
    else:
        print('Error: state must be a 4xt density matrix in super space (Fock-Louvillian space) or a 2xt wave function, where t is arbitrary number of time steps')
        exit(1)
    return R

def plotBlochSphere():
    fig = plt.figure()
    ax = fig.gca(projection = '3d')
    #ax.set_aspect('equal')
    u, v = np.mgrid[0:2*np.pi:50j, 0:np.pi:50j]
    x = np.cos(u)*np.sin(v)
    y = np.sin(u)*np.sin(v)
    z = np.cos(v)
    ax.plot_wireframe(x, y, z, rcount=20, ccount=20, color="gray")
    ax.scatter([0],[0],[0], color='gray', s=10)
    ax.quiver(0,0,0,1.5,0,0, color='gray', arrow_length_ratio=0.05) # x-axis arrow
    ax.quiver(0,0,0,0,1.5,0, color='gray', arrow_length_ratio=0.05) # y-axis arrow
    ax.quiver(0,0,0,0,0,1.5, color='gray', arrow_length_ratio=0.05) # z-axis arrow
    return fig

def plotBlochComponents(fig,R,t):
    ax = fig.gca()
    arrow = ax.quiver(0,0,0,R[0][t],R[1][t],R[2][t],color="black")#,arrow_length_ratio=0.15)
    return arrow

def plotXYZ(t,x,y,z):
    fig,axs = plt.subplots(3)
    #fig.suptitle('x, y and z coords as function of time')
    #print('x',x)
    #print('y',y)
    #print('z',z)
    axs[0].plot(t,x)
    axs[1].plot(t,y)
    axs[2].plot(t,z)
    for ax,label in zip(axs.flat,['x','y','z']):
        ax.set(ylabel=label)
    axs[2].set(xlabel='$\gamma t$')
    plt.tight_layout()

def plotBlochTrajectory(fig,times,state):
    ax = fig.gca()
    R = getBlochComponents(state)
    ax.scatter(R[0,:], R[1,:], R[2,:], color='red', s=5)
    #for i in range(len(times)):
    #    ax.scatter(R[0][i], R[1][i], R[2,i], color='red', s=10)


def animateBlochVector(fig,times,state,animationSteps):
    ax = fig.gca()
    R = getBlochComponents(state)
    arrow = plotBlochComponents(fig,R,0)

    def animate(i):
        new_segments = [[[0, 0, 0], [R[0][i],R[1][i],R[2][i]]]]
        arrow.set_segments(new_segments)
        return arrow

    ani = animation.FuncAnimation(fig, animate, frames=len(times), interval=10, blit=False, repeat=False)#, save_count=50)
    #ani.save('3d_ME_BlochSphere.gif', writer='imagemagick')
    #ani.save('3d_ME_BlochSphere.mp4', writer='ffmpeg')
    print('Showing plot')
    plt.show()

def plotPopulations(ax,times,state):
    if (state.shape == (4,times.size)):
        excited_state_pop = state[0,:]
        ground_state_pop = state[3,:]
    elif (state.shape == (2,times.size)):
        excited_state_pop = state[0,:]
        ground_state_pop = state[1,:]
    else:
        print('Error: state must be a 4xt density matrix in super space (Fock-Louvillian space) or a 2xt state vector, where t is arbitrary number of time steps')
        exit(1)
    #ax.set(xlabel='Time',ylabel='$|c_e|^2$ and $|c_g|^2$')
    ax.set(xlabel='$\gamma t$',ylabel='$|c_e|^2$')
    #ax.set(title='Excited state population')
    ax.plot(times,np.real(excited_state_pop),label='$|c_e|^2$')
    #ax.plot(times,np.real(ground_state_pop),label='$|c_g|^2$')
    return ax

def plotClicks(ax, times, dN, use_homodyne):
    ax.set(xlabel='$\gamma t$',ylabel=(('$dY_t$') if use_homodyne else '$dN_t$'))
    #ax.set(title='Signal versus time')
    ax.plot(times,dN,label='Signal')
    return ax

def enchantAx(ax):
    #ax.xaxis.set_minor_locator(MultipleLocator(0.2))
    ax.xaxis.set_minor_locator(AutoMinorLocator())
    ax.yaxis.set_minor_locator(AutoMinorLocator())
    ax.tick_params(which='both',top=True,right=True, color='grey')
    #ax.tick_params(which='both', width=2)
    ax.tick_params(which='major', length=7)
    ax.tick_params(which='minor', length=3)
    ax.spines['bottom'].set_color('grey')
    ax.spines['top'].set_color('grey') 
    ax.spines['right'].set_color('grey')
    ax.spines['left'].set_color('grey')

# P needs to be a matrix with len(times) columns and 
# rows corresponding to different probabilities
def plotBayesianProbabilityColor(ax, times, P):
    ax.set(xlabel='$\gamma t$',ylabel='Probability')
    #ax.set(title='Probabilities for different Rabi frequencies')
    ax.plot(times,P,label='Probabilities')
    return ax



if __name__=='__main__':
    print()
    #fig,ax = plotBlochSphere()
