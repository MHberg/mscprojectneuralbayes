import os
import datetime
import sys
from pathlib import Path
import h5py
import time
import tensorflow as tf
tf.get_logger().setLevel('ERROR') # suppress anything but errors (info and warnings)
import numpy as np
from tensorflow.keras.models import load_model
from tensorflow.keras.callbacks import ModelCheckpoint, TensorBoard, EarlyStopping
from tensorflow.keras import optimizers
from tensorflow.keras import metrics

from keras.wrappers.scikit_learn import KerasRegressor
from sklearn.pipeline import Pipeline
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.preprocessing import StandardScaler
import sklearn
import pickle


# set the threshold of printed numpy stuff to maximum possible
# specify printed np results as 2 digits precision and suppress scientific notation for small numbers
np.set_printoptions(threshold=sys.maxsize,precision=2,suppress=True)

seed = 1
np.random.seed(seed)

# custom functions and classes
import KerasBatchGenerators
import KerasBatchSequence
import KerasModels
from plottingTools import *
from FileManagement import *

TESTING = False
USE_HOMODYNE = True
dt = 0.001

if (TESTING):
    save_path += 'testing/'

if (USE_HOMODYNE):
    data_path += 'homodyne/'
    save_path += 'homodyne/'
else:
    data_path += 'counting/'
    save_path += 'counting/'

dt_str = str(dt)
dt_str = dt_str.replace('.','_')

data_path += 'dt_' + dt_str + '/'
save_path += 'dt_' + dt_str + '/'

single_omega_data_path = data_path + 'singleOmega/'
data_path += 'linspaced/'
save_path += 'regression/'

# Create directories on save path if they do not exist.
Path(save_path).mkdir(parents=True, exist_ok=True)

def calcAccuracy(pred, actual, filename, save):
    result = sklearn.metrics.mean_absolute_error(pred, actual)
    if save:
        np.save(save_path + filename, result)
    return result

def test_saved_models(Ts, use_binned_data, binningFactor, load_accs=False):
    save = True # Saving results as numpy arrays.
    # Needed for Bayes
    Delta = 0
    gamma = 1

    models = list()
    acc2_lst, bayes_acc2_lst, acc6_lst, bayes_acc6_lst = list(), list(), list(), list()

    for idx,T in enumerate(Ts):
        T_str = T_ToFileNameString(T)
        max_T_str = T_ToFileNameString(Ts[-1])

        # New method. Just using the same trajectories for different T by only loading parts of
        # long trajectories.
        end_idx = get_end_idx(T,dt)
        max_T_str = T_ToFileNameString(15)
        filename_suffix = max_T_str + '.h5'

        scores = np.load(save_path + 'RegressionScores' + T_str + '.npy')
        if (scores.size > 1):
            print(scores[0,:])
            best_model_idx = np.argmin(scores[0,:],axis=1)
            print(best_model_idx)
        else:
            best_model_idx = 0
        # Use list of scores to choose best model (requires all T's to have been run
        # in a single instance of the program - otherwise the RegressionScores array
        # does not contain the scores for the Ts excluded in that run)
        model = load_model(save_path + 'best_model_' + str(best_model_idx) + T_str +'.h5')

        # Use first model in list of models
        #model = load_model(save_path + 'model_0' + T_str +'.h5')
        models.append(model)
        times_test2 = read_data_times(single_omega_data_path + 'test_Omega2' + max_T_str + '.h5',np.arange(0),np.arange(end_idx))
        times_test6 = read_data_times(single_omega_data_path + 'test_Omega6' + max_T_str + '.h5',np.arange(0),np.arange(end_idx))
        test_signal_Omega2, omegas_test2 = read_data_signalsAndFrequencies(single_omega_data_path + 'test_Omega2' + max_T_str + '.h5',np.arange(0),np.arange(end_idx))
        test_signal_Omega6, omegas_test6 = read_data_signalsAndFrequencies(single_omega_data_path + 'test_Omega6' + max_T_str + '.h5',np.arange(0),np.arange(end_idx))

        # If testing binned data, bin signal
        if (use_binned_data):
            test_signal_Omega2, times = binSignal(test_signal_Omega2, times_test2, binningFactor)
            test_signal_Omega6, _ = binSignal(test_signal_Omega6, times_test6, binningFactor)
        else:
            times = times_test2

        pred2 = models[idx].predict(test_signal_Omega2)
        pred6 = models[idx].predict(test_signal_Omega6)
        actual2 = omegas_test2[:,0]
        actual6 = omegas_test6[:,0]
        acc2_lst.append(calcAccuracy(pred2, actual2, 'acc2_nn', save))
        acc6_lst.append(calcAccuracy(pred6, actual6, 'acc6_nn', save))

        fig = plt.figure()
        ax = plt.gca()

        #psi_test,dN_test,times_test,omegas_test = read_data(data_path + 'test' + max_T_str + '.h5', np.arange(0), np.arange(end_idx))
        dN_test,omegas_test = read_data_signalsAndFrequencies(data_path + 'test' + max_T_str + '.h5', np.arange(0), np.arange(end_idx))
        times_test = read_data_times(data_path + 'test' + max_T_str + '.h5', np.arange(0), np.arange(end_idx))

        pred = models[idx].predict(dN_test)
        actual = omegas_test[:,0]
        #plt.errorbar(actual,pred,yerr=scores[idx,0,best_indices[idx]],fmt='.')
        plt.xlim(1.9,14.1)
        plt.ylim(1.9,14.1)
        ax.set(xlabel='True $\Omega$', ylabel='Estimated $\Omega$')
        plt.plot(actual,pred,'k,',label='T='+str(T))
        ax.legend()
        plt.savefig(save_path + 'scatter_' + T_str + '.pdf')
        plt.close(fig)

    ##print("MAE on test data with Omega=2: \n",acc2)
    ##print("MAE on test data with Omega=2: \n",acc2)
    fig,axes = plt.subplots(2)
    axes[0].plot(Ts, acc2_lst, label='NN accuracy')
    axes[1].plot(Ts, acc6_lst, label='NN accuracy')
    #axes[0].plot(Ts, bayes_acc2, label='Bayes accuracy')
    #axes[1].plot(Ts, bayes_acc6, label='Bayes accuracy')
    for ax in axes: 
        ax.set(xlabel="$\gamma T$", ylabel="MAE")
        ax.legend()
    #axes[0].set(title='MAE versus trajectory length $\Omega = 2$')
    #axes[1].set(title='MAE versus trajectory length $\Omega = 6$')
    fig.tight_layout()
    fig.savefig(save_path + 'acc_vs_T.pdf')
    plt.close(fig)

    # For sanity checking manually that trajectories with same Omega differ
    #averageAndPlotInput(psi_test2, test_signal_Omega2, times_test2, 2)
    #averageAndPlotInput(psi_test7, test_signal_Omega7, times_test7, 7)

def checkDuplicatesTrainValidTest(T):
    T_str = T_ToFileNameString(T)

    filename_suffix = T_str + '.h5'

    training_data_path = data_path + 'training' + filename_suffix
    valid_data_path = data_path + 'valid' + filename_suffix
    test_data_path = data_path + 'test' + filename_suffix

    # clean data - load all
    dN_train,omegas_train = read_data_signalsAndFrequencies(training_data_path,np.arange(0))
    dN_valid,omegas_valid = read_data_signalsAndFrequencies(valid_data_path, np.arange(0))
    dN_test,omegas_test = read_data_signalsAndFrequencies(test_data_path, np.arange(0))

    # For checking for duplicates - inserting fake duplicates
    #dN_train[94] = dN_train[3]
    #dN_test[2345] = dN_train[43]
    #dN_valid[1932] = dN_train[87]

    # check for duplicates
    checkDuplicates(dN_train, dN_valid, dN_test)

def main(batch_size, num_epochs, use_dropout, use_kfold, Ts):
    models, scores, histories = list(), list(), list()
    for T in Ts:
        T_str = T_ToFileNameString(T)
        model, score, history = estimateParameter(batch_size, num_epochs, use_dropout, use_kfold, T)
        models.append(model)
        scores.append(score)
        np.save(save_path+'/RegressionScores' + T_str,score, allow_pickle=False)
        histories.append(history)
        # Reset tensorflow graph and create new one
        tf.keras.backend.clear_session()
    models = np.array(models)
    scores = np.array(scores)
    histories = np.array(histories)

def estimateParameter(batch_size, num_epochs, use_dropout, use_kfold, T):
    hidden_size = 0 # not used anymore
    t0 = time.time()

    T_str = T_ToFileNameString(T)
    #filename_suffix = T_str + '.h5'

    # New method. Just using the same trajectories for different T by only loading parts of
    # long trajectories.
    end_idx = get_end_idx(T,dt)
    max_T_str = T_ToFileNameString(50)
    filename_suffix = max_T_str + '.h5'


    training_data_path = data_path + 'training' + filename_suffix
    valid_data_path = data_path + 'valid' + filename_suffix
    test_data_path = data_path + 'test' + filename_suffix

    times = read_data_times(training_data_path,np.arange(end_idx))
    print(times[0])
    print(times[-1])

    # clean data
    N_data_load = 0 # Number of trajectories to load - 0 means all
    dN_train, omegas_train = read_data_signalsAndFrequencies(training_data_path,np.arange(N_data_load), np.arange(end_idx))
    dN_valid, omegas_valid = read_data_signalsAndFrequencies(valid_data_path,np.arange(N_data_load), np.arange(end_idx))
    dN_test, omegas_test = read_data_signalsAndFrequencies(test_data_path,np.arange(N_data_load), np.arange(end_idx))

    #psi_train,dN_train,times_train,omegas_train = read_data(training_data_path)
    #psi_valid,dN_valid,times_valid,omegas_valid = read_data(valid_data_path)
    #psi_test,dN_test,times_test,omegas_test = read_data(test_data_path)

    # noisy data - random delays
    #noisy_data_path = data_path + 'noisy/'
    #training_data_path = data_path + 'training' + filename_suffix
    #valid_data_path = data_path + 'valid' + filename_suffix
    #test_data_path = data_path + 'test' + filename_suffix
    #psi_train,dN_train,times_train,omegas_train = read_data(training_data_path)
    #psi_valid,dN_valid,times_valid,omegas_valid = read_data(valid_data_path)
    #psi_test,dN_test,times_test,omegas_test = read_data(test_data_path)

    #psi_train = psi_train[:1000]
    #dN_train = dN_train[:1000]
    #omegas_train = omegas_train[:1000]

    #times = times_train # the time steps must be equal for the different data sets

    N_trajs_train = dN_train.shape[0]
    nt = len(times)
    timesteps = nt
    if (T != times[-1]):
        print("T not equal to end of times array. Exiting...")
        exit()
    #timesteps = 500
    skip_step = timesteps

    t1 = time.time()
    tDataLoad = t1-t0

    # plot some trajectories
    #NUM_PLOTS = 3
    #fig1,axes1 = plt.subplots(NUM_PLOTS)
    #fig2,axes2 = plt.subplots(NUM_PLOTS)
    #for i in range(NUM_PLOTS):
    #    plotPopulations(axes1[i], times, np.power(np.abs(psi_train[i,:,:].T),2))
    #    axes2[i].plot(times, dN_train[i,:], label=r'$\Omega = $'+str(round(omegas_train[i,0],2)))
    #    axes2[i].legend()
    #    fig2.suptitle('Signal versus time')
    #fig1.tight_layout()
    #fig2.tight_layout()
    #fig1.savefig(save_path + 'population_' + T_str + '_' + str(NUM_PLOTS) + 'trajs' + '.pdf')
    #fig2.savefig(save_path + 'signal_' + T_str + '_' + str(NUM_PLOTS) + 'trajs' + '.pdf')
    #plt.close(fig1)
    #plt.close(fig2)
    t2 = time.time()
    tFirstPlots = t2-t1

    print("Time for execution of different units:")
    print("tDataLoad",np.around(tDataLoad,2))
    print("tFirstPlots",np.around(tFirstPlots,2))

    # callback to save model after every epoch
    checkpointer = ModelCheckpoint(filepath=save_path + '/param_esti_model-{epoch:02d}.h5', verbose=1)


    # callback for using Tensorboard
    logdir = Path(log_path) / datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    tensorboard_callback = TensorBoard(log_dir=os.fspath(logdir))

    # choose model we want to use (NN architecture)
    modelStrategy = KerasModels.RegressionParameterCNNModel(hidden_size, nt, timesteps, use_dropout)
    # For testing with T=50 trajectory
    #modelStrategy = KerasModels.RegressionParameterCNNModelT50(hidden_size, nt, timesteps, use_dropout)

    # using optimizers from tf.keras.optimizers required for cuDNN LSTM
    my_optimizer = tf.keras.optimizers.Nadam()

    # optimizer from Keras library directly
    #my_optimizer = optimizers.Nadam()

    # could use others???
    my_metric = "mae" # mean-squared error for regression model

    # take raw clicks as input
    batchSequence = KerasBatchSequence.SingleClickToConstantParameterRegression

    # set minimum change in val_accuracy before early stopping
    MIN_DELTA = 0.01
    PATIENCE = 10

    class OnEpochEnd(tf.keras.callbacks.Callback):
        def __init__(self,dataGenerator,model):
            super().__init__()
            self.dataGenerator = dataGenerator
            self.model = model
            self.initial_weights = self.model.get_weights()

        # function written by 183amir in comment https://github.com/keras-team/keras/issues/341#issuecomment-539198392 on 07/10/2019 extended by AZippelius in comment https://github.com/keras-team/keras/issues/341#issuecomment-547833394 on 30/10/2019 - not guaranteed to work for all layer types. Should work for Dense, Conv2D, Conv1D, SimpleRNN, GRU, LSTM, ConvLSTM2D according to source.
        def reset_weights(self, model):
            for layer in model.layers:
                if isinstance(layer, tf.keras.Model): #if you're using a model as a layer
                    self.reset_weights(layer) #apply function recursively
                    continue

                #where are the initializers?
                if hasattr(layer, 'cell'):
                    init_container = layer.cell
                else:
                    init_container = layer

                for key, initializer in init_container.__dict__.items():
                    if "initializer" not in key: #is this item an initializer?
                        continue #if no, skip it

                    # find the corresponding variable, like the kernel or the bias
                    if key == 'recurrent_initializer': #special case check
                        var = getattr(init_container, 'recurrent_kernel')
                    else:
                        var = getattr(init_container, key.replace("_initializer", ""))

                    var.assign(initializer(var.shape, var.dtype))
                    #use the initializer

    class MyCallback(tf.keras.callbacks.Callback):
        def __init__(self,dataGenerator):
            super().__init__()
            self.dataGenerator = dataGenerator

        def on_epoch_end(self, epoch, logs):
            self.dataGenerator.on_epoch_end()


    # Trains model for a given initial amount of epochs, evaluates it on test data and returns the
    # model and test_accuracy. For use in e.g. K-fold cross-validation.
    # train_data_generator is generating data from training data set, except a hold out part
    # valid_data_generator is generating data from hold out part of training data set
    # test_data_generator is generating data from validation data set
    def auto_fit_model(model, train_data_generator, valid_data_generator, test_data_generator, num_epochs, idx):
        print('Fitting of model started...')
        # callback to implement early stopping and save the best model found during training
        earlyStopping = EarlyStopping(monitor='val_loss',mode='min', verbose=1, patience=PATIENCE, min_delta=MIN_DELTA)
        modelCheckpoint = ModelCheckpoint(filepath=save_path + 'best_model_' + str(idx) + T_str +'.h5', monitor='val_loss', mode='min', save_best_only=True, verbose=1)
        # Callback for controlling what happens at the end of every epoch.
        onEpochEnd = MyCallback(train_data_generator)
        hist = model.fit(train_data_generator,
                epochs=num_epochs, validation_data = valid_data_generator,
                use_multiprocessing=False, shuffle=False,
                callbacks=[earlyStopping, modelCheckpoint, onEpochEnd])
                #callbacks=[checkpointer]) # checkpointer saves model after each epoch
                #callbacks=[checkpointer,tensorboard_callback])
        print('Fitting of model finished.')
        model = load_model(save_path + 'best_model_' + str(idx) + T_str +'.h5')
        print('Evaluating model on test data (not the same as val_acc)...')
        _, test_acc = model.evaluate(test_data_generator,
                use_multiprocessing=False,
                callbacks=[]) 
        print('Evaluation of model completed.')
        return model, test_acc, hist

    # Trains model for a given initial amount of epochs and then asks if more training epochs should be
    # added. If not, evaluates model on test data and returns model and test_accuracy.
    # train_data_generator is generating data from training data set, except a hold out part
    # valid_data_generator is generating data from hold out part of training data set
    # test_data_generator is generating data from validation data set
    def fit_model(model, train_data_generator, valid_data_generator, test_data_generator, num_epochs):
        remaining_epochs = num_epochs
        total_epochs = 0
        # callback to implement early stopping and save the best model found during training
        earlyStopping = EarlyStopping(monitor='val_loss',mode='min', verbose=1, patience=10, min_delta=MIN_DELTA)
        modelCheckpoint = ModelCheckpoint(filepath=save_path + 'best_model_0' + T_str +'.h5', monitor='val_loss', mode='min', save_best_only=True, verbose=1)
        onEpochEnd = MyCallback(train_data_generator)
        print('Fitting of model started...')
        while (remaining_epochs>0):
            hist = model.fit(train_data_generator, epochs=total_epochs+remaining_epochs, steps_per_epoch=None, validation_data = valid_data_generator, use_multiprocessing=False, shuffle=False, initial_epoch=total_epochs, callbacks=[modelCheckpoint,onEpochEnd, earlyStopping])#, workers=12) #callbacks=[checkpointer,tensorboard_callback])
            total_epochs += remaining_epochs
            remaining_epochs = 0
            try:
                #remaining_epochs = int(input("Enter number of epochs to run (default 0):\n"))
                remaining_epochs = 0
            except ValueError:
                print('Input was not an integer. Ending the program normally.')
            except:
                print('Something went wrong with integer conversion of input. Ending the program normally.')

        model = load_model(save_path + 'best_model_0' + T_str + '.h5')
        print('Fitting of model finished.')
        print('Evaluating model on test data...')
        _, test_acc = model.evaluate(test_data_generator,
                use_multiprocessing=False,
                callbacks=[])
        print('Evaluation of model completed.')
        print("\nTotal amount of epochs run: ", total_epochs)
        return model, test_acc, hist

    # K-fold cross-validation
    def do_KFold_CV(n_folds):
        kfold = KFold(n_folds, True, seed)

        # not using test set atm - splitting training data into training and validation data using kfold and using the c++ simulated validation data as the test set
        test_data_generator = batchSequence(dN_valid, omegas_valid, batch_size, timesteps, skip_step, modelStrategy, False)
        scores, members, histories = list(), list(), list()
        for idx, (train_idx, test_idx) in enumerate(kfold.split(dN_train)):
            model = modelStrategy.defineModel()
            model.compile(loss='mean_absolute_error',
                    optimizer=my_optimizer, metrics=[my_metric])
            train_data_generator = batchSequence(dN_train[train_idx,:,:], omegas_train[train_idx,:], batch_size, timesteps, skip_step, modelStrategy, True)
            valid_data_generator = batchSequence(dN_train[test_idx,:,:], omegas_train[test_idx,:], batch_size, timesteps, skip_step, modelStrategy, False)
            model, test_acc, hist = auto_fit_model(model, train_data_generator, valid_data_generator,
                    test_data_generator, num_epochs, idx)
            scores.append(test_acc)
            members.append(model)
            histories.append(hist)

        for idx,(history,model) in enumerate(zip(histories,members)):
            with open(save_path + 'history_' + str(idx) + T_str, 'wb') as pickle_file:
                pickle.dump(history.history, pickle_file)
                #model.save(save_path + 'model_' + str(idx) + T_str + '.h5')
        print('Estimated accuracy: %.3f (%.3f)' % (np.mean(scores), np.std(scores)))
        print('Estimated accuracy is based on the validation data set whereas the validation loss is calculated from a hold-out part of the training data. Thus estimated accuracy is not expected to be equal to val_loss')

        # generates prediction of ensemble of models on test data set
        # doesnt currently use the dataGenerator and instead accesses data directly through dN_test and omegas_test
        def getEnsemblePrediction(members, dataGenerator):
            #predictions = [model.predict(dataGenerator.generate()) for model in members]
            predictions = np.empty((dN_test.shape[0],len(members)))
            #dataGenerator.reset()
            for idx,model in enumerate(members):
            #    pred = model.predict(dataGenerator.generate(),steps=dataGenerator.getNStepsPerEpoch()) # predict on data
                pred = model.predict(dN_test)
                predictions[:,idx] = pred[:,0]
            actual_values = omegas_test[:,0]
            #predictions = np.array(predictions)
            mean_predictions = np.mean(predictions,axis=1)
            return mean_predictions,actual_values

        def evaluate_n_members(members, n_members, dataGenerator):
            subset = members[:n_members]
            mean_predictions, actual_values = getEnsemblePrediction(subset, dataGenerator)

            # MSE
            #acc = sklearn.metrics.mean_squared_error(actual_values,mean_predictions)

            # MAE
            acc = sklearn.metrics.mean_absolute_error(actual_values,mean_predictions)
            return acc

        ### Evaluating models on test data set
        test_data_generator = batchSequence(dN_test, omegas_test, batch_size, timesteps, skip_step, modelStrategy, False)

        single_scores, ensemble_scores, result_list = list(), list(), list()
        for i in range(1, n_folds+1):
            ensemble_score = evaluate_n_members(members, i, test_data_generator)
            #test_data_generator.reset()
            #_, single_score = members[i-1].evaluate(test_data_generator.generate(), steps=test_data_generator.getNStepsPerEpoch(), verbose=0)
            _, single_score = members[i-1].evaluate(dN_test, omegas_test[:,0], verbose=0)
            result_list.append('> %d: single=%.3f, ensemble=%.3f' % (i, single_score, ensemble_score))
            ensemble_scores.append(ensemble_score)
            single_scores.append(single_score)

        for result in result_list:
            print(result)
        print('Accuracy %.3f (%.3f)' % (np.mean(single_scores), np.std(single_scores)))
        print('Accuracy was obtained using the test data set. So are the single scores and the ensemble scores.')

        ### Plotting result of evaluation
        xs = [i for i in range(1, n_folds+1)]
        fig = plt.figure()
        plt.plot(xs, single_scores, marker='o', linestyle='None',label='Single')
        plt.plot(xs, ensemble_scores, marker='o', label='Ensemble predictions')
        fig.savefig(save_path + 'ensemble_vs_single' + T_str + '.pdf')
        plt.close(fig)

        scores = np.array([single_scores, ensemble_scores])
        return members, scores, histories

    def getSingleModel():
        # not using test set atm - splitting training data into training and validation data using kfold and using the c++ simulated validation data as the test set
        test_data_generator = batchSequence(dN_valid, omegas_valid, batch_size, timesteps, skip_step, modelStrategy, False)
        scores, members, histories = list(), list(), list()
        model = modelStrategy.defineModel()
        model.compile(loss='mean_absolute_error',
                optimizer=my_optimizer, metrics=[my_metric])
        alpha = 9/10
        idx_fold = int(alpha*N_trajs_train) # takes a fraction 1-alpha of the training data as validation data
        train_data_generator = batchSequence(dN_train[:idx_fold,:,:], omegas_train[:idx_fold,:], batch_size, timesteps, skip_step, modelStrategy, True)
        valid_data_generator = batchSequence(dN_train[idx_fold:,:,:], omegas_train[idx_fold:,:], batch_size, timesteps, skip_step, modelStrategy, False)
        model, test_acc, hist = fit_model(model, train_data_generator, valid_data_generator,
                test_data_generator, num_epochs)

        # for loading the best model
        #model = load_model(save_path + '/temp_best_model.h5')
        #_, test_acc = model.evaluate(test_data_generator.generate(),
        #        steps = test_data_generator.getNStepsPerEpoch(), use_multiprocessing=False,
        #        callbacks=[])

        #hist = 0
        scores.append(test_acc)
        members.append(model)
        histories.append(hist)

        with open(save_path + 'history' + T_str, 'wb') as pickle_file:
            pickle.dump(hist.history, pickle_file)

        print('Estimated accuracy: %.3f (%.3f)' % (np.mean(scores), np.std(scores)))
        print('Estimated accuracy is based on the validation data set whereas the validation loss is calculated from a hold-out part of the training data. Thus estimated accuracy is not expected to be equal to val_loss')

        ### Evaluating models on test data set
        test_data_generator = batchSequence(dN_test, omegas_test, batch_size, timesteps, skip_step, modelStrategy, False)
        scores = list()
        _, test_acc = model.evaluate(dN_test,omegas_test[:,0], verbose=0)

        scores.append(test_acc)

        print('Accuracy %.3f' % (test_acc))
        print('Accuracy was obtained using the test data set.')

        return members, scores, histories

    n_folds = 10
    if use_kfold:
        members, scores, histories = do_KFold_CV(n_folds)
    else:
        members, scores, histories = getSingleModel()

    fig,axes = plt.subplots(len(histories))
    if (len(histories) == 1):
        #axes.plot(histories[0].history['mse'], label='Training set')
        #axes.plot(histories[0].history['val_mse'], label='Validation set')
        #axes.set(xlabel='Epoch number', ylabel='Mean-squared error')

        axes.plot(histories[0].history['mae'], label='Training set')
        axes.plot(histories[0].history['val_mae'], label='Validation set')
        axes.set(xlabel='Epoch number', ylabel='Mean-absolute error')
        axes.set_ylim(0,5)
        axes.legend()
    else:
        for idx,hist in enumerate(histories):
            #axes[idx].plot(hist.history['mse'], label='Training set')
            #axes[idx].plot(hist.history['val_mse'], label='Validation set')
            #axes[idx].set(xlabel='Epoch number', ylabel='Mean-squared error')

            axes[idx].plot(hist.history['mae'], label='Training set')
            axes[idx].plot(hist.history['val_mae'], label='Validation set')
            axes[idx].set(ylabel='MAE')
            axes[idx].set_ylim(0,5)
            axes[idx].legend()

        axes[axes.shape[0]-1].set(xlabel='Epoch number')
    fig.tight_layout()
    fig.savefig(save_path + '/training_history' + T_str + '.pdf')
    plt.close(fig)

    def get_test_predictions(num_batches_predict, data_generator, members, dummy_iters = 0):
        """
        Returns array of predictions.

        """
        dummy_iters = 0 # controls how many batches to skip
        num_predict = num_batches_predict # how many batches of test trajectories to do the prediction for

        mean_pred = np.zeros((num_predict,))
        pred = np.empty((num_predict,len(members)))

        print('Predicting on test data started...')
        actual_values = np.empty(num_predict)
        for i in range(num_predict):
            data = data_generator.__getitem__(i+dummy_iters)
            actual_values[i] = data[1]
            for idx,model in enumerate(members):
                pred[i,idx] = model.predict(data[0])

        #return mean of predictions by every model in members
        pred_arr = np.mean(np.array(pred),axis=1)
        return pred_arr, actual_values

    # data generator on test set with batch size 1, so we have fine control over how many trajectories to use for prediction
    test_data_generator = batchSequence(dN_test, omegas_test, 1, timesteps, skip_step, modelStrategy, False)

    # having initialized the data generator with 1 batch size, num_predict will equal number of trajectories to do the prediction for
    num_predict = 50
    pred_arr, actual_values = get_test_predictions(num_predict, test_data_generator, members)
    comparePredictedToTest = np.around(np.array([actual_values,pred_arr,np.abs(actual_values-pred_arr)]),2).T
    print('Predicting on test data finished.')
    print('actual frequencies:, predicted frequencies:\n',comparePredictedToTest)
    print('MAE in shown predictions: %0.3f' % (np.mean(np.abs(comparePredictedToTest[:,0]-comparePredictedToTest[:,1]),axis=0)))
    #print('MAE double check on test data: %0.3f' % (np.mean(np.abs(omegas_test[:,0]-get_test_predictions(omegas_test.shape[0],test_data_generator,members)))))
    return members, scores, histories

if __name__=='__main__':
    main()
