from abc import ABC, abstractmethod
from tensorflow.keras.utils import to_categorical, Sequence
import numpy as np
import time
import math

from FileManagement import *

class TrajectorySequence(ABC, Sequence):
    def __init__(self, filename, traj_indices, nt, batch_size, timesteps, skip_step, shuffle):
        self.batch_size = batch_size
        self.timesteps = timesteps
        self.skip_step = skip_step
        self.traj_indices = traj_indices
        self.n_trajs = traj_indices.size
        self.nt = nt
        self.filename = filename

        self.shuffle = shuffle
        # Generate indices over whole range of data possibly giving indices to subsamples if wanted
        # (if skip_step and timesteps < nt)
        # not sure this formula is correct, should confirm
        if (self.nt - self.timesteps == 0):
            self.subSampleFactor = 1
        else:
            self.subSampleFactor = self.nt//((self.nt-self.timesteps)/self.skip_step)+1
        #print("subSampleFactor (should be 1 for most cases): ",self.subSampleFactor)
        self.indices = np.arange(0,self.n_trajs*self.subSampleFactor)

    def __getitem__(self, idx):
        raise NotImplementedError('Must be defined in subclass')

    # defining number of training/validation steps (number of batches to use) per epoch so that all data is used in every epoch
    @abstractmethod
    def __len__(self):
        raise NotImplementedError('Must be defined in subclass')

    #@abstractmethod
    #def on_epoch_end(self):
        #raise NotImplementedError('Must be defined in subclass')

    def getStepConfig(self):
        return self.stepConfig

    @abstractmethod
    def getEncoding(self):
        raise NotImplementedError('Must be defined in subclass')

#class SingleClickSingleStep(KerasBatchGenerator):
#    def __init__(self, data, timesteps, batch_size, skip_step, shuffle):
#        super().__init__(data, timesteps, batch_size, skip_step, shuffle)
#        self.stepConfig = "single"
#
#    def generate(self):
#        x = np.zeros((self.batch_size, self.timesteps,1))
#        y = np.zeros(x.shape)
#        while True:
#            for i in range(self.batch_size):
#                if (self.current_idx + self.timesteps + 1 > self.nt):
#                    # reset index back to start of data and go to next trajectory
#                    self.current_idx = 0
#                if (self.traj_idx < self.n_trajs-1):
#                    self.traj_idx += 1
#                else:
#                    #print("All trajectories have been used for training. Resetting to first trajectory...")
#                    self.traj_idx = 0
#
#                x[i,:,:] = self.data[self.indices[self.traj_idx],self.current_idx:self.current_idx + self.timesteps]
#                y[i,:,:] = self.data[self.indices[self.traj_idx],self.current_idx+1:self.current_idx + self.timesteps+1]
#                self.current_idx += self.skip_step
#            yield x, y
#
#    def on_epoch_end(self):
#        if self.shuffle:
#            np.random.shuffle(self.indices)
#            #self.data = self.data[self.indices]
#
#    def __len__(self):
#        N_steps = (self.nt*self.n_trajs)//(self.batch_size*self.timesteps) # for going through trajs point by point
#        if (N_steps < 1):
#            print("Batch size and memory too large compared to number of trajectories+timesteps per trajectory in training data. Exiting...")
#            exit()
#        else:
#            return N_steps
#
#    def getEncoding(self):
#        return "single-click"
#
## every click (1) is surrounded by N_spread clicks in the label vector
#class IntervalSingleStep(KerasBatchGenerator):
#    # expects data to be of shape (N_trajs, timesteps, N_features)
#    def __init__(self, data, timesteps, batch_size, skip_step, shuffle):
#        super().__init__(data, timesteps, batch_size, skip_step, shuffle)
#        self.stepConfig = "single"
#
#        data_temp = np.zeros(self.data.shape,dtype='int')
#        N_spread = 5
#        padded_data = np.pad(self.data,((0,0),(N_spread,N_spread),(0,0)))
#        for i in range(-N_spread,N_spread+1):
#            data_temp += padded_data[:,i+N_spread:self.data.shape[1]+N_spread+i]
#        self.data_copy_tweaked = data_temp
#
#    def generate(self):
#        x = np.zeros((self.batch_size, self.timesteps,1))
#        y = np.zeros(x.shape)
#        while True:
#            for i in range(self.batch_size):
#                if (self.current_idx + self.timesteps + 1 > self.nt):
#                    # reset index back to start of data and go to next trajectory
#                    self.current_idx = 0
#                if (self.traj_idx < self.n_trajs-1):
#                    self.traj_idx += 1
#                else:
#                    #print("All trajectories have been used for training. Resetting to first trajectory...")
#                    self.traj_idx = 0
#                x[i,:,:] = self.data[self.indices[self.traj_idx],self.current_idx:self.current_idx + self.timesteps]
#                y[i,:,:] = self.data_copy_tweaked[self.indices[self.traj_idx],self.current_idx + 1:self.current_idx + self.timesteps + 1]
#                self.current_idx += self.skip_step
#            yield x, y
#            
#    def on_epoch_end(self):
#        if self.shuffle:
#            np.random.shuffle(self.indices)
#            #self.data = self.data[self.indices]
#            #self.data_copy_tweaked = self.data_copy_tweaked[self.indices]
#
#    def __len__(self):
#        N_steps = (self.nt*self.n_trajs)//(self.batch_size*self.timesteps) # for going through trajs point by point
#        if (N_steps < 1):
#            print("Batch size and memory too large compared to number of trajectories+timesteps per trajectory in training data. Exiting...")
#            exit()
#        else:
#            return N_steps
#    def getEncoding(self):
#        return "interval-click"
#
#class IntervalMultiStep(KerasBatchGenerator):
#    # expects data to be of shape (N_trajs, timesteps, N_features)
#    # tries to predict outcomes for the rest of the trajectory
#    def __init__(self, data, timesteps, batch_size, skip_step, shuffle):
#        super().__init__(data, timesteps, batch_size, skip_step, shuffle)
#        self.stepConfig = "multi"
#
#        data_temp = np.zeros(self.data.shape,dtype='int')
#        N_spread = 5
#        padded_data = np.pad(self.data,((0,0),(N_spread,N_spread),(0,0))) # put N_spread zero columns on each end of the data
#        for i in range(-N_spread,N_spread+1):
#            data_temp += padded_data[:,i+N_spread:self.data.shape[1]+N_spread+i]
#        self.data_copy_tweaked = data_temp
#
#    def generate(self):
#        x = np.zeros((self.batch_size, self.timesteps, 1),dtype='int')
#        y = np.zeros((self.batch_size, self.nt-self.timesteps, 1),dtype='int')
#        while True:
#            for i in range(self.batch_size):
#                if (self.timesteps >= self.nt):
#                    print('Memory timesteps too large compared to actual time steps nt of trajectories. Exiting...')
#                    exit()
#                if (self.traj_idx + 1 < self.n_trajs):
#                    self.traj_idx += 1
#                else:
#                    #print("All trajectories have been used for training. Resetting to first trajectory...")
#                    self.traj_idx = 0
#                x[i,:,:] = self.data[self.indices[self.traj_idx],0:self.timesteps]
#                y[i,:,:] = self.data_copy_tweaked[self.indices[self.traj_idx], self.timesteps:self.nt]
#                self.traj_idx += 1
#            yield x, y
#
#    def on_epoch_end(self):
#        if self.shuffle:
#            np.random.shuffle(self.indices)
#            #self.data = self.data[self.indices]
#            #self.data_copy_tweaked = self.data_copy_tweaked[self.indices]
#
#    def __len__(self):
#        N_steps = np.ceil(self.n_trajs/self.batch_size) # for going through trajs traj by traj - TODO: do we want to use ceil or floor (// = floor of division). Floor probably might miss trajectories, ceil might repeat some depending on batch size and number of trajectories
#        if (N_steps < 1):
#            print("Batch size and memory too large compared to number of trajectories+timesteps per trajectory in training data. Exiting...")
#            exit()
#        else:
#            return N_steps
#    def getEncoding(self):
#        return "interval-click"
#        
#
#class SignalMultiStep(KerasBatchGenerator):
#    # expects data to be of shape (N_trajs, timesteps, N_features)
#    # tries to predict outcomes for the rest of the trajectory
#    def __init__(self, data, timesteps, batch_size, skip_step, shuffle):
#        super().__init__(data, timesteps, batch_size, skip_step, shuffle)
#        self.stepConfig = "multi"
#
#        self.input_signal = np.zeros(data.shape)
#        cumsum = np.cumsum(self.data,axis=1)
#        np.mod(cumsum,2,self.input_signal)
#
#    def generate(self):
#        x = np.zeros((self.batch_size, self.timesteps, 1),dtype='int')
#        y = np.zeros((self.batch_size, self.nt-self.timesteps, 1),dtype='int')
#        while True:
#            for i in range(self.batch_size):
#                if (self.timesteps >= self.nt):
#                    print('Memory timesteps too large compared to actual time steps nt of trajectories. Exiting...')
#                    exit()
#                x[i,:,:] = self.input_signal[self.indices[self.traj_idx], 0:self.timesteps]
#                y[i,:,:] = self.input_signal[self.indices[self.traj_idx], self.timesteps:self.nt]
#                if (self.traj_idx < self.n_trajs-1):
#                    self.traj_idx += 1
#                else:
#                    #print("All trajectories have been used for training. Resetting to first trajectory...")
#                    self.traj_idx = 0
#            yield x, y
#
#    def on_epoch_end(self):
#        if self.shuffle:
#            np.random.shuffle(self.indices)
#            #self.input_signal = self.input_signal[self.indices]
#
#    def __len__(self):
#        N_steps = np.ceil(self.n_trajs/self.batch_size) # for going through trajs traj by traj - TODO: do we want to use ceil or floor (// = floor of division). Floor probably might miss trajectories, ceil might repeat some depending on batch size and number of trajectories
#        if (N_steps < 1):
#            print("Batch size and memory too large compared to number of trajectories+timesteps per trajectory in training data. Exiting...")
#            exit()
#        else:
#            return N_steps
#    def getEncoding(self):
#        return "signal-click"
#
## Not for predicting future detections but for paramater estimation from a
## hypothesis set.
## This generator does the encoding of the input signal so that it alternates
## between 0 and 1 at detector clicks - thus it cannot be used for the homodyne
## signal.
#class SignalToConstantParameterFromHypotheses(KerasBatchGenerator):
#    # expects data to be of shape (N_trajs, timesteps, N_features)
#    # tries to predict outcomes for the rest of the trajectory
#    def __init__(self, data, timesteps, batch_size, skip_step, hypotheses,
#                 omegas, shuffle):
#        super().__init__(data, timesteps, batch_size, skip_step, shuffle)
#        self.stepConfig = "single" # trying to guess a single parameter
#        self.hypotheses = hypotheses
#
#        # taking Rabi frequency at first time step since it is assumed constant for these trajectories
#        self.omegas = omegas[:,0] 
#
#        # converts a class vector (integers) to a binary class matrix
#        # a 1 in row 0, column 0 means the first trajectory belongs to the first
#        # class of Rabi frequencies, a 1 in row 1, column 0 means the second
#        # trajectory belongs to the second class of Rabi frequencies
#        self.omegas = to_categorical(omegas, hypotheses.size, dtype='int')
#
#        # encodes the input signal as alternating between 0 and 1 at detector
#        # clicks
#        self.input_signal = np.zeros(self.data.shape)
#        cumsum = np.cumsum(self.data,axis=1)
#        np.mod(cumsum,2,self.input_signal)
#
#    def generate(self):
#        x = np.zeros((self.batch_size, self.timesteps, 1),dtype='int')
#        y = np.zeros((self.batch_size, self.hypotheses.size),dtype='int')
#        while True:
#            for i in range(self.batch_size):
#                if (self.timesteps > self.nt):
#                    print('Memory timesteps too large compared to actual time steps nt of trajectories. Exiting...')
#                    exit()
#                x[i,:,:] = self.input_signal[self.indices[self.traj_idx], 0:self.timesteps]
#                y[i,:] = self.omegas[self.indices[self.traj_idx],:]
#                if (self.traj_idx < self.n_trajs-1):
#                    self.traj_idx += 1
#                else:
#                    #print("All trajectories have been used for training. Resetting to first trajectory...")
#                    self.traj_idx = 0
#            yield x, y
#
#    def on_epoch_end(self):
#        if self.shuffle:
#            np.random.shuffle(self.indices)
#            #self.input_signal = self.input_signal[self.indices]
#            #self.omegas = self.omegas[self.indices]
#
#    def __len__(self):
#        N_steps = np.ceil(self.n_trajs/self.batch_size) # for going through trajs traj by traj - TODO: do we want to use ceil or floor (// = floor of division). Floor probably might miss trajectories, ceil might repeat some depending on batch size and number of trajectories
#        if (N_steps < 1):
#            print("Batch size and memory too large compared to number of trajectories+timesteps per trajectory in training data. Exiting...")
#            exit()
#        else:
#            return N_steps
#    def getEncoding(self):
#        return "signal-click"


# not for predicting future detections but for parameter estimation from a hypothesis set
class SingleClickToConstantParameterFromHypotheses(TrajectorySequence):
    def __init__(self, filename, traj_indices, nt, hypotheses, batch_size, timesteps, skip_step, modelStrategy, shuffle):
        super().__init__(filename, traj_indices, nt, batch_size, timesteps, skip_step, shuffle)
        self.stepConfig = "single" # trying to guess a single parameter
        self.hypotheses = hypotheses
        self.n_classes = hypotheses.size
        self.inputDims = modelStrategy.getDimsRequired()
        self.trajs_used = np.empty(shape=(0))

        # runs on_epoch_end() before the first epoch
        self.on_epoch_end()

        # taking Rabi frequency at first time step (omegas[:,0]) since it is assumed constant for these trajectories

    def __data_generation(self, batch_idx):
        ### Initialization
        # 3d input - last dimension is feature number - required when using LSTMs
        if (self.inputDims == 3):
            X_temp = np.zeros((batch_idx.size, self.timesteps, 1))
            #x_batch = np.zeros((self.batch_size, self.timesteps, 1))
        # For using CNN-LSTM where input is required to be 4D
        # (samples, sub_seqs, timesteps, features)
        elif (self.inputDims == 4):
            X_temp = np.zeros((batch_idx.size, self.subSampleFactor, self.timesteps, 1))
        # For using ConvLSTM2D where input is required to be 5D
        # (samples, sub_seqs, rows, cols, features)
        elif (self.inputDims == 5):
            X_temp = np.zeros((batch_idx.size, self.subSampleFactor, 1, self.timesteps, 1))

        else:
            print('Input dimension specified was invalid. Exiting...')
            exit()

        #y_batch = np.zeros((self.batch_size, self.n_classes))

        ### Filling x and y batches
        if (self.timesteps > self.nt):
            print('Memory timesteps %0.3f too large compared to actual time steps nt %0.3f of trajectories. Exiting...' % ( self.timesteps, self.nt))
            exit()


        # 3d input - last dimension is feature number - seems to be required
        if (self.inputDims == 3):
            # 3D input with timesteps data points
            #print(self.x_set.shape)
            #print(np.floor_divide(batch_idx,self.subSampleFactor))
            #print(np.mod(batch_idx,self.subSampleFactor)*self.skip_step)
            #print(np.mod(batch_idx,self.subSampleFactor)*self.skip_step+self.timesteps)

            traj_indices = np.floor_divide(batch_idx,self.subSampleFactor)
            t_start_indices = np.mod(batch_idx,self.subSampleFactor)*self.skip_step
            t_end_indices = t_start_indices + self.timesteps
            #self.trajs_used = np.append(self.trajs_used,traj_indices)


            # cannot do this since "only integer scalar arrays can be converted to scalar index"
            # probably means I cannot use arrays on each side of ":"
            #x_batch[:,:,:] = self.x_set[traj_indices, t_start_indices:t_end_indices]

            traj_indices.sort()
            
            X, y = read_data_signalsAndFrequencies(self.filename, traj_indices)

            # encoding Rabi frequencies of system into one-hot representations
            omegasEncoded = np.empty((y.shape[0],self.nt))
            omegas = y[:,0] # using first element while frequencies are constant
            for i in range(self.hypotheses.size):
                omega_idx = np.where(omegas == self.hypotheses[i])
                omegasEncoded[omega_idx] = i
            y_temp = omegasEncoded

            for i,temp_indices in enumerate(zip(np.arange(traj_indices.size),t_start_indices,t_end_indices)):
                X_temp[i,:,:] = X[temp_indices[0], temp_indices[1]:temp_indices[2]]

        elif (self.inputDims == 4 & self.inputDims == 5):
            X, y = read_data_signalsAndFrequencies(self.filename, batch_idx)
            # encoding Rabi frequencies of system into one-hot representations
            omegasEncoded = np.empty((y.shape[0],nt))
            omegas_i = y[:,0] # using first element while frequencies are constant
            for i in range(self.hypotheses.size):
                omega_idx = np.where(omegas_i == hypotheses[i])
                omegasEncoded[omega_idx] = i
            y_temp = omegasEncoded

            for i in range(batch_idx.size):
                # for splitting each trajectory into nt/timesteps subsequences
                for j in range(self.subSampleFactor):
                    # For using CNN-LSTM where input is required to be 4D
                    # (samples, sub_seqs, timesteps, features)
                    if (self.inputDims == 4):
                        X_temp[i,j,:,:] = X[np.floor_divide(np.arange(batch_idx.size)[i],self.subSampleFactor),batch_idx[i+j]]
                    # For using ConvLSTM2D where input is required to be 5D
                    # (samples, sub_seqs, rows, cols, features), cols=timesteps
                    elif (self.inputDims == 5):
                        X_temp[i,j,0,:,:] = X[np.floor_divide(np.arange(batch_idx.size)[i],self.subSampleFactor),batch_idx[i+j]]

        #print(self.y_set.shape)
        # to_categorical: converts a class vector (integers) to a binary class matrix
        # a 1 in row 0, column 0 means the first trajectory belongs to the first
        # class of Rabi frequencies, a 1 in row 1, column 0 means the second
        # trajectory belongs to the second class of Rabi frequencies
        y_temp = to_categorical(y_temp[:,0], self.n_classes)
        return X_temp, y_temp

    def __getitem__(self, idx):
        #print(self.indices[0:2])

        # Generate batch indices
        batch_idx = self.indices[idx*self.batch_size:(idx+1)*self.batch_size]

        X, y = self.__data_generation(batch_idx)

        return X, y

    def __len__(self):
        #N_steps = (self.n_trajs*self.subSampleFactor) // self.batch_size
        N_steps = math.ceil((self.n_trajs*self.subSampleFactor) / self.batch_size)
        if (N_steps < 1):
            print("Batch size and memory too large compared to number of trajectories+timesteps per trajectory in training data. Exiting...")
            exit()
        else:
            return N_steps


    def on_epoch_end(self):
        #print(self.shuffle)
        #print("number of indices used: ", self.trajs_used.size)
        #self.trajs_used = np.empty(shape=(0))
        if self.shuffle:
            print('Shuffling data...')
            np.random.shuffle(self.indices)

    def getEncoding(self):
        return "single-click"


# not for predicting future detections but for parameter estimation using regression
class SingleClickToConstantParameterRegression(TrajectorySequence):
    def __init__(self, trajectories, omegas, batch_size, timesteps, skip_step, modelStrategy, shuffle):
        super().__init__(trajectories, batch_size, timesteps, skip_step, shuffle)
        self.stepConfig = "single" # trying to guess a single parameter
        self.inputDims = modelStrategy.getDimsRequired()
        self.x_set = trajectories
        self.trajs_used = np.empty(shape=(0))
        self.i = 0

        # runs on_epoch_end() before the first epoch
        self.on_epoch_end()

        # taking Rabi frequency at first time step (omegas[:,0]) since it is assumed constant for these trajectories
        self.y_set = omegas[:,0]

    def __getitem__(self, idx):
        #print('idx:',idx)
        # Generate batch indices
        batch_idx = self.indices[idx*self.batch_size:(idx+1)*self.batch_size]
        #print(batch_idx) # Sanity check on what trajectories are used

        #print(self.indices[0:2])
        ### Initialization
        # 3d input - last dimension is feature number - required when using LSTMs
        if (self.inputDims == 3):
            x_batch = np.zeros((batch_idx.size, self.timesteps, 1))
        # For using CNN-LSTM where input is required to be 4D
        # (samples, sub_seqs, timesteps, features)
        elif (self.inputDims == 4):
            x_batch = np.zeros((batch_idx.size, self.subSampleFactor, self.timesteps, 1))
            #x_batch = np.zeros((self.batch_size, self.subSampleFactor, self.timesteps, 1))
        # For using ConvLSTM2D where input is required to be 5D
        # (samples, sub_seqs, rows, cols, features)
        elif (self.inputDims == 5):
            x_batch = np.zeros((batch_idx.size, self.subSampleFactor, 1, self.timesteps, 1))
            #x_batch = np.zeros((self.batch_size, self.subSampleFactor, 1, self.timesteps, 1))

        else:
            print('Input dimension specified was invalid. Exiting...')
            exit()

        #y_batch = np.zeros(self.batch_size)

        ### Filling x and y batches
        if (self.timesteps > self.nt):
            print('Memory timesteps %0.3f too large compared to actual time steps nt %0.3f of trajectories. Exiting...' % ( self.timesteps, self.nt))
            exit()

        # 3d input - last dimension is feature number - seems to be required
        if (self.inputDims == 3):
            # 3D input with timesteps data points
            #print(self.x_set.shape)
            #print(np.floor_divide(batch_idx,self.subSampleFactor))
            #print(np.mod(batch_idx,self.subSampleFactor)*self.skip_step)
            #print(np.mod(batch_idx,self.subSampleFactor)*self.skip_step+self.timesteps)

            traj_indices = np.floor_divide(batch_idx,self.subSampleFactor)
            t_start_indices = np.mod(batch_idx,self.subSampleFactor)*self.skip_step
            t_end_indices = t_start_indices + self.timesteps
            #self.trajs_used = np.append(self.trajs_used,traj_indices)


            # cannot do this since "only integer scalar arrays can be converted to scalar index"
            # probably means I cannot use arrays on each side of ":"
            #x_batch[:,:,:] = self.x_set[traj_indices, t_start_indices:t_end_indices]

            #x_batch = self.x_set[traj_indices,:]

            for i,temp_indices in enumerate(zip(traj_indices,t_start_indices,t_end_indices)):
                #print(self.x_set[temp_indices[0], temp_indices[1]:temp_indices[2]].shape)
                x_batch[i,:,:] = self.x_set[temp_indices[0], temp_indices[1]:temp_indices[2]]

        elif (self.inputDims == 4 & self.inputDims == 5):
            for i in range(self.batch_size):
                # for splitting each trajectory into nt/timesteps subsequences
                for j in range(self.subSampleFactor):
                    # For using CNN-LSTM where input is required to be 4D
                    # (samples, sub_seqs, timesteps, features)
                    if (self.inputDims == 4):
                        x_batch[i,j,:,:] = self.x_set[np.floor_divide(batch_idx[i],self.subSampleFactor),batch_idx[i+j]]
                    # For using ConvLSTM2D where input is required to be 5D
                    # (samples, sub_seqs, rows, cols, features), cols=timesteps
                    elif (self.inputDims == 5):
                        x_batch[i,j,0,:,:] = self.x_set[np.floor_divide(batch_idx[i],self.subSampleFactor),batch_idx[i+j]]

        y_batch = self.y_set[batch_idx]
        return x_batch, y_batch

    def __len__(self):
        # By doing ceil, N_steps*batch_size can become larger than n_trajs*subsamplefactor
        # but Keras takes care to shorten the last batch so it trains on all trajectories
        # with no duplicates.
        N_steps = math.ceil((self.n_trajs*self.subSampleFactor) / self.batch_size)
        if (N_steps < 1):
            print("Batch size and memory too large compared to number of trajectories+timesteps per trajectory in training data. Exiting...")
            exit()
        else:
            return N_steps

    # This method should override the same method in tf.keras.utils.Sequence but
    # it does not seem to work without calling it in a custom callback.
    def on_epoch_end(self):
        #print(self.shuffle)
        #print("number of indices used: ", self.trajs_used.size)
        # check for duplicates - there will be duplicates in first epoch due to some
        # initialization by Keras - it starts getting one batch from each Sequence
        # before requesting all of the batches (including the one it already requested).
        #sorted_array = np.sort(self.trajs_used)
        #print(sorted_array[:-2][sorted_array[1:] == sorted_array[:-1]])
        self.trajs_used = np.empty(shape=(0))
        if self.shuffle:
            print('Shuffling data...')
            np.random.shuffle(self.indices)

    def getEncoding(self):
        return "single-click"
