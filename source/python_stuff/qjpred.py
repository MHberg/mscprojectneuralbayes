import time
import h5py
import tensorflow as tf
import numpy as np
from keras.models import load_model
from keras.callbacks import ModelCheckpoint

# custom libraries
from plottingTools import *
import KerasBatchGenerators
import KerasModels
from FileManagement import *

#data_path = "../cpp_stuff/build/"
#save_path = "C:/Users/Henneberg/speciale_local/"
#save_path = "C:/Users/Henneberg/speciale_local/"

save_path += 'qjpred/'

# function to get the most likely predicted click in interval of likely clicks
def getMaxPredictionsInSpreadIntervals(pred_arr, N_trajs, num_predict):
    temp_arr = np.empty((2,0))
    pred_arr_result = np.zeros(pred_arr.shape,dtype='int')
    for j in range(N_trajs):
        for i in range(num_predict):
            if (pred_arr[j,i] > 0.5):
                temp_arr = np.c_[temp_arr,[[pred_arr[j,i]],[i]]]
            elif (temp_arr.size != 0):
                idx_max = np.argmax(temp_arr[0,:])
                pred_arr_result[j,temp_arr[1,idx_max].astype(int)] = 1
                temp_arr = np.empty((2,0))
    return pred_arr_result

## Quantum jump prediction
def main(batch_size, num_epochs, use_dropout):
    timesteps = 100
    skip_step = timesteps
    hidden_size = 50

    # clean data
    psi_train,dN_train,times_train,omegas_train = read_data('training.h5')
    psi_valid,dN_valid,times_valid,omegas_valid = read_data('valid.h5')
    psi_test,dN_test,times_test,omegas_test = read_data('test.h5')

    # noisy data - random delays
    #psi_train,dN_train,times_train,omegas_train = read_data('training_noisy.h5')
    #psi_valid,dN_valid,times_valid,omegas_valid = read_data('valid_noisy.h5')
    #psi_test,dN_test,times_test,omegas_test = read_data('test_noisy.h5')

    times = times_train # the time steps must be equal for the different data sets


    t2 = time.time()

    N_trajs = dN_test.shape[0]
    nt = len(times)
    num_predict = nt-timesteps # number of points to predict

    ### Valid matches:
    # batchGenerator        +   modelStrategy
    # ____________________________________________
    # SingleClickSingleStep +   SingleStepLSTMModel
    # IntervalSingleStep    +   SingleStepLSTMModel
    # IntervalMultiStep     +   EncoderDecoderLSTM
    # SignalMultiStep       +   EncoderDecoderLSTM
    batchGenerator = KerasBatchGenerators.SingleClickSingleStep
    modelStrategy = KerasModels.SingleStepLSTMModel(hidden_size, timesteps, use_dropout)#, num_predict)

    train_data_generator = batchGenerator(dN_train, timesteps, batch_size, skip_step, True)
    valid_data_generator = batchGenerator(dN_valid, timesteps, batch_size, skip_step, True)

    t3 = time.time()
    tCreateGenerators = t3-t2

    model = modelStrategy.defineModel()
    if (train_data_generator.getStepConfig() == "single"):
        model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy']) # for binary classification e.g. for forecasting 1 time step
    elif (train_data_generator.getStepConfig() == "multi"):
        model.compile(loss='mse', optimizer='adam') # loss: mean squared error, maybe good for predicting sequence
    else:
        raise RuntimeError("The given batch generator does not supply a valid step configuration. Must be 'single' or 'multi'.")
    checkpointer = ModelCheckpoint(filepath=save_path + '/model-{epoch:02d}.h5', verbose=1)
    t4 = time.time()
    tDefineAndCompileModel = t4-t3

    print('Fitting of model started...')
    model.fit(train_data_generator.generate(), steps_per_epoch=train_data_generator.getNStepsPerEpoch(), epochs=num_epochs, validation_data = valid_data_generator.generate(), validation_steps=valid_data_generator.getNStepsPerEpoch(), use_multiprocessing=False, shuffle=False, initial_epoch=0, callbacks=[checkpointer])
    #model = load_model(save_path+ "\model-01.h5")
    print('Fitting of model completed.')
    t5 = time.time()
    tFitModel = t5-t4

    dummy_iters = 0 # controls where to start from in a single trajectory

    pred_arr = np.zeros((N_trajs,num_predict))

    if (train_data_generator.getStepConfig() == "single"):
        for j in range(N_trajs):
            example_training_generator = batchGenerator(np.expand_dims(dN_test[j,:,:],0), timesteps, batch_size=1, skip_step=1, shuffle=True) # produces timesteps data points
            for i in range(dummy_iters):
                dummy = next(example_training_generator.generate())
            for i in range(num_predict):
                data = next(example_training_generator.generate())
                prediction = model.predict(data[0]) # returns timesteps predicted next detections
                pred_arr[j,i] = prediction[0,-1,0] # last element of prediction i.e. the predicted detection after seeing timesteps detections - index [timesteps-1] = [-1] 
        pred_arr_01 = (pred_arr >= 0.5).astype(int)

    if (train_data_generator.getEncoding() == "signal-click"):
        input_signal = np.empty(dN_test.shape,dtype='int')
        cumsum = np.cumsum(dN_test,axis=1)
        np.mod(cumsum,2,input_signal)

        input_signal_timesteps = np.zeros((N_trajs,timesteps),dtype='int')
        example_training_generator = batchGenerator(dN_test, timesteps, batch_size = 1, skip_step = 1, shuffle=True)

        pred_arr_sig = np.zeros((N_trajs,num_predict))
        for j in range(dummy_iters):
            dummy = next(example_training_generator.generate())
        for j in range(N_trajs):
            data = next(example_training_generator.generate())
            prediction = model.predict(data[0])
            pred_arr_sig[j,:] = prediction[0,:,0]
            input_signal_timesteps[j,:] = data[0].reshape((timesteps,))
        pred_arr_sig_01 = (pred_arr_sig >= 0.5).astype(int)
        pred_arr_01 = np.zeros(pred_arr_sig_01.shape,dtype='int')
        pred_arr_01[:,1:] = (np.mod(pred_arr_sig_01[:,1:] + pred_arr_sig_01[:,:-1],2) == 1).astype(int) # convert from signal to single clicks
        dN_signal_predicted_avg = np.mean(pred_arr_sig_01,axis=0)
        dN_predicted_avg = np.mean(pred_arr_01,axis=0)
        fig,axs = plt.subplots(2,1)
        axs[0].plot(times[dummy_iters+timesteps:num_predict+timesteps],dN_signal_predicted_avg)
        axs[1].plot(times[dummy_iters+timesteps:num_predict+timesteps],dN_predicted_avg)
        plt.savefig(save_path + "pred_signal.pdf")

        test_index = 1
        comp_arr = np.array((dN_test[test_index,timesteps:nt,0],pred_arr_01[test_index,:],pred_arr_sig_01[test_index,:],input_signal[test_index,timesteps:nt,0]))

        print('OUTPUT (O):, PREDICTED OUTPUT (binary) (POB):, PREDICTED OUTPUT (binary signal) (POBS):, OUTPUT (signal) (OS): \n O POB POBS OS \n', comp_arr.T)
        print('INPUT DATA as signal, 0:timesteps : \n',input_signal_timesteps[0,:].T)
        #print('REAL INPUT DATA as signal 0:timesteps : \n',input_signal[0,:timesteps,0].T)
        # TODO: CHECK THAT INPUT IS THE SAME IN GENERATOR AS THE PRINTED SUCH THAT THE TRAJECTORIES COMPARED ARE THE RIGHT ONES

    t6 = time.time()
    tGetPredictions = t6-t5

    if (train_data_generator.getEncoding() == "interval-click"):
        pred_arr_result = getMaxPredictionsInSpreadIntervals(pred_arr, N_trajs, num_predict) # for getting max prob. clicks out of interval of most likely clicks
        print('PREDICTION (interval of most probable clicks): \n',pred_arr_01[0,:])
        print('PREDICTION (taking highest prob. click): \n',pred_arr_result[0,:])
        plt.plot(times[dummy_iters:num_predict],(pred_arr > 0.5).astype(int))
        plt.plot(times[dummy_iters:num_predict],pred_arr_result)
        plt.savefig(save_path + "interval_click.pdf")

    print("Times for execution of different units in qjpred:")
    print("tCreateGenerators",tCreateGenerators)
    print("tDefineAndCompileModel",tDefineAndCompileModel)
    print("tFitModel",tFitModel)
    print("tGetPredictions",tGetPredictions)

