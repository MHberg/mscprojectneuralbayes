from abc import ABC, abstractmethod
from keras.utils import to_categorical
import numpy as np

class KerasBatchGenerator(ABC):
    def __init__(self, data, timesteps, batch_size, skip_step, shuffle):
        self.data = data
        self.timesteps = timesteps
        self.batch_size = batch_size
        self.skip_step = skip_step
        self.nt = data.shape[1]
        self.n_trajs = data.shape[0]
        self.shuffle = shuffle

        self.indices = np.arange(0,self.n_trajs)
        self.current_idx = 0
        self.traj_idx = 0

    #@abstractmethod
    #def __getitem__(self, idx):
    #    raise NotImplementedError('Must be defined in subclass')

    # defining number of training/validation steps (number of batches to use) per epoch so that all data is used in every epoch
    @abstractmethod
    def __len__(self):
        raise NotImplementedError('Must be defined in subclass')

    #@abstractmethod
    #def on_epoch_end(self):
    #    raise NotImplementedError('Must be defined in subclass')

    @abstractmethod
    def generate(self):
        raise NotImplementedError('Must be defined in subclass')

    def reset(self):
        print('Generator has been reset')
        self.current_idx = 0
        self.traj_idx = 0
        print('Traj_idx after reset: ',self.traj_idx)

    def getStepConfig(self):
        return self.stepConfig

    @abstractmethod
    def getEncoding(self):
        raise NotImplementedError('Must be defined in subclass')

class SingleClickSingleStep(KerasBatchGenerator):
    def __init__(self, data, timesteps, batch_size, skip_step, shuffle):
        super().__init__(data, timesteps, batch_size, skip_step, shuffle)
        self.stepConfig = "single"

    def generate(self):
        x = np.zeros((self.batch_size, self.timesteps,1))
        y = np.zeros(x.shape)
        while True:
            for i in range(self.batch_size):
                if (self.current_idx + self.timesteps + 1 > self.nt):
                    # reset index back to start of data and go to next trajectory
                    self.current_idx = 0
                if (self.traj_idx < self.n_trajs-1):
                    self.traj_idx += 1
                else:
                    #print("All trajectories have been used for training. Resetting to first trajectory...")
                    self.traj_idx = 0

                x[i,:,:] = self.data[self.indices[self.traj_idx],self.current_idx:self.current_idx + self.timesteps]
                y[i,:,:] = self.data[self.indices[self.traj_idx],self.current_idx+1:self.current_idx + self.timesteps+1]
                self.current_idx += self.skip_step
            yield x, y

    def on_epoch_end(self):
        if self.shuffle:
            np.random.shuffle(self.indices)
            #self.data = self.data[self.indices]

    def __len__(self):
        N_steps = (self.nt*self.n_trajs)//(self.batch_size*self.timesteps) # for going through trajs point by point
        if (N_steps < 1):
            print("Batch size and memory too large compared to number of trajectories+timesteps per trajectory in training data. Exiting...")
            exit()
        else:
            return N_steps

    def getEncoding(self):
        return "single-click"

# every click (1) is surrounded by N_spread clicks in the label vector
class IntervalSingleStep(KerasBatchGenerator):
    # expects data to be of shape (N_trajs, timesteps, N_features)
    def __init__(self, data, timesteps, batch_size, skip_step, shuffle):
        super().__init__(data, timesteps, batch_size, skip_step, shuffle)
        self.stepConfig = "single"

        data_temp = np.zeros(self.data.shape,dtype='int')
        N_spread = 5
        padded_data = np.pad(self.data,((0,0),(N_spread,N_spread),(0,0)))
        for i in range(-N_spread,N_spread+1):
            data_temp += padded_data[:,i+N_spread:self.data.shape[1]+N_spread+i]
        self.data_copy_tweaked = data_temp

    def generate(self):
        x = np.zeros((self.batch_size, self.timesteps,1))
        y = np.zeros(x.shape)
        while True:
            for i in range(self.batch_size):
                if (self.current_idx + self.timesteps + 1 > self.nt):
                    # reset index back to start of data and go to next trajectory
                    self.current_idx = 0
                if (self.traj_idx < self.n_trajs-1):
                    self.traj_idx += 1
                else:
                    #print("All trajectories have been used for training. Resetting to first trajectory...")
                    self.traj_idx = 0
                x[i,:,:] = self.data[self.indices[self.traj_idx],self.current_idx:self.current_idx + self.timesteps]
                y[i,:,:] = self.data_copy_tweaked[self.indices[self.traj_idx],self.current_idx + 1:self.current_idx + self.timesteps + 1]
                self.current_idx += self.skip_step
            yield x, y
            
    def on_epoch_end(self):
        if self.shuffle:
            np.random.shuffle(self.indices)
            #self.data = self.data[self.indices]
            #self.data_copy_tweaked = self.data_copy_tweaked[self.indices]

    def __len__(self):
        N_steps = (self.nt*self.n_trajs)//(self.batch_size*self.timesteps) # for going through trajs point by point
        if (N_steps < 1):
            print("Batch size and memory too large compared to number of trajectories+timesteps per trajectory in training data. Exiting...")
            exit()
        else:
            return N_steps
    def getEncoding(self):
        return "interval-click"

class IntervalMultiStep(KerasBatchGenerator):
    # expects data to be of shape (N_trajs, timesteps, N_features)
    # tries to predict outcomes for the rest of the trajectory
    def __init__(self, data, timesteps, batch_size, skip_step, shuffle):
        super().__init__(data, timesteps, batch_size, skip_step, shuffle)
        self.stepConfig = "multi"

        data_temp = np.zeros(self.data.shape,dtype='int')
        N_spread = 5
        padded_data = np.pad(self.data,((0,0),(N_spread,N_spread),(0,0))) # put N_spread zero columns on each end of the data
        for i in range(-N_spread,N_spread+1):
            data_temp += padded_data[:,i+N_spread:self.data.shape[1]+N_spread+i]
        self.data_copy_tweaked = data_temp

    def generate(self):
        x = np.zeros((self.batch_size, self.timesteps, 1),dtype='int')
        y = np.zeros((self.batch_size, self.nt-self.timesteps, 1),dtype='int')
        while True:
            for i in range(self.batch_size):
                if (self.timesteps >= self.nt):
                    print('Memory timesteps too large compared to actual time steps nt of trajectories. Exiting...')
                    exit()
                if (self.traj_idx + 1 < self.n_trajs):
                    self.traj_idx += 1
                else:
                    #print("All trajectories have been used for training. Resetting to first trajectory...")
                    self.traj_idx = 0
                x[i,:,:] = self.data[self.indices[self.traj_idx],0:self.timesteps]
                y[i,:,:] = self.data_copy_tweaked[self.indices[self.traj_idx], self.timesteps:self.nt]
                self.traj_idx += 1
            yield x, y

    def on_epoch_end(self):
        if self.shuffle:
            np.random.shuffle(self.indices)
            #self.data = self.data[self.indices]
            #self.data_copy_tweaked = self.data_copy_tweaked[self.indices]

    def __len__(self):
        N_steps = np.ceil(self.n_trajs/self.batch_size) # for going through trajs traj by traj - TODO: do we want to use ceil or floor (// = floor of division). Floor probably might miss trajectories, ceil might repeat some depending on batch size and number of trajectories
        if (N_steps < 1):
            print("Batch size and memory too large compared to number of trajectories+timesteps per trajectory in training data. Exiting...")
            exit()
        else:
            return N_steps
    def getEncoding(self):
        return "interval-click"
        

class SignalMultiStep(KerasBatchGenerator):
    # expects data to be of shape (N_trajs, timesteps, N_features)
    # tries to predict outcomes for the rest of the trajectory
    def __init__(self, data, timesteps, batch_size, skip_step, shuffle):
        super().__init__(data, timesteps, batch_size, skip_step, shuffle)
        self.stepConfig = "multi"

        self.input_signal = np.zeros(data.shape)
        cumsum = np.cumsum(self.data,axis=1)
        np.mod(cumsum,2,self.input_signal)

    def generate(self):
        x = np.zeros((self.batch_size, self.timesteps, 1),dtype='int')
        y = np.zeros((self.batch_size, self.nt-self.timesteps, 1),dtype='int')
        while True:
            for i in range(self.batch_size):
                if (self.timesteps >= self.nt):
                    print('Memory timesteps too large compared to actual time steps nt of trajectories. Exiting...')
                    exit()
                x[i,:,:] = self.input_signal[self.indices[self.traj_idx], 0:self.timesteps]
                y[i,:,:] = self.input_signal[self.indices[self.traj_idx], self.timesteps:self.nt]
                if (self.traj_idx < self.n_trajs-1):
                    self.traj_idx += 1
                else:
                    #print("All trajectories have been used for training. Resetting to first trajectory...")
                    self.traj_idx = 0
            yield x, y

    def on_epoch_end(self):
        if self.shuffle:
            np.random.shuffle(self.indices)
            #self.input_signal = self.input_signal[self.indices]

    def __len__(self):
        N_steps = np.ceil(self.n_trajs/self.batch_size) # for going through trajs traj by traj - TODO: do we want to use ceil or floor (// = floor of division). Floor probably might miss trajectories, ceil might repeat some depending on batch size and number of trajectories
        if (N_steps < 1):
            print("Batch size and memory too large compared to number of trajectories+timesteps per trajectory in training data. Exiting...")
            exit()
        else:
            return N_steps
    def getEncoding(self):
        return "signal-click"

# Not for predicting future detections but for paramater estimation from a
# hypothesis set.
# This generator does the encoding of the input signal so that it alternates
# between 0 and 1 at detector clicks - thus it cannot be used for the homodyne
# signal.
class SignalToConstantParameterFromHypotheses(KerasBatchGenerator):
    # expects data to be of shape (N_trajs, timesteps, N_features)
    # tries to predict outcomes for the rest of the trajectory
    def __init__(self, data, timesteps, batch_size, skip_step, hypotheses,
                 omegas, shuffle):
        super().__init__(data, timesteps, batch_size, skip_step, shuffle)
        self.stepConfig = "single" # trying to guess a single parameter
        self.hypotheses = hypotheses

        # taking Rabi frequency at first time step since it is assumed constant for these trajectories
        self.omegas = omegas[:,0] 

        # converts a class vector (integers) to a binary class matrix
        # a 1 in row 0, column 0 means the first trajectory belongs to the first
        # class of Rabi frequencies, a 1 in row 1, column 0 means the second
        # trajectory belongs to the second class of Rabi frequencies
        self.omegas = to_categorical(omegas, hypotheses.size, dtype='int')

        # encodes the input signal as alternating between 0 and 1 at detector
        # clicks
        self.input_signal = np.zeros(self.data.shape)
        cumsum = np.cumsum(self.data,axis=1)
        np.mod(cumsum,2,self.input_signal)

    def generate(self):
        x = np.zeros((self.batch_size, self.timesteps, 1),dtype='int')
        y = np.zeros((self.batch_size, self.hypotheses.size),dtype='int')
        while True:
            for i in range(self.batch_size):
                if (self.timesteps > self.nt):
                    print('Memory timesteps too large compared to actual time steps nt of trajectories. Exiting...')
                    exit()
                x[i,:,:] = self.input_signal[self.indices[self.traj_idx], 0:self.timesteps]
                y[i,:] = self.omegas[self.indices[self.traj_idx],:]
                if (self.traj_idx < self.n_trajs-1):
                    self.traj_idx += 1
                else:
                    #print("All trajectories have been used for training. Resetting to first trajectory...")
                    self.traj_idx = 0
            yield x, y

    def on_epoch_end(self):
        if self.shuffle:
            np.random.shuffle(self.indices)
            #self.input_signal = self.input_signal[self.indices]
            #self.omegas = self.omegas[self.indices]

    def __len__(self):
        N_steps = np.ceil(self.n_trajs/self.batch_size) # for going through trajs traj by traj - TODO: do we want to use ceil or floor (// = floor of division). Floor probably might miss trajectories, ceil might repeat some depending on batch size and number of trajectories
        if (N_steps < 1):
            print("Batch size and memory too large compared to number of trajectories+timesteps per trajectory in training data. Exiting...")
            exit()
        else:
            return N_steps
    def getEncoding(self):
        return "signal-click"

# not for predicting future detections but for parameter estimation from a hypothesis set
class SingleClickToConstantParameterFromHypotheses(KerasBatchGenerator):
    def __init__(self, data, timesteps, batch_size, skip_step, hypotheses, omegas, modelStrategy, shuffle):
        super().__init__(data, timesteps, batch_size, skip_step, shuffle)
        self.stepConfig = "single" # trying to guess a single parameter
        self.hypotheses = hypotheses
        self.inputDims = modelStrategy.getDimsRequired()

        # taking Rabi frequency at first time step since it is assumed constant for these trajectories
        self.omegas = omegas[:,0]

        self.input_signal = self.data

        # converts a class vector (integers) to a binary class matrix
        # a 1 in row 0, column 0 means the first trajectory belongs to the first
        # class of Rabi frequencies, a 1 in row 1, column 0 means the second
        # trajectory belongs to the second class of Rabi frequencies
        self.omegas = to_categorical(self.omegas, self.hypotheses.size)

    def generate(self):
        # 3d input - last dimension is feature number - required when using LSTMs
        if (self.inputDims == 3):
            x = np.zeros((self.batch_size, self.timesteps, 1))
        # For using CNN-LSTM where input is required to be 4D
        # (samples, sub_seqs, timesteps, features)
        elif (self.inputDims == 4):
            x = np.zeros((self.batch_size, self.nt//self.timesteps, self.timesteps, 1))
        # For using ConvLSTM2D where input is required to be 5D
        # (samples, sub_seqs, rows, cols, features)
        elif (self.inputDims == 5):
            x = np.zeros((self.batch_size, self.nt//self.timesteps, 1, self.timesteps, 1))

        else:
            print('Input dimension specified was invalid. Exiting...')
            exit()

        y = np.zeros((self.batch_size, self.hypotheses.size))
        while True:
            for i in range(self.batch_size):
                if (self.timesteps > self.nt):
                    print('Memory timesteps too large compared to actual time steps nt of trajectories. Exiting...')
                    exit()
                if (self.current_idx + self.timesteps > self.nt):
                    # reset index back to start of data and go to next trajectory
                    self.current_idx = 0
                    if (self.traj_idx < self.n_trajs-1):
                        self.traj_idx += 1
                    else:
                        #print("All trajectories have been used for training. Resetting to first trajectory...")
                        self.traj_idx = 0

                # 3d input - last dimension is feature number - seems to be required
                if (self.inputDims == 3):
                    # 3D input with timesteps data points
                    x[i,:,:] = self.input_signal[self.indices[self.traj_idx],self.current_idx:self.current_idx + self.timesteps]

                elif (self.inputDims == 4 & self.inputDims == 5):
                    # for splitting each trajectory into nt/timesteps subsequences
                    for j in range(self.nt//self.timesteps):
                        # For using CNN-LSTM where input is required to be 4D
                        # (samples, sub_seqs, timesteps, features)
                        if (self.inputDims == 4):
                            x[i,j,:,:] = self.input_signal[self.indices[self.traj_idx],self.current_idx:self.current_idx + self.timesteps]
                        # For using ConvLSTM2D where input is required to be 5D
                        # (samples, sub_seqs, rows, cols, features), cols=timesteps
                        elif (self.inputDims == 5):
                            x[i,j,0,:,:] = self.input_signal[self.indices[self.traj_idx],self.current_idx:self.current_idx + self.timesteps]
                        self.current_idx += self.skip_step


                y[i,:] = self.omegas[self.indices[self.traj_idx],:]
                if (self.inputDims == 3):
                    self.current_idx += self.skip_step

            yield x, y

    def on_epoch_end(self):
        print(self.shuffle)
        print('hjehehehe')
        if self.shuffle:
            print('Shuffling data...')
            np.random.shuffle(self.indices)
            #self.input_signal = self.input_signal[self.indices]
            #self.omegas = self.omegas[self.indices]

    def __len__(self):
        N_steps = (self.nt*self.n_trajs)//(self.batch_size*self.timesteps) # for going through trajs timesteps by timesteps
        if (N_steps < 1):
            print("Batch size and memory too large compared to number of trajectories+timesteps per trajectory in training data. Exiting...")
            exit()
        else:
            return N_steps
    def getEncoding(self):
        return "single-click"


# not for predicting future detections but for parameter estimation from a hypothesis set
class SingleClickToConstantParameterRegression(KerasBatchGenerator):
    def __init__(self, data, timesteps, batch_size, skip_step, omegas, modelStrategy, shuffle):
        super().__init__(data, timesteps, batch_size, skip_step, shuffle)
        self.stepConfig = "single" # trying to guess a single parameter
        self.inputDims = modelStrategy.getDimsRequired()

        # taking Rabi frequency at first time step since it is assumed constant for these trajectories
        self.omegas = omegas[:,0] 

        # let input signal be the raw detected signal
        self.input_signal = self.data

    def generate(self):
        # 3d input - last dimension is feature number - required when using LSTMs
        if (self.inputDims == 3):
            x = np.zeros((self.batch_size, self.timesteps, 1))
        # For using CNN-LSTM where input is required to be 4D
        # (samples, sub_seqs, timesteps, features)
        elif (self.inputDims == 4):
            x = np.zeros((self.batch_size, self.nt//self.timesteps, self.timesteps, 1))
        # For using ConvLSTM2D where input is required to be 5D
        # (samples, sub_seqs, rows, cols, features)
        elif (self.inputDims == 5):
            x = np.zeros((self.batch_size, self.nt//self.timesteps, 1, self.timesteps, 1))

        else:
            print('Input dimension specified was invalid. Exiting...')
            exit()

        y = np.zeros(self.batch_size)
        while True:
            for i in range(self.batch_size):
                if (self.timesteps > self.nt):
                    print('Memory timesteps too large compared to actual time steps nt of trajectories. Exiting...')
                    exit()
                if (self.current_idx + self.timesteps > self.nt):
                    # reset index back to start of data and go to next trajectory
                    self.current_idx = 0
                    if (self.traj_idx < self.n_trajs-1):
                        self.traj_idx += 1
                        print("new traj")
                    else:
                        print("All trajectories have been used for training. Resetting to first trajectory...")
                        self.traj_idx = 0

                # 3d input - last dimension is feature number - seems to be required
                if (self.inputDims == 3):
                    # 3D input with timesteps data points
                    x[i,:,:] = self.input_signal[self.indices[self.traj_idx],self.current_idx:self.current_idx + self.timesteps]

                elif (self.inputDims == 4 & self.inputDims == 5):
                    # for splitting each trajectory into nt/timesteps subsequences
                    for j in range(self.nt//self.timesteps):
                        # For using CNN-LSTM where input is required to be 4D
                        # (samples, sub_seqs, timesteps, features)
                        if (self.inputDims == 4):
                            x[i,j,:,:] = self.input_signal[self.indices[self.traj_idx],self.current_idx:self.current_idx + self.timesteps]
                        # For using ConvLSTM2D where input is required to be 5D
                        # (samples, sub_seqs, rows, cols, features), cols=timesteps
                        elif (self.inputDims == 5):
                            x[i,j,0,:,:] = self.input_signal[self.indices[self.traj_idx],self.current_idx:self.current_idx + self.timesteps]
                        self.current_idx += self.skip_step


                y[i] = self.omegas[self.indices[self.traj_idx]]
                if (self.inputDims == 3):
                    self.current_idx += self.skip_step

            yield x, y

    def on_epoch_end(self):
        if self.shuffle:
            np.random.shuffle(self.indices)
            #self.input_signal = self.input_signal[self.indices]
            #self.omegas = self.omegas[self.indices]

    def __len__(self):
        N_steps = (self.nt*self.n_trajs)//(self.batch_size*self.timesteps) # for going through trajs timesteps by timesteps
        if (N_steps < 1):
            print("Batch size and memory too large compared to number of trajectories+timesteps per trajectory in training data. Exiting...")
            exit()
        else:
            return N_steps
    def getEncoding(self):
        return "single-click"

