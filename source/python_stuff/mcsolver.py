import numpy as np
import scipy as sp
from scipy.linalg import expm
import matplotlib.pyplot as plt
from plottingTools import *

# Inspired by Mølmer, Castin and Dalibard "Monte Carlo wave-function method
# in quantum optics". Equation numbers reference this article. 
# Also looked at code "twoLevelCounting" by Alexander Kiilerich and description
# of the same algorithm in qutip.

# simulation of one qubit evolution using Monte Carlo method
def getMCTrajectory(t0=0, T=10, nt=200):
    plotting = 0
    method1 = 0 # qutip inspired - probably fastest
    method2 = 0 # article inspired - does not work properly
    # if none of these methods, then it does Alexander Kiilerich's approach

    dt = (T-t0)/nt
    times = np.linspace(t0,T,nt)

    Omega = 6 # Rabi frequency
    Delta = 0 # detuning
    gamma = 1 # decay rate

    psi_gs = np.array([[0],[1]]) # on the form [[e],[g]]
    psi0 = np.array([[0],[1]])

    # operators
    Hsys = np.array([[-Delta/2, Omega/2],[Omega/2, Delta/2]]) # single photon atom-field system
    C = np.sqrt(gamma)*np.array([[0,0],[1,0]])
    Heff = Hsys - 1j*(C.conj().T @ C)/2
    U = expm(-1j*Heff*dt)

    psi = np.zeros([2,len(times)],dtype='complex_')
    psi[:,0] = psi0.reshape(2,)
    dN = np.zeros(len(times))
    r = np.random.uniform(0)
    if method1: # my attempt from reading about method in qutip
        for i in range(1,len(times)):
            if (np.linalg.norm(psi[:,i-1])**2 <= r):
                #print('norm less than or equal to r')
                #psi[:,i] = C @ psi[:,i-1]/np.sqrt(psi[:,i-1].conj().T @ (C.conj().T @ C) @ psi[:,i-1])
                psi[:,i] = psi_gs.reshape(2,)
                dN[i] = 1
                r = np.random.uniform(0)
            else:
                psi[:,i] = U @ psi[:,i-1]
                psi[:,i-1] = psi[:,i-1]/np.linalg.norm(psi[:,i-1])
        psi[:,-1] = psi[:,-1]/np.linalg.norm(psi[:,-1])
    elif method2: # my attempt from reading article
        for i in range(1,len(times)):
            epsilon = np.random.uniform(0)
            dp = gamma*dt*psi[:,i-1].conj().T @ C.conj().T @ C @ psi[:,i-1]
            if (epsilon < dp):
                psi[:,i] = (C @ psi[:,i-1])/np.sqrt(dp/dt) # eq. 12
            else:
                psi[:,i] = U @ psi[:,i-1] # eq. 7 and 6 (but using matrix exponential instead of low dt first order approximation - diagonalization)
                psi[:,i] = psi[:,i]/(np.sqrt(psi[:,i].conj().T@psi[:,i]))#np.linalg.norm(psi[:,i]) # eq. 11 and 8
    else: # Inspired by example code written by Alexander Kiilerich - same method
        for i in range(1,len(times)):
            psi[:,i] = U @ psi[:,i-1]
            psi[:,i] = psi[:,i]/np.linalg.norm(psi[:,i])

            epsilon = np.random.uniform(0)
            if (epsilon <= gamma*np.abs(psi[0,i])**2*dt):
                psi[:,i] = psi_gs.reshape(2,)

    if plotting:
        plt.plot(times, np.abs(psi[0,:])**2)
        plt.title('Monte Carlo time evolution of two-level system')
        plt.xlabel('Time')
        plt.ylabel('Atom in excited state')
        plt.legend(('atom excitation probability'))
        plt.show()
    return psi

# monte carlo wave function solving of a two-level system
# N: Number of trajectories
# t0: starting time
# T: end time
# nt: number of time steps
def mcsolve(N,t0,T,nt):
    psi_total = np.zeros((N,2,nt),dtype='complex_')
    #print(psi_total.shape)
    for i in range(N):
        psi_total[i,:,:]= getMCTrajectory(t0,T,nt)
    return psi_total


if __name__=='__main__':
    plotting = 0
    N = 100
    t0 = 0
    T = 5
    nt = 501
    times = np.linspace(t0,T,nt)
    psi = mcsolve(N,t0,T,nt)

    fig = plotBlochSphere()

    #sigmaZ_exp = np.zeros((psi.shape[0],1,psi.shape[2]))
    #sigmaZ = np.array([[1,0],[0,-1]])
    #for i in range(N):
    #    for k in range(nt):
    #        sigmaZ_exp[i,:,k] = np.conj(psi[i,:,k]).T @ sigmaZ @ psi[i,:,k]
    #sigmaZ_exp_avg = np.mean(sigmaZ_exp,axis=0)
    #print(sigmaZ_exp.shape)
    #print(sigmaZ_exp_avg.shape)

    psi_squared_avg = np.mean(np.abs(psi[0,:])**2,axis=0)
    #print(psi_squared_avg.shape)
    #rho_avg = np.mean(getDensityMatrixFromStateVector(psi),axis=0)
    #animateBlochVector(fig,times,psi_squared_avg,0)
    #animateBlochVector(fig,times,psi[0],0)
    plotBlochTrajectory(fig,times,psi[0])
    #R = getBlochComponents(psi[0])
    #fig2 = plotXYZ(times,R[0],R[1],R[2])
    plt.tight_layout()
    plt.show()
    #fig = plt.figure()
    #ax = plt.gca()
    #ax = plotPopulations(ax,times,psi_squared_avg)

    if plotting:

        plt.show()
        plt.plot(np.linspace(t0,T,nt), np.abs(psi_avg[1,:])**2)
        #plt.plot(np.linspace(t0,T,nt), sigmaZ_exp_avg.T)
        plt.title('Monte Carlo time evolution averaged over ' + str(N) + ' trajectories of two-level system')
        plt.xlabel('Time')
        plt.ylabel('Atom in excited state')
        plt.legend(('atom excitation probability'))
        plt.show()

