import numpy as np
import matplotlib.pyplot as plt
from mcsolver import getMCTrajectory

plotting = 1

N = 2 
t0 = 0
T = 10
nt = 200

psi = getMCTrajectory(t0,T,nt)


if plotting:
    plt.plot(np.linspace(t0,T,nt), np.abs(psi[1,:])**2)
    plt.title('Single Monte Carlo trajectory of time evolution of two-level system')
    plt.xlabel('Time')
    plt.ylabel('Atom in excited state')
    plt.legend(('atom excitation probability'))
    plt.show()

